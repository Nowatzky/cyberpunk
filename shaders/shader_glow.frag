#version 120

uniform sampler2D texture;
uniform float blur_radius;

float brightness(vec4 color) {
    return (color.r + color.g + color.b);
}

void main() {
    vec2 offx = vec2(blur_radius, 0.0);
    vec2 offy = vec2(0.0, blur_radius);
    
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
    vec4 blur = texture2D(texture, gl_TexCoord[0].xy + offx) +
                texture2D(texture, gl_TexCoord[0].xy - offx) +
                texture2D(texture, gl_TexCoord[0].xy + offy) + 
                texture2D(texture, gl_TexCoord[0].xy - offy);
      
    blur *= 0.25f;
    
    float yes = float (brightness(blur) * 0.9 > brightness(pixel));
    
    gl_FragColor = mix(pixel, blur * 1.5, yes);
}