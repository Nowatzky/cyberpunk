#ifndef CYBERPUNK_CPLAYER_HPP
#define CYBERPUNK_CPLAYER_HPP

#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"
#include "CCharacter.hpp"
#include "CSkill.hpp"
#include "CCharacterClass.hpp"
#include "CColorDefines.hpp"
#include "CClock.hpp"
#include "CSpawningManager.hpp"

using namespace sf;

class CPlayer : public CCharacter {
private:
	float energyMax = 100;
	float energy = energyMax;
	float energyRegeneration = 20; //per Second
    float healthRegeneration = 1;
	float missingHealthRegenerationBoost = 0.01f; //Percentage of missing health
	
	// movementSpeed in Character
	float maxMovementSpeed = 4.75f; //See constructor
	float accelerationTime = 190;
	float decelerationTime = 110;
	CClock accelerationClock;
	CClock decelerationClock;
	Vector2f movementDirection = Vector2f(0, 0);
	bool isMoving = false;
	bool allowOwnMovement = true;
	bool needsMovementClockRestart = false;

	//statistics
	int kills = 0;
	int assists = 0;
	int deaths = 0;

	vector<CSkill*> activeSkills; //active skill effects that require to tick

	CCharacterClass* characterClass = nullptr;

	Clock clockSwitchClasses;
	float switchClassCooldown = 500;
	
	bool isAlive = true;
	//bool isVisible = true;

public:
	//TODO refactor
	float damagedRecently = 0;

	CPlayer(Vector3f pos, Vector3f dim, CClassName className);
	
	void applyMovementDirection(Vector2f movementDirection);
	Vector2f getMovementDirection() const;
	
	virtual ~CPlayer() {};
	
	void revive();
	virtual void die();

	void setCharacterClass(CClassName className);
	bool isClassSwitchAvailable();
	CCharacterClass *getCharacterClass();

	/* Bound to InputManager */
	virtual void rotate(float angle);

	virtual void setMovementSpeed(float movementSpeed);
	
	float getMaxMovementSpeed();
	
	bool consumeEnergy(float deltaEnergy);
	bool restoreEnergy(float deltaEnergy);
	virtual void setHealth(float health);
	virtual void reduceHealth(float damage);
	virtual void healHealth(float heal);

	void addActiveSkill(CSkill* skill);
	void removeActiveSkill(CSkill* skill);
	
	void setAllowOwnMovement(bool allowOwnMovement);
	
	/* Getter */
	float getEnergy();
	float getEnergyMaximum();

	void reloadPrimarySkill();

    void tick(float frameTime);
    
    bool getIsAlive() const;

	int getKills();
	int getAssists();
	int getDeaths();
	void addKill();
	void addAssist();
	void addDeath();

	static void checkKillPotential(CCharacter *target, CCharacter *initiator);

private:
	void executePlannedMovement(float frameTime);

};


#endif //CYBERPUNK_CPLAYER_HPP