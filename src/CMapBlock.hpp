#ifndef CYBERPUNK_CMAPBLOCK_HPP
#define CYBERPUNK_CMAPBLOCK_HPP


#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"

class CMapBlock : public CObject {
protected:
	CBoundingBox boundingBox;
	CGraphicBox graphicBox;
	
	Vector3f dim;

	VertexArray lines;
	int lineCount;
	CDirection wallDirection;

public:
	CMapBlock(Vector3f pos, Vector3f dim, Color color) :
			CObject(pos, true),
            boundingBox(pos, dim),
            graphicBox(pos, dim, color),
	        dim(dim) {
		boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
		lineCount = 0;
	}

	CMapBlock(Vector3f pos, Vector3f dim, Color color, bool isStatic) :
			CObject(pos, isStatic),
			boundingBox(pos, dim),
			graphicBox(pos, dim, color),
			dim(dim) {
		boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
		lineCount = 0;
	}

	CMapBlock(Vector3f pos, Vector3f dim, Color colorBase, Color colorAccent) :
			CObject(pos, true),
			boundingBox(pos, dim),
			graphicBox(pos, dim, colorBase, colorAccent),
			dim(dim) {
		boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
		lineCount = 0;
	}

	CMapBlock(Vector3f pos, Vector3f dim, Color colorBase, Color colorAccent, bool isStatic) :
			CObject(pos, isStatic),
			boundingBox(pos, dim),
			graphicBox(pos, dim, colorBase, colorAccent),
			dim(dim) {
		boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
		lineCount = 0;
	}

	CMapBlock(Vector2f start, Vector2f end, float z, Color colorBase, Color colorAccent, bool isStatic);
	

	virtual void tick(float frameTime);
	virtual void draw(RenderTarget* target);

	CCollisionContainer* getCollisionContainer();
	CGraphicContainer* getGraphicContainer();
	
	Vector3f getDim() const;

private:
	void update();

};

#endif //CYBERPUNK_CMAPBLOCK_HPP
