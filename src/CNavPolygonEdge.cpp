#include "CNavPolygonEdge.hpp"

CNavPolygonEdge::CNavPolygonEdge(CNavPolygon* target, Vector2f left, Vector2f right) :
		target(target),
        left(left),
        right(right) {
	
	Vector2f fromLeft = CEngine::normalize(right - left);
	Vector2f fromRight = CEngine::normalize(left - right);
	Vector2f inwardOffset = CEngine::rotateZRight90(fromLeft) * 0.5f;
	
	float length = CEngine::getDistance(left, right);

	if(length <= 3) {
		// 1 central hole
		holes.push_back(new CPolygonGatewayHole(left + length/2.f * fromLeft, inwardOffset));		
	} else if(length < 8) {
		// 2 side adjusted holes
		holes.push_back(new CPolygonGatewayHole(left + fromLeft, inwardOffset));
		holes.push_back(new CPolygonGatewayHole(right + fromRight, inwardOffset));		
	} else {
		// 2 side adjusted holes + 1 hole each 4 from (length-2)
		holes.push_back(new CPolygonGatewayHole(left + fromLeft, inwardOffset));
		holes.push_back(new CPolygonGatewayHole(right + fromRight, inwardOffset));
		
		int numHoles = (int) floor((length - 2.f) / 4.f) - 1;
		if(numHoles > 0) {
			float space	= (length - 2.f) / (float)(numHoles + 1);
			
			for(int i = 0; i < numHoles; i++) {
				holes.push_back(new CPolygonGatewayHole(left + (1 + (space * (i + 1))) * fromLeft, inwardOffset));
			}
		}		
	}
}

CNavPolygonEdge::~CNavPolygonEdge() {
	for(CPolygonGatewayHole* hole : holes) {
		delete hole;
	}
}
