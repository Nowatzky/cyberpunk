#ifndef CYBERPUNK_CINTENSITYMANAGER_HPP
#define CYBERPUNK_CINTENSITYMANAGER_HPP
#include <fstream>

using namespace std;


class CIntensityManager {
private:
	float intensity = 0;
	float peak_intensity = 100;
	float intensityModifierEnemies = 0.8f;
	float intensityModifierPlayerHealth = 0.5f;


	float minSpawnInterval = 1000.f;

	float spawnIntervalPeakIncrease = 4000.f;

	fstream intensityData;
public:
	CIntensityManager();
	~CIntensityManager();
	void tick(float frameTime);
	float getIntensity();
	float getSuggestedSpawnInterval();

private:
	float updateIntensity(float frameTime);
	float bridgeFunction();
};


#endif //CYBERPUNK_CINTENSITYMANAGER_HPP
