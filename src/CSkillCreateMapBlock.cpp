#include "CSkillCreateMapBlock.hpp"
#include "CInputManager.hpp"
#include "CPowerBeacon.hpp"
#include "CSceneManager.hpp"
#include "CHoloMapBlock.hpp"
#include "CRpcHandler.hpp"
#include "CClassShotgunner.hpp"
#include "CGui.hpp"

void CSkillCreateMapBlock::onButtonPress() {
	spawnMapBlock();
}

void CSkillCreateMapBlock::spawnMapBlock() {
	Vector3f worldPosition =  CEngine::to3DOnPlane(CInputManager::getMousePositionScene(), 1.001f);

	createMapBlock(worldPosition, owner->getCharacterClass()->getBaseColor(), owner->getCharacterClass()->getAccentColor());

	if(dynamic_cast<CPlayer *>(owner)!= nullptr) {
		dynamic_cast<CPlayer *>(owner)->addAssist();
		CGui::updateKDA();
	}


	CRpcHandler::sendPlayerSkillCreateMapBlock(owner, worldPosition);
}

void CSkillCreateMapBlock::spawnMapBlockRPC(Vector3f worldPosition) {
	createMapBlock(worldPosition, owner->getCharacterClass()->getBaseColor(), owner->getCharacterClass()->getAccentColor());
}

void CSkillCreateMapBlock::spawnMapBlockFallback(Vector3f worldPosition) {
	createMapBlock(worldPosition, COLOR_DARK_RED, COLOR_ORANGE);
}

void CSkillCreateMapBlock::createMapBlock(Vector3f worldPosition, sf::Color colorBase, sf::Color colorAccent) {
	CSceneManager::safeAddObject(new CHoloMapBlock(worldPosition, colorBase, colorAccent));
}
