#ifndef CYBERPUNK_CSKILLCREATEMAPBLOCK_HPP
#define CYBERPUNK_CSKILLCREATEMAPBLOCK_HPP


#include <SFML/Graphics/Color.hpp>
#include "CSkill.hpp"

class CSkillCreateMapBlock : public CSkill {
private:

public:
	CSkillCreateMapBlock(CPlayer *owner) :
	CSkill(owner, 250, 70) {};

	void spawnMapBlock();
	void spawnMapBlockRPC(Vector3f worldPosition);
	static void spawnMapBlockFallback(Vector3f worldPosition);

protected:
	void onButtonPress();
	
private:
	static void createMapBlock(Vector3f worldPosition, sf::Color colorBase, sf::Color colorAccent);

};


#endif //CYBERPUNK_CSKILLCREATEMAPBLOCK_HPP
