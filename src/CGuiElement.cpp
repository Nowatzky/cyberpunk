#include "CGuiElement.hpp"
#include "CGuiManager.hpp"

CGuiElement::~CGuiElement() {
	CGuiManager::removeGuiElement(this);
}

void CGuiElement::setIsVisible(bool isVisible) {
	this->isVisible = isVisible;
}

bool CGuiElement::getIsVisible() {
	return isVisible;
}
