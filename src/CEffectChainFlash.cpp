#include "CEffectChainFlash.hpp"
#include "CEffectManager.hpp"
#include "CCharacter.hpp"
#include "CEnemy.hpp"
#include "CEffectStun.hpp"
#include "CSceneManager.hpp"
#include "CChainFlashEffect.hpp"
#include "CEffectVisible.hpp"
#include "CRpcHandler.hpp"

bool CEffectChainFlash::tick(float frameTime, CEffectAggregate& effectAggregate) {
	if(owner->getIsLocal()) {
		if(wasAlreadyTriggered) {
			return false;
		}
		
		if(lifetimeClock.getTimeMs() >= delay) {
			triggerFlash();
			return false;
		}
	}
	
	return true;
}

void CEffectChainFlash::onStart() {
	int markCount = owner->getEffectManager()->getMarkCount(CEffectMark::EffectMarkType::Lightning);
	owner->getEffectManager()->removeEffectMark(CEffectMark::EffectMarkType::Lightning, markCount);
	
	owner->addEffect(new CEffectStun(owner, stunDuration[markCount - 1]));
	owner->reduceHealth(damage[markCount - 1]);
	CPlayer::checkKillPotential(owner, initiator);
	
	if(owner->getHealth() <= 0) {
		if(owner->getIsLocal()) {
			triggerFlash();
		}
		wasAlreadyTriggered = true;
	}

	owner->addEffect(new CEffectVisible(nullptr, 140, 700));
 }

CCharacter* CEffectChainFlash::findNearestTarget() const {
	Vector2f currentPos = CEngine::to2D(owner->getPosition());
	CCharacter* currentTarget = nullptr;
	float currentDistance = maxDistance;

	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		if(enemy->getEffectManager()->hasMark(CEffectMark::EffectMarkType::Lightning)) {
			float distance = CEngine::getDistance(currentPos, CEngine::to2D(enemy->getPosition()));

			if(distance <= maxDistance && distance <= currentDistance) {
				currentDistance = distance;
				currentTarget = enemy;
			}
		}
	}

	return currentTarget;
}

void CEffectChainFlash::triggerFlash() {
	std::vector<CObjectId> targetIds;
	
	for(int i = 0; i < 2; i++) {
		CCharacter* target = findNearestTarget();

		if(target != nullptr) {
			targetIds.push_back(target->getId());
			hit(target);	
		}
	}	
	
	CRpcHandler::sendEffectChainFlashTriggerFlash(owner, targetIds);
}

void CEffectChainFlash::triggerFlashRPC(std::vector<CObject*> targets) {
	for(CObject* target : targets) {
		CEnemy* enemy = dynamic_cast<CEnemy*>(target);
		hit(enemy);
	}
}

void CEffectChainFlash::triggerFlashFallback(std::vector<CObject*> targets) {
	for(CObject* target : targets) {
		CEnemy* enemy = dynamic_cast<CEnemy*>(target);
		hitFallback(enemy);
	}
}

void CEffectChainFlash::hit(CCharacter* enemy) {
	enemy->addEffect(new CEffectChainFlash(initiator));
	CSceneManager::safeAddObject(new CChainFlashEffect(owner->getPosition(), enemy->getPosition()));
}

void CEffectChainFlash::hitFallback(CCharacter* enemy) {
	enemy->addEffect(new CEffectChainFlash(nullptr));
}
