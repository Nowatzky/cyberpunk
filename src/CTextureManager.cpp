#include <iostream>
#include "CTextureManager.hpp"

std::map<std::string, sf::Texture> CTextureManager::textures = std::map<std::string, sf::Texture>();

std::string CTextureManager::path = "textures/";
std::string CTextureManager::extension = ".png";

const sf::Texture& CTextureManager::getTexture(std::string name) {
	std::map<std::string, sf::Texture>::iterator element = textures.find(name);

	if(element != textures.end()) {
		return ((*element).second);		
	} else {
		loadTexture(name);
		return textures.at(name);
	}
}

bool CTextureManager::loadTexture(std::string name) {
	std::map<std::string, sf::Texture>::iterator element = textures.find(name);

	if(element != textures.end()) {
		return true;
	} else {
		sf::Texture texture;
		if(!texture.loadFromFile(path + name + extension)) {
			std::cout << "Error: Texture " << path << name << extension << " could not be loaded!" << std::endl;
			return false;
		}

		texture.setSmooth(true);
		textures.insert(std::make_pair(name, texture));
		return true;
	}
}
