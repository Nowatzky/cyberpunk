#include "CMessageCreateSpawnPoint.hpp"
#include "CSerializationFunctions.hpp"
#include "CTempSpawnPoint.hpp"
#include "CSceneManager.hpp"

void CMessageCreateSpawnPoint::serializeInto(sf::Packet& packet) {
	packet << objectId
	       << pos;
}

void CMessageCreateSpawnPoint::deserializeFrom(sf::Packet& packet) {
	packet >> objectId
	       >> pos;
}

void CMessageCreateSpawnPoint::process() {
	CTempSpawnPoint* spawnPoint = new CTempSpawnPoint(pos);

	spawnPoint->setNotLocal(objectId);
	CSceneManager::safeAddObject(spawnPoint);
}
