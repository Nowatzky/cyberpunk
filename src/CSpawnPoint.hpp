#ifndef CYBERPUNK_CSPAWNPOINT_HPP
#define CYBERPUNK_CSPAWNPOINT_HPP


#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"
#include "CEnemy.hpp"
#include "CRandom.hpp"
#include "CRectangleArea.hpp"
#include "CSpawningManager.hpp"
#include "CColorDefines.hpp"

class CSpawningManager;

struct SpawningEnemy {
	CEnemy* enemy;
	Clock spawningClock;
	Vector3f startPoint;
	Vector3f endPoint;
	Color colorNormalBase;
	Color colorNormalAccent;
};

class CSpawnPoint : public CObject {
private:
	CBoundingBox boundingBox;
	CGraphicBox graphicBox;

	std::vector<CEnemy*> spawnList;
	CSpawningManager* manager;
	
	Color colorBaseNormal = COLOR_SPAWNPOINT_BASE;
	Color colorAccentNormal = COLOR_SPAWNPOINT_ACCENT;
	
	std::vector<SpawningEnemy> spawningEnemies;

	/* for spawning from list */
	Clock clockSpawnCall;
	float avgSpawnInterval = 1000;
	
	float spawnRadiusStart = 3.25f;
	float spawnRadiusStop = 1.5f;

	float spawningTime = 170;
	float glowingTimeOffset = 0;
	float glowingAmplitude = 45;
	float glowingFrequency = 1.2f;
	float glowingScaleAmplitude = 0.04f;
	
	bool isRising = true;
	Clock clockRise;
	float riseDuration = 800;
	float riseOffsetX;
	float riseOffsetY;
	
	bool isDespawning = false;
	Clock clockDespawning;
	float despawnTime = 2000;
	
	CRectangleArea blockedArea;
	
public:
	CSpawnPoint(Vector3f pos, vector<CEnemy*> spawnList, CSpawningManager* manager);
	~CSpawnPoint() override;

	void tick(float frameTime) override;
	
	// Getter & Setter
	CCollisionContainer* getCollisionContainer() override;
	CGraphicContainer* getGraphicContainer() override;
	
	bool getIsDespawning();
	void deletePoint();

private:
	void spawnEnemy(CEnemy* enemy);
	Vector3f findFreeSpawnPosition(CEnemy* enemy);

};


#endif //CYBERPUNK_CSPAWNPOINT_HPP
