#include "CStatisticsManager.hpp"

int CStatisticsManager::score = 0;

void CStatisticsManager::scorePoints(int points) {
	score += points;
}

int CStatisticsManager::getScore() {
	return score;
}
