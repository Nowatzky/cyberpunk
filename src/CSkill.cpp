#include "CSkill.hpp"
#include "CPlayer.hpp"
#include "CIcon.hpp"
#include "CDebugHelper.hpp"
#include "CGameClock.hpp"

bool CSkill::updateButtonStatus(bool buttonStatus) {
	bool hasChanged = false;
	if(!isButtonPressed && buttonStatus) {
		if(clockCooldown.getTimeMs() >= cooldown && !isReloading) {
			if(energyConsumption == 0 || owner->consumeEnergy(energyConsumption)) {
				clockCooldown.restart();
				onButtonPress();
				isButtonPressed = true;
				hasChanged = true;
			} else {
				displayNoEnergy();
			}
		}
	} else if(isButtonPressed && !buttonStatus) {
		onButtonRelease();
		isButtonPressed = false;
		hasChanged = true;
	}

	return hasChanged;
}

void CSkill::displayNoEnergy() {
	if(!owner->getIsLocal()) {
		return;
	}

	if(displayNoEnergyClock.hasPassed(300)) {
		displayNoEnergyClock.restart();
		float iconSize = CGuiManager::getRelativeToGuiSizeX(0.15f);
		CIcon* noEnergyIcon = new CIcon("noEnergyIcon", CGuiManager::getRelativeToGuiSize(Vector2f(0.5, 0.3f)), Vector2f(iconSize, iconSize));
		noEnergyIcon->setColor(owner->getCharacterClass()->getAccentColor());
		noEnergyIcon->setLifetime(500);
		CGuiManager::addGuiElement(noEnergyIcon);
	}	
}

float CSkill::getCooldown() const {
	return cooldown;
}


