#ifndef CYBERPUNK_CPROJECTILESWEEPINGWALL_HPP
#define CYBERPUNK_CPROJECTILESWEEPINGWALL_HPP

#include "CProjectile.hpp"
#include "CColorDefines.hpp"
#include "CRectangleArea.hpp"
#include "CUtilityDefines.hpp"

class CProjectileSweepingWall : public CProjectile {
	float startSpeed = 20;
	float speedDecrease = 50; //per second
	float minSpeed = 2.0f;

	bool firstContact = false;
	
public:
    CProjectileSweepingWall(CCharacter* initiator, Vector3f pos, Vector2f dir);
	
	virtual void tick(float frameTime);

    void onHitEnemy(CEnemy* target, float frameTime);
};


#endif //CYBERPUNK_CPROJECTILESWEEPINGWALL_H