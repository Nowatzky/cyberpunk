#ifndef CYBERPUNK_CGHOSTSUMMONPOINT_HPP
#define CYBERPUNK_CGHOSTSUMMONPOINT_HPP

#include "CObject.hpp"
#include "CGraphicBox.hpp"
#include "CBoundingBox.hpp"

class CSpecterDmgField : public CObject {
private:
	CGraphicArea graphicArea;
	CBoundingBox boundingBox;
	Clock clockTriggerDuration;
	float triggerDuration;
	
	Color colorBase = Color(0, 0, 80, 180);
	Color colorAccent = Color(70, 70, 150, 255);
public:

	float damage = 20; //TODO make private again

	CSpecterDmgField(Vector3f pos, float fieldHalfWidth, float triggerDuration) :
			CObject(pos, 0),
			graphicArea(Vector3f(pos.x, pos.y, 0.55f), (2 * fieldHalfWidth) / 1.41f, 4, colorBase, colorAccent),
			boundingBox(Vector3f(pos.x, pos.y, 0.55f), Vector3f(fieldHalfWidth, fieldHalfWidth, 0.1f)),
			triggerDuration(triggerDuration) {}

    void tick(float frameTime);

    virtual CCollisionContainer* getCollisionContainer();
    virtual CGraphicContainer* getGraphicContainer();
};


#endif //CYBERPUNK_CGHOSTSUMMONPOINT_HPP
