#ifndef CYBERPUNK_CPROJECTILE_HPP
#define CYBERPUNK_CPROJECTILE_HPP

#include "CObject.hpp"
#include "CGraphicBox.hpp"
#include "CBoundingBox.hpp"
#include "CEnemy.hpp"
#include "CPlayer.hpp"


using namespace sf;

class CProjectile : public CObject {
protected:
	CCharacter* initiator;

	CBoundingBox boundingBox;
	CGraphicBox graphicBox;

	Vector2f dir;
	float speed;

	float lifetime = 10000;

	CClock clockLifetime;
	
public:
    CProjectile(CCharacter* initiator, Vector3f pos, Vector3f dim, Color color, Vector2f dir);
    CProjectile(CCharacter* initiator, Vector3f pos, Vector3f dim, Color color, Vector2f dir, float speed);
	
	virtual ~CProjectile() {};
	
	CCollisionContainer* getCollisionContainer();
	CGraphicContainer* getGraphicContainer();

    virtual void tick(float frameTime);
	
	virtual void onHitWall(float frameTime) {};
	virtual void onHitEnemy(CEnemy* target, float frameTime) {};
	virtual void onHitPlayer(CPlayer* player, float frameTime) {};
	virtual void onHitProjectile(CProjectile* projectile, float frameTime) {};

	void setDirection(Vector2f dir);
	void setSpeed(float speed);
	void setLifetime(float lifetime);
	
	Vector2f getDirection();
	float getSpeed();
};


#endif //CYBERPUNK_CPROJECTILE_HPP
