#include <SFML/System/Vector2.hpp>
#include <iostream>
#include "CDirection.hpp"
#include "CEngine.hpp"

CDirection CDirection::getLeft() const {
	if(dir == PX) {
		return NY;
	} else if(dir == NX) {
		return PY;
	} else if(dir == PY) {
		return PX;
	} else { /* NY */
		return NX;
	}
}

CDirection CDirection::getRight() const {
	if(dir == PX) {
		return PY;
	} else if(dir == NX) {
		return NY;
	} else if(dir == PY) {
		return NX;
	} else { /* NY */
		return PX;
	}
}

CDirection CDirection::getOpposite() const {
	if(dir == PX) {
		return NX;
	} else if(dir == NX) {
		return PX;
	} else if(dir == PY) {
		return NY;
	} else { /* NY */
		return PY;
	}
}

sf::Vector2f CDirection::toVector() {
	if(dir == PX) {
		return Vector2f(1, 0);
	} else if(dir == NX) {
		return Vector2f(-1, 0);
	} else if(dir == PY) {
		return Vector2f(0, 1);
	} else { /* NY */
		return Vector2f(0, -1);
	}
}

bool CDirection::operator==(const CDirection& other) const {
	return this->dir == other.dir;
}

bool CDirection::operator!=(const CDirection& other) const {
	return !this->operator==(other);
}

void CDirection::operator=(const CDirection& other) {
	dir = other.dir;
}

CDirection CDirection::fromVector(Vector2f vector) {
	vector = CEngine::normalize(vector);
	if(vector == Vector2f(1, 0)) {
		return CDirection(PX);
	} else if(vector == Vector2f(-1, 0)) {
		return CDirection(NX);
	} else if(vector == Vector2f(0, 1)) {
		return CDirection(PY);
	} else if(vector == Vector2f(0, -1)) {
		return CDirection(NY);
	} else {
		cout << "No direction found for vector " << vector.x << "/" << vector.y  << endl;
		return CDirection(NY);
	}
}

bool CDirection::isParallel(const CDirection& other) const {
	return (dir == other.dir || dir == other.getOpposite().dir);
}

bool CDirection::isOrthogonal(const CDirection& other) const {
	return !isParallel(other);
}

