#include "CProjectileRock.hpp"
#include "CSceneManager.hpp"

void CProjectileRock::onHitEnemy(CEnemy* target, float frameTime) {

    float current_hp = target->getHealth();
    target->reduceHealth(current_hp/2.f);


    CSceneManager::safeRemoveObject(this);
}
