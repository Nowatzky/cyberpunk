#include "CSpawningManager.hpp"
#include "CEnemyTypeHolograms.hpp"
#include "CRandom.hpp"
#include "CSpawnPoint.hpp"
#include "CColorDefines.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CGuiPopupText.hpp"
#include "CRenderManager.hpp"
#include "CNetworkManager.hpp"

CSpawningManager::CSpawningManager(CDifficulty difficulty, CEnemyTypeName enemyTypeName) :
	difficulty(difficulty) {
	
	switch (difficulty) {
		case CDifficulty::DEBUG:
			initialBudget = 0;
			budgetIncreasePerRoom = 0; 
			waveDivisor = 1;
			numberActiveSpawners = 0;
			break;
		case CDifficulty::EASY:
			initialBudget = 40;
			budgetIncreasePerRoom = 8;
			waveDivisor = 40.f;
			numberActiveSpawners = 1;
			break;
		case CDifficulty::MEDIUM:
			initialBudget = 50;
			budgetIncreasePerRoom = 11;
			waveDivisor = 40.f;
			numberActiveSpawners = 2;
			break;
		case CDifficulty::HARD:
			initialBudget = 60;
			budgetIncreasePerRoom = 15;
			waveDivisor = 40.f;
			numberActiveSpawners = 3;
			break;
	}
	budget = initialBudget;

	intensityManager = new CIntensityManager();
	
	if(enemyTypeName == CEnemyTypeName::HOLOGRAMS) {
		enemyType = (CEnemyType*) new CEnemyTypeHolograms();
	}
	/* Add new enemy types here */
}

void CSpawningManager::createSpawnList(vector<float> enemyTypeDistribution) {
	resetEnemyPoolList((int) enemyTypeDistribution.size());
	enemyWaveTable.clear();

	/* Determine general spawn list */
	float minRequiredCost = enemyType->getEnemyMinCosts(enemyTypeDistribution);

	while(budget > minRequiredCost) {
		CEnemyType::ENEMY_INSTANCE enemyInstanceType = (CEnemyType::ENEMY_INSTANCE) CRandom::getChoiceFromBackground(enemyTypeDistribution);
		if(budget >= enemyType->enemyCostList[(int) enemyInstanceType]) {
			acquireEnemyInstance(enemyInstanceType);
		}
	}
	/* Assign pool collection to waves */
	for(int i = 0; i < numberOfWaves; ++i) { //Set up table with zeros
		vector<int> wave_vector;
		for(unsigned int j = 0; j < enemyTypeDistribution.size(); ++j) {
			wave_vector.push_back(0);
		}
		enemyWaveTable.push_back(wave_vector);
	}

	// Fill
	for(unsigned int enemy_instance_type = 0; enemy_instance_type < enemyTypeDistribution.size(); ++enemy_instance_type) {
		for(int enemy_instance_Num = 0; enemy_instance_Num < enemyPoolList[enemy_instance_type]; ++enemy_instance_Num) {
			int c_groupNum = CRandom::getInRangeI(0, numberOfWaves - 1);
			++enemyWaveTable[c_groupNum][enemy_instance_type];
		}
	}
}

void CSpawningManager::acquireEnemyInstance(CEnemyType::ENEMY_INSTANCE enemy_instance_type) {
	budget -= enemyType->enemyCostList[enemy_instance_type];
	++enemyPoolList[enemy_instance_type];
}

void CSpawningManager::resetEnemyPoolList(int size) {
	enemyPoolList.clear();
	for (int i = 0; i < size; ++i) {
		enemyPoolList.push_back(0);
	}
}

CSpawningManager::~CSpawningManager() {
	delete enemyType;
}

void CSpawningManager::deployNewSpawner() {
	return;
	if(enemyWaveTable.empty()) {
		return;
	}

	//Determine spawner position
	Vector3f spawnPos = CEngine::toPlanar3D(CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(CSceneManager::getPlayers().at(0)->getPosition()))->getRandomPointInside());
	spawnPos.z = 1.301f;

	vector<CEnemy *> spawnList;
	vector<int> spawnListCounter = (vector<int>&&) enemyWaveTable.back();
	enemyWaveTable.pop_back();

	while(!std::all_of(spawnListCounter.begin(), spawnListCounter.end(), [](int i) { return i==0;})) { //checks if all zeros
		int index = CRandom::getInRangeI(0, (int) spawnListCounter.size() - 1);

		if(spawnListCounter[index] > 0) {
			spawnList.push_back(enemyType->getEnemyObject((CEnemyType::ENEMY_INSTANCE) index));
			--spawnListCounter[index];
		}
	}

	CSpawnPoint* p = new CSpawnPoint(spawnPos, spawnList, this);
	CSceneManager::safeAddObject(p);
	activeSpawnPoints.push_back(p);
}

void CSpawningManager::tick(float frameTime) {
	if(CNetworkManager::getRole() != CNetworkRole::HOST) {
		return;
	}
	return;
	
	intensityManager->tick(frameTime);

	CNavMesh* newNavMesh = CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(CSceneManager::getPlayers().at(0)->getPosition()));
	if(currentNavMesh != newNavMesh) {
		currentNavMesh = newNavMesh;
		isFloor = !isFloor;

		// Trigger new room
		if(!isFloor && !hasEnteredNewRoom && difficulty != CDifficulty::DEBUG) {
			newRoomEnteredClock.restart();
			hasEnteredNewRoom = true;
		}
	}

	if(hasEnteredNewRoom && newRoomEnteredClock.getElapsedTime().asMilliseconds() >= 1500) {
		hasEnteredNewRoom = false;

		//++roomNumber;
		initialBudget = initialBudget + budgetIncreasePerRoom;
		budget = initialBudget;
		numberOfWaves = max(1, (int) (initialBudget / waveDivisor));
		int eType = CRandom::getInRangeI(1,7, 3); //6; // CRandom::getInRangeI(1,6);
		//cout << "room " << roomNumber << " entered. Spawning enemies of type: " << eType <<  endl;
		createSpawnList(enemyType->getEnemySpawnProbabilities(eType));
		
		CSceneManager::getPlayers()[0]->reloadPrimarySkill();
		CSceneManager::getPlayers()[0]->healHealth(100);
		CSceneManager::getPlayers()[0]->restoreEnergy(100);

		//CSceneManager::getMap()->sealOfRoomAt(CSceneManager::getPlayers().at(0)->getPosition());
		hasOpenedRoom = false;	
	}

	for(unsigned int i = 0; i < activeSpawnPoints.size(); ++i) {
		if(activeSpawnPoints[i]->getIsDespawning()) {
			activeSpawnPoints.erase(activeSpawnPoints.begin() + i);
			--i;
		}
	}

	if(CSceneManager::getEnemies().empty() && enemyWaveTable.empty() && activeSpawnPoints.empty() && !hasOpenedRoom) {
		hasOpenedRoom = true;		
		//CSceneManager::getMap()->openRoomAt(CSceneManager::getPlayers().at(0)->getPosition());

		//std::string message = "CLEARED   ROOM   " + std::to_string(roomNumber);
		//CGuiPopupText* popupText = new CGuiPopupText(message, (unsigned int) (90 * CGuiManager::getGuiFontSizeFactor()), CSceneManager::getPlayers()[0]->getCharacterClass()->getBaseColor(), CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.3f)), 2500, true);
		//CGuiManager::addGuiElement(popupText);
	}
	
	if (enemyWaveTable.empty()) { //TODO delete this
		return;
	}

	if(activeSpawnPoints.size() < numberActiveSpawners) {
		if(activeSpawnPoints.size() == 0 || CRandom::occurAtChancePerSecond(spawnerDeployChance, frameTime)) {
			deployNewSpawner();
		}
	}
}

float CSpawningManager::getSpawnInterval() {
	return intensityManager->getSuggestedSpawnInterval();
}

float CSpawningManager::getIntensity() {
	return intensityManager->getIntensity();
}

CEnemyType* CSpawningManager::getEnemyType() {
	return enemyType;
}


