#include <iostream>
#include "CEnemyTypeHolograms.hpp"
#include "CEnemyGhost.hpp"
#include "CEnemyGhostSummoner.hpp"
#include "CEnemySpecter.hpp"
#include "CEnemyLich.hpp"


CEnemyTypeHolograms::CEnemyTypeHolograms() {
	setUpEnemyLists();
}

vector<float> CEnemyTypeHolograms::getEnemySpawnProbabilities(int spawnAssemblyType) {
	switch (spawnAssemblyType) {
		case 1:
			return {0,      1,      0,      0,      0};     //only invis
		case 2:
			return {0.3,      0.1,      0.6,    0,    0};   //mainly spawners
		case 3:
			return {0.47f, 0.4f, 0.1f, 0.03f, 0};   //default
		case 4:
			return {0.35f, 0.25f, 0.15f, 0.15f, 0.1f};
		case 5:
			return {0.3,   0.3,      0.2,    0,   0.2};  //enter lich
		case 6:
			return {0.45f,      0.4,   0,      0.15f,   0};   //some specters
		case 7:
			return {0.8f, 0,0,0.2f,0};

		default:
			return {0, 0, 0, 0, 1};
	}
}

float CEnemyTypeHolograms::getEnemyMinCosts(vector<float> enemyTypeDistribution) {
	float minRequiredCost = 0;
	for (int e = RANK_LAST; e >= RANK_FIRST; --e) {
		if (enemyTypeDistribution[e] > 0) {
			minRequiredCost = enemyCostList[e];
		}
	}

	return minRequiredCost;
}

CEnemy* CEnemyTypeHolograms::getEnemyObject(CEnemyType::ENEMY_INSTANCE enemy_instance) {
	float angle = 0;
	Vector3f pos;

	CEnemy* newEnemy;

	switch(enemy_instance) {
		case (ENEMY_INSTANCE) GHOST:
			pos.z = 1.0f;
			newEnemy = new CEnemyGhost(pos, angle, false);
			break;
		case (ENEMY_INSTANCE) GHOST_INV:
			pos.z = 1.0f;
			newEnemy = new CEnemyGhost(pos, angle, true);
			break;
		case (ENEMY_INSTANCE) GHOSTSUMMONER:
			pos.z = 1.2f;
			newEnemy = new CEnemyGhostSummoner(pos, angle);
			dynamic_cast<CEnemyGhostSummoner*>(newEnemy)->metaDataGhost = {enemyCostList[(int) GHOST], enemyPointList[(int) GHOST], enemyIntensityList[(int) GHOST]};
			dynamic_cast<CEnemyGhostSummoner*>(newEnemy)->metaDataInvisibleGhost = {enemyCostList[(int) GHOST_INV], enemyPointList[(int) GHOST_INV], enemyIntensityList[(int) GHOST_INV]};
			break;
		case (ENEMY_INSTANCE) SPECTER:
			pos.z = 1.3f;
			newEnemy = new CEnemySpecter(pos, angle);
			break;
		case (ENEMY_INSTANCE) LICH:
		default:
			pos.z = 1.2f;
			newEnemy = new CEnemyLich(pos, angle);
			break;
	}

	vector<float> metaData = {enemyCostList[(int) enemy_instance], enemyPointList[(int) enemy_instance], enemyIntensityList[(int) enemy_instance]};
	newEnemy->metaData = metaData;
	return newEnemy;
}

void CEnemyTypeHolograms::setUpEnemyLists() {
	enemyCostList = {4, 7, 22, 25, 35};
	enemyPointList = {4, 7, 22, 25, 35};
	enemyIntensityList = {4, 7, 12, 15, 20};
}
