#include "CFlatCircle.hpp"
#include "CSceneManager.hpp"

void CFlatCircle::tick(float frameTime) {
	if(lifeClock.getElapsedTime().asMilliseconds() > lifetime)
		CSceneManager::safeRemoveObject(this);
}

CCollisionContainer *CFlatCircle::getCollisionContainer() {
	return nullptr;
}

CGraphicContainer *CFlatCircle::getGraphicContainer() {
	return &graphicArea;
}
