#ifndef CYBERPUNK_CSKILLTELEPORT_HPP
#define CYBERPUNK_CSKILLTELEPORT_HPP


#include "CSkill.hpp"

class CSkillTeleport : public CSkill {
private:
	float distance = 4.5f;

public:
	CSkillTeleport(CPlayer* owner) :
			CSkill(owner, 250, 35) {}

protected:
	void onButtonPress();
};

#endif //CYBERPUNK_CSKILLTELEPORT_HPP
