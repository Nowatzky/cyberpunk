#ifndef CYBERPUNK_CNETWORKMANAGER_HPP
#define CYBERPUNK_CNETWORKMANAGER_HPP


#include <vector>
#include <SFML/Network.hpp>
#include "CObject.hpp"
#include "CMessage.hpp"
#include "CClock.hpp"
#include "CCharacterClass.hpp"
#include "CDifficultyManager.hpp"

#define HOST_PORT 8350

enum class CNetworkRole {HOST, PEER};

enum class CPacketType : sf::Uint8 {
	CONNECTION_BROADCAST_SEARCH = 128,  // 
	CONNECTION_BROADCAST_ANSWER,        // host name
	CONNECTION_REQUEST,                 // peer name
	CONNECTION_CONFIRM,                 //
	LOBBY_BROADCAST_CONNECTED_PEERS,    // numPeers, peer connections
	LOBBY_BROADCAST_CLASSNAME,          // className
	LOBBY_BROADCAST_DIFFICULTY,         // difficulty
	LOBBY_GAME_START,                   // startId, map seed, int playerNumber[0, 3]
	NTP_PING,                           // peerGameTimeOnSend 
	NTP_PONG,                           // peerGameTimeOnSend, hostGameTime
	MESSAGES,                           //
};

struct CConnection {
	sf::IpAddress address;
	unsigned short port;
	bool isHost;
	
	std::string name;
	CClassName className = CClassName::MEDIATOR;
	
	CClock isAliveClock;

	bool operator==(const CConnection other) const {
		return address == other.address && port == other.port;
	}
};

class CNetworkManager {
private:
	static bool active;
	static bool gameStarted;
	
	static int playerNumber;
	static int maxPlayers;
	static CDifficulty difficulty;
	
	static CNetworkRole networkRole;
	static sf::UdpSocket socket;
	
	static std::vector<CConnection> connections;
	
	static std::vector<CMessage*> pendingOutgoingMessages;
	static std::vector<CMessage*> pendingReceivedMessages;

	static std::vector<CObject*> replicatingObjects;

	static CClock updateClock;
	static CClock NTPUpdateClock;
	static float networkTickFrequency;
	static float ntpUpdateFrequency;
	
	static float movementInterpolationSafetyPackets;
	
public:
	static bool isActive();
	
	static bool hasGameStarted();
	static int getPlayerNumber();
	static CDifficulty getDifficulty();
	
	static float getNetworkTickFrequency();
	static float getMovementInterpolationSafetyPackets();
	
	// Lobby Init
	static void initNetworkSocket(CNetworkRole networkRole);
	static void closeNetworkSocket();
	static std::vector<CConnection> searchForHosts(bool useBroadcast);
	static void connectToHost(CConnection connection, std::string peerName);
	static void listenForConnectingPeers(std::string hostName);
	static void listenForLobbyMessages();
	static void broadcastConnectedPeers();
	static void broadcastDifficulty(CDifficulty difficulty);
	static void broadcastClassName(CClassName className);
	
	static void resetIsAliveClock(sf::IpAddress address, unsigned short port);
	static void kickTimeoutPeers(float timeout);
	
	// Basic getters
	static CNetworkRole getRole();
	static std::vector<CConnection> getConnections();
	
	static void addReplicatingObject(CObject* object);
	static void removeReplicatingObject(CObject* object);
	
	// Special Messages
	static void broadcastGameStart(sf::Uint32 seed);
	
	// Message API
	static CMessage* createMessage(CMessageType messageType);
	static void queueForBroadcast(CMessage* message);	
	
	static void receiveMessages();
	static void processReceivedMessages();
	
	static void tick();

	static void sendNTP();
	
private:
	CNetworkManager() {};
	
	static CPacketType getPacketType(sf::Packet& packet);
	static void setPacketType(sf::Packet& packet, CPacketType packetType);
	
	static void answerNTPPing(sf::IpAddress address, unsigned short port, sf::Packet packet);
	static void processNTPPong(sf::Packet packet);

	static void sendPendingMessages();
	static void updateReplicatedObjects();
};


#endif //CYBERPUNK_CNETWORKMANAGER_HPP
