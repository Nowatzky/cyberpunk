#ifndef CYBERPUNK_CMESSAGEOBJRPC_HPP
#define CYBERPUNK_CMESSAGEOBJRPC_HPP

#include "CMessage.hpp"
#include "CObjectIdManager.hpp"
#include "CRpcHandler.hpp"

class CMessageObjRPC : public CMessage {
public:
	CObjRPCType rpcType;
	CObjectId objectId;
	float gameTime;
	
	sf::Packet buffer;

	CMessageObjRPC() :
			CMessage(CMessageType::OBJ_RPC) {}

	virtual void serializeInto(sf::Packet& packet);
	virtual void deserializeFrom(sf::Packet& packet);
	virtual void process();
};

#endif //CYBERPUNK_CMESSAGEOBJRPC_HPP
