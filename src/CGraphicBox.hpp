#ifndef CYBERPUNK_CGRAPHICBOX_HPP
#define CYBERPUNK_CGRAPHICBOX_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "CGraphicContainer.hpp"
#include "CEngine.hpp"

using namespace sf;

class CGraphicBox : public CGraphicContainer {
private:
    /* Variables */
    Vector3f dim; // Only half of size
	VertexArray sides;
	VertexArray lines;

public:
    CGraphicBox(Vector3f pos, Vector3f dim, Color color0);
	CGraphicBox(Vector3f pos, Vector3f dim, Color colorBase, Color colorAccent);

	virtual bool doesOverlap(const CGraphicContainer& other) const;
	virtual bool isInFrontOf(const CGraphicContainer& other) const;

	virtual void draw(RenderTarget* target);
	virtual void drawOutline(RenderTarget* target);
	virtual void drawOverlap(RenderTarget *target) const;

	/* Accessors */
	virtual void setPosition(Vector3f pos);
	virtual void setPosOffset(Vector3f offset);
	virtual void setRotation(float rot);
	virtual void setDimension(Vector3f dim);
	virtual void setScaleOffset(float offset);

	virtual void setColorBase(Color color);
	virtual void setColorAccent(Color color);

	virtual void move(Vector3f movement);
	virtual void move(Vector2f movement);
	virtual void rotate(float angle);
	virtual void scale(float factor);

	virtual Vector3f getPosition() const;
	virtual Vector3f getDimension() const;
	virtual float getRotation() const;
	
private:
	void update();
};

#endif //CYBERPUNK_CGRAPHICBOX_HPP