#ifndef CYBERPUNK_CEFFECTMOVEMENTSLOW_HPP
#define CYBERPUNK_CEFFECTMOVEMENTSLOW_HPP


#include "CEffect.hpp"

class CEffectMovementSlow : public CEffect {
private:
	float movementSpeedModifier;
	
public:
	CEffectMovementSlow(CCharacter* initiator, float movementSpeedModifier, float lifetime) :
			CEffect(initiator, lifetime),
	        movementSpeedModifier(movementSpeedModifier) {}

	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);

	void setMovementSpeedModifier(float modifier);

};


#endif //CYBERPUNK_CEFFECTMOVEMENTSLOW_HPP
