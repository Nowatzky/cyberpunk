#ifndef CYBERPUNK_CTOPOLOGICALMAP_HPP
#define CYBERPUNK_CTOPOLOGICALMAP_HPP

#include <vector>
#include <deque>

#include "CObject.hpp"
#include "CTopologicalNode.hpp"

using namespace std;

class CTopologicalMap {
private:
	bool needsReconstruction = true;
	
	vector<CTopologicalNode> nodesMovable;
	vector<CTopologicalNode> nodesStatic;

    void visitNode(CTopologicalNode* node, deque<CObject*>* list);
    void constructTopologicalMap();
	void reconstructTopologicalMapForStatics();
	
	void clearNodeChildrenStatic();
	void clearNodeChildrenMovable();
	
	void unmarkAllNodes();
	
public:
    CTopologicalMap() {}
	
	void addObject(CObject* object);
    void removeObject(CObject* object);
	
	void requestReconstruction();
	
    deque<CObject*> getObjectHierarchy();
};

#endif //CYBERPUNK_CTOPOLOGICALMAP_HPP