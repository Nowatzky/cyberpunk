#include "CClock.hpp"
#include "CGameClock.hpp"
#include "CDebugHelper.hpp"

CClock::CClock() {
	this->offset = sf::Time::Zero;
}

CClock::CClock(sf::Time offset) {
	this->offset = offset;
}

CClock::CClock(float offset) {
	this->offset = sf::milliseconds(static_cast<sf::Int32>(offset));
}

sf::Time CClock::getElapsedTime() const {
	return clock.getElapsedTime() + offset;
}

float CClock::getTimeMs() const {
	return getElapsedTime().asMilliseconds();
}

sf::Time CClock::restart() {
	return restart(sf::Time::Zero);
}

sf::Time CClock::restart(sf::Time offset) {
	sf::Time time = clock.restart() + this->offset;
	this->offset = offset;

	return time;
}

sf::Time CClock::restart(float offset) {
	return restart(sf::milliseconds(static_cast<sf::Int32>(offset)));
}

void CClock::restartAtGameTime(float gameTime) {
	restart(CGameClock::getGameTime() - gameTime);
}

bool CClock::hasPassed(float time) const {
	return getTimeMs() >= time;
}
