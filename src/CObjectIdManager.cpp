#include "CObjectIdManager.hpp"
#include "CDebugHelper.hpp"

CObjectId CObjectIdManager::objectIdGenerator;

void CObjectIdManager::init(CObjectId initialId) {
	objectIdGenerator = initialId;
}

CObjectId CObjectIdManager::getUniqueId() {
	return objectIdGenerator++;
}
