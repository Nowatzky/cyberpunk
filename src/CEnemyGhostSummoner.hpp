#ifndef CYBERPUNK_CENEMYGHOSTSUMMONER_HPP
#define CYBERPUNK_CENEMYGHOSTSUMMONER_HPP

#include "CEnemy.hpp"
#include "CAIControllerGhostSummoner.hpp"
#include "CGhostSummonPoint.hpp"

class CEnemyGhostSummoner : public CEnemy {
private:
	bool isSummoning = false;
	float cooldownSummon = 3500;

	float cooldownSummonIncrease = 750;
	Clock clockSummonCooldown;
	float summonDuration = 1500;
	Clock clockSummonDuration;
	float summonInvisibleGhostChance = 0.2f;

	CGhostSummonPoint* summonPoint = nullptr;
public:

	CEnemyGhostSummoner(Vector3f pos, float rot = 0) :
			CEnemy(new CAIControllerGhostSummoner(this), pos, rot, Vector3f(0.3f, 0.3f, 0.7f), Color(100, 150, 220, 180), 200, 2.f) {
		turningMovementProfile.maxTurningSpeed = 160.f;
		turningMovementProfile.turningSpeedFactor = 2.f;
		turningMovementProfile.slowdownFactorAtMaxMisalignment = 0.4f;
	}
	virtual void die();

	void tick(float frameTime);
	virtual bool canBePushedByBullet();

	// Special Abilities
	void summon();

	// AI Controller interface
	bool isHostSummoning() const;

	bool isSummonReady() const;
	vector<float> metaDataGhost;
	vector<float> metaDataInvisibleGhost;

protected:
	virtual void onPushed();
	virtual void onStunned();


};

#endif //CYBERPUNK_CENEMYGHOSTSUMMONER_HPP
