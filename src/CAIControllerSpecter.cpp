#include "CAIControllerSpecter.hpp"

#include "CEnemySpecter.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"

void CAIControllerSpecter::tick(float frameTime) {
	if(!host->getIsPulling()) {
		if(host->hasReachedTarget() || host->isStuck(frameTime)) {
			pickNewWanderTarget();
		}

		host->updatePath(CEngine::to2D(CSceneManager::getPlayers().at(0)->getPosition()));
		host->followPath(frameTime);
	}
}

void CAIControllerSpecter::pickNewWanderTarget() {
	host->findPathTo(CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(host->getPosition()))->getRandomPointInside());
    // TODO Reimplement
    /*vector<Vector2f> freeFields = CSceneManager::getMap()->getFreeFields();

    while(!freeFields.empty()) {
        int pos = CRandom::getInRange(0, (int) (freeFields.size() - 1));
        Vector2f vec = freeFields.at((unsigned long) pos);
        freeFields.erase(freeFields.begin() + pos);

        float distToPlayer = CEngine::getDistance(vec, CEngine::to2D(CSceneManager::getPlayers().at(0)->getPosition()));
        float distToHost = CEngine::getDistance(vec, CEngine::to2D(host->getPosition()));

        if(distToPlayer >= 4 && distToHost >= 5) {
            wanderTarget = vec;
            break;
        }
    }*/
}

