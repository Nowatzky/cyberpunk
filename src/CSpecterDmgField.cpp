#include <iostream>
#include "CSpecterDmgField.hpp"
#include "CSceneManager.hpp"

void CSpecterDmgField::tick(float frame) {
    if(clockTriggerDuration.getElapsedTime().asMilliseconds() >= triggerDuration) {
	    vector<CObject*> charactersInside = CSceneManager::getIntersecting(&boundingBox, CCollisionChannel::Channel::CHARACTER);
	    
	    for(CObject* object : charactersInside) {
		    if(dynamic_cast<CPlayer*>(object) != nullptr) {
			    dynamic_cast<CPlayer*>(object)->reduceHealth(damage);
		    }
	    }
	    
        CSceneManager::safeRemoveObject(this);
    }
    
    float alpha = clockTriggerDuration.getElapsedTime().asMilliseconds() / triggerDuration;
	alpha = CEngine::clamp(alpha, 0.2f, 1);
	
	Color colorBaseStart = colorBase;
	Color colorAccentStart = colorAccent;
	
	getGraphicContainer()->setColorBase(CEngine::lerp(colorBaseStart, colorBase, alpha));
	getGraphicContainer()->setColorAccent(CEngine::lerp(colorAccentStart, colorAccent, alpha));
}

CCollisionContainer* CSpecterDmgField::getCollisionContainer() {
    return nullptr;
}

CGraphicContainer* CSpecterDmgField::getGraphicContainer() {
    return &graphicArea;
}
