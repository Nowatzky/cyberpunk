#include "CMapRoomGeneratorDigOut.hpp"
#include "CMapFloorBlock.hpp"

CMapRoom* CMapRoomGeneratorDigOut::generateRoom(CRectangleArea roomArea_, Doorway entrance_, CDirection exitDirection,
                                                int exitSize) {
	roomArea = roomArea_;
	entrance = entrance_;
		
	// Check if entrance lies within size
	entrance.left -= roomArea.offset;
	entrance.right -= roomArea.offset;
	
	if(!isPosValid(entrance.left) || !isPosValid(entrance.right)) {
		cout << "Entrance not within size!" << endl;
	}
	
	blocks.resize((unsigned long) roomArea.size.x);
	for(int i = 0; i < roomArea.size.x; i++) {
		blocks[i].resize((unsigned long) roomArea.size.y);
	}
	
	// Generate Blocks
	for(int x = 0; x < roomArea.size.x; x++) {
		for(int y = 0; y < roomArea.size.y; y++) {
			blocks[x][y].pos.x = x;
			blocks[x][y].pos.y = y;
		}
	}
	
	init();
	generateExit(exitDirection, exitSize);

	// Clear entrance
	for(Vector2f pos : getDoorwayBlocks(entrance)) {
		blocks[pos.x][pos.y].isDoorway = true;
		mapFloorBlocks.push_back(new CMapFloorBlock(Vector3f(pos.x + roomArea.offset.x, pos.y + roomArea.offset.y, 0), Vector3f(0.5f, 0.5f, 0.5f), colorFloorBase, colorFloorAccent));
		
	}

	// Clear exit
	for(Vector2f pos : getDoorwayBlocks(exit)) {
		blocks[pos.x][pos.y].isDoorway = true;
		mapFloorBlocks.push_back(new CMapFloorBlock(Vector3f(pos.x + roomArea.offset.x, pos.y + roomArea.offset.y, 0), Vector3f(0.5f, 0.5f, 0.5f), colorFloorBase, colorFloorAccent));
	}

	//generateObstacles();
	
	generateMapBlocks();

	for(int x = 0; x < roomArea.size.x; x++) {
		for(int y = 0; y < roomArea.size.y; y++) {
			if(!blocks[x][y].isSolid) {
				mapFloorBlocks.push_back(new CMapFloorBlock(Vector3f(x + roomArea.offset.x, y + roomArea.offset.y, 0), Vector3f(0.5f, 0.5f, 0.5f), colorFloorBase, colorFloorAccent));
			}
		}
	}

	mapFloorBlocks = reduceFloorBlocks(mapFloorBlocks);	

	return new CMapRoom(getEntranceWithOffset(), getExitWithOffset(), mapWallBlocks, mapFloorBlocks);
}

void CMapRoomGeneratorDigOut::init() {
	//Dig first Blocks in front of entrance
	digWall(Wall{entrance.left + entrance.direction.toVector(), entrance.right + entrance.direction.toVector(), entrance.direction}, 8);

	while(getDiggedFields() < roomArea.size.x * roomArea.size.y * digOutFactor) {
		updateWalls();
		int w = CRandom::getInRangeI(0, (int) (walls.size() - 1));
		
		if(canWallBeDigged(walls.at((unsigned long) w))) {
			Wall wall = walls.at((unsigned long) w);
			// Alter wall
			// 0 = whole wall
			// 1 = from left
			// 2 = from right
			// 3 = middle
			vector<float> probabilities = {0.20f, 0.30f, 0.30f, 0.2f};
			float wallLength = getWallLength(wall);
			int cutoff;
			
			switch(CRandom::getChoiceFromBackground(probabilities)) {
				case 0: /* whole wall */
					break;
				case 1: { /* from left */
					cutoff = CRandom::getInRangeI((int) (wallLength / 3.f), (int) (wallLength / 2.f));
					wall.right -= wall.direction.getRight().toVector() * (float) cutoff;
					
					break; 
				}
				case 2: { /* from right */
					cutoff = CRandom::getInRangeI((int) (wallLength / 3.f), (int) (wallLength / 2.f));
					wall.left -= wall.direction.getLeft().toVector() * (float) cutoff;
					
					break;
				}
				case 3: { /* middle */
					cutoff = CRandom::getInRangeI((int) (wallLength / 3.f), (int) (wallLength / 2.f));
					float cutoffLeft = (float) CRandom::getInRangeI((int) (cutoff / 4.f), cutoff);
					float cutoffRight = (float) cutoff - cutoffLeft;

					wall.left -= wall.direction.getLeft().toVector() * cutoffLeft;
					wall.right -= wall.direction.getRight().toVector() * cutoffRight;
					
					break;
				}
				default:
					break;
			}
			
			if(getWallLength(wall) < minDigWallLength) {
				continue;
			}
			
			int digDepth = CRandom::getInRangeI(minDigDepth, min(getMaxDigDepth(wall), maxDigDepth)); 
			
			digWall(wall, digDepth);
		} 		
	}	
	
	// Remove slim walls
	for(int i = 0; i < 2; i++) {
		for(int x = 1; x < roomArea.size.x - 1; x++) {
			for(int y = 1; y < roomArea.size.y - 1; y++) {
				if(isSlimWall(Vector2f(x, y))) {
					digBlock(Vector2f(x, y));
				}
			}
		}
	}
}

void CMapRoomGeneratorDigOut::generateMapBlocks() {
	vector<Vector2f> requiredWalls;
	
	for(int x = 0; x < roomArea.size.x; x++) {
		for(int y = 0; y < roomArea.size.y; y++) {
			if(isWallRequired(Vector2f(x, y))) {
				requiredWalls.push_back(Vector2f(x, y));
			}
		}
	}
	
	while(!requiredWalls.empty()) {
		Vector2f current = requiredWalls.back();
		requiredWalls.pop_back();

		if(blocks[current.x][current.y].hasMapBlock) {
			continue;
		}
		
		// Expand
		CDirection dirExpand;
		bool canExpand = false;
		for(CDirection dir : CDirection::allDirections()) {
			Vector2f adjacentPos = current + dir.toVector();
			if(isPosValid(adjacentPos) && isWallRequired(adjacentPos) && !getBlock(adjacentPos).hasMapBlock) {
				dirExpand = dir;
				canExpand = true;
				break;
			}
		}

		if(canExpand) {
			vector<Vector2f> listBlocks;
			listBlocks.push_back(current);
			Vector2f start = current;
			Vector2f end = current;
			
			// Expand in both directions
			for(Vector2f step = dirExpand.toVector(); step != dirExpand.getOpposite().toVector(); step = dirExpand.getOpposite().toVector()) {
				float i = 1;
				Vector2f pos = current + i * step;
				while(isPosValid(pos)) {
					if(isWallRequired(pos) && !getBlock(pos).hasMapBlock) {
						listBlocks.push_back(pos);

						// Update current last point
						if(step == dirExpand.toVector()) {
							start = pos;
						} else {
							end = pos;
						}
						
						i++;
						pos = current + i * step;
					} else {
						break;
					}
				}
			}
			
			for(Vector2f blockToBeAdded : listBlocks) {
				requiredWalls.erase(std::remove(requiredWalls.begin(), requiredWalls.end(), blockToBeAdded), requiredWalls.end());
				blocks[blockToBeAdded.x][blockToBeAdded.y].hasMapBlock = true;
			}

			mapWallBlocks.push_back(new CMapBlock(start + roomArea.offset, end + roomArea.offset, 1, colorWallBase + Color( 0,0,0), colorWallAccent, true));
		} else { /* cant expand */
			blocks[current.x][current.y].hasMapBlock = true;
			
			mapWallBlocks.push_back(new CMapBlock(Vector3f(current.x + roomArea.offset.x, current.y + roomArea.offset.y, 1), Vector3f(0.5f, 0.5f, 0.5f), colorWallBase, colorWallAccent));
		}
	}
}

void CMapRoomGeneratorDigOut::generateExit(CDirection exitDirection, int exitSize) {
	updateWalls();
	
	vector<Wall> possibleExitWalls;

	exit.direction = exitDirection;
	
	for(Wall wall : walls) {
		if(wall.direction == exitDirection && getWallLength(wall) >= exitSize + 2) {
			possibleExitWalls.push_back(wall);
		}
	}
	
	while(!possibleExitWalls.empty()) {
		int index = CRandom::getInRangeI(0, (int) (possibleExitWalls.size() - 1));
		Wall currentWall = possibleExitWalls.at((unsigned long) index);
		possibleExitWalls.erase(possibleExitWalls.begin() + index);
		
		// Check if suited as exit
		int cutoff = getWallLength(currentWall) - exitSize;
		float cutoffLeft = floor((float) cutoff / 2.f);
		float cutoffRight = (float) cutoff - cutoffLeft;

		exit.left = currentWall.left + exitDirection.getRight().toVector() * cutoffLeft;
		exit.right = currentWall.right + exitDirection.getLeft().toVector() * cutoffRight;
		
		if(isDoorwayPossible(exit)) {
			return;
		}
	}
	
	// No exit found TODO fix
	cout << "No exit found for size " << exitSize << "!" << endl;
	if(exitSize >= 2) {
		cout << "Trying again with size " << (exitSize - 1) << endl;
		generateExit(exitDirection, exitSize - 1);
	} else {
		cout << "Error!" << endl;
	}
	return;
}

bool CMapRoomGeneratorDigOut::isDoorwayPossible(Doorway doorway) {
	Vector2f direction = doorway.direction.toVector();
	
	// Make doorway more wide to check if there is place for walls too
	doorway.left += doorway.direction.getLeft().toVector() * 2.f;
	doorway.right += doorway.direction.getRight().toVector() * 2.f;
	
	for(Vector2f block : getDoorwayBlocks(doorway)) {
		int d = 0;
		while(isPosValid(block + (float) d * direction)) {
			if(!getBlock(block + (float) d * direction).isSolid) {
				return false;
			}
			d++;			
		}
	}
	
	return true;
}

void CMapRoomGeneratorDigOut::digBlock(Vector2f pos) {
	blocks[(int) pos.x][(int) pos.y].isSolid = false;
}

void CMapRoomGeneratorDigOut::fillBlock(Vector2f pos) {
	blocks[(int) pos.x][(int) pos.y].isSolid = true;
}

vector<Vector2f> CMapRoomGeneratorDigOut::getDoorwayBlocks(Doorway doorway) {
	Wall wall;
	wall.direction = doorway.direction;
	wall.left = doorway.left;
	wall.right = doorway.right;
	return getWallBlocks(wall);
}

vector<Vector2f> CMapRoomGeneratorDigOut::getWallBlocks(Wall wall) {
	vector<Vector2f> blocks;

	Vector2f step = wall.direction.getRight().toVector();
	Vector2f current = wall.left;

	while(current != wall.right) {
		blocks.push_back(current);
		current += step;
	}
	blocks.push_back(current);

	return blocks;
}

void CMapRoomGeneratorDigOut::updateWalls() {
	walls.clear();
	clearAllIsInWall();
	
	for(int x = 0; x < roomArea.size.x; x++) {
		for(int y = 0; y < roomArea.size.y; y++) {
			if(!blocks[x][y].isSolid) {
				for(Vector2f neighbour : getNeighbours(Vector2f(x, y))) {
					if(getBlock(neighbour).isSolid && (!getBlock(neighbour).isInWall || isCornerWall(neighbour))) {
						Wall newWall;
						newWall.direction = CDirection::fromVector(neighbour - Vector2f(x, y));
						
						Vector2f step = newWall.direction.getLeft().toVector();
						newWall.left = getLastWallInDirection(neighbour, Vector2f(x, y), step);

						step = newWall.direction.getRight().toVector();
						newWall.right = getLastWallInDirection(neighbour, Vector2f(x, y), step);	
						
						walls.push_back(newWall);
					}					
				}	
			}
		}
	}
}

void CMapRoomGeneratorDigOut::clearAllIsInWall() {
	for(int x = 0; x < roomArea.size.x; x++) {
		for(int y = 0; y < roomArea.size.y; y++) {
			blocks[x][y].isInWall = false;
		}
	}
}

bool CMapRoomGeneratorDigOut::isCornerWall(Vector2f pos) {
	int freeNeighbours = 0;
	for(Vector2f neighbour : getNeighbours(pos)) {
		if(!getBlock(neighbour).isSolid) {
			freeNeighbours++;
		}
	}

	return freeNeighbours > 1;
}

CMapRoomGeneratorDigOut::Block CMapRoomGeneratorDigOut::getBlock(Vector2f pos) {
	return blocks[(int) pos.x][(int) pos.y];
}

Vector2f CMapRoomGeneratorDigOut::getLastWallInDirection(Vector2f currentWall, Vector2f currentFree, Vector2f step) {
	while(isPosValid(currentWall) && isPosValid(currentFree) && getBlock(currentWall).isSolid && !getBlock(currentFree).isSolid) {
		blocks[(int)currentWall.x][(int)currentWall.y].isInWall = true;
		currentWall += step;
		currentFree += step;
	}
	return currentWall - step;
}

void CMapRoomGeneratorDigOut::printWalls() {
	for(Wall wall : walls) {
		string dir;
		if(wall.direction == CDirection::PX) {
			dir = "PX";
		}
		if(wall.direction == CDirection::NX) {
			dir = "NX";
		}
		if(wall.direction == CDirection::PY) {
			dir = "PY";
		}
		if(wall.direction == CDirection::NY) {
			dir = "NY";
		}
		
		cout << "Wall in direction " << dir << " from " << wall.left.x << "/" << wall.left.y << " to " << wall.right.x << "/" << wall.right.y << endl;
	}
}

void CMapRoomGeneratorDigOut::digWall(Wall wall, int depth) {
	Vector2f direction = wall.direction.toVector();
	for(Vector2f block : getWallBlocks(wall)) {
		for(int d = 0; d < depth; d++) {
			digBlock(block + (float) d * direction);
		}
	}
}

bool CMapRoomGeneratorDigOut::canWallBeDigged(Wall wall) {
	// Not towards entrance
	if(wall.direction == entrance.direction.getOpposite()) {
		return false;
	}
	// Minimum length
	if(getWallLength(wall) < minDigWallLength) {
		return false;
	}
	if(getMaxDigDepth(wall) < minDigDepth) {
		return false;
	}
	
	return true;
}

int CMapRoomGeneratorDigOut::getWallLength(Wall wall) {
	return (int) (abs(wall.left.x - wall.right.x) + abs(wall.left.y - wall.right.y)) + 1;
}

void CMapRoomGeneratorDigOut::printWallsAscii() {
	cout << endl << endl;
	for(int y = 0; y < roomArea.size.y; y++) {
		for(int x = 0; x < roomArea.size.x; x++) {
			bool isEntrance = false;
			for(Vector2f e : getDoorwayBlocks(entrance)) {
				if(e.x == x && e.y == y) {
					isEntrance = true;
					break;
				}
			}
			bool isExit = false;
			for(Vector2f e : getDoorwayBlocks(exit)) {
				if(e.x == x && e.y == y) {
					isExit = true;
					break;
				}
			}
			if(isEntrance) {
				cout << "()";
			} else if(isExit) {
				cout << "<>";
			} else if(blocks[x][y].isSolid) {
				cout << "XX";
			} else {
				cout << "  ";
			}
		}
		cout << endl;
	}
	cout << endl;
}

bool CMapRoomGeneratorDigOut::isWallRequired(Vector2f pos) {
	if(getBlock(pos).isDoorway || !getBlock(pos).isSolid || getBlock(pos).isPartOfDoorway) {
		return false;
	}
	
	for(Vector2f neighbour : getNeighboursExtended(pos)) {
		if(!getBlock(neighbour).isSolid) {
			return true;
		}
	}

	return false;
}

void CMapRoomGeneratorDigOut::generateObstacles() {
	int diggedFieldsStart = getDiggedFields();
	
	while((float)(diggedFieldsStart - getDiggedFields()) < diggedFieldsStart * 0.05f) {
		generateObstacle();
	}
	
	// Fill holes
	for(int i = 0; i < 2; i++) {
		for(int x = 1; x < roomArea.size.x - 1; x++) {
			for(int y = 1; y < roomArea.size.y - 1; y++) {
				int solidNeighbours = 0;
				for(Vector2f n : getNeighbours(Vector2f(x, y))) {
					if(getBlock(n).isSolid) {
						solidNeighbours++;
					}
				}
				if(solidNeighbours >= 3) {
					fillBlock(Vector2f(x, y));
				}
			}
		}
	}
	
	// Fill gangways
	for(int x = 1; x < roomArea.size.x - 1; x++) {
		for(int y = 1; y < roomArea.size.y - 1; y++) {
			if(isGangway(Vector2f(x, y))) {
				fillBlock(Vector2f(x, y));
			}
		}
	}	
	
	// Remove slim walls again
	for(int i = 0; i < 2; i++) {
		for(int x = 1; x < roomArea.size.x - 1; x++) {
			for(int y = 1; y < roomArea.size.y - 1; y++) {
				if(isSlimWall(Vector2f(x, y))) {
					digBlock(Vector2f(x, y));
				}
			}
		}
	}
}

void CMapRoomGeneratorDigOut::generateObstacle() {
	Vector2f pos;
	while(true) {
		pos = Vector2f(CRandom::getInRangeI(1, (int) (roomArea.size.x - 1)), CRandom::getInRangeI(1, (int) (roomArea.size.y - 1)));

		int solidNeighbours = 0;
		for(Vector2f n : getNeighboursExtended(pos)) {
			if(getBlock(n).isSolid) {
				solidNeighbours++;
			}
		}
		
		if(!getBlock(pos).isSolid && !getBlock(pos).isDoorway && solidNeighbours == 0) {
			break;
		}
	}
	
	fillBlock(pos);
	
	vector<Vector2f> neighbours = getNeighboursExtended(pos);
	
	for(Vector2f neighbourPos : neighbours) {
		if(!getBlock(neighbourPos).isDoorway) {
			if(CRandom::occurAtChance(0.75f)) {
				fillBlock(neighbourPos);
			}
		}	
	}
}

vector<Vector2f> CMapRoomGeneratorDigOut::getNeighbours(Vector2f pos) {
	vector<Vector2f> neighbours;

	Vector2f n;
	n = Vector2f(pos.x + 1, pos.y);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x - 1, pos.y);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x, pos.y + 1);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x, pos.y - 1);
	if(isPosValid(n))
		neighbours.push_back(n);

	return neighbours;
}

vector<Vector2f> CMapRoomGeneratorDigOut::getNeighboursExtended(Vector2f pos) {
	vector<Vector2f> neighbours;

	Vector2f n;
	n = Vector2f(pos.x + 1, pos.y);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x - 1, pos.y);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x, pos.y + 1);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x, pos.y - 1);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x + 1, pos.y + 1);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x + 1, pos.y - 1);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x - 1, pos.y + 1);
	if(isPosValid(n))
		neighbours.push_back(n);
	n = Vector2f(pos.x - 1, pos.y - 1);
	if(isPosValid(n))
		neighbours.push_back(n);

	return neighbours;
}

bool CMapRoomGeneratorDigOut::isSlimWall(Vector2f pos) {
	return (!getBlock(pos - Vector2f(1, 0)).isSolid && !getBlock(pos - Vector2f(-1, 0)).isSolid) ||
	       (!getBlock(pos - Vector2f(0, 1)).isSolid && !getBlock(pos - Vector2f(0, -1)).isSolid);

}

bool CMapRoomGeneratorDigOut::isGangway(Vector2f pos) {
	return (getBlock(pos - Vector2f(1, 0)).isSolid && getBlock(pos - Vector2f(-1, 0)).isSolid) ||
	       (getBlock(pos - Vector2f(0, 1)).isSolid && getBlock(pos - Vector2f(0, -1)).isSolid);
}

int CMapRoomGeneratorDigOut::getDiggedFields() {
	int diggedFields = 0;
	
	for(int x = 0; x < roomArea.size.x; x++) {
		for(int y = 0; y < roomArea.size.y; y++) {
			if(!blocks[x][y].isSolid) {
				diggedFields++;
			}
		}
	}

	return diggedFields;
}

bool CMapRoomGeneratorDigOut::isPosValid(Vector2f pos) {
	return !(pos.x < 0 || pos.y < 0 || pos.x >= roomArea.size.x || pos.y >= roomArea.size.y);
}

int CMapRoomGeneratorDigOut::getMaxDigDepth(CMapRoomGeneratorDigOut::Wall wall) {
	Vector2f current = wall.left;
	Vector2f step = wall.direction.toVector();
	int depth = 0;
	
	while(isPosValid(current)) {
		depth++;
		current += step;
	}
	
	return depth - 1; // So one wall is always left	
}

// TODO Refactor, speed up by using array of blocks instead of looping over whole list over and over again
vector<CMapBlock*> CMapRoomGeneratorDigOut::reduceFloorBlocks(vector<CMapBlock*> listOriginal) {
	vector<CRectangleArea> listRoomAreas;
	
	while(!listOriginal.empty()) {
		// Pick new block to expand
		CRectangleArea currentArea;
		currentArea.offset = Vector2f(listOriginal.back()->getPosition().x, listOriginal.back()->getPosition().y);
		currentArea.size = Vector2f(0, 0);
		listOriginal.pop_back();
		
		deque<CDirection> directionsToExpand = {CDirection::PX, CDirection::NX, CDirection::PY, CDirection::NY};
		
		// Expand
		while(!directionsToExpand.empty()) {
			CDirection direction = directionsToExpand.front();
			
			if((direction == CDirection::PX || direction == CDirection::NX) && currentArea.size.x > 14) {
				directionsToExpand.pop_front();
				continue;
			} else if((direction == CDirection::PY || direction == CDirection::NY) && currentArea.size.y > 14) {
				directionsToExpand.pop_front();
				continue;
			}
			
			vector<CMapBlock*> blocksFound;
			int neededSize;
			
			if(direction == CDirection::PX || direction == CDirection::NX) {
				float neededX;
				if(direction == CDirection::PX) {
					neededX = currentArea.offset.x + currentArea.size.x + 1;	
				} else {
					neededX = currentArea.offset.x - 1;
				}					
				float minY =  currentArea.offset.y;
				float maxY = currentArea.offset.y + currentArea.size.y;
				neededSize = (int) (currentArea.size.y + 1);
				
				for(CMapBlock* mapBlock : listOriginal) {
					if(mapBlock->getPosition().x == neededX && mapBlock->getPosition().y >= minY && mapBlock->getPosition().y <= maxY) {
						blocksFound.push_back(mapBlock);
					}
				}					
			} else {
				float neededY;
				if(direction == CDirection::PY) {
					neededY = currentArea.offset.y + currentArea.size.y + 1;
				} else {
					neededY = currentArea.offset.y - 1;
				}
				float minX = currentArea.offset.x;
				float maxX = currentArea.offset.x + currentArea.size.x;
				neededSize = (int) (currentArea.size.x + 1);

				for(CMapBlock* mapBlock : listOriginal) {
					if(mapBlock->getPosition().y == neededY && mapBlock->getPosition().x >= minX && mapBlock->getPosition().x <= maxX) {
						blocksFound.push_back(mapBlock);
					}
				}
			}

			if(blocksFound.size() == (unsigned int) neededSize) {
				if(direction == CDirection::PX) {
					currentArea.size.x++;
				} else if(direction == CDirection::NX) {
					currentArea.offset.x--;
					currentArea.size.x++;
				} else if(direction == CDirection::PY) {
					currentArea.size.y++;
				} else {
					currentArea.offset.y--;
					currentArea.size.y++;
				}
				
				for(CMapBlock* mapBlock : blocksFound) {
					listOriginal.erase(remove(listOriginal.begin(), listOriginal.end(), mapBlock), listOriginal.end());
				}				
			} else {
				directionsToExpand.erase(remove(directionsToExpand.begin(), directionsToExpand.end(), direction), directionsToExpand.end());
			}				
		}
		
		listRoomAreas.push_back(currentArea);
	}
	
	// Generate floorBlocks
	vector<CMapBlock*> floorBlocks;
	for(CRectangleArea area : listRoomAreas) {
		if(area.size.x == 0 && area.size.y == 0) {
			floorBlocks.push_back(new CMapFloorBlock(Vector3f(area.offset.x, area.offset.y, 0), Vector3f(0.5f, 0.5f, 0.5f), colorFloorBase, colorFloorAccent));
		} else {
			area.size.x++;
			area.size.y++;
			floorBlocks.push_back(new CMapFloorBlock(area, 0, 0.5f, colorFloorBase, colorFloorAccent));
		}
	}

	return floorBlocks;	
}

Doorway CMapRoomGeneratorDigOut::getExitWithOffset() {
	Doorway exitWithOffset;
	exitWithOffset.direction = exit.direction;
	exitWithOffset.left = exit.left + roomArea.offset;
	exitWithOffset.right = exit.right + roomArea.offset;

	return exitWithOffset;
}

Doorway CMapRoomGeneratorDigOut::getEntranceWithOffset() {
	Doorway entranceWithOffset;
	entranceWithOffset.direction = entrance.direction;
	entranceWithOffset.left = entrance.left + roomArea.offset;
	entranceWithOffset.right = entrance.right + roomArea.offset;

	return entranceWithOffset;
}

