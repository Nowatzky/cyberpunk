#ifndef CYBERPUNK_CNAVPATH_HPP
#define CYBERPUNK_CNAVPATH_HPP

#include <deque>
#include <SFML/System.hpp>
#include "CObject.hpp"

class CNavMesh; 

using namespace sf;
using namespace std;

class CNavPath final {
private:
	deque<Vector2f> waypoints;

	const CNavMesh* envelopingNavMesh; // TODO Split into two path classes, one for within one navMesh, one spanningMultipleNavMeshes
	vector<CObject*> debugVisuals;

public:
	CNavPath(const CNavMesh* envelopingNavMesh) :
		envelopingNavMesh(envelopingNavMesh) {}
	
	~CNavPath();
	
	Vector2f getFirstWaypoint() const;
	void removeFirstWaypoint();
	
	void prependWaypoint(Vector2f waypoint);
	void appendWaypoint(Vector2f waypoint);
	
	void smoothFromStart(Vector2f start, float agentHalfWidth);
	void smoothCompletely(Vector2f start, float agentHalfWidth);
	bool updateTarget(Vector2f start, Vector2f target, float agentHalfWidth);
	
	float getLength(Vector2f start) const;

	void addDebugVisuals(Vector2f startPoint);
	void removeDebugVisuals();


private:
	CObject* createLine(Vector2f p1, Vector2f p2);

};


#endif //CYBERPUNK_CNAVPATH_HPP
