#ifndef CYBERPUNK_CNAVAGENT_HPP
#define CYBERPUNK_CNAVAGENT_HPP

#include "CNavPath.hpp"

class CEnemy;

class CNavAgent {
private:
	CEnemy* host;
	float agentHalfWidth;
	
	CNavPath* currentPath = nullptr;
	Clock lastPathReconstructionClock;
	
	Clock lastPathUpdate;
	
	// isStuck detection
	Vector3f lastPosition;
	int isStuckFramesCount = 0;
	int maxIsStuckFramesCount = 8;

protected:
	CNavAgent(CEnemy* host);
	
public:
	void findPathTo(Vector2f target);
	void followPath(float frameTime);
	void setPath(CNavPath* path);
	
	void updatePath(Vector2f target);
	void updatePath();
	
	float getAgentHalfWidth() const;
	
	bool hasReachedTarget() const;
	
	bool isStuck(float frameTime);
	
	void addDebugVisuals();
	void removeDebugVisuals();
	
private:
	bool needsPathReconstruction();
	Vector2f getCurrentWaypoint();




};


#endif //CYBERPUNK_CNAVAGENT_HPP
