#include "CSkillHealthBait.hpp"
#include "CInputManager.hpp"
#include "CSceneManager.hpp"
#include "CProjectileHealthBait.hpp"
#include "CFlatCircle.hpp"

void CSkillHealthBait::onButtonPress() {
	spawnHealthBait();
}

void CSkillHealthBait::spawnHealthBait() {
	Vector2f screenPos = CInputManager::getMousePositionScene();
	Vector3f worldPos = CEngine::to3DOnPlane(screenPos, owner->getPosition().z);
	Vector2f dir = CEngine::normalize(Vector2f(worldPos.x - owner->getPosition().x, worldPos.y - owner->getPosition().y));

	CProjectileHealthBait* bait = new CProjectileHealthBait(owner, worldPos, dir);
	CSceneManager::safeAddObject(bait);
	CSceneManager::safeAddObject(new CFlatCircle(worldPos, 0.25, owner->getCharacterClass()->getBaseColor(), owner->getCharacterClass()->getAccentColor(),1000));
}
