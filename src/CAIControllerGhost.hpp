#ifndef CYBERPUNK_CAICONTROLLERLURKER_HPP
#define CYBERPUNK_CAICONTROLLERLURKER_HPP

#include "CAIController.hpp"
#include "CObject.hpp"

class CEnemyGhost;

class CAIControllerGhost : public CAIController {
private:
	CEnemyGhost* host;

	CObject* prevTarget = nullptr;

public:
	CAIControllerGhost(CEnemyGhost* host) :
			host(host) {}

	virtual void tick(float frameTime);

private:
	bool isWayToTargetFree(CObject* target) const;

};


#endif //CYBERPUNK_CAICONTROLLERLURKER_HPP
