//
// Created by chaperone on 13.03.17.
//

#include <stdlib.h>
#include <sstream>
#include <cassert>
#include <iostream>
#include <cmath>
#include <fstream>
#include "CPatternRecognitionMngr.hpp"
#include "CDebugHelper.hpp"
#include "CGuiDrawing.hpp"
#include "CGuiManager.hpp"
#include <math.h>       /* pow */


using namespace std;

CPatternRecognitionMngr::CPatternRecognitionMngr() {
    //load neural network from source
    if(!loadNeuralNetFromSource("./src/net_structure.txt")) {
        cout << "Issue occurred while loading neural network from structure source" << endl;
        //abort();
    }

    //To be removed: Create and live-train network

    //vector<unsigned> topology = {(unsigned int) (2 * netResolution), 24, 24, 5}; //used to be 18 20 10 5
    //ensure trash.txt is gonna be last
    //vector<string> trainPaths = {"./trainingData/alpha.txt", "./trainingData/hline.txt", "./trainingData/vline.txt","./trainingData/wedge_up.txt", "./trainingData/epsilon.txt", "./trainingData/trash.txt"};
    //net = call(topology, trainPaths, 100000);

    assert(net);
}

SIGN CPatternRecognitionMngr::determineSign(vector<Vector2f> &pointsDrawn, float *quality_score, vector<double> &netOutput) {
    if(pointsDrawn.size() < netResolution / 2.f + 1) {
        *quality_score = -1;
        return NONE;
    }

    //process point records
    vector<Vector2f> pointsSelected;
    vector<Vector2f> pointDirectionVectors;

    if (!fetchSelectPoints(pointsDrawn, pointsSelected, pointDirectionVectors)) {
        cout << "Depreciated number of selected vector derived from user input" << endl;

        *quality_score = -1;
        return NONE;
    }
    vector <double> netInput;
    //netOutput.clear(); //TODO rationalize
    ////vector<double> netOutput;
    convertVectorToNetInput(pointDirectionVectors, netInput);

    //run the network
    assert(net);

    net->feedForward(netInput);
    net->getResults(netOutput);

    //determine validated pattern from net output
    return validateSign(netOutput, quality_score);
}

bool CPatternRecognitionMngr::loadNeuralNetFromSource(string path) {
    ifstream in_file(path, ios::in);

    vector<vector <vector<double> > > weight_cube;
    vector<unsigned> topology;

    //get topology
    string line;
    string label;

    getline(in_file, line);
    stringstream ss1(line);

    ss1 >> label;

    if(in_file.eof() || label.compare("topology:") != 0)
        return false;
    while(!ss1.eof()) {
        double n;
        ss1 >> n;
        topology.push_back(n);
    }

    //get weights for every neuron
    unsigned int i;
	int j;
    vector<vector<double> > weight_layer_matrix;
    for(i = 0, j = 0; i < topology.size() - 1; ++j) {
        getline(in_file, line);
        stringstream ss2(line);

        ss2 >> label;

        string expected_label = "[";
        expected_label+=to_string(i);
        expected_label+=",";
        expected_label+=to_string(j);
        expected_label+="]";

        if(in_file.eof() || label.compare(expected_label) != 0) {
            return false;
            //abort();
        }

        double c_weight;
        vector<double> weights_of_c_neuron;
        while(ss2 >> c_weight) {
            weights_of_c_neuron.push_back(c_weight);
        }

        if(weights_of_c_neuron.size() != topology[i+1]) {
	        return false;
        }

        weight_layer_matrix.push_back(weights_of_c_neuron);

        if(j == (int) topology[i]) {
            weight_cube.push_back(weight_layer_matrix);
            weight_layer_matrix.clear();
            j = -1;
            ++i;
        }
    }
    in_file.close();

    net = new Net(topology, weight_cube);
    return true;
}

//Validate from a confidence list of all pattern (SIGNs) the most likely pattern
//Compute quality score as the highest confidence subtracted by any other observed positive confidences
SIGN CPatternRecognitionMngr::validateSign(vector<double> &confidenceArray ,float *quality_score) {
    int c_max = NONE;
    float c_max_confidence;

    c_max_confidence = 0;
    *quality_score = 0;
    for(unsigned int i = 0; i < confidenceArray.size(); ++i) {
        if(debugMode) {
            CDebugHelper::print(sign_names[i] + ": " + to_string(confidenceArray[i]));
        }
        if(confidenceArray[i] > c_max_confidence) {
            *quality_score -= c_max_confidence; //Subtract the prev confidence as it's a non max positive value
            c_max = i;
            c_max_confidence = (float) confidenceArray[i];
        } else if(confidenceArray[i] > 0) {
            *quality_score -= confidenceArray[i];
        }
    }

    *quality_score += c_max_confidence;

    this->prompt = to_string(c_max) + string(" [") + to_string(*quality_score) + string("]");

	if(*quality_score < -0.2f) {
		return NONE;
	}
    return (SIGN) c_max;
}

//TODO Refactor
bool CPatternRecognitionMngr::fetchSelectPoints(vector<Vector2f> &pointsDrawn, vector<Vector2f> &pointsSelected,
                                               vector<Vector2f> &pointDirectionVectors) {
    pointsSelected.clear();
    pointDirectionVectors.clear();

    //netResolution = 12;
    int n = (int) pointsDrawn.size();

    if(n < 300) { //Smooth out points iff too little
        vector<Vector2f> pointsExtended;
        for (int i = 0; i < n - 1; ++i) {
            pointsExtended.push_back(pointsDrawn[i]);
            Vector2f intermediate_coordinate = (pointsDrawn[i] + pointsDrawn[i + 1])/ 2.f;
            pointsExtended.push_back(intermediate_coordinate);
        }
        pointsExtended.push_back(pointsDrawn.back());
        return fetchSelectPoints(pointsExtended, pointsSelected, pointDirectionVectors);
    }

    float selection_distance = n / (float) netResolution;

    //select representative points equally distribute among the drawn ones
    for (int i = 0; i < netResolution; ++i) {
        pointsSelected.push_back(pointsDrawn[i * (int) selection_distance]);
    }
    pointsSelected.push_back(pointsDrawn.back());

    //compute vectors between selected points
    for(unsigned int i = 0; i < pointsSelected.size() - 1; ++i) {
        Vector2f direction = pointsSelected[i + 1] - pointsSelected[i];
        float length = (float) (sqrt(direction.x * direction.x + direction.y * direction.y) >= 0.001 ? sqrt(direction.x * direction.x + direction.y * direction.y) : 0.001f);
        pointDirectionVectors.push_back(direction / length);
    }

    return (int) pointDirectionVectors.size() == netResolution;

}

void CPatternRecognitionMngr::convertVectorToNetInput(vector<Vector2f> &pointDirectionVectors, vector<double> &netInput) {
    netInput.clear();

    for(Vector2f v : pointDirectionVectors) {
        netInput.push_back(v.x);
        netInput.push_back(v.y);
    }
}

static Vector2f vectorAbs(Vector2f vec) {
    return Vector2f(abs(vec.x), abs(vec.y));
}

float CPatternRecognitionMngr::kernelDensity(Vector2f smoothingPoint, float bandwidth, vector<Vector2f> dataPoints) {

    int n = (int) dataPoints.size();

    float densitySum = 0;
    for(Vector2f point : dataPoints) {
        densitySum += kernel(vectorAbs(smoothingPoint - point) / bandwidth);
    }

    cout << "densitySum " << densitySum << endl;



    return  1.f / (n * bandwidth * bandwidth) * densitySum;
}

float CPatternRecognitionMngr::kernel(Vector2f u) {
    cout << "u : " << u.x << " " << u.y << endl;
    cout << "weigths : " << pow(2 * 3.14159f, -0.5f) * exp(-0.5f * u.x * u.x) << " * " << pow(2 * 3.14159f, -0.5f) * exp(-0.5f * u.y * u.y) << endl;

    return pow(2 * 3.14159f, -0.5f) * exp(-0.5f * u.x * u.x) * pow(2 * 3.14159f, -0.5f) * exp(-0.5f * u.y * u.y);
}

