#include "CGui.hpp"

CIconList* CGui::skillIconList;
CPlayerDisplayBar* CGui::healthBar;
CPlayerDisplayBar* CGui::energyBar;
CPlayer* CGui::localPlayer = nullptr;
CIconTextBox* CGui::reloadStatusBox;
vector<CIconTextBox*> CGui::availableSkillBoxes;
vector<CIconTextBox*> CGui::kdaStatisticsBoxes;
CIcon* CGui::reloadStatusIcon = nullptr;
float CGui::informativeIconSize;
float CGui::textSize;


void CGui::setupLocalPlayerGui(CPlayer* newLocalPlayer) {
	localPlayer = newLocalPlayer;
	informativeIconSize = CGuiManager::getRelativeToGuiSizeX(0.04f);
	textSize = 18;

	// Health and Energy Bar
	healthBar = new CPlayerDisplayBar(localPlayer->getMaxHealth(), localPlayer->getCharacterClass()->getBaseColor(), (int) CGuiManager::getRelativeToGuiSizeY(0.10f));
	energyBar = new CPlayerDisplayBar(localPlayer->getEnergyMaximum(), localPlayer->getCharacterClass()->getAccentColor(), (int) CGuiManager::getRelativeToGuiSizeY(0.05f));
	
	healthBar->setSkew(1.125f);

	// Icon List
 	float listIconSize = CGuiManager::getRelativeToGuiSizeX(0.08f);
	skillIconList = new CIconList(CGuiManager::getRelativeToGuiSize(Vector2f(0.9f, 0.1f)), Vector2f(listIconSize, listIconSize));
	skillIconList->setColor(localPlayer->getCharacterClass()->getBaseColor());
	CGuiManager::addGuiElement(skillIconList);


	//Available Skills Icon and Text Display
	vector<pair<SIGN,string> > skillProfiles = localPlayer->getCharacterClass()->getSkillDisplayProfiles();
	for(int i = 0; i < skillProfiles.size(); ++i) {
		CIconTextBox *skillTextBox = new CIconTextBox(CGuiManager::getRelativeToGuiSize(Vector2f(0.025f, 0.05f + (0.075f * i))), CIcon::getIconPath(skillProfiles[i].first), skillProfiles[i].second, localPlayer->getCharacterClass()->getAccentColor(), informativeIconSize, textSize);
		availableSkillBoxes.push_back(skillTextBox);
	}

	//KDA Icon and Text Display
	CIconTextBox *killsTextBox = new CIconTextBox(CGuiManager::getRelativeToGuiSize(Vector2f(0.96f, 0.85f)), "targetIcon", "0", localPlayer->getCharacterClass()->getAccentColor(), informativeIconSize * 0.5f, textSize);
	CIconTextBox *assistsTextBox = new CIconTextBox(CGuiManager::getRelativeToGuiSize(Vector2f(0.96f, 0.9f)), "anarchyIcon", "0", localPlayer->getCharacterClass()->getAccentColor(), informativeIconSize * 0.5f, textSize);
	CIconTextBox *deathTextBox = new CIconTextBox(CGuiManager::getRelativeToGuiSize(Vector2f(0.96f, 0.95f)), "skullIcon", "0", localPlayer->getCharacterClass()->getAccentColor(), informativeIconSize * 0.5f, textSize);
	kdaStatisticsBoxes.push_back(killsTextBox);
	kdaStatisticsBoxes.push_back(assistsTextBox);
	kdaStatisticsBoxes.push_back(deathTextBox);


	// Reload Icon and Text Display
	reloadStatusBox = new CIconTextBox(CGuiManager::getRelativeToGuiSize(Vector2f(0.05f, 0.95f)), "recycleIcon", "Invisible", localPlayer->getCharacterClass()->getAccentColor(), informativeIconSize, textSize);
	reloadStatusBox->setIsVisibile(false);
	
	reloadStatusIcon = nullptr;
}

CIconList* CGui::getSkillIconList() {
	return skillIconList;
}

CPlayerDisplayBar* CGui::getHealthBar() {
	return healthBar;
}

CPlayerDisplayBar* CGui::getEnergyBar() {
	return energyBar;
}

void CGui::updateLocalPlayerCharacterClass() {
	if(localPlayer != nullptr) {
		healthBar->setColor(localPlayer->getCharacterClass()->getBaseColor());
		energyBar->setColor(localPlayer->getCharacterClass()->getAccentColor());
		skillIconList->setColor(localPlayer->getCharacterClass()->getBaseColor());
		reloadStatusBox->setColor(localPlayer->getCharacterClass()->getAccentColor());

		for(CIconTextBox* skillBox : availableSkillBoxes) {
			delete (skillBox); //supposedly safe
		}
		availableSkillBoxes.clear();

		vector<pair<SIGN, string> > skillProfiles = localPlayer->getCharacterClass()->getSkillDisplayProfiles();
		for(int i = 0; i < skillProfiles.size(); ++i) {
			CIconTextBox* skillTextBox = new CIconTextBox(
					CGuiManager::getRelativeToGuiSize(Vector2f(0.025f, 0.05f + (0.075f * i))),
					CIcon::getIconPath(skillProfiles[i].first), skillProfiles[i].second,
					localPlayer->getCharacterClass()->getAccentColor(), informativeIconSize, textSize);
			availableSkillBoxes.push_back(skillTextBox);
		}

		for(CIconTextBox* box : kdaStatisticsBoxes) {
			box->setColor(localPlayer->getCharacterClass()->getAccentColor());
		}
	}
}

void CGui::updateLocalPlayerReloadStatus(CReloadStatus reloadStatus) {
	if(reloadStatus == CReloadStatus::NONE) {
		reloadStatusBox->setIsVisibile(false);
	} else if(reloadStatus == CReloadStatus::RELOADING) {
		reloadStatusBox->changeIcon("recycleIcon");
		reloadStatusBox->updateText("Reloading");
		reloadStatusBox->setIsVisibile(true);
		reloadStatusBox->setComponentLifetime(localPlayer->getCharacterClass()->getPrimarySkill()->getCooldown());
	} else if(reloadStatus == CReloadStatus::QUICKDRAW) {
		reloadStatusBox->changeIcon("quickDrawIcon");
		reloadStatusBox->updateText("Quickdraw");
		reloadStatusBox->setIsVisibile(true);
		reloadStatusBox->setComponentLifetime(600);		
	}

	updateReloadStatusIcon(reloadStatus);
}

void CGui::updateReloadStatusIcon(CReloadStatus reloadStatus) {
	CGuiManager::removeGuiElement(reloadStatusIcon);
	if(reloadStatusIcon != nullptr) {
		delete reloadStatusIcon;
		reloadStatusIcon = nullptr;
	}

	if(reloadStatus == CReloadStatus::RELOADING || reloadStatus == CReloadStatus::QUICKDRAW) {
		float iconSize = CGuiManager::getRelativeToGuiSizeX(0.04f);
		std::string iconName = (reloadStatus == CReloadStatus::RELOADING) ? "recycleIcon" : "quickDrawIcon";

		reloadStatusIcon = new CIcon(iconName, CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.4f)), Vector2f(iconSize, iconSize));
		reloadStatusIcon->setColor(localPlayer->getCharacterClass()->getAccentColor());
		reloadStatusIcon->setLifetime(600);
		reloadStatusIcon->setKeepAfterFadeout(true);
		CGuiManager::addGuiElement(reloadStatusIcon);
	}	
}

void CGui::updateKDA() {
	kdaStatisticsBoxes[0]->updateText(to_string(localPlayer->getKills()));
	kdaStatisticsBoxes[1]->updateText(to_string(localPlayer->getAssists()));
	kdaStatisticsBoxes[2]->updateText(to_string(localPlayer->getDeaths()));
}

void CGui::updatePlayerGuiVisibility() {
	bool isVisible = localPlayer->getIsAlive();
	
	healthBar->setIsVisible(isVisible);
	energyBar->setIsVisible(isVisible);

	skillIconList->setIsVisible(isVisible);
	reloadStatusBox->setIsVisibile(isVisible);
	
	for(CIconTextBox* skillBox : availableSkillBoxes) {
		skillBox->setIsVisibile(isVisible);
	}
	for(CIconTextBox* kdaStatisticBox : kdaStatisticsBoxes) {
		kdaStatisticBox->setIsVisibile(isVisible);
	}

	updateReloadStatusIcon(CReloadStatus::NONE);
}
