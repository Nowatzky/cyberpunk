#include "CNavMesh.hpp"
#include "CRandom.hpp"
#include "CNavRectangle.hpp"
#include "CNavMeshPathfindingInformation.hpp"
#include "CNetworkManager.hpp"
#include <map>
#include <queue>
#include <vector>


CNavMesh::CNavMesh(vector<CRectangleArea> roomAreas) {
	for(CRectangleArea roomArea : roomAreas) {
		polygons.push_back(new CNavRectangle(roomArea));
	}

	for(vector<CNavPolygon*>::iterator it1 = polygons.begin(); it1 != polygons.end(); ++it1) {
		for(vector<CNavPolygon*>::iterator it2 = it1 + 1; it2 != polygons.end(); ++it2) {
			(*it1)->connectTo(*it2, true);
		}
	}
	
	for(CNavPolygon* polygon : polygons) {
		for(CNavPolygonEdge* edge : polygon->getAdjacent()) {
			if(edge->target == polygon) {
				cout << "Edge target is polygon itself!" << endl;
			}
		}
	}
	
	updateRoomAreaCovered();
	updateProbabilityTable();
}

CNavMesh::~CNavMesh() {
	for(CNavPolygon* polygon : polygons) {
		delete polygon;
	}
}

void CNavMesh::updateRoomAreaCovered() {
	float maxX = -99999;
	float minX = +99999;
	float maxY = -99999;
	float minY = +99999;

	for(CNavPolygon* polygon : polygons) {
		CNavPolygonExtend extend = polygon->getExtend();
		maxX = max(maxX, extend.maxX);
		minX = min(minX, extend.minX);
		maxY = max(maxY, extend.maxY);
		minY = min(minY, extend.minY);
	}

	roomAreaCovered.offset = Vector2f(minX, minY);
	roomAreaCovered.size = Vector2f(maxX - minX, maxY - minY);
}

void CNavMesh::updateProbabilityTable() {
	probabilityTable.clear();
	float totalArea = 0;

	for(CNavPolygon* polygon : polygons) {
		totalArea += polygon->getAreaSize();
	}

	for(CNavPolygon* polygon : polygons) {
		probabilityTable.push_back(polygon->getAreaSize() / totalArea);	
	}	
}

bool CNavMesh::isInside(Vector2f pos) const {
	if(!roomAreaCovered.isPointInside(pos)) {
		return false;
	}

	for(CNavPolygon* polygon : polygons) {
		if(polygon->isInside(pos)) {
			return true;
		}
	}

	return false;
}

bool CNavMesh::isInside(Vector3f pos) const {
	return isInside(CEngine::to2D(pos));
}

Vector2f CNavMesh::getRandomPointInside() const {
	return polygons.at((unsigned long) CRandom::getChoiceFromBackground(probabilityTable))->getRandomPointInside();
}

void CNavMesh::addDebugVisuals() {
	hasDebugVisuals = true;
	
	for(CNavPolygon* polygon : polygons) {
		polygon->addDebugVisuals();
	}
}

void CNavMesh::removeDebugVisuals() {
	hasDebugVisuals = false;
	
	for(CNavPolygon* polygon : polygons) {
		polygon->removeDebugVisuals();
	}
}

CNavPath* CNavMesh::findPath(Vector2f start, Vector2f target, float agentHalfWidth) const {
	CNavPath* path;
	
	CNavPolygon* startPolygon = getPolygonAt(start);
	CNavPolygon* targetPolygon = getPolygonAt(target);

	if(startPolygon == nullptr || targetPolygon == nullptr) {
		cout << "No path found: start or target not withing navMesh!" << endl;
		return new CNavPath(this);
	}

	if(startPolygon == targetPolygon || isLineOfSightFree(start, target, agentHalfWidth)) {
		path = new CNavPath(this);
		path->prependWaypoint(target);
		return path;
	}
		
	PathfindingInformationMap exploredSet;
	PriorityPathfindingInformationSet openSet;
	
	exploredSet.insert(make_pair(start, CNavMeshPathfindingInformation(start, startPolygon)));
	exploredSet.at(start).g = 0;
	
	openSet.push(&exploredSet.at(start));
		
	while(!openSet.empty()) {
		CNavMeshPathfindingInformation* current = openSet.top();
		openSet.pop();

		// Finished
		if(current->pos == target) {
			path = new CNavPath(this);
			path->prependWaypoint(target);

			while(current->pos != start) {
				path->prependWaypoint(current->pos);
				current = current->prev;
			}
			
			path->smoothCompletely(start, agentHalfWidth);

			return path;
		}
		
		// Explore holes (from inward point)
		for(CNavPolygonEdge* edge : current->polygon->getAdjacent()) {
			// Skip if edge leads to prev polygon
			if(current->prev == nullptr || (current->prev != nullptr && edge->target != current->prev->polygon)) {
				for(CPolygonGatewayHole* hole : edge->holes) {
					CNavMeshPathfindingInformation* inwardHoleInfo = CNavMeshPathfindingInformation::getForPoint(
							&exploredSet, hole->inward, current->polygon);
					inwardHoleInfo->explore(&openSet, current, target, false);

					// Explore other side of hole too
					CNavMeshPathfindingInformation* outwardHoleInfo = CNavMeshPathfindingInformation::getForPoint(
							&exploredSet, hole->outward, edge->target);
					outwardHoleInfo->explore(&openSet, inwardHoleInfo, target, true);
				}
			}
		}
		
		// Explore target if possible
		if(current->polygon == targetPolygon) {
			CNavMeshPathfindingInformation* targetInfo = CNavMeshPathfindingInformation::getForPoint(&exploredSet, target, current->polygon);
			targetInfo->explore(&openSet, current, target, true);
		}		
	}

	cout << "No path found: no connection possible" << endl;
	return new CNavPath(this);
}

CNavPolygon* CNavMesh::getPolygonAt(Vector2f pos) const {
	for(CNavPolygon* polygon : polygons) {
		if(polygon->isInside(pos)) {
			return polygon;
		}
	}

	return nullptr;
}

CNavPolygon* CNavMesh::getPolygonAt(Vector3f pos) const {
	return getPolygonAt(CEngine::to2D(pos));
}

bool CNavMesh::isLineOfSightFree(Vector2f start, Vector2f end, float agentHalfWidth) const {
	if(agentHalfWidth == 0) {
		return isLineInsideMesh(start, end);
	}
	Vector2f dir = CEngine::normalize(end - start) * agentHalfWidth;
	Vector2f offsetLeft = CEngine::rotateZLeft90(dir);
	Vector2f offsetRight = CEngine::rotateZRight90(dir);
	
	return (isLineInsideMesh(start + dir + offsetLeft, end - dir + offsetLeft) && isLineInsideMesh(start + dir + offsetRight, end - dir + offsetRight));
}

bool CNavMesh::isLineInsideMesh(Vector2f start, Vector2f end) const {
	CNavPolygon* current = getPolygonAt(start);
	CNavPolygon* endPoly = getPolygonAt(end);
	if(current == nullptr || endPoly == nullptr) {
		return false;
	}
	
	CNavPolygon* prev = nullptr;
	
	while(current != endPoly) {
		bool foundGateway = false;
		for(CNavPolygonEdge* edge : current->getAdjacent()) {
			if(edge->target != prev) {
				if(CEngine::doLineSegmentsIntersect(edge->left, edge->right, start, end)) {
					foundGateway = true;
					prev = current;
					current = edge->target;
					break;
				}
			}
		}

		if(!foundGateway) {
			return false;
		}
	}

	return true;
}

void CNavMesh::blockArea(CRectangleArea rectangleArea) {
	if(CNetworkManager::isActive() && CNetworkManager::getRole() == CNetworkRole::PEER) {
		return;
	}
	
	// Assumes all polygons are rectangles
	vector<CNavPolygon*> affectedPolygons;
	
	for(CNavPolygon* polygon : polygons) {
		if(polygon->intersectsRectangleAreaExclusiveBorders(rectangleArea)) {
			affectedPolygons.push_back(polygon);
		}
	}

	for(CNavPolygon* polygon : affectedPolygons) {
		vector<CNavPolygon*> newPolygons = polygon->splitUp(rectangleArea);
		polygons.insert(polygons.end(), newPolygons.begin(), newPolygons.end());
		polygons.erase(remove(polygons.begin(), polygons.end(), polygon), polygons.end());
		delete polygon;		
	}
	
	updateProbabilityTable();
}

void CNavMesh::freeArea(CRectangleArea rectangleArea) {
	if(CNetworkManager::isActive() && CNetworkManager::getRole() == CNetworkRole::PEER) {
		return;
	}
	
	// Assumes all polygons are rectangles
	CRectangleArea polygonAreaToBeAdded = rectangleArea;
	
	vector<CNavPolygon*> affectedPolygons;
	
	for(CNavPolygon* polygon : polygons) {
		if(polygon->intersectsRectangleAreaExclusiveBorders(rectangleArea)) {
			cout << "Error: free area should not intersect existing polygons!" << endl;
		}

		if(polygon->intersectsRectangleArea(rectangleArea)) {
			affectedPolygons.push_back(polygon);
		}
	}
	
	// ToDo: Only handles case with 4 affectedPolygons, can be generalized
	if(affectedPolygons.size() == 4) {
		float size = rectangleArea.getAreaSize();
		CNavPolygonExtend combinedExtend = affectedPolygons.at(0)->getExtend();

		for(vector<CNavPolygon*>::iterator it = affectedPolygons.begin(); it != affectedPolygons.end(); ++it) {
			size += (*it)->getAreaSize();
			CNavPolygonExtend extend = (*it)->getExtend();
			
			combinedExtend.minX = min(combinedExtend.minX, extend.minX);
			combinedExtend.maxX = max(combinedExtend.maxX, extend.maxX);
			combinedExtend.minY = min(combinedExtend.minY, extend.minY);
			combinedExtend.maxY = max(combinedExtend.maxY, extend.maxY);
		}		
		
		float extendSize = (combinedExtend.maxX - combinedExtend.minX) * (combinedExtend.maxY - combinedExtend.minY);

		if(fabs(size - extendSize) < EPS) {
			for(vector<CNavPolygon*>::iterator it = affectedPolygons.begin(); it != affectedPolygons.end(); ++it) {
				polygons.erase(remove(polygons.begin(), polygons.end(), *it), polygons.end());
				delete *it;
			}
			polygonAreaToBeAdded = CRectangleArea(Vector2f(combinedExtend.minX, combinedExtend.minY),
			                                      Vector2f(combinedExtend.maxX - combinedExtend.minX, combinedExtend.maxY - combinedExtend.minY));
		}		
	}

	addPolygon(polygonAreaToBeAdded);
	
	updateProbabilityTable();
}

void CNavMesh::addPolygon(CRectangleArea rectangleArea) {
	CNavPolygon* newPolygon = new CNavRectangle(rectangleArea);

	for(vector<CNavPolygon*>::iterator it = polygons.begin(); it != polygons.end(); ++it) {
		newPolygon->connectTo(*it, true);
	}

	if(hasDebugVisuals) {
		newPolygon->addDebugVisuals();
	}

	polygons.push_back(newPolygon);
}
