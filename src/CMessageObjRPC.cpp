#include "CMessageObjRPC.hpp"

void CMessageObjRPC::serializeInto(sf::Packet& packet) {
	sf::Uint8 tmpRpcType = static_cast<sf::Uint8>(rpcType);
	packet << tmpRpcType;
	
	packet << objectId
           << gameTime;
	
	//std::size_t bufferSize = buffer.getDataSize();
	//packet << (sf::Uint8) (bufferSize);	
	//packet.append(buffer.getData(), bufferSize);

	std::size_t bufferSize = buffer.getDataSize();
	packet << (sf::Uint8) (bufferSize);
	
	for(unsigned i = 0; i < bufferSize; i++) {
		sf::Uint8 tmp = 0;
		buffer >> tmp;
		packet << tmp;
	}
}

void CMessageObjRPC::deserializeFrom(sf::Packet& packet) {
	sf::Uint8 tmpRpcType;
	packet >> tmpRpcType;
	rpcType = static_cast<CObjRPCType>(tmpRpcType);
	
	packet >> objectId
           >> gameTime;

	sf::Uint8 bufferSize;
	packet >> bufferSize;

	for(unsigned i = 0; i < bufferSize; i++) {
		sf::Uint8 tmp = 0;
		packet >> tmp;
		buffer << tmp;
	}
	
	//buffer.append(packet.getData(), bufferSize); // Get data return pointer to [0] which is WRONG
	// Also, data needs to be "deleted" (reading pos advanced).
}

void CMessageObjRPC::process() {
	CRpcHandler::receiveMessage(this);
}
