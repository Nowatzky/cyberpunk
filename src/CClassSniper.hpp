#ifndef CYBERPUNK_CCLASSSNIPER_HPP
#define CYBERPUNK_CCLASSSNIPER_HPP


#include "CCharacterClass.hpp"

class CClassSniper : public CCharacterClass {
public:
	CClassSniper(CPlayer *player);

	virtual CSkill* getSkill(SIGN sign);

	virtual Color getBaseColor();
	virtual Color getAccentColor();

};


#endif //CYBERPUNK_CCLASSSNIPER_HPP
