#ifndef CYBERPUNK_CSKILL_HPP
#define CYBERPUNK_CSKILL_HPP

#include <SFML/System.hpp>
#include "CClock.hpp"

using namespace sf;

class CPlayer;

class CSkill {
protected:
	CPlayer* owner;
	bool isButtonPressed = false;
	float cooldown;
	float energyConsumption;
	CClock clockCooldown;
	bool isReloading = false;
	
	CClock displayNoEnergyClock;

	CSkill(CPlayer* owner, float cooldown, float energyConsumption) :
		owner(owner),
		cooldown(cooldown),
		energyConsumption(energyConsumption),
	    clockCooldown(cooldown)	{
	}

public:
	virtual ~CSkill() {}
	
	bool updateButtonStatus(bool buttonStatus);
	
	virtual void tick(float frameTime) {};
	
	virtual void quickDraw() {};
	virtual void quickDrawRPC() {};
	virtual void reload() {};
	virtual void reloadRPC(float gameTime) {};
	
	float getCooldown() const;

protected:
	virtual void onButtonPress() {};
	virtual void onButtonRelease() {};

	virtual void displayNoEnergy();
};

#endif //CYBERPUNK_CSKILL_HPP