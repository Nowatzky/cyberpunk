#include <SFML/Graphics/Sprite.hpp>
#include <iostream>
#include "CIcon.hpp"
#include "CGuiManager.hpp"
#include "CRenderManager.hpp"
#include "CEngine.hpp"
#include "CTextureManager.hpp"

void CIcon::draw(RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(sprite);
	}
}

void CIcon::setImage(string source) {
	sprite.setTexture(CTextureManager::getTexture(source));
	sprite.setOrigin(sprite.getTexture()->getSize().x / 2.f, sprite.getTexture()->getSize().y / 2.f);
}

void CIcon::setPosition(Vector2f position) {
	this->position = position;
	sprite.setPosition(position);
}

CIcon::CIcon(string source, Vector2f position, Vector2f size) {
	setImage(source);
	setSize(size);
	setPosition(position);
}

void CIcon::setColor(Color color) {
	sprite.setColor(color);
}

void CIcon::setLifetime(float lifetime) {
	this->lifetime = lifetime;
	isLifetimeLimited = true;
	isFading = true;
	clockLifetime.restart();
}

void CIcon::tick(float frameTime) {
	if(isLifetimeLimited && clockLifetime.getElapsedTime().asMilliseconds() > lifetime) {
		CGuiManager::removeGuiElement(this);

		if(!keepAfterFadeout) {
			delete this;
		}		
		return;
	}

	if(isFading && clockLifetime.hasPassed(lifetime * fadeAfter)) {
		Color color = sprite.getColor();
		float alpha = (clockLifetime.getTimeMs() - lifetime * fadeAfter)/(lifetime - lifetime * fadeAfter);
		color.a = (Uint8) CEngine::lerp(255, 50, alpha);
		sprite.setColor(color);
	}
}

void CIcon::setSize(Vector2f size) {
	this->size = size;
	sprite.setScale(size.x/sprite.getTexture()->getSize().x, size.y/sprite.getTexture()->getSize().y);
}

string CIcon::getIconPath(SIGN sign) {
	switch (sign) {
		case ALPHA:
			return "IconALPHA";
		case HLINE_FROMLEFT:
			return "IconHLINE";
		case HLINE_FROMRIGHT:
			return "IconHLINE";
		case VLINE_DOWN:
			return "IconVLINE";
		case VLINE_UP:
			return "IconVLINE";
		case WEDGE_UP:
			return "IconWEDGE_UP";
		case WEDGE_DOWN:
			return "IconWEDGE_DOWN";
		case GAMMA:
			return "IconGAMMA";
		case CIRCLE:
			return "IconCIRCLE";
		case FLASH:
			return "IconFLASH";
		case EPSILON:
			return "IconEPSILON";
		case BETA:
			return "IconBETA";
		case NONE:
			break;
	}
	return "sign texture not defined";
}

void CIcon::setKeepAfterFadeout(bool keepAfterFadeout) {
	this->keepAfterFadeout = keepAfterFadeout;
}

