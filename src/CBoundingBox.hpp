#ifndef CYBERPUNK_CBOUNDINGBOX_HPP
#define CYBERPUNK_CBOUNDINGBOX_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "CEngine.hpp"
#include "CCollisionContainer.hpp"

using namespace sf;

class CBoundingBox : public CCollisionContainer {
protected:
	Vector3f dim;
	c2Poly poly;
	float maxDimSquared;

public:
	CBoundingBox(Vector3f pos, Vector3f dim);
	CBoundingBox(Vector3f pos, Vector3f dim, float rot);

	/* Intersection */
	bool doesIntersect(const CCollisionContainer& other) const;

	const c2Poly* getPoly() const;

	/* Accessors */
	void setPosition(Vector3f pos);
	void setRotation(float rot);
	void setDimension(Vector3f dim);

	void move(Vector3f movement);
	void move(Vector2f movement);
	void rotate(float angle);
	void scale(float factor);

	Vector3f getDimension() const;
	
	float getMaxDimSquared() const;

private:
	void updatePoly();
	void updateMaxDimSquared();

};

#endif //CYBERPUNK_CBOUNDINGBOX_HPP
