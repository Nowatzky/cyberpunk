#include "CNavAgent.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CEnemy.hpp"
#include "CUtilityDefines.hpp"

CNavAgent::CNavAgent(CEnemy* host) :
	host(host) {
	if(dynamic_cast<CBoundingBox*>(host->getCollisionContainer()) != nullptr) {
		agentHalfWidth = dynamic_cast<CBoundingBox*>(host->getCollisionContainer())->getDimension().x;
	} else {
		agentHalfWidth = 0;
	}
}

void CNavAgent::findPathTo(Vector2f target) {
	if(currentPath != nullptr) {
		delete currentPath;
	}
	currentPath = CSceneManager::getMap()->findPath(CEngine::to2D(host->getPosition()), target, agentHalfWidth);
	lastPathReconstructionClock.restart();
}

void CNavAgent::setPath(CNavPath* path) {
	if(currentPath != nullptr) {
		delete currentPath;
	}
	currentPath = path;
	lastPathReconstructionClock.restart();
}

void CNavAgent::followPath(float frameTime) {
	Vector2f currentWaypoint = getCurrentWaypoint();
	
	CTurningEnemyMovementProfile movementProfile = host->getTurningEnemyMovementProfile();
	float misalignedSlowdownFactor;
	Vector2f movementDirection;
	
	if(movementProfile.isTurningEnemy) {
		float angle = CEngine::getAngleToTarget(host, currentWaypoint);
		float sign = sgn(angle);
		angle = abs(angle);
		float turningAngle = frameTime * CEngine::clamp(angle * movementProfile.turningSpeedFactor, 0.f, movementProfile.maxTurningSpeed);
		turningAngle = (turningAngle <= angle) ? turningAngle : angle;
		
		host->rotate(turningAngle * sign);
		
		float clampedRemainingAngle = CEngine::clamp(angle - turningAngle, -90, 90);		
		misalignedSlowdownFactor = CEngine::lerp(movementProfile.slowdownFactorAtMaxMisalignment, 1.f, cos(clampedRemainingAngle * TO_RAD));		
		movementDirection = CEngine::getUnitInDirection(host->getRotation());
	} else {
		misalignedSlowdownFactor = 1.f;
		movementDirection = CEngine::normalize(currentWaypoint - CEngine::to2D(host->getPosition()));
	}

	host->move(movementDirection * misalignedSlowdownFactor * frameTime, true);
}

bool CNavAgent::needsPathReconstruction() {
	return lastPathReconstructionClock.getElapsedTime().asMilliseconds() >= 1000;
}

void CNavAgent::updatePath(Vector2f target) {
	if(needsPathReconstruction()) {
		findPathTo(target);
	} else {
		if(lastPathUpdate.getElapsedTime().asMilliseconds() > 1000.f/20.f) {
			lastPathUpdate.restart();
			
			bool targetUpdated = currentPath->updateTarget(CEngine::to2D(host->getPosition()), target, agentHalfWidth);
			
			if(targetUpdated) {
				currentPath->smoothFromStart(CEngine::to2D(host->getPosition()), agentHalfWidth);			
			} else {
				cout << "triggered new path" << endl;
				findPathTo(target);
			}
		}
	}
}

void CNavAgent::updatePath() {
	currentPath->smoothFromStart(CEngine::to2D(host->getPosition()), agentHalfWidth);
}

float CNavAgent::getAgentHalfWidth() const {
	return agentHalfWidth;
}

bool CNavAgent::hasReachedTarget() const {
	if(currentPath != nullptr) {
		return currentPath->getLength(CEngine::to2D(host->getPosition())) <= agentHalfWidth;
	}

	return true; // So findPath will be triggered
}

Vector2f CNavAgent::getCurrentWaypoint() {
	Vector2f currentWaypoint;
	
	bool foundFirstWaypoint = false;
	while(!foundFirstWaypoint) {
		currentWaypoint = currentPath->getFirstWaypoint();

		if(CEngine::getDistance(currentWaypoint, CEngine::to2D(host->getPosition())) <= agentHalfWidth) {
			currentPath->removeFirstWaypoint();
		} else {
			foundFirstWaypoint = true;
		}		
	}

	return currentWaypoint;	
}

bool CNavAgent::isStuck(float frameTime) {
	float dist = CEngine::getDistance(lastPosition, host->getPosition());
	lastPosition = host->getPosition();

	if(dist < host->getMovementSpeed() * frameTime * host->getTurningEnemyMovementProfile().slowdownFactorAtMaxMisalignment) {
		if(++isStuckFramesCount >= maxIsStuckFramesCount) {
			isStuckFramesCount = 0;
			return true;
		} 
	} else {
		isStuckFramesCount = 0;
	}

	return false;
}

void CNavAgent::addDebugVisuals() {
	currentPath->addDebugVisuals(CEngine::to2D(host->getPosition()));
}

void CNavAgent::removeDebugVisuals() {
	currentPath->removeDebugVisuals();
}
