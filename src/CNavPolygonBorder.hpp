#ifndef CYBERPUNK_CNAVPOLYGONBORDER_HPP
#define CYBERPUNK_CNAVPOLYGONBORDER_HPP

#include <SFML/System.hpp>

using namespace sf;

class CNavPolygonBorder final {
private:
	// Borders are assumed to be directing out of polygon!
	Vector2f left;
	Vector2f right;

public:
	CNavPolygonBorder() {}

	CNavPolygonBorder(Vector2f left, Vector2f right) :
			left(left),
			right(right) {}

	bool isOpposingParallelAndIntersecting(CNavPolygonBorder other) const;

	Vector2f getLeft() const;
	Vector2f getRight() const;

	bool operator==(const CNavPolygonBorder& other) const;
	bool operator!=(const CNavPolygonBorder& other) const;
	
private:
	bool opposes(CNavPolygonBorder other) const;

};


#endif //CYBERPUNK_CNAVPOLYGONBORDER_HPP
