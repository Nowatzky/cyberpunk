#ifndef CYBERPUNK_CMESSAGEOBJTCREATEPLAYER_HPP
#define CYBERPUNK_CMESSAGEOBJTCREATEPLAYER_HPP

#include "CMessage.hpp"
#include "CObject.hpp"
#include "CCharacterClass.hpp"

class CMessageObjCreatePlayer : public CMessage {
public:
	CObjectId objectId;
	sf::Vector3f pos;
	float rot;
	CClassName className;

	CMessageObjCreatePlayer() :
			CMessage(CMessageType::OBJ_CREATE_PLAYER) {}

	virtual void serializeInto(sf::Packet& packet);
	virtual void deserializeFrom(sf::Packet& packet);
	virtual void process();

};


#endif //CYBERPUNK_CMESSAGEOBJTCREATEPLAYER_HPP
