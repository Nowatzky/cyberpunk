#include "CRpcHandler.hpp"
#include "CNetworkManager.hpp"
#include "CGameClock.hpp"
#include "CSceneManager.hpp"
#include "CMessageObjRPC.hpp"
#include "CEnemyGhost.hpp"
#include "CSerializationFunctions.hpp"
#include "CSkillPrimaryLaserFire.hpp"
#include "CSkillShotGunFire.hpp"
#include "CSkillForceSweep.hpp"
#include "CProjectileLaserBullet.hpp"
#include "CSkillChainFlash.hpp"
#include "CEffectChainFlash.hpp"
#include "CSkillCreateMapBlock.hpp"
#include "CSkillHackSignal.hpp"
#include "CSkillSnipeShot.hpp"
#include "CTempSpawnPoint.hpp"

CMessageObjRPC* CRpcHandler::createMessage(CObject* caller, CObjRPCType rpcType) {
	CMessageObjRPC* message = static_cast<CMessageObjRPC*>(CNetworkManager::createMessage(CMessageType::OBJ_RPC));
	message->rpcType = rpcType;
	message->objectId = caller->getId();
	message->gameTime = CGameClock::getGameTime();
	return message;
}

void CRpcHandler::receiveMessage(CMessageObjRPC* message) {
	CObject* caller = CSceneManager::getObject(message->objectId);

	float gameTime = message->gameTime;
	bool callerExists = true;

	if(caller == nullptr) {
		callerExists = false;
		std::cout << "Got RPC for id " << message->objectId << " which does not exist! RpcType: " << std::to_string((sf::Uint8) message->rpcType) << endl;
	}
	
	switch(message->rpcType) {
		case CObjRPCType::CHARACTER_DIE: {
			if(callerExists) {
				CEnemy* enemy = dynamic_cast<CEnemy*>(caller);
				if(enemy != nullptr) { 
					enemy->die();					
				} else {
					CPlayer* player = dynamic_cast<CPlayer*>(caller);
					if(player != nullptr) {
						player->die();
						
						cout << "Player death message received" << endl;
					}	
				}
			}
			break;
		}
		case CObjRPCType::PLAYER_RELOAD: {
			if(callerExists) {
				CPlayer* player = dynamic_cast<CPlayer*>(caller);
				player->getCharacterClass()->getPrimarySkill()->reloadRPC(gameTime);
			}
			break;
		}
		case CObjRPCType::PLAYER_QUICKDRAW: {
			if(callerExists) {
				CPlayer* player = dynamic_cast<CPlayer*>(caller);
				player->getCharacterClass()->getPrimarySkill()->quickDrawRPC();
			}

			break;
		}
		case CObjRPCType::PLAYER_MEDIATOR_PRIMARY_FIRE: {
			Vector2f dir;
			Vector3f originalPosition;
			std::vector<CObjectId> projectileIds;

			message->buffer >> dir
			                >> originalPosition
			                >> projectileIds;
			
			if(callerExists) {
				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillPrimaryLaserFire* skill = dynamic_cast<CSkillPrimaryLaserFire*>(player->getCharacterClass()->getPrimarySkill());
				if(skill != nullptr) {
					skill->fireRPC(gameTime, originalPosition, dir, projectileIds);
				} else {
					std::cout << "Got MEDIATOR_PRIMARY_FIRE but skill did not exist" << endl;
				}
			}
			break;
		}
		case CObjRPCType::PLAYER_SHOTGUNNER_PRIMARY_FIRE: {
			Vector2f dir;
			std::vector<CSkillShotGunFireHitResult> hitResults;
			std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxes;

			message->buffer >> dir
			                >> hitResults
			                >> popupBoxes;
			
			if(callerExists) {
				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillShotGunFire* skill = dynamic_cast<CSkillShotGunFire*>(player->getCharacterClass()->getPrimarySkill());
				if(skill != nullptr) {
					skill->fireRPC(dir, hitResults, popupBoxes);
				} else {
					std::cout << "Got SHOTGUNNER_PRIMARY_FIRE but skill did not exist" << endl;
				}
			}
			break;
		}
		case CObjRPCType::PLAYER_SNIPER_PRIMARY_FIRE: {
			Vector3f hitPosition;
			bool doesHitTarget;
			CObjectId targetId;
			float dmg;

			message->buffer >> hitPosition
			                >> doesHitTarget
			                >> targetId
							>> dmg;

			if(callerExists) {
				CEnemy* target = dynamic_cast<CEnemy*>(CSceneManager::getObject(targetId));

				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillSnipeShot* skill = dynamic_cast<CSkillSnipeShot*>(player->getCharacterClass()->getPrimarySkill());
				if(skill != nullptr) {
					skill->fireRPC(hitPosition, doesHitTarget, target, dmg);
				} else {
					std::cout << "Got SNIPER_PRIMARY_FIRE but skill did not exist" << endl;
				}
			}
			break;
		}
		case CObjRPCType::PLAYER_SKILL_FORCE_SWEEP: {
			bool executedSkill = false;

			Vector2f dir;
			Vector3f originalPosition;
			message->buffer >> dir
			                >> originalPosition;

			if(callerExists) {
				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillForceSweep* skill = dynamic_cast<CSkillForceSweep*>(player->getCharacterClass()->getActiveSkills().at(0));
				if(skill != nullptr) {
					skill->fireRPC(gameTime, originalPosition, dir);
					executedSkill = true;
				} 
			}

			if(!executedSkill) {
				std::cout << "Got SKILL_FORCE_SWEEP but skill did not exist. Fallback cast instead!" << endl;
				CSkillForceSweep::fireFallback(originalPosition, dir);
			}
			
			break;
		}
		case CObjRPCType::PLAYER_SKILL_CHAIN_FLASH: {
			bool executedSkill = false;
			
			std::vector<CObjectId> targets;
			message->buffer >> targets;

			if(callerExists) {
				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillChainFlash* skill = dynamic_cast<CSkillChainFlash*>(player->getCharacterClass()->getActiveSkills().at(3));
				if(skill != nullptr) {
					skill->fireRPC(targets);
					executedSkill = true;
				}
			}

			if(!executedSkill) {
				std::cout << "Got SKILL_CHAIN_FLASH but skill did not exist. Fallback cast instead!" << endl;
				CSkillChainFlash::fireFallback(targets);
			}
			
			break;
		}
		case CObjRPCType::PLAYER_SKILL_CREATE_MAP_BLOCK: {
			bool executedSkill = false;
			
			Vector3f pos;
			message->buffer >> pos;

			if(callerExists) {
				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillCreateMapBlock* skill = dynamic_cast<CSkillCreateMapBlock*>(player->getCharacterClass()->getActiveSkills().at(2));
				if(skill != nullptr) {
					skill->spawnMapBlockRPC(pos);
				}
			}

			if(!executedSkill) {
				std::cout << "Got SKILL_CREATE_BLOCK but skill did not exist. Fallback cast instead!" << endl;
				CSkillCreateMapBlock::spawnMapBlockFallback(pos);
			}
			break;
		}
		case CObjRPCType::PLAYER_SKILL_HACK_SIGNAL: {
			Vector2f dir;
			Vector3f originalPosition;
			CObjectId projectileId;

			message->buffer >> dir
			                >> originalPosition
			                >> projectileId;

			if(callerExists) {
				CPlayer* player = static_cast<CPlayer*>(caller);
				CSkillHackSignal* skill = dynamic_cast<CSkillHackSignal*>(player->getCharacterClass()->getActiveSkills()[4]);
				if(skill != nullptr) {
					skill->fireRPC(gameTime, originalPosition, dir, projectileId);
				} else {
					std::cout << "Got Hack Skill message but skill did not exist" << endl;
				}
			}
			break;
		}
		case CObjRPCType::ENEMY_GHOST_EXPLODE: {
			std::vector<CObjectId> targetIds;
			float damage;
			message->buffer >> targetIds
			                >> damage;

			std::vector<CObject*> targets = CSceneManager::getObjectsRemoveNull(targetIds);

			if(callerExists) {
				CEnemyGhost* ghost = static_cast<CEnemyGhost*>(caller);
				ghost->explodeRPC(targets, damage);	
			} else {
				CEnemyGhost::explodeFallback(targets, damage);
			}
			
			break;
		}
		case CObjRPCType::ENEMY_GHOST_DASH: {
			if(callerExists) {
				CEnemyGhost* ghost = static_cast<CEnemyGhost*>(caller);
				ghost->dashRPC(gameTime);
			}
			break;
		}
		case CObjRPCType::PROJECTILE_LASER_BULLET_ON_HIT_ENEMY: {
			CObjectId targetId;
			bool addMark;
			Vector2f dir;

			message->buffer >> targetId
			                >> addMark
			                >> dir;

			CEnemy* target = dynamic_cast<CEnemy*>(CSceneManager::getObject(targetId));
			if(target != nullptr) {
				if(callerExists) {
					CProjectileLaserBullet* projectile = static_cast<CProjectileLaserBullet*>(caller);
					projectile->onHitEnemyRPC(target, addMark);					
				} else {
					CProjectileLaserBullet::onHitEnemyFallback(target, addMark, dir);					
				}
			}
			break;
		}
		case CObjRPCType::PROJECTILE_HACK_SIGNAL_ON_HIT_ENEMY: {
			CObjectId targetId;
			Vector2f dir;

			message->buffer >> targetId
			                >> dir;

			CEnemy* target = dynamic_cast<CEnemy*>(CSceneManager::getObject(targetId));
			if(target != nullptr) {
				if(callerExists) {
					CProjectileHackSignal* projectile = static_cast<CProjectileHackSignal*>(caller);
					projectile->onHitEnemyRPC(target);
				} else {
					//TODO fallback
					//CProjectileLaserBullet::onHitEnemyFallback(target, addMark, dir);
				}
			}
			break;
		}
		case CObjRPCType::EFFECT_CHAIN_FLASH_TRIGGER_FLASH: {
			std::vector<CObjectId> targetIds;
			message->buffer >> targetIds;
			
			std::vector<CObject*> targets = CSceneManager::getObjectsRemoveNull(targetIds);

			if(callerExists) {
				CCharacter* target = dynamic_cast<CCharacter*>(caller);
				CEffectChainFlash* effect = dynamic_cast<CEffectChainFlash*>(target->getEffectManager()->getEffectWithId("CEffectChainFlash"));

				if(effect != nullptr) {
					effect->triggerFlashRPC(targets);
				} else{
					CEffectChainFlash::triggerFlashFallback(targets);
				}
			}
			break;
		}
		case CObjRPCType::SPAWN_POINT_DELETE: {
			if(callerExists) {
				CTempSpawnPoint* spawnPoint = dynamic_cast<CTempSpawnPoint*>(caller);
				spawnPoint->deletePointRPC();				
			}
			break;
		}
	}	

	// message* gets deleted in NetworkManager	
}

void CRpcHandler::sendCharacterDie(CObject* caller) {
	if(!CNetworkManager::isActive()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::CHARACTER_DIE);
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerReload(CObject* caller) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_RELOAD);
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerQuickDraw(CObject* caller) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_QUICKDRAW);
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerMediatorPrimaryFire(CObject* caller, Vector2f dir, Vector3f originalPosition, std::vector<CObjectId> projectileIds) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}	
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_MEDIATOR_PRIMARY_FIRE);
	message->buffer << dir
                    << originalPosition
	                << projectileIds;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerShotgunnerPrimaryFire(CObject* caller, Vector2f dir,
                                                  std::vector<CSkillShotGunFireHitResult> hitResults,
                                                  std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxes) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_SHOTGUNNER_PRIMARY_FIRE);
	message->buffer << dir
	                << hitResults
	                << popupBoxes;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerSkillForceSweep(CObject* caller, Vector2f dir, Vector3f originalPosition) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_SKILL_FORCE_SWEEP);
	message->buffer << dir
	                << originalPosition;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerSkillChainFlash(CObject* caller, std::vector<CObjectId> targets) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_SKILL_CHAIN_FLASH);
	message->buffer << targets;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerSkillCreateMapBlock(CObject* caller, Vector3f pos) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_SKILL_CREATE_MAP_BLOCK);
	message->buffer << pos;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendEnemyGhostExplode(CObject* caller, std::vector<CObjectId> targets, float damage) {
	if(!CNetworkManager::isActive()) { // Everybody can send
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::ENEMY_GHOST_EXPLODE);
	message->buffer << targets
	                << damage;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendEnemyGhostDash(CObject* caller) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::ENEMY_GHOST_DASH);
	CNetworkManager::queueForBroadcast(message);
	}

void CRpcHandler::sendProjectileLaserBulletOnHitEnemy(CObject* caller, CObjectId target, bool addMark, Vector2f dir) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PROJECTILE_LASER_BULLET_ON_HIT_ENEMY);
	message->buffer << target
	                << addMark
	                << dir;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendEffectChainFlashTriggerFlash(CObject* caller, std::vector<CObjectId> targetIds) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::EFFECT_CHAIN_FLASH_TRIGGER_FLASH);
	message->buffer << targetIds;
	CNetworkManager::queueForBroadcast(message);
}

void CRpcHandler::sendPlayerSkillHackSignal(CObject *caller, Vector2f dir, Vector3f originalPosition,
                                            CObjectId projectileId) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PLAYER_SKILL_HACK_SIGNAL);
	message->buffer << dir
	                << originalPosition
	                << projectileId;
	CNetworkManager::queueForBroadcast(message);

}

void CRpcHandler::sendProjectileHackSignalOnHitEnemy(CObject *caller, CObjectId target, Vector2f dir) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::PROJECTILE_HACK_SIGNAL_ON_HIT_ENEMY);
	message->buffer << target
	                << dir;
	CNetworkManager::queueForBroadcast(message);
}

void
CRpcHandler::sendPlayerSniperPrimaryFire(CObject *caller, Vector3f hitPosition, bool doesHitTarget, CObjectId targetId,
                                         float dmg) {
	if (!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC *message = createMessage(caller, CObjRPCType::PLAYER_SNIPER_PRIMARY_FIRE);
	message->buffer << hitPosition
	                << doesHitTarget
	                << targetId
	                << dmg;
	CNetworkManager::queueForBroadcast(message);

}

void CRpcHandler::sendSpawnPointDelete(CObject* caller) {
	if(!CNetworkManager::isActive() || !caller->getIsLocal()) {
		return;
	}
	CMessageObjRPC* message = createMessage(caller, CObjRPCType::SPAWN_POINT_DELETE);
	CNetworkManager::queueForBroadcast(message);
}

