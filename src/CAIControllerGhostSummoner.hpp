#ifndef CYBERPUNK_CAICONTROLLERGHOSTSUMMONER_HPP
#define CYBERPUNK_CAICONTROLLERGHOSTSUMMONER_HPP

#include <SFML/System.hpp>
#include "CAIController.hpp"



using namespace sf;

class CEnemyGhostSummoner;

class CAIControllerGhostSummoner : public CAIController {
private:
	CEnemyGhostSummoner* host;

	void pickNewWanderTarget();
	
	Clock clockFindNewPath;
	float maxPathFollowTime = 4000;

public:
	CAIControllerGhostSummoner(CEnemyGhostSummoner* host) :
		host(host) {}

	virtual void tick(float frameTime);
};

#endif //CYBERPUNK_CAICONTROLLERGHOSTSUMMONER_HPP
