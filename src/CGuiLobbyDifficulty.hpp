#ifndef CYBERPUNK_CGUILOBBYDIFFICULTY_HPP
#define CYBERPUNK_CGUILOBBYDIFFICULTY_HPP


#include <SFML/Graphics/Text.hpp>
#include "CGuiElement.hpp"
#include "CDifficultyManager.hpp"

class CLobby;

class CGuiLobbyDifficulty : public CGuiElement {
private:
	CLobby* lobby;
	sf::Text text;

public:
	explicit CGuiLobbyDifficulty(CLobby* lobby);

	void draw(sf::RenderTarget* renderTarget) override;

	void tick(float frameTime) override;

	~CGuiLobbyDifficulty();


private:
	sf::Color getDifficultyColor(CDifficulty difficulty) const;
	unsigned int getDifficultyTextSize(CDifficulty difficulty) const;
	std::string getDifficultyText(CDifficulty difficulty) const;
	float getDifficultyRotation(CDifficulty difficulty) const;

};


#endif //CYBERPUNK_CGUILOBBYDIFFICULTY_HPP
