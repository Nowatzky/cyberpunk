#include "CSerializationFunctions.hpp"
#include "CSkillShotGunFire.hpp"

Packet& operator<<(Packet& packet, Vector2f& vec) {
	packet << vec.x
	       << vec.y;
	return packet;
}

Packet& operator>>(Packet& packet, Vector2f& vec) {
	packet >> vec.x
	       >> vec.y;
	return packet;
}

Packet& operator<<(Packet& packet, Vector3f& vec) {
	packet << vec.x
	       << vec.y
	       << vec.z;
	return packet;
}

Packet& operator>>(Packet& packet, Vector3f& vec) {
	packet >> vec.x
	       >> vec.y
	       >> vec.z;
	return packet;
}

Packet& operator<<(Packet& packet, std::vector<CObjectId>& vector) {
	sf::Uint8 size;
	size = static_cast<sf::Uint8>(vector.size());

	packet << size;
	for(unsigned i = 0; i < size; i++) {
		packet << vector[i];
	}

	return packet;
}

Packet& operator>>(Packet& packet, std::vector<CObjectId>& vector) {
	sf::Uint8 size;
	packet >> size;

	CObjectId id;
	for(unsigned i = 0; i < size; i++) {
		packet >> id;
		vector.push_back(id);
	}

	return packet;
}

Packet& operator<<(Packet& packet, std::vector<CSkillShotGunFireHitResult>& vector) {
	sf::Uint8 size;
	size = static_cast<sf::Uint8>(vector.size());

	packet << size;
	for(unsigned i = 0; i < size; i++) {
		packet << vector[i].dir
		       << vector[i].bulletCount
		       << vector[i].targetId;
	}

	return packet;
}

Packet& operator>>(Packet& packet, std::vector<CSkillShotGunFireHitResult>& vector) {
	sf::Uint8 size;
	packet >> size;

	CSkillShotGunFireHitResult hitResult;
	for(unsigned i = 0; i < size; i++) {
		packet >> hitResult.dir
		       >> hitResult.bulletCount
		       >> hitResult.targetId;
		vector.push_back(hitResult);
	}

	return packet;
}

Packet& operator<<(Packet& packet, std::vector<CSkillShotGunFirePopupBoxNetwork>& vector) {
	sf::Uint8 size;
	size = static_cast<sf::Uint8>(vector.size());

	packet << size;
	for(unsigned i = 0; i < size; i++) {
		packet << vector[i].target
		       << vector[i].relativePos;
	}

	return packet;
}

Packet& operator>>(Packet& packet, std::vector<CSkillShotGunFirePopupBoxNetwork>& vector) {
	sf::Uint8 size;
	packet >> size;

	CSkillShotGunFirePopupBoxNetwork popupBoxNetwork;
	for(unsigned i = 0; i < size; i++) {
		packet >> popupBoxNetwork.target
		       >> popupBoxNetwork.relativePos;
		vector.push_back(popupBoxNetwork);
	}

	return packet;
}
