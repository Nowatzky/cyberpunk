#ifndef CYBERPUNK_CGUI_HPP
#define CYBERPUNK_CGUI_HPP

#include "CIconList.hpp"
#include "CPlayer.hpp"
#include "CPlayerDisplayBar.hpp"
#include "CIconTextBox.hpp"

/*
 * Class listing all the major gui elements for static access
 */

enum class CReloadStatus {
	NONE,
	RELOADING,
	QUICKDRAW,
};

class CGui {
private:
	CGui() {}

	static CPlayer* localPlayer;

	static CPlayerDisplayBar* healthBar;
	static CPlayerDisplayBar* energyBar;

	static CIconList* skillIconList;
	static CIconTextBox* reloadStatusBox;
	static vector<CIconTextBox*> availableSkillBoxes;
	static vector<CIconTextBox*> kdaStatisticsBoxes;

	static CIcon* reloadStatusIcon;

	static float informativeIconSize;
	static float textSize;
	
public:
	static void setupLocalPlayerGui(CPlayer* newLocalPlayer);
	static void updateLocalPlayerCharacterClass();
	static void updateLocalPlayerReloadStatus(CReloadStatus reloadStatus);
	static void updateKDA();

	//Getter
	static CIconList* getSkillIconList();
	static CPlayerDisplayBar* getHealthBar();
	static CPlayerDisplayBar* getEnergyBar();
	
	static void updatePlayerGuiVisibility();

private:
	static void updateReloadStatusIcon(CReloadStatus reloadStatus);
};


#endif //CYBERPUNK_CGUI_HPP
