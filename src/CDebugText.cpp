#include "CDebugText.hpp"
#include "CGuiManager.hpp"

void CDebugText::draw(sf::RenderTarget* renderTarget) {
	renderTarget->draw(text);
}

sf::Text* CDebugText::getText() {
	return &text;
}

CDebugText::CDebugText() {
	text.setFont(CGuiManager::getFontDebug());
	text.setColor(Color::Red);
	text.setCharacterSize((unsigned int) (CGuiManager::getGuiFontSizeFactor() * 22));
}
