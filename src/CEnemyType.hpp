#ifndef CYBERPUNK_CENEMYTYPE_HPP
#define CYBERPUNK_CENEMYTYPE_HPP

#include <vector>

class CEnemy;

class CEnemyType {
public:
	enum ENEMY_INSTANCE {
		ENEMY_1, ENEMY_2, ENEMY_3, ENEMY_4, ENEMY_5
	};
	
	virtual ~CEnemyType() = default;

	std::vector<float> enemyCostList;
	std::vector<float> enemyPointList;
	std::vector<float> enemyIntensityList;

	virtual std::vector<float> getEnemySpawnProbabilities(int spawnAssemblyType) = 0;
	virtual float getEnemyMinCosts(std::vector<float> enemyTypeDistribution) = 0;
	virtual CEnemy* getEnemyObject(ENEMY_INSTANCE enemy_instance) = 0;

};


#endif //CYBERPUNK_CENEMYTYPE_HPP
