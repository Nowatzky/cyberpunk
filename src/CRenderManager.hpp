#ifndef CYBERPUNK_CRENDERMANAGER_HPP
#define CYBERPUNK_CRENDERMANAGER_HPP


#include <SFML/System.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>

using namespace sf;

class CRenderManager {
private:
	static View viewScene;
	static View viewGui;
	static RenderTexture textureScene;
	static RenderTexture textureGui;
	
	static Shader shaderGlow;
	
	static Sprite renderSprite;

	// Ripple effect
	static Vector2f ripplePos;
	static float rippleFac;


	// Private Constructor 
	CRenderManager() {}
public:
	static void initialize(VideoMode videoMode);
	static View* getViewScene();
	static View* getViewGui();
	static void updateView();
	
	static Vector2f convertSceneToGui(Vector2f pos);
	
	static void clear();
	static void drawGui();
	
	static RenderTarget* getTextureScene();
	static RenderTarget* getTextureGui();
	
	static void render(RenderWindow* window);
	
	static void addRippleEffect(Vector3f pos);
	
	static void tick(float frameTime);
	
	static void setBlurStrength(float blurStrength);
	
};

#endif //CYBERPUNK_CRENDERMANAGER_HPP