#include "CMapRoom.hpp"
#include "CSceneManager.hpp"
#include "CNavMesh.hpp"
#include "CDirection.hpp"
#include "CColorDefines.hpp"

CMapRoom::CMapRoom(Doorway entrance, Doorway exit, vector<CMapBlock*> wallBlocks,
                   vector<CMapBlock*> floorBlocks) :
    entrance(entrance),
    exit(exit),
    wallBlocks(wallBlocks),
    floorBlocks(floorBlocks) {
	
	generateRoomAreaFromWallBlocks();
	generateNavMeshFromFloorBlocks();
}

CRectangleArea CMapRoom::getRoomArea() const {
	return roomArea;
}

Doorway CMapRoom::getEntrance() const {
	return entrance;
}

Doorway CMapRoom::getExit() const {
	return exit;
}

const vector<CMapBlock*>* CMapRoom::getWallBlocks() const {
	return &wallBlocks;
}

void CMapRoom::generateRoomAreaFromWallBlocks() {
	float maxX = -99999;
	float minX = +99999;
	float maxY = -99999;
	float minY = +99999;

	for(CMapBlock* mapBlock : wallBlocks) {
		Vector3f pos = mapBlock->getPosition();
		Vector3f dim = mapBlock->getDim();
		
		maxX = max(maxX, pos.x + dim.x);
		minX = min(minX, pos.x - dim.x);
		maxY = max(maxY, pos.y + dim.y);
		minY = min(minY, pos.y - dim.y);
	}

	roomArea.offset = Vector2f(minX + 0.5f, minY + 0.5f);
	roomArea.size = Vector2f(maxX - minX, maxY - minY);
}

void CMapRoom::generateNavMeshFromFloorBlocks() {
	vector<CRectangleArea> roomAreas;
	
	for(CMapBlock* mapBlock : floorBlocks) {
		CRectangleArea roomArea;
		roomArea.offset = CEngine::to2D(mapBlock->getPosition() - mapBlock->getDim());
		roomArea.size = CEngine::to2D(mapBlock->getDim()) * 2.f;
		
		roomAreas.push_back(roomArea);
	}
	
	navMesh = new CNavMesh(roomAreas);
}

void CMapRoom::registerToSceneManager() {
	for(CMapBlock* mapBlock : wallBlocks) {
		CSceneManager::safeAddObject(mapBlock);
	}
	for(CMapBlock* mapBlock : floorBlocks) {
		CSceneManager::safeAddObject(mapBlock);
	}
}

void CMapRoom::removeFromSceneManager() {
	for(CMapBlock* mapBlock : wallBlocks) {
		CSceneManager::safeRemoveObject(mapBlock);
	}
	for(CMapBlock* mapBlock : floorBlocks) {
		CSceneManager::safeRemoveObject(mapBlock);
	}
}

int CMapRoom::getMinimumExitCorridorLength() const {
	if(exit.direction == CDirection::PX) {
		return (int) ((roomArea.offset.x + roomArea.size.x + 1) - exit.left.x);		
	} else if(exit.direction == CDirection::NX) {
		return (int) (exit.left.x - roomArea.offset.x + 1);
	} else if(exit.direction == CDirection::PY) {
		return (int) ((roomArea.offset.y + roomArea.size.y + 1) - exit.left.y);
	} else {
		return (int) (exit.left.y - roomArea.offset.y + 1);
	}	
}

CNavMesh* CMapRoom::getNavMesh() const {
	return navMesh;
}

void CMapRoom::sealOfExit() {
	sealOfDoorway(exit, true);
}

void CMapRoom::permanentlySealEntrance() {
	sealOfDoorway(entrance, false);
	isEntrancePermanentlySealed = true;
}

void CMapRoom::openExit() {
	for(CMapBlock* block : doorwayBlocks) {
		cout << "removing 1 doorway block" << endl;
		CSceneManager::safeRemoveObject(block);
		doorwayBlocks.erase(std::remove(doorwayBlocks.begin(), doorwayBlocks.end(), block), doorwayBlocks.end());
	}

	Vector2f offset = Vector2f(min(exit.left.x, exit.right.x) - 0.5f, min(exit.left.y, exit.right.y) - 0.5f);
	Vector2f size;
	if(exit.direction.isParallel(CDirection::PX)) {
		size = Vector2f(1.f, fabs(exit.left.y - exit.right.y) + 1.f);
	} else {
		size = Vector2f(fabs(exit.left.x - exit.right.x) + 1.f, 1.f);
	}
	navMesh->freeArea(CRectangleArea(offset, size));
}

void CMapRoom::sealOfDoorway(Doorway doorway, bool isSealTemporary) {
	CMapBlock* block;
	if(isSealTemporary) {
		block = new CMapBlock(doorway.left, doorway.right, 1, COLOR_NRW_DOORWAY_WALL_BASE, COLOR_NRW_DOORWAY_WALL_ACCENT, false);
		doorwayBlocks.push_back(block);
	} else {
		block = new CMapBlock(doorway.left, doorway.right, 1, COLOR_NRW_DARK_WALL_BASE, COLOR_NRW_DARK_WALL_ACCENT, false);
		wallBlocks.push_back(block);
	}
	
	CSceneManager::safeAddObject(block);

	Vector2f offset = Vector2f(min(doorway.left.x, doorway.right.x) - 0.5f, min(doorway.left.y, doorway.right.y) - 0.5f);
	Vector2f size;
	if(doorway.direction.isParallel(CDirection::PX)) {
		size = Vector2f(1.f, fabs(doorway.left.y - doorway.right.y) + 1.f);
	} else {
		size = Vector2f(fabs(doorway.left.x - doorway.right.x) + 1.f, 1.f);
	}

	CRectangleArea blockedArea(offset, size);
	navMesh->blockArea(blockedArea);

	// Push players outside
	blockedArea.offset -= Vector2f(0.5f, 0.5f);
	blockedArea.size += Vector2f(1.f, 1.f);
	
	// Push players outside
	for(CPlayer* player : CSceneManager::getPlayers()) {
		if(player->getIsLocal()) {
			if(blockedArea.isPointInside(CEngine::to2D(player->getPosition()))) {
				Vector2f pushVector;
				if(doorway.direction == entrance.direction) { 
					pushVector = entrance.direction.toVector();
				} else {
					pushVector = exit.direction.getOpposite().toVector();
				}
				
				player->teleport(player->getPosition() + CEngine::toPlanar3D(pushVector * 2.f));
			}
		}
	}
}
