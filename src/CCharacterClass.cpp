#include "CCharacterClass.hpp"

CSkill* CCharacterClass::getMovementSkill() {
	return movementSkill;
}

CSkill* CCharacterClass::getPrimarySkill() {
	return primarySkill;
}

std::vector<CSkill*> CCharacterClass::getActiveSkills() {
	return activeSkills;
}

CCharacterClass::~CCharacterClass() {
	delete primarySkill;
	delete movementSkill;
	
	for(CSkill* activeSkill : activeSkills) {
		delete activeSkill;
	}
}

CClassName CCharacterClass::getClassName() const {
	return className;
}

void CCharacterClass::storeObject(CObject *object) {
	objectStorage.push_back(object);
}

vector<CObject *> CCharacterClass::getObjectStorage() {
	return objectStorage;
}

vector<pair<SIGN, string> > CCharacterClass::getSkillDisplayProfiles() {
	return skillDisplayProfiles;
}
