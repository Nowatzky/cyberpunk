#ifndef CYBERPUNK_CLOBBY_HPP
#define CYBERPUNK_CLOBBY_HPP


#include <SFML/Graphics/RenderWindow.hpp>
#include "CClock.hpp"
#include "CCharacterClass.hpp"
#include "CGuiLobbyPlayerList.hpp"
#include "CDifficultyManager.hpp"
#include "CGuiLobbyDifficulty.hpp"

class CLobby {
private:
	bool isHost;
	sf::RenderWindow* window;

	CClock broadcastStatusClock;
	float broadcastStatusFrequency = 1000.f/5.f;
	
	std::string name;
	CClassName className;
	CDifficulty difficulty;
	
	CGuiLobbyPlayerList lobbyPlayerList;
	CGuiLobbyDifficulty lobbyDifficulty;
	float kickTimeout = 3500;

public:
	CLobby(bool isHost, sf::RenderWindow* window);
	
	void enterLobby();
	
	std::string getName() const;
	CClassName getClassName() const;
	CDifficulty getDifficulty() const;
	
	void setName(std::string name);
	void setClassName(CClassName className);
	void setDifficulty(CDifficulty difficulty);

private:
	void tickHost();
	void tickPeer();
	
	std::string readNameFromFile();
	
	void updateClassName(bool forward);
	void updateDifficulty(bool upward);

};


#endif //CYBERPUNK_CLOBBY_HPP
