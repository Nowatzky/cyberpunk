#include "CSkillPrimaryLaserFire.hpp"
#include "CInputManager.hpp"
#include "CSceneManager.hpp"
#include "CProjectileLaserBullet.hpp"
#include "CObjectProjectileHitPopUpBox.hpp"
#include "CGui.hpp"
#include "CRpcHandler.hpp"

void CSkillPrimaryLaserFire::tick(float frameTime) {
	if(isReloading && reloadCooldownClock.getTimeMs() >= reloadCooldown) {
		stopReloading(false);
	}
	
	if(waitingForDoubleShoot && doubleShootDelayClock.getTimeMs() >= doubleShotDelay) {
		waitingForDoubleShoot = false;
		CProjectile* projectile = spawnProjectile(lastShootDirection, false);
		if(owner->getIsLocal()) {
			projectile->setId(projectileIdForDoubleShot);
		} else{
			projectile->setNotLocal(projectileIdForDoubleShot);
		}
	}
	
	if(isButtonPressed && !isReloading && remainingBullets > 0 && fireCooldownClock.getTimeMs() >= fireCooldown) {
		remainingBullets--;
		fireCooldownClock.restart();

		Vector2f dir = CInputManager::getDirToMouseInScene(owner->getPosition());
		fire(dir);

		if(remainingBullets <= 0) {
			reload();
		}		
	}
}

void CSkillPrimaryLaserFire::quickDraw() {
	if(isReloading) {
		stopReloading(true);

		CRpcHandler::sendPlayerQuickDraw(owner);
	}
}

void CSkillPrimaryLaserFire::quickDrawRPC() {
	stopReloading(true);
}

void CSkillPrimaryLaserFire::reload() {
	if(!isReloading) {
		isReloading = true;
		remainingBullets = maxBullets;
		reloadCooldownClock.restart();
		
		Color col = owner->getGraphicContainer()->getColorBase();
		col.a = 90;
		owner->getGraphicContainer()->setColorBase(col);

		if(owner->getIsLocal()) {
			CGui::updateLocalPlayerReloadStatus(CReloadStatus::RELOADING);
		}

		CRpcHandler::sendPlayerReload(owner);
	}
}

void CSkillPrimaryLaserFire::reloadRPC(float gameTime) {
	isReloading = true;
	remainingBullets = maxBullets;
	reloadCooldownClock.restartAtGameTime(gameTime);

	Color col = owner->getGraphicContainer()->getColorBase();
	col.a = 90;
	owner->getGraphicContainer()->setColorBase(col);
}

void CSkillPrimaryLaserFire::fire(Vector2f dir) {
	CProjectile* projectile = spawnProjectile(dir, true);

	waitingForDoubleShoot = true;
	doubleShootDelayClock.restart();
	lastShootDirection = dir;
	projectileIdForDoubleShot = CObjectIdManager::getUniqueId();
	
	std::vector<CObjectId> projectileIds;
	projectileIds.push_back(projectile->getId());
	projectileIds.push_back(projectileIdForDoubleShot);

	CRpcHandler::sendPlayerMediatorPrimaryFire(owner, dir, owner->getPosition(), projectileIds);
}

void CSkillPrimaryLaserFire::fireRPC(float gameTime, Vector3f originalPos, Vector2f dir, std::vector<CObjectId> projectileIds) {
	CProjectile* projectile = spawnProjectile(dir, true);
	projectile->setNotLocal(projectileIds.at(0));
	
	waitingForDoubleShoot = true;
	doubleShootDelayClock.restart();
	lastShootDirection = dir;
	projectileIdForDoubleShot = projectileIds.at(1);
}

CProjectile* CSkillPrimaryLaserFire::spawnProjectile(Vector2f dir, bool isFirstShot) {
	Vector3f offset;
	float angleOffsetSign = 0;
	
	if(isFirstShot) {
		offset = CEngine::toPlanar3D(CEngine::rotateZLeft90(dir)) * doubleShootOffset;
		angleOffsetSign = 1.f;
	} else {
		offset = CEngine::toPlanar3D(CEngine::rotateZRight90(dir)) * doubleShootOffset;
		angleOffsetSign = -1.f;
	}
	
	Vector3f spawnPos = owner->getPosition() + CEngine::toPlanar3D(dir) * 0.5f + offset;

	CProjectileLaserBullet* p = new CProjectileLaserBullet(owner, spawnPos, CEngine::rotateZ(dir, angleOffsetSign * doubleShootAngleOffsetInside));
	CSceneManager::safeAddObject(p);
	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(spawnPos, Vector3f(popupSize, popupSize, popupSize), popupColor, popupDuration));

	return p;
}

void CSkillPrimaryLaserFire::stopReloading(bool wasQuickdraw) {
	isReloading = false;
	owner->getGraphicContainer()->setColorBase(owner->getCharacterClass()->getBaseColor());

	if(owner->getIsLocal()) {
		if(wasQuickdraw) {
			CGui::updateLocalPlayerReloadStatus(CReloadStatus::QUICKDRAW);
		} else {
			CGui::updateLocalPlayerReloadStatus(CReloadStatus::NONE);
		}
	}	
}
