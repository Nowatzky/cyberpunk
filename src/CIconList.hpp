#ifndef CYBERPUNK_CICONLIST_HPP
#define CYBERPUNK_CICONLIST_HPP

#include <SFML/System.hpp>
#include "CGuiElement.hpp"
#include "CIcon.hpp"

using namespace sf;
using namespace std;

class CIconList : public CGuiElement {
	vector<std::string> iconList;

	Color color;
	Vector2f iconSize;
	Vector2f position;
	
public:
	CIconList(Vector2f position, Vector2f iconSize);
	~CIconList();

	virtual void draw(sf::RenderTarget* renderTarget);
	virtual void tick(float frameTime);

	void setColor(Color color);
	void setIconSize(Vector2f iconSize);

	void pushFront(std::string iconSource);
	void pushBack(std::string iconSource);
	void popFront();
	void popBack();
};


#endif //CYBERPUNK_CICONLIST_HPP
