#include "CAIControllerGhostSummoner.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"

#include "CEnemyGhostSummoner.hpp"

void CAIControllerGhostSummoner::tick(float frameTime) {
	if(!host->isHostSummoning()) {
		if(host->hasReachedTarget() || host->isStuck(frameTime) || clockFindNewPath.getElapsedTime().asMilliseconds() >= maxPathFollowTime) {
			pickNewWanderTarget();
		}

		host->updatePath();
		host->followPath(frameTime);

		// Summoning
		if(host->isSummonReady()) {
			host->summon();
		}
	}
	else {
		// Stand still
	}
}

void CAIControllerGhostSummoner::pickNewWanderTarget() {
	clockFindNewPath.restart();
	
	int maxTries = 5;
	
	Vector2f hostPos = CEngine::to2D(host->getPosition());
	Vector2f playerPos = CEngine::to2D(CSceneManager::getPlayers().at(0)->getPosition());

	for(int i = 0; i < maxTries; i++) {
		Vector2f target = CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(host->getPosition()))->getRandomPointInside();

		Vector2f dirToTarget = target - hostPos;
		Vector2f dirToPlayer = playerPos - hostPos;
		if(abs(CEngine::getAngleDifference(dirToTarget, dirToPlayer)) >= 120 - (i * 10)) {
			host->findPathTo(target);
			return;
		}
	}
	
	host->findPathTo(CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(host->getPosition()))->getRandomPointInside());
	
	// TODO Reimplement
	/*vector<Vector2f> freeFields = CSceneManager::getMap()->getFreeFields();

	while(!freeFields.empty()) {
		int pos = CRandom::getInRange(0, (int) (freeFields.size() - 1));
		Vector2f vec = freeFields.at((unsigned long) pos);
		freeFields.erase(freeFields.begin() + pos);

		float distToPlayer = CEngine::getDistance(vec, CEngine::to2D(CSceneManager::getPlayers().at(0)->getPosition()));
		float distToHost = CEngine::getDistance(vec, CEngine::to2D(host->getPosition()));

		if(distToPlayer >= 4 && distToHost >= 5) {
			wanderTarget = vec;
			break;
		}
	}*/
}

