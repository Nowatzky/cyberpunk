#ifndef CYBERPUNK_CCOLLISIONCHANNEL_HPP
#define CYBERPUNK_CCOLLISIONCHANNEL_HPP

#include <cstdint>
#include <initializer_list>

using namespace std;

class CCollisionChannel {
private:
	uint32_t bitmask;

public:
	enum class Channel : uint32_t {
		STATIC      = 1 << 0,
		CHARACTER   = 1 << 1,
		PROJECTILE  = 1 << 2};

	CCollisionChannel() {
		bitmask = 0;
	}

	CCollisionChannel(Channel channel) {
		bitmask = static_cast<uint32_t>(channel);
	}

	CCollisionChannel(initializer_list<Channel> channels) {
		bitmask = 0;
		for(Channel channel : channels) {
			set(channel);
		}
	}

	inline uint32_t getBitmask() {
		return bitmask;
	}

	inline void set(Channel channel) {
		bitmask |= static_cast<uint32_t>(channel);
	}

	inline void set(initializer_list<Channel> channels) {
		for(Channel channel : channels) {
			set(channel);
		}
	}

	inline void clear(Channel channel) {
		bitmask &= !static_cast<uint32_t>(channel);
	}

	inline void clear(initializer_list<Channel> channels) {
		for(Channel channel : channels) {
			clear(channel);
		}
	}

	inline void clearAll() {
		bitmask = 0;
	}

	inline bool isSet(Channel channel) const {
		return static_cast<bool>(bitmask & static_cast<uint32_t>(channel));
	}

	inline bool isSet(initializer_list<Channel> channels) const {
		for(Channel channel : channels) {
			if(isSet(channel)) {
				return true;
			}
		}
		return false;
	}

	inline bool match(CCollisionChannel& other) const {
		return static_cast<bool>(bitmask & other.getBitmask());
	}
};

#endif //CYBERPUNK_CCOLLISIONCHANNEL_HPP