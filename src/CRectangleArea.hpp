#ifndef CYBERPUNK_CRECTANGLEAREA_HPP
#define CYBERPUNK_CRECTANGLEAREA_HPP

#include <SFML/System.hpp>
#include <vector>

using namespace std;
using namespace sf;

class CRectangleArea {
public:
	Vector2f offset;
	Vector2f size;
	
	CRectangleArea() = default;

	CRectangleArea(Vector2f offset, Vector2f size) : 
			offset(offset),
            size(size) {}
	
	// Construct from 3 points, if first two are not on one axis they are sufficient already
	CRectangleArea(Vector2f p1, Vector2f p2, Vector2f p3);
	
	bool isPointInside(Vector2f point) const;
	bool isPointInsideExclusiveBorder(Vector2f point) const;
	bool isPointOnBorders(Vector2f point) const;
	bool doesIntersect(const CRectangleArea& otherArea) const;
	bool doesIntersectExclusiveBorder(const CRectangleArea& otherArea) const;
	Vector2f getRandomPointInside() const;
	Vector2f getCenter() const;
	float getAreaSize() const;
	vector<Vector2f> getCorners() const;

};


#endif //CYBERPUNK_CRECTANGLEAREA_HPP
