#ifndef CYBERPUNK_CSKILLALPHABLAST_HPP
#define CYBERPUNK_CSKILLALPHABLAST_HPP


#include "CSkill.hpp"
#include "CObject.hpp"
#include "CClock.hpp"

class CSkillAlphaBlast : public CSkill {
protected:
	float maxLockOnDistance = 2.f;
	
	const int numberOfProjectiles = 3;
	const float delay = 300;
	CClock firingClock;
	
	bool isFiring = false;
	int numberOfProjectilesFired = 0;
	Vector3f targetPos;
	
public:
    CSkillAlphaBlast(CPlayer* owner) :
		    CSkill(owner, 900, 60) {}
	
protected:
	virtual void tick(float frameTime);
    virtual void onButtonPress();
	
private:
	CObject* findNearestTarget(Vector2f pos) const;
	void fireProjectile(Vector3f pos);

};


#endif //CYBERPUNK_CSKILLALPHABLAST_HPP
