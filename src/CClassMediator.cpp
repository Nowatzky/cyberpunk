#include <iostream>
#include "CClassMediator.hpp"
#include "CColorDefines.hpp"
#include "CSkillPrimaryLaserFire.hpp"
#include "CSkillDash.hpp"
#include "CSkillForceSweep.hpp"
#include "CSkillTeleport.hpp"
#include "CSkillAlphaBlast.hpp"
#include "CSkillChainFlash.hpp"
#include "CSkillHackSignal.hpp"

CClassMediator::CClassMediator(CPlayer* player) :
		CCharacterClass(CClassName::MEDIATOR) {
	
	primarySkill = new CSkillPrimaryLaserFire(player);
	movementSkill = new CSkillDash(player);

	activeSkills.push_back(new CSkillForceSweep(player)); //0
	activeSkills.push_back(new CSkillTeleport(player));
	activeSkills.push_back(new CSkillAlphaBlast(player));
	activeSkills.push_back(new CSkillChainFlash(player));
	activeSkills.push_back(new CSkillHackSignal(player));

	skillDisplayProfiles = {make_pair(SIGN::HLINE_FROMLEFT, "Force Sweep"), make_pair(SIGN::WEDGE_UP, "Teleport"), make_pair(SIGN::ALPHA, "Alpha Blast"), make_pair(SIGN::FLASH, "Chain Flash"), make_pair(SIGN::GAMMA, "Hack Signal")};

}

CSkill* CClassMediator::getSkill(SIGN sign) {
	if(sign == SIGN::HLINE_FROMLEFT || sign == SIGN::HLINE_FROMRIGHT) {
		return activeSkills[0];
	}
	else if(sign == SIGN::WEDGE_UP) {
		return activeSkills[1];
	}
	else if(sign == SIGN::ALPHA) {
		return activeSkills[2];
	}
	else if(sign == SIGN::FLASH) {
		return activeSkills[3];
	}
	else if(sign == SIGN::EPSILON) {
		return activeSkills[4];
	}
	else if(sign == SIGN::GAMMA) {
		return activeSkills[4];
	}
	else {
		return nullptr;
	}
}

Color CClassMediator::getBaseColor() {
	return COLOR_PINK;
}

Color CClassMediator::getAccentColor() {
	return COLOR_YELLOW;
}






