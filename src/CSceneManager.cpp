#include "CSceneManager.hpp"
#include "CTopologicalMap.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CEnemyGhost.hpp"
#include "CEnemyGhostSummoner.hpp"
#include "CEnemySpecter.hpp"
#include "CEnemyLich.hpp"
#include "CUtilityDefines.hpp"
#include "CPlayerDummy.hpp"
#include "CDebugHelper.hpp"
#include "CNetworkManager.hpp"
#include "tinyc2.h"
#include <chrono>
#include <thread>

vector<CObject*> CSceneManager::objects = vector<CObject*>();
vector<CObject*> CSceneManager::objectsPendingForAddition = vector<CObject*>();
vector<CObject*> CSceneManager::objectsPendingForRemoval = vector<CObject*>();
vector<CObject*> CSceneManager::objectsPendingForUnregistration = vector<CObject*>();
vector<CPlayer*> CSceneManager::players = vector<CPlayer*>();
vector<CEnemy*> CSceneManager::enemies = vector<CEnemy*>();
vector<CProjectile*> CSceneManager::projectiles = vector<CProjectile*>();

vector<Vector2f> CSceneManager::pointsDrawn = vector<Vector2f>();

CMap* CSceneManager::map = nullptr;
CTopologicalMap* CSceneManager::topologicalMap = nullptr;

int CSceneManager::staticObjectsCount;

void CSceneManager::safeAddObject(CObject* object) {
	objectsPendingForAddition.push_back(object);
}

void CSceneManager::safeRemoveObject(CObject* object) {
	objectsPendingForRemoval.push_back(object);
}

void CSceneManager::safeUnregisterObject(CObject* object) {
	objectsPendingForUnregistration.push_back(object);
}

void CSceneManager::addObject(CObject* object) {
	CPlayer* player = dynamic_cast<CPlayer*>(object);
	if(player != nullptr) {
		if(dynamic_cast<CPlayerDummy*>(player)) {
			players.insert(players.begin(), player);
		}
		players.push_back(player);
	}

	CEnemy* enemy = dynamic_cast<CEnemy*>(object);
	if(enemy != nullptr) {
		enemies.push_back(enemy);
	}

	CProjectile* projectile = dynamic_cast<CProjectile*>(object);
	if(projectile != nullptr) {
		projectiles.push_back(projectile);
	}
	
	if(object->getGraphicContainer() != nullptr) {
		topologicalMap->addObject(object);
	}
	
	objects.push_back(object);
}

void CSceneManager::removeObject(CObject* object, bool deleteObject) {
	CPlayer* player = dynamic_cast<CPlayer*>(object);
	if(player != nullptr) {
		players.erase(remove(players.begin(), players.end(), player), players.end());
	}

	CEnemy* enemy = dynamic_cast<CEnemy*>(object);
	if(enemy != nullptr) {
		enemies.erase(remove(enemies.begin(), enemies.end(), enemy), enemies.end());
	}

	CProjectile* projectile = dynamic_cast<CProjectile*>(object);
	if(projectile != nullptr) {
		projectiles.erase(remove(projectiles.begin(), projectiles.end(), projectile), projectiles.end());
	}

	if(object->getGraphicContainer() != nullptr) {
		topologicalMap->removeObject(object);
	}
	
	objects.erase(remove(objects.begin() + max(staticObjectsCount - 50, 0), objects.end(), object), objects.end());

	if(deleteObject) {
		CNetworkManager::removeReplicatingObject(object);
		delete object;
	}
}

void CSceneManager::safeAddPendingObjects() {
	CObject* temp;
	while(!objectsPendingForAddition.empty()) {
		temp = objectsPendingForAddition.back();
		objectsPendingForAddition.pop_back();
		addObject(temp);
	}
}

void CSceneManager::safeRemovePendingObjects() {
	CObject* temp;
	while(!objectsPendingForRemoval.empty()) {
		temp = objectsPendingForRemoval.back();
		objectsPendingForRemoval.pop_back();

		// Remove duplicates
		objectsPendingForRemoval.erase(remove(objectsPendingForRemoval.begin(), objectsPendingForRemoval.end(), temp), objectsPendingForRemoval.end());

		// Remove if pending for addition
		objectsPendingForAddition.erase(remove(objectsPendingForAddition.begin(), objectsPendingForAddition.end(), temp), objectsPendingForAddition.end());

		removeObject(temp);
	}
}

void CSceneManager::safeUnregisterPendingObjects() {
	CObject* temp;
	while(!objectsPendingForUnregistration.empty()) {
		temp = objectsPendingForUnregistration.back();
		objectsPendingForUnregistration.pop_back();

		// Remove duplicates
		objectsPendingForUnregistration.erase(remove(objectsPendingForUnregistration.begin(), objectsPendingForUnregistration.end(), temp), objectsPendingForUnregistration.end());

		// Remove if pending for addition
		objectsPendingForAddition.erase(remove(objectsPendingForAddition.begin(), objectsPendingForAddition.end(), temp), objectsPendingForAddition.end());

		removeObject(temp, false);
	}
}

deque<CObject*> CSceneManager::getObjectHierarchy() {
	return topologicalMap->getObjectHierarchy();
}

void CSceneManager::requestReconstructionForTopologicalMap() {
	topologicalMap->requestReconstruction();
}

void CSceneManager::tickAll(float frameTime) {
	vector<CObject*>::iterator iter = objects.begin() + staticObjectsCount;
	
	while(iter != objects.end()) {
		CObject* object = *iter;
		iter++;
		
		if(object->getIsLocal()) {
			object->tickAuthority(frameTime);
		}

		object->tick(frameTime);
	}
}

void CSceneManager::setMap(CMap* newMap) {
	map = newMap;
	topologicalMap = new CTopologicalMap();
	
	safeAddPendingObjects();
	
	staticObjectsCount = (int) (objects.size() - 1);
}

CMap* CSceneManager::getMap() {
	return map;
}

vector<CObject*> CSceneManager::getObjects() {
	return objects;
}

vector<CPlayer*> CSceneManager::getPlayers() {
	return players;
}

vector<CEnemy*> CSceneManager::getEnemies() {
	return enemies;
}

vector<CProjectile*> CSceneManager::getProjectiles() {
	return projectiles;
}

bool CSceneManager::doesIntersectAny(CObject* object, CCollisionChannel collisionChannel) {
	for(CObject* o : objects) {
		if(o != object && o->getCollisionContainer() != nullptr && o->getCollisionContainer()->matchCollisionChannel(collisionChannel)) {
			if(o->getCollisionContainer()->doesIntersect(*object->getCollisionContainer())) {
				return true;
			}
		}
	}
	return false;
}

vector<CObject*> CSceneManager::getIntersecting(CCollisionContainer* collisionContainer, CCollisionChannel collisionChannel) {
	vector<CObject*> listIntersecting;

	for(CObject* o : objects) {
		if(o != nullptr && o->getCollisionContainer() != nullptr && collisionContainer->getPosition().z != 0 && o->getCollisionContainer()->matchCollisionChannel(collisionChannel)) {
			if(o->getCollisionContainer()->doesIntersect(*collisionContainer)) {
				listIntersecting.push_back(o);
			}
		}
	}

	return listIntersecting;
}

vector<CObject*> CSceneManager::getIntersecting(CObject* object, CCollisionChannel collisionChannel) {
	vector<CObject*> listIntersecting;

	for(CObject* o : objects) {
		if(o != nullptr && o->getCollisionContainer() != nullptr && o != object && o->getCollisionContainer()->getPosition().z != 0 && o->getCollisionContainer()->matchCollisionChannel(collisionChannel)) {
			if(o->getCollisionContainer()->doesIntersect(*object->getCollisionContainer())) {
				listIntersecting.push_back(o);
			}
		}
	}

	return listIntersecting;
}

Vector3f CSceneManager::findSpawnPoint(Vector3f origin, Vector3f dimension, CCollisionChannel collisionChannel, float rot, float radiusStart, float radiusStop, float radiusStep, CObject* self) {
	Vector3f spawnPos;
	
	if(radiusStart > radiusStop) {
		radiusStep = -radiusStep;
	}

	CNavMesh* navMesh = map->getNavMeshForPosition(CEngine::to2D(origin));
	if(navMesh == nullptr) {
		return VECTOR3D_INVALID;
	}
	
	CBoundingBox box(origin, dimension, rot);

	// Check origin position
	if(self != nullptr && navMesh->isInside(origin)) {
		vector<CObject*> intersecting = getIntersecting(&box, collisionChannel);

		if(intersecting.empty() || (intersecting.size() == 1 && intersecting.at(0) == self)) {
			return origin;
		}
	}
	
	float angleSteps = 360.f/8.f;
	float radius = radiusStart;
	bool spawnPosFound = false;
	bool isRadiusStopReached = false;
	
	while(!spawnPosFound && !isRadiusStopReached) {
		float angleStart = CRandom::getInRange(0, 360);
		for(float angle = angleStart; angle < angleStart + 360; angle += angleSteps) {
			spawnPos = origin + CEngine::toPlanar3D(CEngine::getUnitInDirection(fmod(angle, 360)) * radius);

			if(!navMesh->isInside(spawnPos)) {
				continue;
			}
			
			box.setPosition(spawnPos);

			if(getIntersecting(&box, collisionChannel).empty()) {
				spawnPosFound = true;
				break;
			}
		}
		radius += radiusStep;
		isRadiusStopReached = ((radiusStep > 0 && radius > radiusStop) || (radiusStep < 0 && radius < radiusStop));
	}

	if(spawnPosFound) {
		return spawnPos;
	}
	return VECTOR3D_INVALID;
}

bool CSceneManager::doesObjectExist(CObject* object) {
	if(object == nullptr) {
		return false;
	}

	return (std::find(objects.begin(), objects.end(), object) != objects.end());
}

void CSceneManager::updateAllGraphicContainers() {
	for(CObject* object : objects) {
		object->getGraphicContainer()->needsUpdate();
	}
}

CHitResult CSceneManager::raycast(Vector3f start, Vector2f dir, CCollisionChannel collisionChannel) {
	dir = CEngine::normalize(dir);

	c2Ray ray;
	ray.p = CEngine::toC2v(CEngine::to2D(start));
	ray.d = CEngine::toC2v(CEngine::normalize(dir));
	ray.t = 9999;

	CHitResult hitResult;
	hitResult.target = nullptr;
	
	c2Raycast rayCast;
	float minDist = 9999;	
	
	for(CObject* object : objects) {
		if(object->getPosition().z != 0
		   && CEngine::isFurtherInDirection(CEngine::to2D(object->getPosition()), CEngine::to2D(start) - dir, dir)
		   && object->getCollisionContainer() != nullptr && object->getCollisionContainer()->matchCollisionChannel(collisionChannel)
		   && std::find(players.begin(), players.end(), object) == players.end()) { // If no player

			if(c2RaytoPoly(ray, object->getCollisionContainer()->getPoly(), nullptr, &rayCast)) {
				if(rayCast.t < minDist) {
					minDist = rayCast.t;
					
					hitResult.target = object;
					hitResult.hitPos = start + CEngine::toPlanar3D(dir) * rayCast.t;
					hitResult.hitNormal = CEngine::toSFVec(rayCast.n);
					hitResult.hitDistance = rayCast.t;
				}
			}			
		}
	}

	return hitResult;
}

CObject* CSceneManager::getObject(CObjectId id) {
	for(CObject* object : objects) {
		if(object->getId() == id) {
			return object;
		}
	}

	//std::cout << "Error: Object with id " << id << " does not exist!" << endl;
	return nullptr;
}

std::vector<CObject*> CSceneManager::getObjectsRemoveNull(std::vector<CObjectId> ids) {
	std::vector<CObject*> objects;
	
	for(CObjectId id : ids) {
		CObject* object = getObject(id);

		if(object != nullptr) {
			objects.push_back(object);
		}
	}

	return objects;
}

vector<CPlayer*> CSceneManager::getTargetablePlayers() {
	std::vector<CPlayer*> targetablePlayers = players;
	
	for(auto iter = targetablePlayers.begin(); iter != targetablePlayers.end(); iter++) {
		if(!(*iter)->getIsAlive()) {
			targetablePlayers.erase(iter);
			iter--;
		}
	}

	// At least one player to avoid nullptr at game over
	if(targetablePlayers.empty()) {
		CPlayer* player = players.front();
		if(player != nullptr) {
			targetablePlayers.push_back(player);
		} else {
			CPlayer* dummy = new CPlayer(enemies.front()->getPosition(), Vector3f(0,0,0), CClassName::MEDIATOR);
			targetablePlayers.push_back(dummy);	
		}		
	}
	
	return targetablePlayers;
}
