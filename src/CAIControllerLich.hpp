#ifndef CYBERPUNK_CAICONTROLLERLICH_HPP
#define CYBERPUNK_CAICONTROLLERLICH_HPP

#include <SFML/System.hpp>
#include "CAIController.hpp"

using namespace sf;

class CEnemyLich;
enum class LichSpell;

class CAIControllerLich : public CAIController {
private:
    CEnemyLich* host;
	
	float previousHealth = 0;

public:
    CAIControllerLich(CEnemyLich* host) :
            host(host) {
    }

    void tick(float frameTime);

private:
	void pickNewWanderTarget();
	void castAnySpell();
	LichSpell pickNextSpell();
	bool isFireSpellApplicable();
		    

};


#endif //CYBERPUNK_CAICONTROLLERLICH_HPP
