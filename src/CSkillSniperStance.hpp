#ifndef CYBERPUNK_CSKILLSNIPERSTANCE_HPP
#define CYBERPUNK_CSKILLSNIPERSTANCE_HPP


#include "CSkill.hpp"

class CSkillSniperStance : public CSkill {
private:
	float targetTileSize = 64;
	float targetTileSizeDefault = 64;
	float targetTileSizeSniperStance = 45;


public:
	CSkillSniperStance(CPlayer* owner) :
	CSkill(owner, 250, 30) {}

	virtual void tick(float frameTime);

protected:
	void onButtonPress();
	void onButtonRelease();
};


#endif //CYBERPUNK_CSKILLSNIPERSTANCE_HPP
