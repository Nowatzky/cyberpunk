#ifndef CYBERPUNK_CNEURALNET_H
#define CYBERPUNK_CNEURALNET_H

#include <vector>
#include <string>

using namespace std;
typedef vector<vector<double> > matrix;

class TrainingData {
public:
    TrainingData(const vector<string> filename_v);
    ~TrainingData() {/*for(ifstream *i : m_trainingDataFiles){
            delete(i);
        }
        m_trainingDataFiles.clear();
    */};
    //bool isEOF() { return m_trainingDataFile.eof();}
    void getTopology(vector<unsigned> &topology);

    unsigned getNextInputs(vector<double> &inputVals);
    unsigned getTargetOutputs(vector<double> &targetOutputVals);
    vector<string> m_trainingDataPaths;
    int c_inputNum;
    int total_inputNum;
    //vector<ifstream*> m_trainingDataFiles;
    vector<matrix> input_cube;
private:

};

class Neuron;
typedef vector<Neuron> Layer;

struct Connection {
    double weight;
    double deltaWeight;
};

class Neuron {
public:
    Neuron(unsigned numOutputs, unsigned index); //random weight
    Neuron(unsigned numOutputs, unsigned index, vector<double> &weight);

    void setOutputVal(double val) {m_outputVal = val;}
    double getOutputVal() const { return m_outputVal;}
    void feedForward(Layer &prevLayers);
    void calcOutputGradients(double targetVal);
    void calcHiddenGradients(const Layer &nextLayer);
    void updateInputWeights(Layer &prevLayer);

    vector<Connection> m_outputsWeights;
private:
    static double eta; //[0.0 .. 1.0] overall net training rate
    static double alpha; //[0.0 .. n] multiplier of the last weight change (monumentum)

    static double randomWeight();//TODO return rand() / (double) RAND_MAX;}
    static double transferFunction(double x);
    static double transferFunctionDerivative(double x);
    double sumDOW(const Layer &nextLayer) const;
    unsigned m_index;
    double m_outputVal;
    double m_gradient;

};

class Net {
public:
    Net(vector<unsigned> &topology);
    Net(vector<unsigned> &topology, vector<vector<vector <double> > > &weight_cube);
    void feedForward(const vector<double> &inputVals);
    void backProp(const vector<double> &targetVals);
    void getResults(vector<double> &resultVals) const;
    //std::stringstream getNetStructure() const;
    double getRecentAverageError() { return m_recentAverageError;}
private:
    vector<Layer> m_layers; //m_layers[layerNum][neuronNum]
    double m_error;
    double m_recentAverageError;
    double m_recentAverageSmoothingFactor;
};
#endif //CYBERPUNK_CNEURALNET_H
