#include <iostream>
#include "CBoundingBox.hpp"
#include "tinyc2.h"

using namespace std;
using namespace sf;

CBoundingBox::CBoundingBox(Vector3f pos, Vector3f dim) :
		CCollisionContainer(pos),
        dim(dim) {
	poly.count = 4;
	updateMaxDimSquared();
	updatePoly();
}

CBoundingBox::CBoundingBox(Vector3f pos, Vector3f dim, float rot) :
		CCollisionContainer(pos, rot),
		dim(dim) {
	poly.count = 4;
	updateMaxDimSquared();
	updatePoly();
}

bool CBoundingBox::doesIntersect(const CCollisionContainer& other) const {
	if(const CBoundingBox* boundingBox = dynamic_cast<const CBoundingBox*>(&other)) {
		return CEngine::doesIntersect(*this, *boundingBox);
	}

	return false;
}

Vector3f CBoundingBox::getDimension() const {
	return dim;
}

void CBoundingBox::scale(float factor) {
	dim *= fmax(0.01f, factor);
	updateMaxDimSquared();
	updatePoly();
}

void CBoundingBox::setDimension(Vector3f dim) {
	this->dim = dim;
	updateMaxDimSquared();
	updatePoly();
}

void CBoundingBox::updatePoly() {
	poly.verts[0] = CEngine::toC2v(CEngine::to2D(pos) + CEngine::rotateZ(Vector2f(-dim.x, dim.y), rot));
	poly.verts[1] = CEngine::toC2v(CEngine::to2D(pos) + CEngine::rotateZ(Vector2f(-dim.x, -dim.y), rot));
	poly.verts[2] = CEngine::toC2v(CEngine::to2D(pos) + CEngine::rotateZ(Vector2f(dim.x, -dim.y), rot));
	poly.verts[3] = CEngine::toC2v(CEngine::to2D(pos) + CEngine::rotateZ(Vector2f(dim.x, dim.y), rot));
	c2MakePoly(&poly);
}

void CBoundingBox::setPosition(Vector3f pos) {
	CCollisionContainer::setPosition(pos);
	updatePoly();
}

void CBoundingBox::setRotation(float rot) {
	CCollisionContainer::setRotation(rot);
	updatePoly();
}

void CBoundingBox::move(Vector3f movement) {
	CCollisionContainer::move(movement);
	updatePoly();
}

void CBoundingBox::move(Vector2f movement) {
	CCollisionContainer::move(movement);
	updatePoly();
}

void CBoundingBox::rotate(float angle) {
	CCollisionContainer::rotate(angle);
	updatePoly();
}

const c2Poly* CBoundingBox::getPoly() const {
	return &poly;
}

float CBoundingBox::getMaxDimSquared() const {
	return maxDimSquared;
}

void CBoundingBox::updateMaxDimSquared() {
	maxDimSquared = dim.x * dim.x + dim.y * dim.y;
}


