#ifndef CYBERPUNK_CCHARACTERCLASS_HPP
#define CYBERPUNK_CCHARACTERCLASS_HPP


#include "CPatternRecognitionMngr.hpp"
#include "CSkill.hpp"
#include "CObject.hpp"

enum class CClassName : sf::Uint8 {
	MEDIATOR,
	SNIPER,
	SHOTGUNNER
};

class CCharacterClass {
protected:
	CClassName className;
	
	CSkill* movementSkill;
	CSkill* primarySkill;
	vector<CSkill*> activeSkills;
	vector<pair<SIGN, string> > skillDisplayProfiles;
	vector<CObject*> objectStorage;


	/* Protected Constructor */
	CCharacterClass(CClassName className) :
			className(className) {}

public:
	virtual ~CCharacterClass();

	CClassName getClassName() const;

	//Skills
	virtual CSkill* getPrimarySkill();
	virtual CSkill* getMovementSkill();
	virtual std::vector<CSkill*> getActiveSkills();
	virtual CSkill* getSkill(SIGN sign) = 0;


	//Visuals
	virtual Color getBaseColor() = 0;
	virtual Color getAccentColor() = 0;

	vector<pair<SIGN, string> > getSkillDisplayProfiles();


	//Handle object storage (if relevant)
	void storeObject(CObject *object);
	vector<CObject*> getObjectStorage();

};


#endif //CYBERPUNK_CCHARACTERCLASS_HPP
