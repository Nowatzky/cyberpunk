#ifndef CYBERPUNK_CHEALTHBARPLAYER_HPP
#define CYBERPUNK_CHEALTHBARPLAYER_HPP


#include <SFML/Graphics/Text.hpp>
#include "CGuiElement.hpp"

using namespace sf;

class CPlayerDisplayBar : public CGuiElement {
private:
	float maxValue;
	float currentValue;
	int barDashCount;
	int yOffset;
	float skew = 1.f;

	Text* textBar;

public:
	CPlayerDisplayBar(float maxValue, Color color, int yOffset);
	
	virtual ~CPlayerDisplayBar();
	
	virtual void draw(sf::RenderTarget* renderTarget);
	virtual void tick(float frameTime);
	
	void setCurrentValue(float value);
	void setCenterPosition(Vector2f position);
	void setColor(Color color);
	void setSkew(float skew);

};


#endif //CYBERPUNK_CHEALTHBARPLAYER_HPP
