#ifndef CYBERPUNK_CGUILOBBYPLAYERLIST_HPP
#define CYBERPUNK_CGUILOBBYPLAYERLIST_HPP


#include "CGuiElement.hpp"
#include "CCharacterClass.hpp"

class CLobby;

class CGuiLobbyPlayerList : public CGuiElement {
private:
	CLobby* lobby;
	
	sf::Text ownText;
	std::vector<sf::Text> otherTexts;
	
	unsigned int ownNameSize = 70;
	unsigned int otherNameSize = 50;
	
	float isAliveTimeout = 1000;
	bool markHostConnection = false;
	
public:
	explicit CGuiLobbyPlayerList(CLobby* lobby);
	
	void draw(sf::RenderTarget* renderTarget) override;

	void tick(float frameTime) override;

	~CGuiLobbyPlayerList() override;


private:
	sf::Color getClassColor(CClassName className) const;
};


#endif //CYBERPUNK_CGUILOBBYPLAYERLIST_HPP
