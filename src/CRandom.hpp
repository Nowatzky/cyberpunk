#ifndef CYBERPUNK_CRANDOM_HPP
#define CYBERPUNK_CRANDOM_HPP

#include <random>
#include <iostream>
#include <SFML/System.hpp>


using namespace std;
using namespace sf;

class CRandom {
private:
	CRandom() {}

	static mt19937 rng;
	static uniform_real_distribution<float> distFloat;
	static uniform_int_distribution<int> distInt;

public:
	static void initialize();
	static void initialize(unsigned long seed);
	static sf::Uint32 getRandomSeed();

	static float getInRange(float min, float max);
	static float getInRange(float min, float max, int rolls);
	static int getInRangeI(int min, int max);
	static int getInRangeI(int min, int max, int rolls);
	static int getChoiceFromBackground(vector<float> backgroundProbabilities);

	static bool occurAtChance(float chance);
	static bool occurAtChancePerSecond(float chance, float frameTime);

	static float randomSign();
	
	static Vector2f getRandomPosInCircle(Vector2f center, float radius);
	static Vector2f getRandomPosInSquare(Vector2f center, float side);
};

#endif //CYBERPUNK_CRANDOM_HPP
