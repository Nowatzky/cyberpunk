#ifndef CYBERPUNK_CCOLLISIONCONTAINER_HPP
#define CYBERPUNK_CCOLLISIONCONTAINER_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "CCollisionChannel.hpp"
#include "tinyc2.h"

using namespace sf;

class CCollisionContainer {
protected:
	Vector3f pos;
	float rot;
	CCollisionChannel collisionChannel;

public:
	CCollisionContainer(Vector3f pos) :
			pos(pos),
			rot(0) {}

	CCollisionContainer(Vector3f pos, float rot) :
			pos(pos),
			rot(rot) {}

	virtual bool doesIntersect(const CCollisionContainer& other) const = 0;

	virtual const c2Poly* getPoly() const = 0;

	/* Accessors */
	virtual void setPosition(Vector3f pos);
	virtual void setRotation(float rot);

	virtual void move(Vector3f movement);
	virtual void move(Vector2f movement);
	virtual void rotate(float angle);

	virtual Vector3f getPosition() const;
	virtual float getRotation() const;

	void setCollisionChannel(CCollisionChannel collisionChannel);
	CCollisionChannel* getCollisionChannel();
	bool matchCollisionChannel(CCollisionChannel channel) const;
};

#endif //CYBERPUNK_CCOLLISIONCONTAINER_HPP
