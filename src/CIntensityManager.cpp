#include <iostream>
#include "CIntensityManager.hpp"
#include "CEnemy.hpp"
#include "CSceneManager.hpp"

CIntensityManager::CIntensityManager() {
	intensityData.open("./statistics/intensityDistribution.tsv", ios::out);
}

void CIntensityManager::tick(float frameTime) {
	updateIntensity(frameTime);
}


float CIntensityManager::getSuggestedSpawnInterval() {
	return bridgeFunction();
}

//private:
float CIntensityManager::updateIntensity(float frameTime) {

	//Intensity from present enemies
	float enemyIntensity = 0;
	for(CEnemy* e : CSceneManager::getEnemies()) {
		enemyIntensity += e->metaData[2];
	}

	//Intensity from missing health (absolut factor)
	float playerHealthFactor = CSceneManager::getPlayers()[0]->getMaxHealth() - CSceneManager::getPlayers()[0]->getHealth();

	//Combine
	intensity = intensityModifierEnemies * enemyIntensity + intensityModifierPlayerHealth * playerHealthFactor;
	if (intensity < 0)
		intensity = 0;

	intensityData << frameTime << "\t" << intensity << "\t" << getSuggestedSpawnInterval() << endl;
	return 0;
}

float CIntensityManager::bridgeFunction() {
	return minSpawnInterval + spawnIntervalPeakIncrease * intensity / peak_intensity;
}

CIntensityManager::~CIntensityManager() {
	intensityData.close();
}

float CIntensityManager::getIntensity() {
	return intensity;
}
