#ifndef CYBERPUNK_CGUIELEMENT_HPP
#define CYBERPUNK_CGUIELEMENT_HPP

#include <SFML/Graphics/RenderTarget.hpp>

class CGuiElement {
protected:
	bool isVisible = true;
	
public:
	virtual void draw(sf::RenderTarget* renderTarget) = 0;

	virtual void tick(float frameTime) = 0;
	
	virtual ~CGuiElement();
	
	void setIsVisible(bool isVisible);
	bool getIsVisible();
};

#endif //CYBERPUNK_CGUIELEMENT_HPP
