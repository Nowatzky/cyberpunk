#include "CMessageObjTranslationUpdate.hpp"
#include "CDebugHelper.hpp"
#include "CSceneManager.hpp"

void CMessageObjTranslationUpdate::serializeInto(sf::Packet& packet) {
	packet << objectId
	       << translationSnapshot.gameTime 
           << translationSnapshot.pos.x
           << translationSnapshot.pos.y
           << translationSnapshot.pos.z
           << translationSnapshot.rot;
}

void CMessageObjTranslationUpdate::deserializeFrom(sf::Packet& packet) {
	packet >> objectId
	       >> translationSnapshot.gameTime
	       >> translationSnapshot.pos.x
           >> translationSnapshot.pos.y
           >> translationSnapshot.pos.z
           >> translationSnapshot.rot;
}

void CMessageObjTranslationUpdate::process() {
	CObject* object = CSceneManager::getObject(objectId);

	if(object != nullptr) {
		object->updateTranslationSnapshotBuffer(translationSnapshot);
	}
}
