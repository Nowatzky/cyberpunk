#ifndef CYBERPUNK_CSKILLHEALTHBAIT_HPP
#define CYBERPUNK_CSKILLHEALTHBAIT_HPP

#include "CSkill.hpp"

class CSkillHealthBait : public CSkill {
protected:
public:
	CSkillHealthBait(CPlayer *owner) :
			CSkill(owner, 250, 50) {};

protected:
	void onButtonPress();

private:
	void spawnHealthBait();


};

#endif //CYBERPUNK_CSKILLHEALTHBAIT_HPP
