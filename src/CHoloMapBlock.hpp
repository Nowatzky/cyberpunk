#ifndef CYBERPUNK_CHOLOMAPBLOCK_HPP
#define CYBERPUNK_CHOLOMAPBLOCK_HPP


#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"
#include "CRectangleArea.hpp"

class CHoloMapBlock : public CObject {
	CBoundingBox boundingBox;
	CGraphicBox graphicBox;

	Color colorBaseNormal;
	Color colorAccentNormal;
	CRectangleArea blockedArea;

	Clock clockLifetime;
	float fadeDuration = 1000;
	float lifeTime = 20000;

public:
	CHoloMapBlock(Vector3f pos, Color colorBase, Color colorAccent);

	~CHoloMapBlock();
	virtual void tick(float frameTime);

	// Getter & Setter
	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();

};


#endif //CYBERPUNK_CHOLOMAPBLOCK_HPP
