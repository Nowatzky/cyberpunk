#include "CGuiLobbyDifficulty.hpp"
#include "CGuiManager.hpp"
#include "CLobby.hpp"

CGuiLobbyDifficulty::CGuiLobbyDifficulty(CLobby* lobby) :
	lobby(lobby) {

	text = sf::Text("", CGuiManager::getFontDisplay(), 1);
	text.setStyle(sf::Text::Style::Bold);
}

void CGuiLobbyDifficulty::tick(float frameTime) {
	CDifficulty difficulty = lobby->getDifficulty();
	
	text.setString(getDifficultyText(difficulty));
	text.setColor(getDifficultyColor(difficulty));
	text.setCharacterSize(getDifficultyTextSize(difficulty));
	//text.setRotation(getDifficultyRotation(difficulty));

	text.setOrigin(Vector2f(text.getLocalBounds().width, text.getLocalBounds().height) * 0.5f);
	text.setPosition(CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.5f)));
}

void CGuiLobbyDifficulty::draw(sf::RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(text);
	}
}

sf::Color CGuiLobbyDifficulty::getDifficultyColor(CDifficulty difficulty) const {
	switch(difficulty) {
		case CDifficulty::DEBUG:
			return sf::Color(100, 100, 120, 130);
		case CDifficulty::EASY:
			return sf::Color(250, 100, 170, 180);
		case CDifficulty::MEDIUM:
			return sf::Color(100, 100, 220, 180);
		case CDifficulty::HARD:
			return sf::Color(255, 50, 0, 180);
	}
}

unsigned int CGuiLobbyDifficulty::getDifficultyTextSize(CDifficulty difficulty) const {
	unsigned int size;
	
	switch(difficulty) {
		case CDifficulty::DEBUG:
			size = 140;
			break;
		case CDifficulty::EASY:
			size = 155;
			break;
		case CDifficulty::MEDIUM:
			size = 200;
			break;
		case CDifficulty::HARD:
			size = 320;
			break;
	}

	return static_cast<unsigned int>(size * CGuiManager::getGuiFontSizeFactor());
}

std::string CGuiLobbyDifficulty::getDifficultyText(CDifficulty difficulty) const {
	switch(difficulty) {
		case CDifficulty::DEBUG:
			return "Debug";
		case CDifficulty::EASY:
			return "Easy";
		case CDifficulty::MEDIUM:
			return "Medium";
		case CDifficulty::HARD:
			return "Hard";
	}
}

float CGuiLobbyDifficulty::getDifficultyRotation(CDifficulty difficulty) const {
	switch(difficulty) {
		case CDifficulty::DEBUG:
			return -25;
		case CDifficulty::EASY:
			return -28;
		case CDifficulty::MEDIUM:
			return -30;
		case CDifficulty::HARD:
			return -33;
	}
}

CGuiLobbyDifficulty::~CGuiLobbyDifficulty() {
	CGuiManager::removeGuiElement(this);
}

