#ifndef CYBERPUNK_CDEBUGTEXT_HPP
#define CYBERPUNK_CDEBUGTEXT_HPP


#include <SFML/Graphics/Text.hpp>
#include "CGuiElement.hpp"

class CDebugText : public CGuiElement {
private:
	sf::Text text;

public:
	CDebugText();
	
	sf::Text* getText();
	
	virtual void draw(sf::RenderTarget* renderTarget);
	virtual void tick(float frameTime) {};

};


#endif //CYBERPUNK_CDEBUGTEXT_HPP
