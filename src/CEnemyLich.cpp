#include "CEnemyLich.hpp"
#include "CProjectileRock.hpp"
#include "CSceneManager.hpp"
#include "CProjectileSpecterBullet.hpp"
#include "CSpecterDmgField.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CObjectSpectralDust.hpp"
#include "CUtilityDefines.hpp"
#include "CObjectProjectileHitPopUpBox.hpp"
#include "CLineEffect.hpp"

void CEnemyLich::tick(float frameTime) {
    CEnemy::tick(frameTime);
    healHealth(healthRegeneration * frameTime);
    movementSpeed = baseSpeed + CEngine::invertAlpha(CEngine::clampAlpha(health/maxHealth)) * speedIncreaseOnMissingHealth;

	if(isDodging) {
		if(clockDodging.getTimeMs() >= dodgeDuration) {
			isDodging = false;
			graphicBox.setColorBase(colorBase);
			executeDodge();
		} else {
			float alpha = CEngine::invertAlpha(CEngine::clampAlpha(clockDodging.getTimeMs() / dodgeDuration));
			Color color = colorBase;
			color.a = (Uint8) CEngine::lerp(40, color.a, alpha);
			graphicBox.setColorBase(color);
		}
	}
}

bool CEnemyLich::isAnySpellCastReady() {
    return cooldownCastAnySpell <= clockCastAnySpellCooldown.getTimeMs() && !effectManager.hasStun() && !isDodging;
}

void CEnemyLich::deployFireSpell() {
	if(!isAnySpellCastReady()) {
		return;
	}
    clockCastAnySpellCooldown.restart();

    Vector3f targetPos = CSceneManager::getPlayers()[0]->getPosition();
    Vector2f dir = CEngine::normalize(Vector2f(targetPos.x - this->getPosition().x, targetPos.y - this->getPosition().y));

    CProjectileSpecterBullet* p = new CProjectileSpecterBullet(this, this->getPosition(), dir, fireSpellDamage);
    p->setRotation(CEngine::getRotation(dir));
	p->getGraphicContainer()->rotate(45);
	p->move(dir * 0.75f);
	CSceneManager::safeAddObject(p);
}

void CEnemyLich::spawnDmgField() {
	if(!isAnySpellCastReady()) {
		return;
	}
	clockCastAnySpellCooldown.restart();
		
    Vector3f currentPlayerPosition = CSceneManager::getPlayers()[0]->getPosition();

    Vector2f playerMovingDir = CEngine::normalize(CSceneManager::getPlayers()[0]->getMovementDirection());
    Vector2f spawnPos = CEngine::to2D(currentPlayerPosition) + playerMovementOffset * playerMovingDir;

	if(!CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(currentPlayerPosition))->isInside(spawnPos)) {
		spawnPos = CEngine::to2D(currentPlayerPosition) + (playerMovementOffset * playerMovingDir * 0.5f);
	}

    CSpecterDmgField* field = new CSpecterDmgField(Vector3f(spawnPos.x, spawnPos.y, 1.f), dmgFieldDimension, dmgFieldTriggerDuration);
	CSceneManager::safeAddObject(field);
}

bool CEnemyLich::isCasting() {
    return clockCastDuration.getElapsedTime().asMilliseconds() < spellCastDuration[static_cast<int>(currentSpell)];
}

void CEnemyLich::beginCasting(LichSpell spell) {
    isActing = true;
    currentSpell = spell;

    switch (currentSpell) {
	    case LichSpell::FIRE:
		    deployFireSpell();
		    break;
	    case LichSpell::DMG_FIELD:
		    spawnDmgField();
            break;
    }

    clockCastDuration.restart();
}

void CEnemyLich::deployCurrentSpell() {
    switch (currentSpell) {
	    case LichSpell::FIRE:
            deployFireSpell();
            break;
	    case LichSpell::DMG_FIELD:
            spawnDmgField();
            break;
    }
    clockCastAnySpellCooldown.restart();
    isActing = false;
}

void CEnemyLich::spawnSpectralDust() {
	Vector3f spawnPos = getPosition();
	spawnPos.z = 0.5f + 0.2f;
	CObjectSpectralDust* specterDust = new CObjectSpectralDust(spawnPos);
	specterDust->setLifetime(spectralDustLifetime);
	CSceneManager::safeAddObject(specterDust);
}

void CEnemyLich::die() {
	spawnSpectralDust();
	CEnemy::die();
}

void CEnemyLich::dodge() {
	if(!isDodgeReady()) {
		return;
	}
	
	isDodging = true;
	clockDodging.restart();	
}

bool CEnemyLich::isDodgeReady() {
	return clockDodgeCooldown.getTimeMs() >= dodgeCooldown && !isCasting() && !getEffectManager()->hasStun() && !isDodging;
}

void CEnemyLich::executeDodge() {
	clockDodgeCooldown.restart();
	isDodging = false;

	Vector3f teleportPos = findDodgePosition();

	if(teleportPos != VECTOR3D_INVALID) {
		Color color = graphicBox.getColorBase();
		color.a = 40;
		
		CObjectProjectileHitPopUpBox*decoy = new CObjectProjectileHitPopUpBox(getPosition(), graphicBox.getDimension(), color, graphicBox.getColorAccent(), 700);
		CSceneManager::safeAddObject(decoy);
		CLineEffect * line = new CLineEffect(decoy, this, color);
		line->setLifetime(700);
		CSceneManager::safeAddObject(line);

		teleport(teleportPos);
	} else {
		clockDodgeCooldown.restart(dodgeCooldown);
	}
}

bool CEnemyLich::getIsDodging() const {
	return isDodging;
}

Vector3f CEnemyLich::findDodgePosition() {
	Vector3f dodgePos;
	Vector3f origin = getPosition();
	
	CNavMesh* navMesh = CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(origin));
	if(navMesh == nullptr) {
		return VECTOR3D_INVALID;
	}

	CCollisionChannel collisionChannel = {CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER};
	Vector3f dim = boundingBox.getDimension();
	dim.z = 0.1f;
	CBoundingBox box(origin, dim, getRotation());
	
	Vector2f baseDir = CEngine::to2D(origin - CSceneManager::getPlayers()[0]->getPosition());
	float baseRot = CEngine::getRotation(baseDir);
	
	float minRadius = 0.75f;
	float radiusStep = 0.25f;

	int maxAngleOffsetSteps = 3;
	float angleOffsetPerStep = 110.f/(float)maxAngleOffsetSteps;
	
	bool dodgePosFound = false;
	float radius = dodgeDistance;
	
	while(!dodgePosFound && radius > minRadius) {
		int i = 0;
		while(i <= maxAngleOffsetSteps * 2) { // <= because 0 is included as first step
			float angleOffsetFactor;
			
			if(i == 0) {
				angleOffsetFactor = 0;
			} else if(i % 2 == 0) {
				angleOffsetFactor = -(i/2);
			} else {
				angleOffsetFactor = (i+1)/2;
			}
			
			float angle = static_cast<float>(fmod(baseRot + (angleOffsetFactor * angleOffsetPerStep), 360));
			dodgePos = origin + CEngine::toPlanar3D(CEngine::getUnitInDirection(angle) * radius);
			
			cout << "Tried to dodge with angle = " << angleOffsetFactor * angleOffsetPerStep << " and radius = " << radius << endl;

			if(!navMesh->isInside(dodgePos)) {
				i++;
				continue;
			}

			box.setPosition(dodgePos);

			if(CSceneManager::getIntersecting(&box, collisionChannel).empty()) {
				dodgePosFound = true;
				break;
			}
			
			i++;  
		}
		
		radius -= radiusStep;
	}

	if(dodgePosFound) {
		return dodgePos;
	}
	return VECTOR3D_INVALID;
}   


