#include "CEffectSpecterPull.hpp"

CEffectSpecterPull::~CEffectSpecterPull() {
	if(lineEffect != nullptr) {
		CSceneManager::safeRemoveObject(lineEffect);
	}
}

bool CEffectSpecterPull::tick(float frameTime, CEffectAggregate& effectAggregate) {
	if(hasExpiredLifetime()) {
		if(!isPulling) {
			isPulling = true;
			lifetime = pullDuration;
			lifetimeClock.restart();
		} else {
			return false;
		}
	}

	if(!CSceneManager::doesObjectExist(initiator)) {
		return false;
	}

	if(isPulling) {
		float alpha = CEngine::invertAlpha(CEngine::clampAlpha(lifetimeClock.getTimeMs()/lifetime));
		float currentPullStrength;
		if(alpha >= 0.5f) {
			currentPullStrength = pullStrength;
		} else {
			currentPullStrength = pullStrength * (alpha * 2.f);
		}

		Vector2f direction = CEngine::normalize(CEngine::to2D(initiator->getPosition() - owner->getPosition()));

		effectAggregate.hasForcePush = true;
		effectAggregate.forcePush += direction * currentPullStrength;
	} 
	
	effectAggregate.movementSpeedModifier = 0.f;

	return true;
}

void CEffectSpecterPull::onStart() {
	lineEffect = new CLineEffect(initiator, owner, Color(40, 40, 120, 230));
	lineEffect->setLifetime(10000);
	CSceneManager::safeAddObject(lineEffect);
}

std::vector<const sf::Texture*> CEffectSpecterPull::getVisuals() {
	vector<const sf::Texture*> textures;

	textures.push_back(&CTextureManager::getTexture("EffectPulled"));

	return textures;
}

int CEffectSpecterPull::getVisualPriority() {
	return 3;
}



