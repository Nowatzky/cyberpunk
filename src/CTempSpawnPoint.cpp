#include "CTempSpawnPoint.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CRpcHandler.hpp"

CTempSpawnPoint::CTempSpawnPoint(Vector2f pos) :
		CObject(Vector3f(pos.x, pos.y, 1.301f)),
		boundingBox(Vector3f(pos.x, pos.y, 1.301f), Vector3f(0.2f, 0.2f, 0.8f)),
		graphicBox(Vector3f(pos.x, pos.y, 1.301f), Vector3f(0.2f, 0.2f, 0.8f), colorBaseNormal, colorAccentNormal) {

	Vector2f dim = Vector2f(boundingBox.getDimension().x, boundingBox.getDimension().y);

	blockedArea = CRectangleArea(pos - dim, dim * 2.f);
	CSceneManager::getMap()->getNavMeshForPosition(pos)->blockArea(blockedArea);

	boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);

	riseOffsetX = CRandom::getInRange(-3,3);
	riseOffsetY = CRandom::getInRange(-3,3);	
}

CTempSpawnPoint::~CTempSpawnPoint() {
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->freeArea(blockedArea);
}

void CTempSpawnPoint::tick(float frameTime) {
	if(isRising) {
		float alpha = clockRise.getElapsedTime().asMilliseconds()/riseDuration;

		if(alpha >= 1) {
			alpha = 1;
			isRising = false;
		}

		graphicBox.setPosOffset(Vector3f(CEngine::lerp(riseOffsetX, 0, alpha), CEngine::lerp(riseOffsetY, 0, alpha), CEngine::lerp(40, 0, alpha)));
		graphicBox.setScaleOffset(CEngine::lerp(0.4f, 1, alpha));
	}

	// Glow
	glowingTimeOffset += frameTime;
	float sinus = sin(glowingTimeOffset * glowingFrequency);
	float offsetR = 0.6f * glowingAmplitude * sinus;
	float offsetG = 0.2f * glowingAmplitude * sinus;
	float offsetB = 1.f  * glowingAmplitude * sinus;
	float scaleOffset = glowingScaleAmplitude * sinus;

	Color colorBase = colorBaseNormal;
	colorBase.r = (Uint8) CEngine::clamp(colorBase.r + offsetR, 0, 255);
	colorBase.g = (Uint8) CEngine::clamp(colorBase.g + offsetG, 0, 255);
	colorBase.b = (Uint8) CEngine::clamp(colorBase.b + offsetB, 0, 255);
	graphicBox.setColorBase(colorBase);

	Color colorAccent = colorAccentNormal;
	colorAccent.r = (Uint8) CEngine::clamp(colorAccent.r + offsetR, 0, 255);
	colorAccent.g = (Uint8) CEngine::clamp(colorAccent.g + offsetG, 0, 255);
	colorAccent.b = (Uint8) CEngine::clamp(colorAccent.b + offsetB, 0, 255);
	graphicBox.setColorAccent(colorAccent);

	graphicBox.setScaleOffset(1 + scaleOffset);

	if(isDespawning) {
		if(clockDespawning.getElapsedTime().asMilliseconds() >= despawnTime) {
			CSceneManager::safeRemoveObject(this);
			return;
		}

		float despawnAlpha = clockDespawning.getElapsedTime().asMilliseconds()/despawnTime;
		despawnAlpha = CEngine::clampAlpha(despawnAlpha);

		Color currentColorBase = graphicBox.getColorBase();
		currentColorBase.a = (Uint8) CEngine::lerp(255, 0, despawnAlpha);
		graphicBox.setColorBase(currentColorBase);

		Color currentColorAccent = graphicBox.getColorAccent();
		currentColorAccent.a = (Uint8) CEngine::lerp(255, 0, despawnAlpha);
		graphicBox.setColorAccent(currentColorAccent);
	}
}

void CTempSpawnPoint::deletePoint() {
	isDespawning = true;
	clockDespawning.restart();
	
	CRpcHandler::sendSpawnPointDelete(this);
}

void CTempSpawnPoint::deletePointRPC() {
	isDespawning = true;
	clockDespawning.restart();
}

CCollisionContainer* CTempSpawnPoint::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer* CTempSpawnPoint::getGraphicContainer() {
	return &graphicBox;
}
