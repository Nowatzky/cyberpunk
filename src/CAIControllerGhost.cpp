#include "CAIControllerGhost.hpp"
#include "CEnemyGhost.hpp"
#include "CSceneManager.hpp"
#include "CObjectSpectralDust.hpp"

void CAIControllerGhost::tick(float frameTime) {
	CObject* target;
	
	if(host->isCalledToDust()) {
		target = host->getDust();
	} else {
		CObject* nearestTarget = CSceneManager::getTargetablePlayers().at(0);
		float dist = CEngine::getDistance(host->getPosition(), nearestTarget->getPosition());
		
		for(CObject* object : CSceneManager::getTargetablePlayers()) {
			if(CEngine::getDistance(host->getPosition(), object->getPosition()) < dist) {
				dist = CEngine::getDistance(host->getPosition(), object->getPosition());
				nearestTarget = object;				
			}
		}
		
		target = nearestTarget;
	}
	if(prevTarget != target) {
		host->findPathTo(CEngine::to2D(target->getPosition()));
		prevTarget = target;
	}
	
	host->updatePath(CEngine::to2D(target->getPosition()));
	host->followPath(frameTime);

	// Dashing
	if(host->isDashReady()) {
		if(isWayToTargetFree(target)) {
			host->dash();
		}
	}
}

bool CAIControllerGhost::isWayToTargetFree(CObject* target) const {
	float dist = CEngine::getDistance(host->getPosition(), target->getPosition());
	if(dist <= 5.f && abs(CEngine::getAngleToTarget(host, target->getPosition())) <= 20) {
		Vector3f pos = (host->getPosition() + target->getPosition()) * 0.5f;
		Vector3f dim = Vector3f(host->getAgentHalfWidth() * 1.5f, dist / 2.f, 0.25f);
		Vector2f dirToTarget = CEngine::to2D(target->getPosition() - host->getPosition());
		float rot = CEngine::getRotation(dirToTarget);

		CBoundingBox box(pos, dim, rot);
		CCollisionChannel collisionChannel({CCollisionChannel::Channel::CHARACTER, CCollisionChannel::Channel::STATIC,
		                                    CCollisionChannel::Channel::PROJECTILE});

		for(CObject* colliding : CSceneManager::getIntersecting(&box, collisionChannel)) {
			if(colliding != target && colliding != host) {
				return false;
			}
		}
		return true;
	}
	return false;
}