#ifndef CYBERPUNK_CENGINE_HPP
#define CYBERPUNK_CENGINE_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics/Color.hpp>
#include "CGraphicContainer.hpp"
#include "CDirection.hpp"
#include "CGraphicArea.hpp"

extern "C" {
	#include "tinyc2.h"
}

/* Forward declare */
class CObject;
class CBoundingBox;
class CGraphicBox;

using namespace sf;

#define TO_RAD 0.01745329252f
#define TO_DEG 57.2957795131f

#define EPS 1e-6

//#define TILE_SIZE 64

// Universal sign function
template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

class CEngine {
private:
	static long uniqueId;
	
public:
	static float tile_size;
	static float angle;
	static float light_angle;
	
	/* ID Management */
	static long getUniqueId();
	
    /* Isometric Conversion */
    static Vector2f toIso(const Vector3f& v);
    static float getDepth(const Vector3f& v);

	static Vector3f to3DOnPlane(const Vector2f& pos, float z);
	static Vector2f to2D(const Vector3f& v);

    /* Vector Math */
    static float getLength(const Vector2f& v);
    static float getLength(const Vector3f& v);
    static Vector2f normalize(const Vector2f& v);
	static Vector3f normalize(const Vector3f& v);
	static float getDistance(Vector2f v1, Vector2f v2);
	static float getDistance(Vector3f v1, Vector3f v2);
	static float getDistanceSquared(Vector3f v1, Vector3f v2);
	static float getDimensionSquared(Vector3f v1, Vector3f v2);

    static Vector3f toPlanar3D(const Vector2f& v);
	static Vector2f rotateZ(const Vector2f& v, float angle);
	static Vector3f rotateZ(const Vector3f& v, float angle);
	static Vector2f rotateZLeft90(const Vector2f& v);
	static Vector2f rotateZRight90(const Vector2f& v);
	
	static float rotate(float rot, float angle);
	static float getRotation(Vector2f& v);
	static float getAngleDifference(Vector2f& v1, Vector2f& v2); // Angle from v1 to v2, positive clockwise
	static float getAngleDifference(float a1, float a2);
	static float getAngleToTarget(CObject* object, Vector3f target);
	static float getAngleToTarget(CObject* object, Vector2f target);

	static Vector2f getUnitInDirection(float angle);
	static Vector3f getUnitInDirection3D(float angle);
	
	static float getDotProduct(Vector2f& v1, Vector2f& v2);
	static float getDotProduct(Vector3f& v1, Vector3f& v2);
	
	static bool areInSameDirection(Vector2f v1, Vector2f v2);
	static bool areInOppositeDirection(Vector2f v1, Vector2f v2);
	static bool isFurtherInDirection(Vector2f v, Vector2f vReference, Vector2f dir);
	static bool areLineSegmentsParallelOrColinear(Vector2f l1Start, Vector2f l1End, Vector2f l2Start, Vector2f l2End);
	
	static bool doLineSegmentsIntersect(Vector2f l1Start, Vector2f l1End, Vector2f l2Start, Vector2f l2End);
	static Vector2f getLineSegmentIntersection(Vector2f l1Start, Vector2f l1End, Vector2f l2Start, Vector2f l2End);

	/* Collision */
	static bool doesIntersect(const CBoundingBox& b1, const CBoundingBox& b2);

	/* tinyc2 */
	static c2v toC2v(Vector2f& v);
	static c2v toC2v(Vector2f v);
	static c2v toC2v(Vector3f& v);
	static c2v toC2v(Vector3f v);
	static Vector2f toSFVec(c2v v);

	/* Ordering */
	static bool isInFrontOf(const CGraphicBox& b1, const CGraphicBox& b2);
	static bool isInFrontOf(const CGraphicBox& b1, const CGraphicArea& a1);
	static bool isInFrontOf(const CGraphicArea& a1, const CGraphicBox& b1);
	static bool isInFrontOf(const CGraphicArea& a1, const CGraphicArea& a2);
	static bool doesOverlap(const CGraphicContainer& c1, const CGraphicContainer& c2);

	/* Basic utility */
	static float clamp(float input, float min, float max);
	static float clampAlpha(float input);
	static float lerp(float start, float end, float alpha);
	static Vector3f lerp(const Vector3f& start, const Vector3f& end, float alpha);
	static Color lerp(const Color& start, const Color& end, float alpha);
	
	static Vector2f roundToInt(Vector2f v);
	static float invertAlpha(float alpha);
	
	/* Vector and geometry utility */
	static vector<CDirection> getOutwardDirectionsFromCorner(Vector2f corner, Vector2f center);

	/* Light calculations */
	static Color scaleColor(Color baseColor, float factor);
	static Color getColorFromLightFactor(Color baseColor, float lightFactor);
	static float getLightFactor(float angle);

	struct CompareVector {
		bool operator()(const Vector2f lhs, const Vector2f rhs) const {
			if(lhs.x != rhs.x) {
				return lhs.x < rhs.x;
			} else {
				return lhs.y < rhs.y;
			}
		}
	};

private:
	/* Private Constructor to prevent initialization */
    CEngine() {}
};


#endif //CYBERPUNK_CENGINE_HPP
