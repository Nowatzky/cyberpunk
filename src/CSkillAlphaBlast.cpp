#include "CSkillAlphaBlast.hpp"
#include "CInputManager.hpp"
#include "CProjectileAlphaBlast.hpp"
#include "CSceneManager.hpp"

void CSkillAlphaBlast::tick(float frameTime) {
	if(!isFiring) {
		return;
	} else {
		if(numberOfProjectilesFired >= numberOfProjectiles) {
			isFiring = false;
			return;
		}

		if(firingClock.getTimeMs() >= delay) {
			fireProjectile(targetPos);
		}
	}
}

void CSkillAlphaBlast::onButtonPress() {
	isFiring = true;
	numberOfProjectilesFired = 0;

	Vector2f screenPos = CInputManager::getMousePositionScene();
	targetPos = CEngine::to3DOnPlane(screenPos, owner->getPosition().z);

	fireProjectile(targetPos);
}

CObject* CSkillAlphaBlast::findNearestTarget(Vector2f pos) const {
	CCharacter* currentTarget = nullptr;
	float currentDistance = maxLockOnDistance;

	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		float distance = CEngine::getDistance(pos, CEngine::to2D(enemy->getPosition()));

		if(distance <= maxLockOnDistance && distance <= currentDistance) {
			currentDistance = distance;
			currentTarget = enemy;
		}
	}

	return currentTarget;
}

void CSkillAlphaBlast::fireProjectile(Vector3f pos) {
	firingClock.restart();
	numberOfProjectilesFired++;
	
	CObject* target = findNearestTarget(CEngine::to2D(pos));
	
	CProjectileAlphaBlast* p;
	if(target != nullptr) {
		p = new CProjectileAlphaBlast(owner, owner->getPosition() + Vector3f(0, 0, 0.4f), target);
	} else {
		p = new CProjectileAlphaBlast(owner, owner->getPosition() + Vector3f(0, 0, 0.4f), pos);
	}

	CSceneManager::safeAddObject(p);
}
