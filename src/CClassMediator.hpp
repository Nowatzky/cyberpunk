#ifndef CYBERPUNK_CCLASSMEDIATOR_HPP
#define CYBERPUNK_CCLASSMEDIATOR_HPP


#include "CCharacterClass.hpp"

class CClassMediator : public CCharacterClass {
public:
	CClassMediator(CPlayer* player);

	virtual CSkill* getSkill(SIGN sign);

	virtual Color getBaseColor();
	virtual Color getAccentColor();
};


#endif //CYBERPUNK_CCLASSMEDIATOR_HPP
