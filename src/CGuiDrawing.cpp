#include <iostream>
#include "CGuiDrawing.hpp"
#include "CEngine.hpp"

CGuiDrawing::CGuiDrawing(Vector2f start, Color color) :
		color(color) {
	
	vertices.setPrimitiveType(PrimitiveType::TrianglesStrip);
	prevPoint = start;
}

void CGuiDrawing::draw(sf::RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(vertices);
	}
}

void CGuiDrawing::addPoint(Vector2f currentPoint) {
	Vector2f direction = CEngine::normalize(currentPoint - prevPoint);
	if(direction.x == 0 && direction.y == 0) {
		return;
	}
	
	float halfThickness = 0.5f * CEngine::clamp(CEngine::getDistance(prevPoint, currentPoint), minThickness, maxThickness);
	
	if(isFirstPoint) {
		isFirstPoint = false;
		
		vertices.append(Vertex(prevPoint + CEngine::rotateZLeft90(direction) * halfThickness, color));
		vertices.append(Vertex(prevPoint + CEngine::rotateZRight90(direction) * halfThickness, color));
	}

	vertices.append(Vertex(currentPoint + CEngine::rotateZLeft90(direction) * halfThickness, color));
	vertices.append(Vertex(currentPoint + CEngine::rotateZRight90(direction) * halfThickness, color));
	
	prevPoint = currentPoint;	
}

void CGuiDrawing::tick(float frameTime) {

}

void CGuiDrawing::setMinMaxThickness(float minThickness, float maxThickness) {
	this->minThickness = minThickness;
	this->maxThickness = maxThickness;
}
