#ifndef CYBERPUNK_CRPCHANDLER_HPP
#define CYBERPUNK_CRPCHANDLER_HPP

#include <SFML/Config.hpp>
#include <SFML/System.hpp>
#include <vector>
#include "CObjectIdManager.hpp"
#include "CSkillShotGunFire.hpp"

using sf::Vector2f;
using sf::Vector3f;

class CObject;
class CMessageObjRPC;

enum class CObjRPCType : sf::Uint8 {
	CHARACTER_DIE,                              //
	PLAYER_RELOAD,                              //
	PLAYER_QUICKDRAW,                           //
	PLAYER_MEDIATOR_PRIMARY_FIRE,               //
	PLAYER_SHOTGUNNER_PRIMARY_FIRE,             //
	PLAYER_SNIPER_PRIMARY_FIRE,                 //
	PLAYER_SKILL_FORCE_SWEEP,                   //
	PLAYER_SKILL_CHAIN_FLASH,                   //
	PLAYER_SKILL_CREATE_MAP_BLOCK,              //
	PLAYER_SKILL_HACK_SIGNAL,                   //
	ENEMY_GHOST_EXPLODE,                        //
	ENEMY_GHOST_DASH,                           // 
	PROJECTILE_LASER_BULLET_ON_HIT_ENEMY,       //
	PROJECTILE_HACK_SIGNAL_ON_HIT_ENEMY,        //
	EFFECT_CHAIN_FLASH_TRIGGER_FLASH,           // 
	SPAWN_POINT_DELETE,                         //
};

class CRpcHandler {
public:
	static void receiveMessage(CMessageObjRPC* message);
	
	/* Messages */
	static void sendCharacterDie(CObject* caller);
	static void sendPlayerReload(CObject* caller);
	static void sendPlayerQuickDraw(CObject* caller);
	static void sendPlayerMediatorPrimaryFire(CObject* caller, Vector2f dir, Vector3f originalPosition, std::vector<CObjectId> projectileIds);
	static void sendPlayerShotgunnerPrimaryFire(CObject* caller, Vector2f dir, std::vector<CSkillShotGunFireHitResult> hitResults, std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxes);
	static void sendPlayerSniperPrimaryFire(CObject* caller, Vector3f hitPosition, bool doesHitTarget, CObjectId targetId, float dmg);
	static void sendPlayerSkillForceSweep(CObject* caller, Vector2f dir, Vector3f originalPosition);
	static void sendPlayerSkillChainFlash(CObject* caller, std::vector<CObjectId> targets);
	static void sendPlayerSkillCreateMapBlock(CObject* caller, Vector3f pos);
	static void sendPlayerSkillHackSignal(CObject* caller, Vector2f dir, Vector3f originalPosition, CObjectId projectileId);
	static void sendEnemyGhostExplode(CObject* caller, std::vector<CObjectId> targets, float damage);
	static void sendEnemyGhostDash(CObject* caller);
	static void sendProjectileLaserBulletOnHitEnemy(CObject* caller, CObjectId target, bool addMark, Vector2f dir);
	static void sendProjectileHackSignalOnHitEnemy(CObject* caller, CObjectId target, Vector2f dir);
	static void sendEffectChainFlashTriggerFlash(CObject* caller, std::vector<CObjectId> targetIds);
	static void sendSpawnPointDelete(CObject* caller);
	
private:
	static CMessageObjRPC* createMessage(CObject* caller, CObjRPCType rpcType);

};


#endif //CYBERPUNK_CRPCHANDLER_HPP
