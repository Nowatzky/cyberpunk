#include "CEnemyGhostSummoner.hpp"
#include "CSceneManager.hpp"
#include "CStatisticsManager.hpp"
#include "CUtilityDefines.hpp"

void CEnemyGhostSummoner::tick(float frameTime) {
	CEnemy::tick(frameTime);

	if(isSummoning && clockSummonDuration.getElapsedTime().asMilliseconds() >= summonDuration) {
		clockSummonCooldown.restart();
		isSummoning = false;
	}
}

void CEnemyGhostSummoner::summon() {
	if(clockSummonCooldown.getElapsedTime().asMilliseconds() >= cooldownSummon) {
		CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
		Vector3f spawnPos = CSceneManager::findSpawnPoint(getPosition(), Vector3f(0.4f, 0.4f, 0.35f), collisionChannel, 0, 0.75f, 5, 0.5f);

		if(spawnPos == VECTOR3D_INVALID) {
			return;
		}

		clockSummonDuration.restart();
		isSummoning = true;

		spawnPos.z = 1;

		Vector2f spawnDir = CEngine::to2D(spawnPos - getPosition());
		float angle = CEngine::getRotation(spawnDir);

		bool invisible = CRandom::occurAtChance(summonInvisibleGhostChance);
		summonPoint = new CGhostSummonPoint(spawnPos, summonDuration, angle, invisible);
		if(invisible) //passing on respective meta data
			summonPoint->metaDataGhost = metaDataInvisibleGhost;
		else
			summonPoint->metaDataGhost = metaDataGhost;

		CSceneManager::safeAddObject(summonPoint);
        cooldownSummon += cooldownSummonIncrease;
	}
}

bool CEnemyGhostSummoner::isSummonReady() const {
	return clockSummonCooldown.getElapsedTime().asMilliseconds() >= cooldownSummon && !effectManager.hasStun();
}

bool CEnemyGhostSummoner::isHostSummoning() const {
	return isSummoning;
}

void CEnemyGhostSummoner::die() {
	if(isSummoning) {
		CSceneManager::safeRemoveObject(summonPoint);
	}

	CStatisticsManager::scorePoints(15);

	CEnemy::die();
}

bool CEnemyGhostSummoner::canBePushedByBullet() {
	return !isSummoning;
}

void CEnemyGhostSummoner::onPushed() {
	if(isSummoning) {
		isSummoning = false;
		clockSummonCooldown.restart();
		CSceneManager::safeRemoveObject(summonPoint);
	}
}

void CEnemyGhostSummoner::onStunned() {
	if(isSummoning) {
		isSummoning = false;
		clockSummonCooldown.restart();
		CSceneManager::safeRemoveObject(summonPoint);
	}
}
