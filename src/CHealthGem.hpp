#ifndef CYBERPUNK_CHEALTHGEM_HPP
#define CYBERPUNK_CHEALTHGEM_HPP


#include "CObject.hpp"
#include "CGraphicBox.hpp"
#include "CBoundingBox.hpp"
#include "CEnemy.hpp"
#include "CPlayer.hpp"

class CHealthGem : public CObject {
protected:
	CBoundingBox boundingBox;
	CGraphicBox graphicBox;

	Vector2f dir;
	float rotationSpeed = 45;
	float lifetime;
	float healthBonus;
	bool isBeingDrained;

	CClock clockLifetime;

public:
	CHealthGem(Vector3f pos, Vector3f dim, Color color, float healthBonus, float lifetime = 30000) :
			CObject(pos, rot),
			boundingBox(pos, dim),
			graphicBox(pos, dim, color),
	        lifetime(lifetime),
	        healthBonus(healthBonus) {}


	virtual void tick(float frameTime);

	CCollisionContainer* getCollisionContainer();
	CGraphicContainer* getGraphicContainer();

	void onHitPlayer(CPlayer *player, float frameTime);
	void setAsBeingDrained();
	float getHealthBonus();
};


#endif //CYBERPUNK_CHEALTHGEM_HPP
