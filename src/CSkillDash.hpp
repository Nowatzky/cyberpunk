#ifndef CYBERPUNK_CSKILLDASH_HPP
#define CYBERPUNK_CSKILLDASH_HPP

#include "CPlayer.hpp"
#include "CSkill.hpp"

class CSkillDash : public CSkill {
private:
	bool isDashing = false;
	Vector2f dir;
	CClock clock;
	
	float duration = 340;
	float strength = 250;
	float offset = 2.25f;
	
	float slowReduce = 0.66f;
		
public:
	CSkillDash(CPlayer* owner) :
		CSkill(owner, 650, 35) {}

protected:
	void onButtonPress();
	void tick(float frameTime);

};


#endif //CYBERPUNK_CSKILLDASH_HPP
