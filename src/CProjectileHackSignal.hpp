#ifndef CYBERPUNK_CPROJECTILEHACKSIGNAL_HPP
#define CYBERPUNK_CPROJECTILEHACKSIGNAL_HPP


#include "CProjectile.hpp"
#include "CFlatCircle.hpp"

class CProjectileHackSignal : public CProjectile {
private:

	float hackingDuration = 15000;


	float dotDistance = 0.5f;
	float dotDistanceOffset = 0.25f;
	float circleRadius = 0.25f;
	float dotRadius = 0.1f;

	vector<CFlatCircle*> trailVisuals;
	Color colorBase;
	Color colorAccent;

public:
	CProjectileHackSignal(CCharacter* initiator, const Vector3f &pos, const Vector2f &dir, Color colorBase, Color colorAccent);
	~CProjectileHackSignal();

	virtual void tick(float frameTime);

	void onHitEnemy(CEnemy* target, float frameTime);
	void onHitEnemyRPC(CEnemy *target);
};


#endif //CYBERPUNK_CPROJECTILEHACKSIGNAL_HPP
