#ifndef CYBERPUNK_CTRANSLATIONSNAPSHOTBUFFER_HPP
#define CYBERPUNK_CTRANSLATIONSNAPSHOTBUFFER_HPP


#include "CTranslationSnapshot.hpp"

class CTranslationSnapshotBuffer {
private:
	std::vector<CTranslationSnapshot> buffer;
	
	const float maxEntryAge = 2000;
	float ping = 0;

public:
	CTranslationSnapshotBuffer() {};

	void insert(CTranslationSnapshot snapshot);
	CTranslationSnapshot interpolateForTime(float gameTime) const;
	float getPing() const;

};


#endif //CYBERPUNK_CTRANSLATIONSNAPSHOTBUFFER_HPP
