#include "CSkillDash.hpp"

void CSkillDash::onButtonPress() {
	if(!isDashing) {
		isDashing = true;
		clock.restart();
		dir = owner->getMovementDirection();
		owner->setAllowOwnMovement(false);
		owner->reloadPrimarySkill();
	}
}

void CSkillDash::tick(float frameTime) {
	if(isDashing) {
		float remainingTime = (duration - clock.getTimeMs()) / 1000.f;
		float currentSpeed = owner->getMaxMovementSpeed() - offset + (remainingTime * remainingTime * strength);
		float movementSpeedModifier = owner->getEffectManager()->getMovementSpeedModifier();
		if(movementSpeedModifier < 1) {
			movementSpeedModifier = CEngine::clampAlpha(movementSpeedModifier / slowReduce);
		}
		
		bool wasBlocked = !owner->move(dir * currentSpeed * frameTime * movementSpeedModifier, false, false);
		
		if(clock.getTimeMs() >= duration || wasBlocked) {
			isDashing = false;
			owner->setAllowOwnMovement(true);
			owner->setMovementSpeed(min(currentSpeed, owner->getMaxMovementSpeed()));
		}		
	}

}
