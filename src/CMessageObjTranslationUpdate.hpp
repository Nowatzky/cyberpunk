#ifndef CYBERPUNK_CMESSAGEOBJTRANSLATIONUPDATE_HPP
#define CYBERPUNK_CMESSAGEOBJTRANSLATIONUPDATE_HPP


#include <SFML/Window.hpp>
#include "CMessage.hpp"
#include "CObject.hpp"
#include "CTranslationSnapshot.hpp"

class CMessageObjTranslationUpdate : public CMessage {
public:
	CObjectId objectId;
	CTranslationSnapshot translationSnapshot;

	CMessageObjTranslationUpdate() :
			CMessage(CMessageType::OBJ_TRANSLATION_UPDATE) {}

	virtual void serializeInto(sf::Packet& packet);
	virtual void deserializeFrom(sf::Packet& packet);
	virtual void process();

};


#endif //CYBERPUNK_CMESSAGEOBJTRANSLATIONUPDATE_HPP
