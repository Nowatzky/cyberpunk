#include "CLobby.hpp"
#include "CNetworkManager.hpp"
#include "CRenderManager.hpp"
#include "CGameClock.hpp"
#include "CDebugHelper.hpp"
#include "CGuiManager.hpp"
#include "CRandom.hpp"
#include <fstream>
#include <stdlib.h>

CLobby::CLobby(bool isHost, sf::RenderWindow* window) :
		isHost(isHost),
		window(window),
		lobbyPlayerList(this),
		lobbyDifficulty(this) {

	name = readNameFromFile();
	className = CClassName::MEDIATOR;
	difficulty = CDifficulty::MEDIUM;

	CGuiManager::addGuiElement(&lobbyDifficulty);
	CGuiManager::addGuiElement(&lobbyPlayerList);
}

void CLobby::enterLobby() {
	if(isHost) {
		CNetworkManager::initNetworkSocket(CNetworkRole::HOST);
	} else {
		CNetworkManager::initNetworkSocket(CNetworkRole::PEER);
	}

	CGameClock::init();
	
	bool gameStarted = false;
	while(window->isOpen() && !gameStarted) {
		Event event;
		while(window->pollEvent(event)) {
			if(event.type == Event::Closed || (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)) {
				window->close();
				CNetworkManager::closeNetworkSocket();
				return;
			}
			if(event.type == Event::KeyPressed) {
				if(isHost) {
					if(event.key.code == Keyboard::Space) {
						CNetworkManager::broadcastGameStart(CRandom::getRandomSeed());
						gameStarted = true;
					} else if(event.key.code == Keyboard::Up) {
						updateDifficulty(true);
					} else if(event.key.code == Keyboard::Down) {
						updateDifficulty(false);
					}
				}
				
				if(event.key.code == Keyboard::Right) {
					updateClassName(true);
				} else if(event.key.code == Keyboard::Left) {
					updateClassName(false);
				}
			}
		}

		if(CNetworkManager::hasGameStarted()) {
			gameStarted = true;
		}

		if(!gameStarted) {
			if(isHost) {
				tickHost();
			} else {
				tickPeer();
			}

			CRenderManager::clear();
			
			CDebugHelper::updateDebugContent();
			CGuiManager::tickAll(0);
			CRenderManager::drawGui();
			CRenderManager::render(window);
		}
	}
}

void CLobby::tickHost() {
	CNetworkManager::listenForConnectingPeers(name);

	if(broadcastStatusClock.hasPassed(broadcastStatusFrequency)) {
		broadcastStatusClock.restart();
		
		CNetworkManager::broadcastConnectedPeers();
		CNetworkManager::broadcastClassName(className);
		CNetworkManager::broadcastDifficulty(difficulty);
	}
	
	CNetworkManager::kickTimeoutPeers(kickTimeout);
}

void CLobby::tickPeer() {
	CNetworkManager::listenForLobbyMessages();
	
	if(CNetworkManager::getConnections().empty()) {
		std::vector<CConnection> hosts = CNetworkManager::searchForHosts(true);

		if(!hosts.empty() && CNetworkManager::getConnections().empty()) {
			CNetworkManager::connectToHost(hosts.front(), name);
		}
	}

	if(broadcastStatusClock.hasPassed(broadcastStatusFrequency)) {
		broadcastStatusClock.restart();
		
		CNetworkManager::broadcastClassName(className);
		CNetworkManager::sendNTP();
	}
	
	for(CConnection connection : CNetworkManager::getConnections()) {
		if(connection.isHost && connection.isAliveClock.hasPassed(5000)) {
			CNetworkManager::closeNetworkSocket();
			std::exit(1);
		}
	}
	
	difficulty = CNetworkManager::getDifficulty();
	
}

std::string CLobby::readNameFromFile() {
	std::string returnName;

	std::ifstream file("networkname.txt");
	if(file) {
		file >> returnName;
	}

	if(returnName == "") {
		returnName = isHost ? "Host-" : "Peer-";
		returnName += sf::IpAddress::getLocalAddress().toString();
	}

	return returnName;
}

std::string CLobby::getName() const {
	return name;
}

CClassName CLobby::getClassName() const {
	return className;
}

void CLobby::setName(std::string name) {
	this->name = name;

	if(name == "left") {
		className = CClassName::MEDIATOR;
	} else if(name == "right") {
		className = CClassName::SHOTGUNNER;
	} else if(name == "bot") {
		className = CClassName::SNIPER;
	}
}

void CLobby::setClassName(CClassName className) {
	this->className = className;
}

CDifficulty CLobby::getDifficulty() const {
	return difficulty;
}

void CLobby::setDifficulty(CDifficulty difficulty) {
	this->difficulty = difficulty;
}

void CLobby::updateClassName(bool forward) {
	if(forward) {
		if(className == CClassName::MEDIATOR) {
			className = CClassName::SNIPER;
		} else if(className == CClassName::SNIPER) {
			className = CClassName::SHOTGUNNER;
		} else if(className == CClassName::SHOTGUNNER) {
			className = CClassName::MEDIATOR;
		}
	} else {
		if(className == CClassName::MEDIATOR) {
			className = CClassName::SHOTGUNNER;
		} else if(className == CClassName::SNIPER) {
			className = CClassName::MEDIATOR;
		} else if(className == CClassName::SHOTGUNNER) {
			className = CClassName::SNIPER;
		}
	}
}

void CLobby::updateDifficulty(bool upward) {
	if(upward) {
		if(difficulty == CDifficulty::DEBUG) {
			difficulty = CDifficulty::EASY;
		} else if(difficulty == CDifficulty::EASY) {
			difficulty = CDifficulty::MEDIUM;
		} else if(difficulty == CDifficulty::MEDIUM) {
			difficulty = CDifficulty::HARD;
		}
	} else {
		if(difficulty == CDifficulty::HARD) {
			difficulty = CDifficulty::MEDIUM;
		} else if(difficulty == CDifficulty::MEDIUM) {
			difficulty = CDifficulty::EASY;
		} else if(difficulty == CDifficulty::EASY) {
			difficulty = CDifficulty::DEBUG;
		}
	}
}
