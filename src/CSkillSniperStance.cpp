#include "CSkillSniperStance.hpp"
#include "CEngine.hpp"
#include "CSceneManager.hpp"

void CSkillSniperStance::onButtonPress() {
	targetTileSize = targetTileSizeSniperStance;
	owner->restoreEnergy(30);
}

void CSkillSniperStance::tick(float frameTime) {
	if(targetTileSize != CEngine::tile_size) {
		if(abs(targetTileSize - CEngine::tile_size) < 0.25f) {
			CEngine::tile_size = targetTileSize;
		} else {
			CEngine::tile_size += 9.f * (targetTileSize - CEngine::tile_size) * frameTime;
		}
		CSceneManager::updateAllGraphicContainers();
	}

	if(isButtonPressed) {
		owner->consumeEnergy(20 * frameTime);
	}
}

void CSkillSniperStance::onButtonRelease() {
	targetTileSize = targetTileSizeDefault;
}
