#include <iostream>
#include <SFML/Graphics/Sprite.hpp>
#include "CIconList.hpp"
#include "CTextureManager.hpp"
#include "CGuiManager.hpp"

CIconList::CIconList(Vector2f position, Vector2f iconSize) :
	position(position),
	iconSize(Vector2i((int) iconSize.x, (int) iconSize.y)),
	color(Color::White) {}
	

CIconList::~CIconList() {
	CGuiManager::removeGuiElement(this);
}

void CIconList::draw(RenderTarget *renderTarget) {
	if(isVisible) {
		Vector2f screenPos = position - Vector2f((iconSize.x * iconList.size())/2.f, 0);

		for(unsigned int i = 0; i < iconList.size(); i++) {
			Sprite sprite(CTextureManager::getTexture(iconList[i]));
			sprite.setColor(color);
			sprite.setScale(iconSize.x/sprite.getTexture()->getSize().x, iconSize.y/sprite.getTexture()->getSize().y);
			sprite.setOrigin(iconSize / 2.f);
				
			Vector2f pos = screenPos + Vector2f(i * iconSize.x, 0);
			sprite.setPosition((int) pos.x, (int) pos.y);
			renderTarget->draw(sprite);
		}
	}
}

void CIconList::tick(float frameTime) {

}

void CIconList::setColor(Color color) {
	this->color = color;
}

void CIconList::setIconSize(Vector2f iconSize) {
	this->iconSize = iconSize;
}

void CIconList::pushFront(std::string iconSource) {
	iconList.insert(iconList.begin(), iconSource);
}

void CIconList::pushBack(std::string iconSource) {
	iconList.push_back(iconSource);
}

void CIconList::popFront() {
	if(!iconList.empty()) {
		iconList.erase(iconList.begin());
	}
}

void CIconList::popBack() {
	if(!iconList.empty()) {
		iconList.pop_back();
	}
}


