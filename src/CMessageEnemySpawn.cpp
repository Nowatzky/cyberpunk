#include "CMessageEnemySpawn.hpp"
#include "CSerializationFunctions.hpp"
#include "CEnemyGhost.hpp"
#include "CEnemyGhostSummoner.hpp"
#include "CEnemySpecter.hpp"
#include "CEnemyLich.hpp"

void CMessageEnemySpawn::serializeInto(sf::Packet& packet) {
	packet << static_cast<sf::Uint8>(enemyType) 
	       << objectId
	       << pos
	       << rot;
}

void CMessageEnemySpawn::deserializeFrom(sf::Packet& packet) {
	sf::Uint8 tmp;
	
	packet >> tmp 
	       >> objectId
	       >> pos
	       >> rot;

	enemyType = static_cast<CEnemyTypeHologram>(tmp);
}

void CMessageEnemySpawn::process() {
	CEnemy* newEnemy;

	switch(enemyType) {
		case CEnemyTypeHologram::GHOST:
			pos.z = 1.0f;
			newEnemy = new CEnemyGhost(pos, rot, false);
			break;
		case CEnemyTypeHologram::GHOST_INVISIBLE:
			pos.z = 1.0f;
			newEnemy = new CEnemyGhost(pos, rot, true);
			break;
		case CEnemyTypeHologram::GHOSTSUMMONER:
			pos.z = 1.2f;
			newEnemy = new CEnemyGhostSummoner(pos, rot);
			//dynamic_cast<CEnemyGhostSummoner*>(newEnemy)->metaDataGhost = {enemyCostList[(int) GHOST], enemyPointList[(int) GHOST], enemyIntensityList[(int) GHOST]};
			//dynamic_cast<CEnemyGhostSummoner*>(newEnemy)->metaDataInvisibleGhost = {enemyCostList[(int) GHOST_INV], enemyPointList[(int) GHOST_INV], enemyIntensityList[(int) GHOST_INV]};			
			break;
		case CEnemyTypeHologram::SPECTER:
			pos.z = 1.3f;
			newEnemy = new CEnemySpecter(pos, rot);
			break;
		case CEnemyTypeHologram::LICH:
			pos.z = 1.2f;
			newEnemy = new CEnemyLich(pos, rot);
			break;
	}
	//std::vector<float> metaData = {enemyCostList[(int) enemy_instance], enemyPointList[(int) enemy_instance], enemyIntensityList[(int) enemy_instance]};
	//newEnemy->metaData = metaData;
	
	newEnemy->setNotLocal(objectId);
	newEnemy->setIsMovementReplicated(true);
	CSceneManager::safeAddObject(newEnemy);
}
