#include <iostream>
#include "CDebugHelper.hpp"
#include "CGuiManager.hpp"

using namespace sf;

std::deque<std::string> CDebugHelper::debugConsoleMessages;
unsigned int CDebugHelper::maxMessageCount = 12;
CDebugText* CDebugHelper::debugConsole;
CClock CDebugHelper::messageTimeoutClock;
float CDebugHelper::messageTimeout = 5000;

void CDebugHelper::initDebugHelper() {
	debugConsole = new CDebugText();
	
	debugConsole->getText()->setColor(Color::Red);
	debugConsole->getText()->setPosition(5, CGuiManager::getGuiSize().y - 180);
	
	CGuiManager::addGuiElement(debugConsole);
}

void CDebugHelper::updateDebugContent() {
	if(messageTimeoutClock.hasPassed(messageTimeout)) {
		if(debugConsoleMessages.front().substr(0, 9) == "Cyberpunk") {
			messageTimeoutClock.restart();
			debugConsoleMessages.pop_front();
		}
	}
	
	std::string debugConsoleContent;
	
	for(std::string message : debugConsoleMessages) {
		debugConsoleContent += message + "\n";
	}

	debugConsole->getText()->setString(debugConsoleContent);
}

void CDebugHelper::print(std::string message) {
	debugConsoleMessages.push_back(message);
	messageTimeoutClock.restart();

	if(debugConsoleMessages.size() > maxMessageCount) {
		debugConsoleMessages.pop_front();
	}
	
	std::cout << message << endl;
}
