#ifndef CYBERPUNK_CTRANSLATIONSNAPSHOT_HPP
#define CYBERPUNK_CTRANSLATIONSNAPSHOT_HPP

#include <SFML/Window.hpp>

using sf::Vector3f;

struct CTranslationSnapshot {
	float gameTime;
	Vector3f pos;
	float rot;
};

#endif //CYBERPUNK_CTRANSLATIONSNAPSHOT_HPP
