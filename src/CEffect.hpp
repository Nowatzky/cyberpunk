#ifndef CYBERPUNK_CEFFECT_HPP
#define CYBERPUNK_CEFFECT_HPP

#include <SFML/Graphics/Texture.hpp>
#include "CClock.hpp"

struct CEffectAggregate;
class CCharacter;

class CEffect {
protected:
	CCharacter* owner = nullptr;
	CCharacter* initiator;
	
	float lifetime;
	CClock lifetimeClock;
	
	std::string id;
	
	CEffect(CCharacter* initiator, float lifetime = -1) :
			initiator(initiator),
	        lifetime(lifetime) {}
	
	bool hasExpiredLifetime() const;

public:
	virtual ~CEffect() {}; 
	
	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate) = 0;
	// TODO implement
	//virtual bool tickAuthority(float frameTime, CEffectAggregate& effectAggregate) = 0;
	
	virtual std::vector<const sf::Texture*> getVisuals();
	virtual int getVisualPriority();
	
	virtual void onStart() {}
	virtual void onEnd() {}
	
	virtual void onOwnerDeath() {}
	
	virtual bool canMergeEffect(CEffect* otherEffect);
	
	void setOwner(CCharacter* owner);
	void restartLifetime();
	
	std::string getId() const;
	long getIdAsLong() const;
	void setId(std::string id);
	void setId(long id);
};

#endif //CYBERPUNK_CEFFECT_HPP
