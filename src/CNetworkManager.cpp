#include <iostream>
#include "CNetworkManager.hpp"
#include "CDebugHelper.hpp"
#include "CMessageObjTranslationUpdate.hpp"
#include "CMessageObjCreatePlayer.hpp"
#include "CGameClock.hpp"
#include "CMessageObjRPC.hpp"
#include "CRandom.hpp"
#include "CMessageEnemySpawn.hpp"
#include "CMessageCreateSpawnPoint.hpp"
#include "CMessageRoomStatus.hpp"

bool CNetworkManager::active = false;

bool CNetworkManager::gameStarted = false;
int CNetworkManager::playerNumber;
int CNetworkManager::maxPlayers = 6;
CDifficulty CNetworkManager::difficulty = CDifficulty::MEDIUM;

sf::UdpSocket CNetworkManager::socket;
CNetworkRole CNetworkManager::networkRole;
std::vector<CConnection> CNetworkManager::connections;
std::vector<CObject*> CNetworkManager::replicatingObjects;
std::vector<CMessage*> CNetworkManager::pendingOutgoingMessages;
std::vector<CMessage*> CNetworkManager::pendingReceivedMessages;

CClock CNetworkManager::updateClock;
CClock CNetworkManager::NTPUpdateClock;

float CNetworkManager::networkTickFrequency = 1000.f/45.f;
float CNetworkManager::ntpUpdateFrequency = 1000.f/2.f;

float CNetworkManager::movementInterpolationSafetyPackets = 2.f;


void CNetworkManager::initNetworkSocket(CNetworkRole networkRole) {
	CNetworkManager::active = true;
	
	CNetworkManager::networkRole = networkRole;

	sf::Socket::Status status;
	if(networkRole == CNetworkRole::HOST) {
		status = socket.bind(HOST_PORT); 
		
		if(status == sf::Socket::Done) {
			CDebugHelper::print("Host - listening on port " + std::to_string(socket.getLocalPort()));
		} else {
			CDebugHelper::print("Host - failed to bind to port " + std::to_string(socket.getLocalPort()));
		}
	} else { /* PEER */
		status = socket.bind(sf::Socket::AnyPort);
		
		if(status == sf::Socket::Done) {
			CDebugHelper::print("Peer - using port " + std::to_string(socket.getLocalPort()));
		} else {
			CDebugHelper::print("Peer - failed to bind to port " + std::to_string(socket.getLocalPort()));
		}
	}
	
	socket.setBlocking(false);
}

void CNetworkManager::closeNetworkSocket() {
	socket.unbind();
}

void CNetworkManager::listenForConnectingPeers(std::string hostName) {
	if(networkRole != CNetworkRole::HOST) {
		return;
	}
	
	sf::Packet packet;
	sf::IpAddress address;
	unsigned short port;

	if(socket.receive(packet, address, port) == Socket::Status::Done) {
		resetIsAliveClock(address, port);
		CPacketType packetType = getPacketType(packet);

		if(packetType == CPacketType::CONNECTION_BROADCAST_SEARCH) {
			sf::Packet answerPacket;
			setPacketType(answerPacket, CPacketType::CONNECTION_BROADCAST_ANSWER);
			answerPacket << hostName;
			socket.send(answerPacket, address, port);
			
		} else if(packetType == CPacketType::CONNECTION_REQUEST) {
			if(connections.size() + 1 <= maxPlayers) {
				sf::Packet answerPacket;
				setPacketType(answerPacket, CPacketType::CONNECTION_CONFIRM);
				socket.send(answerPacket, address, port);

				CConnection connection;
				connection.address = address;
				connection.port = port;
				connection.isHost = false;
				packet >> connection.name;
				connections.push_back(connection);

				CDebugHelper::print(connection.name + " (" + connection.address.toString() + ":" + std::to_string(connection.port) + ") connected");
			} else {
				CDebugHelper::print("Player tried to connect but lobby is full");
			}
		} else if(packetType == CPacketType::LOBBY_BROADCAST_CLASSNAME) {
			sf::Uint8 className;
			packet >> className;

			for(CConnection& connection : connections) {
				if(connection.address == address && connection.port == port) {
					connection.className = static_cast<CClassName>(className);
				}
			}
		} else if(packetType == CPacketType::NTP_PING) {
			answerNTPPing(address, port, packet);
		} 
	}
}

void CNetworkManager::listenForLobbyMessages() {
	if(networkRole != CNetworkRole::PEER) {
		return;
	}

	sf::Packet packet;
	sf::IpAddress address;
	unsigned short port;

	if(socket.receive(packet, address, port) == Socket::Status::Done) {
		resetIsAliveClock(address, port);
		CPacketType packetType = getPacketType(packet);

		if(packetType == CPacketType::LOBBY_BROADCAST_CONNECTED_PEERS) {			
			std::vector<CConnection> receivedConnections;
			sf::Uint8 connectionSize;
			packet >> connectionSize;

			for(int i = 0; i < connectionSize; i++) {
				CConnection newConnection;
				sf::Uint32 peerAddress;
				sf::Uint8 className;
				
				packet >> peerAddress
				       >> newConnection.port
				       >> newConnection.name
				       >> className;
				
				newConnection.address = sf::IpAddress(peerAddress);
				newConnection.className = static_cast<CClassName>(className);
				newConnection.isHost = false;

				if(newConnection.address != sf::IpAddress::getLocalAddress() && newConnection.port != socket.getLocalPort()) {
					receivedConnections.push_back(newConnection);
				}
			}

			// Remove connection if not contained in received
			for(auto iter = connections.begin(); iter != connections.end(); iter++) {
				if((*iter).isHost) {
					continue;
				}
				
				// Check if exists in received connections
				if(std::find(receivedConnections.begin(), receivedConnections.end(), (*iter)) != receivedConnections.end()) {
					continue;
				} else {
					connections.erase(iter);
					iter--;
				}
			}
			
			// Add new connections
			for(auto iter = receivedConnections.begin(); iter != receivedConnections.end(); iter++) {
				if(!(std::find(connections.begin(), connections.end(), (*iter)) != connections.end())) {
					connections.push_back(*iter);
				}
			}
			
		} else if(packetType == CPacketType::LOBBY_BROADCAST_CLASSNAME) {
			sf::Uint8 className;
			packet >> className;

			for(CConnection& connection : connections) {
				if(connection.address == address && connection.port == port) {
					connection.className = static_cast<CClassName>(className);
				}
			}
		} else if(packetType == CPacketType::LOBBY_BROADCAST_DIFFICULTY) {
			sf::Uint8 receivedDifficulty;
			packet >> receivedDifficulty;

			difficulty = static_cast<CDifficulty>(receivedDifficulty);
		} else if(packetType == CPacketType::LOBBY_GAME_START) {
			CObjectId idOffset;
			sf::Uint32 seed;
			sf::Uint8 number;
			packet >> idOffset
			       >> seed
			       >> number;
			
			CObjectIdManager::init(idOffset);
			CRandom::initialize(seed);
			
			gameStarted = true;
			playerNumber = (int) number;
			
		} else if(packetType == CPacketType::NTP_PONG) {
			processNTPPong(packet);
		}
	}
}

void CNetworkManager::broadcastConnectedPeers() {
	if(networkRole != CNetworkRole::HOST) {
		return; 
	}
	
	sf::Packet packet;
	
	setPacketType(packet, CPacketType::LOBBY_BROADCAST_CONNECTED_PEERS);
	sf::Uint8 connectionSize = static_cast<Uint8>(connections.size()); 
	packet << connectionSize;
	
	for(CConnection connection : connections) {
		packet << connection.address.toInteger()
		       << connection.port
		       << connection.name
		       << static_cast<Uint8>(connection.className);
	}
	
	for(CConnection connection : connections) {
		socket.send(packet, connection.address, connection.port);
	}
}

void CNetworkManager::broadcastDifficulty(CDifficulty difficulty) {
	if(networkRole != CNetworkRole::HOST) {
		return;
	}

	sf::Packet packet;

	setPacketType(packet, CPacketType::LOBBY_BROADCAST_DIFFICULTY);
	packet << static_cast<sf::Uint8>(difficulty);

	for(CConnection connection : connections) {
		socket.send(packet, connection.address, connection.port);
	}
}

void CNetworkManager::broadcastClassName(CClassName className) {
	sf::Packet packet;

	setPacketType(packet, CPacketType::LOBBY_BROADCAST_CLASSNAME);
	packet << static_cast<sf::Uint8>(className);

	for(CConnection connection : connections) {
		socket.send(packet, connection.address, connection.port);
	}
}

std::vector<CConnection> CNetworkManager::searchForHosts(bool useBroadcast) {
	sf::Packet packetBroadcast;
	setPacketType(packetBroadcast, CPacketType::CONNECTION_BROADCAST_SEARCH);
	
	socket.send(packetBroadcast, sf::IpAddress::LocalHost, HOST_PORT);

	if(useBroadcast) {
		socket.send(packetBroadcast, sf::IpAddress::Broadcast, HOST_PORT);
	}
	
	sf::Clock clockTimeout;
	sf::Time timeout = sf::milliseconds(2000);

	sf::IpAddress address;
	unsigned short port;
	
	std::vector<CConnection> hostsFound;
	
	while(clockTimeout.getElapsedTime() < timeout) {
		sf::Packet packetAnswer;
		if(socket.receive(packetAnswer, address, port) == Socket::Status::Done) {
			CPacketType packetType = getPacketType(packetAnswer);

			if(packetType == CPacketType::CONNECTION_BROADCAST_ANSWER) {
				CConnection connection;
				std::string hostName;

				packetAnswer >> hostName;
				
				connection.address = sf::IpAddress(address);
				connection.port = port;
				connection.isHost = true;
				connection.name = hostName;

				hostsFound.push_back(connection);
				CDebugHelper::print("Found local host " + connection.name + " at " + connection.address.toString() + " : " + std::to_string(connection.port));
				break;
			}
		}
	}

	if(hostsFound.empty()) {
		if(useBroadcast) {
			CDebugHelper::print("No host found on local network");
		} else {
			CDebugHelper::print("No host found on local machine");
		}
	}

	return hostsFound;	
}

void CNetworkManager::connectToHost(CConnection connection, std::string peerName) {
	if(!connection.isHost) {
		std::cout << "Error: Tried to connect to host connection but is not host!" << endl; 
	}
	
	sf::Packet packet;
	setPacketType(packet, CPacketType::CONNECTION_REQUEST);
	packet << peerName;

	CDebugHelper::print("Connecting to " + connection.name + " at " + connection.address.toString() + " : " + std::to_string(connection.port) + "...");

	socket.send(packet, connection.address, connection.port);

	sf::Clock clockTimeout;
	sf::Time timeout = sf::milliseconds(3000);

	sf::IpAddress address;
	unsigned short port;

	while(clockTimeout.getElapsedTime() < timeout) {
		sf::Packet packetAnswer;
		if(socket.receive(packetAnswer, address, port) == Socket::Status::Done) {
			CPacketType packetType = getPacketType(packetAnswer);

			if(packetType == CPacketType::CONNECTION_CONFIRM) {
				CDebugHelper::print("Connection to " + connection.name + " successfully established");
				connections.push_back(connection);
				return;
			}
		}
	}

	CDebugHelper::print("No connection established - timeout");	
}

CPacketType CNetworkManager::getPacketType(sf::Packet& packet) {
	sf::Uint8 packetTypeRaw;
	packet >> packetTypeRaw;
	return static_cast<CPacketType>(packetTypeRaw);
}

std::vector<CConnection> CNetworkManager::getConnections() {
	return connections;
}

void CNetworkManager::setPacketType(sf::Packet& packet, CPacketType packetType) {
	auto packetTypeRaw = static_cast<Uint8>(packetType);
	packet << packetTypeRaw;
}

CNetworkRole CNetworkManager::getRole() {
	return networkRole;
}

CMessage* CNetworkManager::createMessage(CMessageType messageType) {
	CMessage* message = nullptr;

	if(messageType == CMessageType::OBJ_TRANSLATION_UPDATE) {
		message = new CMessageObjTranslationUpdate();
	} else if(messageType == CMessageType::OBJ_CREATE_PLAYER) {
		message = new CMessageObjCreatePlayer();
	} else if(messageType == CMessageType::OBJ_RPC) {
		message = new CMessageObjRPC();
	} else if(messageType == CMessageType::ENEMY_SPAWN) {
		message = new CMessageEnemySpawn();
	} else if(messageType == CMessageType::CREATE_SPAWN_POINT) {
		message = new CMessageCreateSpawnPoint();
	} else if(messageType == CMessageType::ROOM_STATUS) {
		message = new CMessageRoomStatus();
	}

	return message;
}

void CNetworkManager::queueForBroadcast(CMessage* message) {
	pendingOutgoingMessages.push_back(message);
}

void CNetworkManager::sendPendingMessages() {
	if(pendingOutgoingMessages.empty()) {
		return;
	}
	
	sf::Packet packet;
	setPacketType(packet, CPacketType::MESSAGES);
	
	for(CMessage* message : pendingOutgoingMessages) {
		packet << static_cast<CMessageTypeUnderlying>(message->getMessageType());
		message->serializeInto(packet);
		delete message;
	}
	pendingOutgoingMessages.clear();
	
	for(CConnection connection : connections) {
		socket.send(packet, connection.address, connection.port);	
	}	
}

void CNetworkManager::receiveMessages() {
	sf::IpAddress address;
	unsigned short port;
	sf::Packet packet;
	
	while(socket.receive(packet, address, port) == Socket::Status::Done) {
		CPacketType packetType = getPacketType(packet);

		if(packetType == CPacketType::MESSAGES) {
			CMessageTypeUnderlying messageTypeUnderlying;

			while(packet >> messageTypeUnderlying) {
				auto messageType = static_cast<CMessageType>(messageTypeUnderlying);
				CMessage* message = nullptr;
				
				if(messageType == CMessageType::OBJ_TRANSLATION_UPDATE) {
					message = new CMessageObjTranslationUpdate();
				} else if(messageType == CMessageType::OBJ_CREATE_PLAYER) {
					message = new CMessageObjCreatePlayer();
				} else if(messageType == CMessageType::OBJ_RPC) {
					message = new CMessageObjRPC();
				} else if(messageType == CMessageType::ENEMY_SPAWN) {
					message = new CMessageEnemySpawn();
				} else if(messageType == CMessageType::CREATE_SPAWN_POINT) {
					message = new CMessageCreateSpawnPoint();
				} else if(messageType == CMessageType::ROOM_STATUS) {
					message = new CMessageRoomStatus();
				}

				if(message != nullptr) {
					message->deserializeFrom(packet);
					pendingReceivedMessages.push_back(message);
				}
			}
		} else if(packetType == CPacketType::NTP_PING) {
			answerNTPPing(address, port, packet);
		} else if(packetType == CPacketType::NTP_PONG) {
			processNTPPong(packet);
		}
		
		packet.clear();
	}
}

void CNetworkManager::processReceivedMessages() {
	for(CMessage* message : pendingReceivedMessages) {
		message->process();
		delete message;
	}
	
	pendingReceivedMessages.clear();
}

void CNetworkManager::sendNTP() {
	if(networkRole != CNetworkRole::PEER) {
		std::cout << "Error: Tried to send NTP_PING as host!" << endl;
		return;
	}
	
	CConnection hostConnection;
	for(CConnection connection : connections) {
		if(connection.isHost) {
			hostConnection = connection;
			break;
		}
	}

	if(hostConnection.port != HOST_PORT) {
		std::cout << "Error: No host connection found!" << endl;
		return;
	}
	
	sf::Packet packet;
	setPacketType(packet, CPacketType::NTP_PING);
	packet << CGameClock::getGameTime();
	
	socket.send(packet, hostConnection.address, hostConnection.port);	
}

void CNetworkManager::answerNTPPing(sf::IpAddress address, unsigned short port, sf::Packet packet) {
	if(networkRole != CNetworkRole::HOST) {
		std::cout << "Error: Received NTP_PING as peer!" << endl;
		return;
	}
	
	float peerGameTimeOnSend;
	packet >> peerGameTimeOnSend;

	sf::Packet answerPacket;
	setPacketType(answerPacket, CPacketType::NTP_PONG);
	answerPacket << peerGameTimeOnSend << CGameClock::getGameTime();

	socket.send(answerPacket, address, port);
}

void CNetworkManager::processNTPPong(sf::Packet packet) {
	if(networkRole != CNetworkRole::PEER) {
		std::cout << "Error: Received NTP_PONG as host!" << endl;
		return;
	}
	
	float peerGameTimeOnSend;
	float peerGameTimeOnReceive;
	float hostGameTime;
	
	packet >> peerGameTimeOnSend >> hostGameTime;
	peerGameTimeOnReceive = CGameClock::getGameTime();
	
	float rtt = peerGameTimeOnReceive - peerGameTimeOnSend;
	CGameClock::adjustTowards(hostGameTime + rtt/2.f);
	
	CDebugHelper::print("Ping: " + std::to_string(rtt/2.f).substr(0, 6));
}

void CNetworkManager:: broadcastGameStart(sf::Uint32 seed) {
	if(networkRole != CNetworkRole::HOST) {
		return;
	}
	
	CObjectIdManager::init(1);
	CRandom::initialize(static_cast<unsigned long>(seed));
	
	int idOffset = (int) (std::numeric_limits<CObjectId>::max() / (connections.size() + 1));
	playerNumber = 0;
	
	for(unsigned int i = 0; i < connections.size(); i++) {
		sf::Packet packet;
		setPacketType(packet, CPacketType::LOBBY_GAME_START);
		
		sf::Uint8 number = static_cast<Uint8>(i + 1);
		packet << static_cast<CObjectId>((i + 1) * idOffset)
		       << seed
		       << number;
		
		socket.send(packet, connections[i].address, connections[i].port);
	}
}

bool CNetworkManager::isActive() {
	return active;
}

void CNetworkManager::addReplicatingObject(CObject* object) {
	replicatingObjects.push_back(object);
}

void CNetworkManager::removeReplicatingObject(CObject* object) {
	replicatingObjects.erase(std::remove(replicatingObjects.begin(), replicatingObjects.end(), object), replicatingObjects.end());
}

void CNetworkManager::updateReplicatedObjects() {
	float gameTime = CGameClock::getGameTime();
	
	for(CObject* object : replicatingObjects) {
		object->createTranslationSnapshotMessage(gameTime);	
	}
}

void CNetworkManager::tick() {
	if(networkRole == CNetworkRole::PEER && NTPUpdateClock.hasPassed(ntpUpdateFrequency)) {
		NTPUpdateClock.restart();
		sendNTP();
	}

	if(updateClock.hasPassed(networkTickFrequency)) {
		updateClock.restart();
		updateReplicatedObjects();		
	}

	sendPendingMessages();
}

float CNetworkManager::getNetworkTickFrequency() {
	return networkTickFrequency;
}

float CNetworkManager::getMovementInterpolationSafetyPackets() {
	return movementInterpolationSafetyPackets;
}

void CNetworkManager::resetIsAliveClock(sf::IpAddress address, unsigned short port) {
	for(CConnection& connection : connections) {
		if(connection.address == address && connection.port == port) {
			connection.isAliveClock.restart();
			break;
		}
	}
}

void CNetworkManager::kickTimeoutPeers(float timeout) {
	if(networkRole != CNetworkRole::HOST) {
		return;
	}
	
	for(auto iter = connections.begin(); iter != connections.end(); iter++) {
		if((*iter).isAliveClock.hasPassed(timeout)) {
			connections.erase(iter);
			iter--;
			CDebugHelper::print((*iter).name + " was kicked due to timeout!");
		}
	}
}

bool CNetworkManager::hasGameStarted() {
	return gameStarted;
}

int CNetworkManager::getPlayerNumber() {
	return playerNumber;
}

CDifficulty CNetworkManager::getDifficulty() {
	return difficulty;
}
