#include "CSkillChainFlash.hpp"
#include "CSceneManager.hpp"
#include "CChainFlashEffect.hpp"
#include "CEffectChainFlash.hpp"
#include "CRpcHandler.hpp"

void CSkillChainFlash::onButtonPress() {
	std::vector<CCharacter*> targets;
	
	CCharacter* target = findNearestTarget();
	if(target != nullptr) {
		targets.push_back(target);

		target = findNearestTarget(target);
		if(target != nullptr) {
			targets.push_back(target);
		}
	}
	
	fire(targets);
}

CCharacter* CSkillChainFlash::findNearestTarget() const {
	Vector2f currentPos = CEngine::to2D(owner->getPosition());
	CCharacter* currentTarget = nullptr;
	float currentDistance = maxDistance;

	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		if(enemy->getEffectManager()->hasMark(CEffectMark::EffectMarkType::Lightning)) {
			float distance = CEngine::getDistance(currentPos, CEngine::to2D(enemy->getPosition()));

			if(distance <= maxDistance && distance <= currentDistance) {
				currentDistance = distance;
				currentTarget = enemy;
			}
		}
	}

	return currentTarget;
}

CCharacter* CSkillChainFlash::findNearestTarget(CCharacter* prevTarget) const {
	Vector2f currentPos = CEngine::to2D(owner->getPosition());
	CCharacter* currentTarget = nullptr;
	float currentDistance = maxDistance;

	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		if(enemy->getEffectManager()->hasMark(CEffectMark::EffectMarkType::Lightning)) {
			if(prevTarget != enemy && CEngine::getDistance(CEngine::to2D(prevTarget->getPosition()), CEngine::to2D(enemy->getPosition())) > maxDistance) {
				float distance = CEngine::getDistance(currentPos, CEngine::to2D(enemy->getPosition()));

				if(distance <= maxDistance && distance <= currentDistance) {
					currentDistance = distance;
					currentTarget = enemy;
				}
			}
		}
	}

	return currentTarget;
}

void CSkillChainFlash::fire(std::vector<CCharacter*> targets) {
	std::vector<CObjectId> targetIds;
	
	for(CCharacter* enemy : targets) {
		hit(owner, enemy);
		targetIds.push_back(enemy->getId());
	}
	
	CRpcHandler::sendPlayerSkillChainFlash(owner, targetIds);
}

void CSkillChainFlash::fireRPC(std::vector<CObjectId> targetIds) {
	for(CObjectId id : targetIds) {
		CCharacter* enemy = dynamic_cast<CCharacter*>(CSceneManager::getObject(id));
		if(enemy != nullptr) {
			hit(owner, enemy);
		}
	}
}

void CSkillChainFlash::fireFallback(std::vector<CObjectId> targetIds) {
	for(CObjectId id : targetIds) {
		CCharacter* enemy = dynamic_cast<CCharacter*>(CSceneManager::getObject(id));
		if(enemy != nullptr) {
			hit(nullptr, enemy);
		}
	}
}

void CSkillChainFlash::hit(CCharacter* initiator, CCharacter* target) {
	target->addEffect(new CEffectChainFlash(initiator));

	if(initiator != nullptr) {
		CSceneManager::safeAddObject(new CChainFlashEffect(initiator->getPosition(), target->getPosition()));
	}
}
