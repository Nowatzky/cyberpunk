#ifndef CYBERPUNK_CMESSAGEROOMSTATUS_HPP
#define CYBERPUNK_CMESSAGEROOMSTATUS_HPP


#include "CMessage.hpp"

class CMessageRoomStatus : public CMessage {
public:
	int roomNumber;
	bool roomCompleted;

	CMessageRoomStatus() :
			CMessage(CMessageType::ROOM_STATUS) {}

	void serializeInto(sf::Packet& packet) override;
	void deserializeFrom(sf::Packet& packet) override;
	void process() override;

};


#endif //CYBERPUNK_CMESSAGEROOMSTATUS_HPP
