#ifndef CYBERPUNK_CPROJECTILEHEALTHBAIT_HPP
#define CYBERPUNK_CPROJECTILEHEALTHBAIT_HPP


#include "CProjectile.hpp"
#include "CColorDefines.hpp"
#include "CLineEffect.hpp"

class CProjectileHealthBait : public CProjectile {
private:
	float baitHealPercentage = 0.33;
	float baitExplosionDamage = 50;
	float rotationSpeed = 45;

	bool isFalling = true;
	Clock clockFall;
	float fallDuration = 800;
	float fallOffsetX;
	float fallOffsetY;

	CCharacter *baitTarget = nullptr;
	CLineEffect *baitLine = nullptr;
	float baitDelay = 500;
	float baitDistance = 6;
	float baitMaxDistance = 7;
	float baitPullSpeed = 20;
	Clock clockBait;

public:
	CProjectileHealthBait(CCharacter* initiator, const Vector3f &pos, const Vector2f &dir) :
			CProjectile(initiator, pos, Vector3f(0.25,0.25,0.25), COLOR_YELLOW_GREEN, dir, 5000) {
		boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
	}

	~CProjectileHealthBait();

	void onHitEnemy(CEnemy* target, float frameTime);
	void onHitPlayer(CPlayer* player, float frameTime);
	virtual void tick(float frameTime);
	bool hasTargetAcquired();
	CCharacter *getTarget();
	float getBaitExplosionDamage();

private:
	void acquireTarget(CCharacter *target);
	void pullTarget(float frameTime);
};


#endif //CYBERPUNK_CPROJECTILEHEALTHBAIT_HPP
