#include "CPlayer.hpp"
#include "CClassMediator.hpp"
#include "CClassSniper.hpp"
#include "CClassShotgunner.hpp"
#include "CGui.hpp"
#include "CSceneManager.hpp"
#include "CDebugHelper.hpp"
#include "CGameClock.hpp"
#include "CNetworkManager.hpp"
#include "CRpcHandler.hpp"

CPlayer::CPlayer(Vector3f pos, Vector3f dim, CClassName className) :
		CCharacter(pos, dim, 0, Color::Black, 100, 0) {

	movementSpeed = 0;
	setCharacterClass(className);
}

void CPlayer::tick(float frameTime) {
	CCharacter::tick(frameTime);
	
	getCharacterClass()->getPrimarySkill()->tick(frameTime);
	getCharacterClass()->getMovementSkill()->tick(frameTime);

	for(CSkill* activeSkill : activeSkills) {
		activeSkill->tick(frameTime);
	}

	restoreEnergy(energyRegeneration * frameTime);
	healHealth((healthRegeneration + (getMaxHealth() - health) * missingHealthRegenerationBoost) * frameTime);
	damagedRecently -= (damagedRecently * 20.f * frameTime);

	if(isLocal && isAlive) {
		executePlannedMovement(frameTime);
	}
}

void CPlayer::rotate(float angle) {
	CCharacter::rotate(angle);
}

bool CPlayer::consumeEnergy(float deltaEnergy) {
	if(energy - deltaEnergy >= 0) {
		energy -= deltaEnergy;

		if(isLocal) {
			CGui::getEnergyBar()->setCurrentValue(energy);
		}
		
		return true;
	}
	return false;
}

bool CPlayer::restoreEnergy(float deltaEnergy) {
	if(energy == energyMax) {
		return false;
	}

	energy = CEngine::clamp(energy + deltaEnergy, 0, energyMax);

	if(isLocal) {
		CGui::getEnergyBar()->setCurrentValue(energy);
	}
	
	return true;
}

float CPlayer::getEnergy() {
	return energy;
}

float CPlayer::getEnergyMaximum() {
	return energyMax;
}

void CPlayer::setCharacterClass(CClassName className) {
	clockSwitchClasses.restart();
	activeSkills.clear();

	if(characterClass != nullptr) {
		delete characterClass; //deletes skills
	}
	
	switch(className) {
		case CClassName::MEDIATOR:
			characterClass = new CClassMediator(this);
			break;
		case CClassName::SNIPER:
			characterClass = new CClassSniper(this);
			break;
		case CClassName::SHOTGUNNER:
			characterClass = new CClassShotgunner(this);
			break;
	}
	
	activeSkills = characterClass->getActiveSkills();
	
	getGraphicContainer()->setColorBase(getCharacterClass()->getBaseColor());
	getGraphicContainer()->setColorAccent(getCharacterClass()->getAccentColor());
	CGui::updateLocalPlayerCharacterClass();
	
	energy = 0;
}

CCharacterClass* CPlayer::getCharacterClass() {
	return characterClass;
}

void CPlayer::addActiveSkill(CSkill* skill) {
	activeSkills.push_back(skill);
}

void CPlayer::removeActiveSkill(CSkill* skill) {
	for(unsigned i = 0; i < activeSkills.size(); ++i) {
		if(activeSkills[i] == skill) {
			activeSkills.erase(activeSkills.begin() + i);
			break;
		}
	}
}

bool CPlayer::isClassSwitchAvailable() {
	return clockSwitchClasses.getElapsedTime().asMilliseconds() > switchClassCooldown && !CNetworkManager::isActive();
}

void CPlayer::setHealth(float health) {
	CCharacter::setHealth(health);
	if(isLocal) {
		CGui::getHealthBar()->setCurrentValue(health);
	}
}

void CPlayer::reduceHealth(float damage) {
	if(!isLocal) {
		damage = min(damage, abs(health - 1.f));
	}
	
	CCharacter::reduceHealth(damage);
	if(isLocal) {
		CGui::getHealthBar()->setCurrentValue(health);
	}
}

void CPlayer::healHealth(float heal) {
	CCharacter::healHealth(heal);

	if(isLocal) {
		CGui::getHealthBar()->setCurrentValue(health);
	}
}

void CPlayer::setMovementSpeed(float movementSpeed) {
	CCharacter::setMovementSpeed(CEngine::clamp(movementSpeed, 0 , maxMovementSpeed));
}

void CPlayer::applyMovementDirection(Vector2f movementDirection) {
	if(!allowOwnMovement) {
		return;
	}
	
	movementDirection = CEngine::normalize(movementDirection);
	
	if(CEngine::getLength(movementDirection) > 0) {
		// If standing still take new direction, otherwise lerp
		if(movementSpeed < 0.05f * maxMovementSpeed) {
			this->movementDirection = movementDirection;
		} else { // ToDo use frameTime
			// TODO reenable
			this->movementDirection = movementDirection;
			//this->movementDirection = CEngine::rotateZ(this->movementDirection,
			//                                           0.05f * CEngine::getAngleDifference(this->movementDirection, movementDirection));
		}		
		
		if(!isMoving) {
			needsMovementClockRestart = true;
		}
		isMoving = true;
	} else {
		if(isMoving) {
			needsMovementClockRestart = true;
		}
		isMoving = false;
	}
}

void CPlayer::executePlannedMovement(float frameTime) {
	if(!allowOwnMovement) {
		needsMovementClockRestart = true;
		return;
	}

	if(needsMovementClockRestart) {
		needsMovementClockRestart = false;

		if(isMoving) {
			accelerationClock.restart(movementSpeed/maxMovementSpeed * accelerationTime);
		} else {
			decelerationClock.restart(CEngine::invertAlpha(movementSpeed/maxMovementSpeed) * decelerationTime);
		}
	}
	
	if(isMoving) {
		float alpha = CEngine::clampAlpha(accelerationClock.getElapsedTime().asMilliseconds()/accelerationTime);
		movementSpeed = alpha * maxMovementSpeed;
				
	} else {
		float alpha = CEngine::clampAlpha(decelerationClock.getElapsedTime().asMilliseconds()/decelerationTime);
		movementSpeed = CEngine::invertAlpha(alpha) * maxMovementSpeed;
	}

	if(movementSpeed > 0) {
		CCharacter::move(movementDirection * movementSpeed * frameTime * effectManager.getMovementSpeedModifier(), false, false);
	}
}

void CPlayer::setAllowOwnMovement(bool allowOwnMovement) {
	this->allowOwnMovement = allowOwnMovement;
}

float CPlayer::getMaxMovementSpeed() {
	return maxMovementSpeed;
}

Vector2f CPlayer::getMovementDirection() const {
	return movementDirection;
}

void CPlayer::reloadPrimarySkill() {
	getCharacterClass()->getPrimarySkill()->quickDraw();
}

int CPlayer::getKills() {
	return kills;
}

int CPlayer::getAssists() {
	return assists;
}

int CPlayer::getDeaths() {
	return deaths;
}

void CPlayer::addKill() {
	CGui::updateKDA();
	++kills;
}

void CPlayer::addDeath() {
	CGui::updateKDA();
	++deaths;
}

void CPlayer::addAssist() {
	CGui::updateKDA();
	++assists;
}

void CPlayer::checkKillPotential(CCharacter *target, CCharacter *initiator) {
	if(target->isGoingToDie() && dynamic_cast<CPlayer *>(initiator) != nullptr) {
		dynamic_cast<CPlayer *>(initiator)->addKill();
		CGui::updateKDA();
	}

}

bool CPlayer::getIsAlive() const {
	return isAlive;
}

void CPlayer::die() {
	if(isAlive) {
		isAlive = false;
		
		effectManager.ownerDeath();
		//CSceneManager::safeUnregisterObject(this);
		graphicBox.setVisible(false);
		boundingBox.setCollisionChannel(CCollisionChannel());

		addDeath();

		if(isLocal) {
			CRpcHandler::sendCharacterDie(this);
			CGui::updatePlayerGuiVisibility();
			
			// Revert sniper stance
			CEngine::tile_size = 64.f;
			CSceneManager::updateAllGraphicContainers();
		}
	}
}

void CPlayer::revive() {
	isAlive = true;
	//CSceneManager::safeAddObject(this);
	graphicBox.setVisible(true);
	boundingBox.setCollisionChannel(CCollisionChannel::Channel::CHARACTER);

	if(isLocal) {
		CGui::updatePlayerGuiVisibility();
		//CNetworkManager::addReplicatingObject(this);
	}
}
