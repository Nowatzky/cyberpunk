#include "CProjectileSweepingWall.hpp"
#include "CGui.hpp"

CProjectileSweepingWall::CProjectileSweepingWall(CCharacter* initiator, Vector3f pos, Vector2f dir) :
		CProjectile(initiator, pos, Vector3f(1.25f, 0.15f, 0.6f), COLOR_PINK_ALPHA, dir) {

	getCollisionContainer()->setCollisionChannel(CCollisionChannel::Channel::PROJECTILE);
	setSpeed(startSpeed);
	setLifetime(2000);
	setRotation(CEngine::getRotation(dir));
}

void CProjectileSweepingWall::onHitEnemy(CEnemy* target, float frameTime) {
	if(target->getIsLocal()) {
		if(!firstContact && isLocal) {
			firstContact = true;
			if(dynamic_cast<CPlayer *>(initiator)!= nullptr) {
				dynamic_cast<CPlayer *>(initiator)->addAssist();
				CGui::updateKDA();
			}
		}
		target->push(getDirection() * getSpeed() * frameTime);
	}
}

void CProjectileSweepingWall::tick(float frameTime) {
    CProjectile::tick(frameTime);
    setSpeed(CEngine::clamp(startSpeed - (clockLifetime.getTimeMs()/1000.f * speedDecrease), minSpeed, startSpeed));
}
