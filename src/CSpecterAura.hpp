#ifndef CYBERPUNK_CSPECTERAURA_HPP
#define CYBERPUNK_CSPECTERAURA_HPP

#include "CObject.hpp"
#include "CGraphicArea.hpp"

class CSpecterAura : public CObject {
private:
	CGraphicArea graphicArea;
	float radius;
	
public:

	CSpecterAura(Vector3f pos, float radius, Color colorBase, Color colorAccent) :
	CObject(pos, 0),
	graphicArea(Vector3f(pos.x, pos.y, 0.55f), radius, 32, colorBase, colorAccent),
	radius(radius) {}

	void tick(float frameTime);
	
	void updatePosition(Vector3f pos);

	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();

};


#endif //CYBERPUNK_CSPECTERAURA_HPP
