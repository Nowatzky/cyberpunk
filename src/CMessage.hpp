#ifndef CYBERPUNK_CMESSAGE_HPP
#define CYBERPUNK_CMESSAGE_HPP

#include <SFML/Config.hpp>
#include <SFML/Network/Packet.hpp>

typedef sf::Uint8 CMessageTypeUnderlying;

enum class CMessageType : CMessageTypeUnderlying {
	OBJ_TRANSLATION_UPDATE,         // ObjectId, Vec3 pos, float rot
	OBJ_RPC,                        // ObjectId, 
	OBJ_CREATE_PLAYER,              // ObjectId, Vec3 pos, float rot, ClassType class
	ENEMY_SPAWN,                    //
	CREATE_SPAWN_POINT,             //
	ROOM_STATUS,                    //
};

class CMessage {
protected:
	CMessageType messageType;
	
	CMessage(CMessageType messageType) :
			messageType(messageType) {}
	
	//template<typename Enum>
	//void serializeEnumInto(sf::Packet& packet, Enum e);
	//template<typename Enum>
	//Enum deserializeEnumFrom(sf::Packet& packet);

public:
	virtual ~CMessage() {};
	
	CMessageType getMessageType() const;

	virtual void process() = 0;
	virtual void serializeInto(sf::Packet& packet) = 0;
	virtual void deserializeFrom(sf::Packet& packet) = 0;

	
};


#endif //CYBERPUNK_CMESSAGE_HPP
