#ifndef CYBERPUNK_CSKILLPOWERBEACON_HPP
#define CYBERPUNK_CSKILLPOWERBEACON_HPP


#include "CSkill.hpp"
#include "CPowerBeacon.hpp"

class CSkillPowerBeacon : public CSkill {
private:
	vector<CPowerBeacon*> beacons;
	int maxNumOfBeacons = 3;
	
public:
	CSkillPowerBeacon(CPlayer *owner) :
	CSkill(owner, 250, 50) {};

protected:
	void onButtonPress();

private:
	void spawnPowerBeacon();

};


#endif //CYBERPUNK_CSKILLPOWERBEACON_HPP
