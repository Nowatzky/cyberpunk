#ifndef CYBERPUNK_CCLOCK_HPP
#define CYBERPUNK_CCLOCK_HPP

#include <SFML/System/Clock.hpp>

class CClock {
private:
	sf::Clock clock;
	sf::Time offset = sf::Time::Zero;;

public:
	CClock();
	CClock(sf::Time offset);
	CClock(float offset);

public:
	sf::Time restart();
	sf::Time restart(sf::Time offset);
	sf::Time restart(float offset);
	void restartAtGameTime(float gameTime);
	
	sf::Time getElapsedTime() const;
	float getTimeMs() const;
	bool hasPassed(float time) const;
};


#endif //CYBERPUNK_CCLOCK_HPP


