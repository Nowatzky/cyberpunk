#ifndef CYBERPUNK_CCHARACTER_HPP
#define CYBERPUNK_CCHARACTER_HPP

#include "CObject.hpp"
#include "CEffectManager.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"


class CCharacter : public CObject {
protected:
	float maxHealth;
	float health;
	float movementSpeed;

	CBoundingBox boundingBox;
	CGraphicBox graphicBox;
	
	CEffectManager effectManager;

public:
	CCharacter(Vector3f pos, Vector3f dim, float rot = 0, Color color = Color::Red, float maxHealth = 100, float movementSpeed = 5);
	
	virtual ~CCharacter() {};

	// ToDo refactor
	bool isPushed = false;
	Vector2f restrictedMovement = Vector2f(0, 0);
	
	virtual void tick(float frameTime);

	/* Accessors */
	float getMovementSpeed() const;
	float getHealth() const;
	float getMaxHealth() const;

	virtual void setMovementSpeed(float movementSpeed);
	virtual void setHealth(float health);
	virtual void reduceHealth(float damage);
	virtual void healHealth(float heal);
	virtual bool isGoingToDie();

	virtual bool move(Vector2f desiredMovement, bool restrictMovement, bool wasPushed);
	virtual void rotate(float angle);
	virtual void push(Vector2f desiredMovement);
	
	virtual void teleport(Vector3f targetPos);

	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();
	
	void addEffect(CEffect* effect);
	CEffectManager* getEffectManager();

	virtual void onPushed() {};
	virtual void onStunned() {};

	virtual void die();

private:
	struct PossibleMovement{
		bool hit;
		float length;
		Vector2f surfaceNormal;
	};

	PossibleMovement getPossibleMovement(const c2Poly* p, Vector3f movement, vector<const c2Poly*> obstacles);
};


#endif //CYBERPUNK_CCHARACTER_HPP
