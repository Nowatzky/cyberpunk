#ifndef CYBERPUNK_CGAMESTATEMANAGER_HPP
#define CYBERPUNK_CGAMESTATEMANAGER_HPP


#include "CDebugHelper.hpp"
#include "CPlayer.hpp"
#include "CTempSpawningManager.hpp"

class CGameStateManager {
private:
	static bool isAuthoritative;
	static std::vector<CPlayer*> actualPlayers;
	static CPlayer* localPlayer;
	static CTempSpawningManager* spawningManager;
	
	static int currentRoom;
	static bool arePlayersTravelingToCurrentRoom;
	static bool wasGameOverTriggered;
	
	static bool waitingForRoomEnterDelay;
	static CClock roomEnterDelayClock;
	
public:
	static void init(CPlayer* localPlayer, CTempSpawningManager* spawningManager);
	static void addRemotePlayer(CPlayer* remotePlayer);
	
	static void tick();
	
	static void setCurrentRoom(int currentRoom);	
	
	static Vector3f getViewCenter();
	static bool isLocalPlayerAlive();
	static bool isGameOver();
	static void triggerGameOver();
	
	static int getSurvivedWaves();
	
	static void reviveParty();
	
private:
	explicit CGameStateManager() = default;
	
	static bool areAllPlayersInCurrentRoom();
	static void broadcastRoomStatus(bool roomCompleted); 
	static void updateBlurEffect();
};


#endif //CYBERPUNK_CGAMESTATEMANAGER_HPP
