#ifndef CYBERPUNK_CPROJECTILEROCK_HPP
#define CYBERPUNK_CPROJECTILEROCK_HPP

#include "CProjectile.hpp"
#include "CColorDefines.hpp"

class CProjectileRock : public CProjectile {
private:
	float damageFactor = 0.5;
	
public:
    CProjectileRock(CCharacter* initiator, const Vector3f &pos, const Vector2f &dir) :
            CProjectile(initiator, pos, Vector3f(0.5,0.5,0.5), COLOR_PINK_ALPHA, dir, 10) {
	    setLifetime(3000);
    }

    void onHitEnemy(CEnemy* target, float frameTime);
};


#endif //CYBERPUNK_CPROJECTILEROCK_HPP
