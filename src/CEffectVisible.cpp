#include "CEffectVisible.hpp"
#include "CEffectManager.hpp"
#include "CEngine.hpp"

bool CEffectVisible::tick(float frameTime, CEffectAggregate& effectAggregate) {
	if(hasExpiredLifetime()) {
		return false;
	}
	
	float newMinAlpha = CEngine::invertAlpha(CEngine::clampAlpha(lifetimeClock.getTimeMs() / lifetime)) * maxAlpha;
	
	effectAggregate.hasMinAlpha = true;
	effectAggregate.minAlpha = max(effectAggregate.minAlpha, newMinAlpha);
	
	return true;
}
