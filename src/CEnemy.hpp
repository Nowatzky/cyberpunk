#ifndef CYBERPUNK_CENEMY_HPP
#define CYBERPUNK_CENEMY_HPP


#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"
#include "CCharacter.hpp"
#include "CHealthBar.hpp"
#include "CNavAgent.hpp"
#include "CDifficultyManager.hpp"

class CAIController;

using namespace sf;

struct CTurningEnemyMovementProfile {
	bool isTurningEnemy;
	float turningSpeedFactor; // Max percentage of angle to target // TODO does this make any sense? Rework!
	float maxTurningSpeed; // Max degree per second
	float slowdownFactorAtMaxMisalignment; // Slowdown factor at >= 90 degree misalignment
};

class CEnemy : public CCharacter, public CNavAgent {
protected:
	bool isActive = true;
	
	CAIController* aiController;
	CHealthBar healthBar;
	
	CTurningEnemyMovementProfile turningMovementProfile;

protected:
	CEnemy(CAIController* aiController, Vector3f pos, float rot, Vector3f dim, Color color, float maxHealth, float movingSpeed) :
			CCharacter(pos, dim, rot, color, maxHealth * CDifficultyManager::getEnemyHealthModifier(), movingSpeed),
			CNavAgent(this),
			aiController(aiController),
			healthBar(maxHealth * CDifficultyManager::getEnemyHealthModifier()) {
		
		turningMovementProfile.isTurningEnemy = true;
		turningMovementProfile.maxTurningSpeed = 420.f;
		turningMovementProfile.turningSpeedFactor = 3.5f;
		turningMovementProfile.slowdownFactorAtMaxMisalignment = 0.25f;

	}

	~CEnemy();

public:
	virtual void tick(float frameTime);
	virtual void tickAuthority(float frameTime);

	virtual bool move(Vector2f desiredMovement, bool restrictMovement);
	virtual void rotate(float angle);

	virtual void setHealth(float health);
	virtual void reduceHealth(float damage);
	virtual void healHealth(float heal);

	CTurningEnemyMovementProfile getTurningEnemyMovementProfile();
	
	void setActive(bool active);

	void setNotLocal(CObjectId) override;
	
	virtual bool canBePushedByBullet();
	
	vector<float> metaData = {0, 0, 0, 0}; //vector of enemy [1] cost [2] points [3] intensity //TODO

	void die() override;
};

#endif //CYBERPUNK_CENEMY_HPP
