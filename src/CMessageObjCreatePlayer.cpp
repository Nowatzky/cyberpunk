#include "CMessageObjCreatePlayer.hpp"
#include "CDebugHelper.hpp"
#include "CSceneManager.hpp"
#include "CGameStateManager.hpp"

void CMessageObjCreatePlayer::serializeInto(sf::Packet& packet) {
	packet << objectId
           << pos.x
           << pos.y
           << pos.z
           << rot;
			
	Uint8 tmp = static_cast<sf::Uint8>(className);
	packet << tmp;
	
	//serializeEnumInto(packet, className);
}

void CMessageObjCreatePlayer::deserializeFrom(sf::Packet& packet) {
	packet >> objectId
           >> pos.x
           >> pos.y
           >> pos.z
           >> rot;

	Uint8 tmp;
	packet >> tmp;
	className = static_cast<CClassName>(tmp);
			
	//className = deserializeEnumFrom<CClassName>(packet);
}

void CMessageObjCreatePlayer::process() {
	CPlayer* newPlayer = new CPlayer(pos, Vector3f(0.35f, 0.35f, 0.6f), className);
	newPlayer->setNotLocal(objectId);
	newPlayer->setIsMovementReplicated(true);
	CSceneManager::safeAddObject(newPlayer);
	
	CGameStateManager::addRemotePlayer(newPlayer);
}
