#ifndef CYBERPUNK_CTEMPSPAWNINGMANAGER_HPP
#define CYBERPUNK_CTEMPSPAWNINGMANAGER_HPP


#include "CDifficultyManager.hpp"
#include "CTempSpawnPoint.hpp"
#include "CClock.hpp"

class CNavMesh;

enum class CEnemyTypeHologram : sf::Uint8 {
	GHOST,
	GHOST_INVISIBLE,
	GHOSTSUMMONER,
	SPECTER,
	LICH,
};

struct CSpawnPointInfo {
	CTempSpawnPoint* spawnPoint = nullptr;
	std::vector<CEnemyTypeHologram> enemies;
	CClock spawnClock;
};

struct CRoomSpawnInfo {
	int budget;
	int totalSpawnPointCount;
	int maxActiveSpawnPoints;
};

struct CWaveType {
	std::string name;
	std::vector<float> distribution;
	int minBudget;
};

class CTempSpawningManager {
private:
	CDifficulty difficulty;
	int playerCount;

	bool isCurrentRoomCompleted;
	
	CNavMesh* currentNavMesh;
	std::vector<CSpawnPointInfo> activeSpawnPoints;
	std::vector<CSpawnPointInfo> remainingSpawnPoints;
	int maxActiveSpawnPoints;
	
	float spawnInterval = 1100;
	
	CClock spawnPointSpawnClock;
	float spawnPointSpawnInterval = 2000;
	
	float playerScalingExponent = 1.2f;

public:
	explicit CTempSpawningManager(CDifficulty difficulty, int playerCount);

	void tick(float frameTime);
	void enterRoom(int roomNumber, CNavMesh* navMesh);
	
	bool getIsCurrentRoomCompleted();

private:
	void spawnEnemy(CEnemyTypeHologram enemyType, Vector2f spawnPointPos);
	CTempSpawnPoint* spawnSpawnPoint();

	std::vector<CSpawnPointInfo> generateSpawnPoints(CRoomSpawnInfo roomSpawnInfo, CWaveType waveType);
	CRoomSpawnInfo getRoomSpawnInfo(int roomNumber);
	CWaveType getRandomWaveType(int budget);
	int getEnemyCost(CEnemyTypeHologram enemyType);
	Vector2f findFreeSpawnPos(Vector2f spawnPoint);
	
};


#endif //CYBERPUNK_CTEMPSPAWNINGMANAGER_HPP
