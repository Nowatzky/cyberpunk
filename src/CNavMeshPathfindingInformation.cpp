#include "CNavMeshPathfindingInformation.hpp"

void CNavMeshPathfindingInformation::explore(PriorityPathfindingInformationSet* openSet, CNavMeshPathfindingInformation* prevInfo, Vector2f targetPos, bool addToSet) {
	float dist = CEngine::getDistance(pos, prevInfo->pos);
	if(prevInfo->g + dist < g) {
		prev = prevInfo;
		g = prevInfo->g + dist;
		f = CEngine::getDistance(pos, targetPos);
		if(addToSet) {
			openSet->push((CNavMeshPathfindingInformation*&&) this);
		}
	}
}

CNavMeshPathfindingInformation* CNavMeshPathfindingInformation::getForPoint(PathfindingInformationMap* exploredSet, Vector2f pos, CNavPolygon* polygon) {
	return &(exploredSet->insert(make_pair(pos, CNavMeshPathfindingInformation(pos, polygon))).first)->second;
}
