
#include "CProjectileAlphaBlast.hpp"
#include "CSceneManager.hpp"
#include "CObjectProjectileHitPopUpBox.hpp"
#include "CEffectVisible.hpp"

void CProjectileAlphaBlast::tick(float frameTime) {
	checkTargetLost(frameTime);
	
	if(target != nullptr) {
		targetPos = target->getPosition();
		dir = CEngine::normalize(CEngine::to2D(targetPos - pos));	
	}

	updatePosition();	

	if(pos.z <= targetPos.z + 0.01f) {
		onHitWall(frameTime);
		return;
	}
	
	CProjectile::tick(frameTime);
}

void CProjectileAlphaBlast::onHitEnemy(CEnemy* target, float frameTime) {
	target->reduceHealth(damage);
	target->addEffect(new CEffectMark(nullptr, CEffectMark::EffectMarkType::Lightning, 1));
	target->addEffect(new CEffectVisible(nullptr, 140, 500));
	CPlayer::checkKillPotential(target, initiator);

	CSceneManager::safeRemoveObject(this);
}

void CProjectileAlphaBlast::onHitWall(float frameTime) {
	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(getPosition(), Vector3f(0.15f, 0.15f, 0.15f), COLOR_PINK_ALPHA - Color(0, 0, 0, 30), 35));
	CSceneManager::safeRemoveObject(this);
}

void CProjectileAlphaBlast::updatePosition() {
	if(!hasPassedApex) {
		speed = preApexSpeed;

		float alphaZ = CEngine::clampAlpha(CEngine::getDistance(CEngine::to2D(startPos), CEngine::to2D(pos)) / apexDistance);

		alphaZ = CEngine::clamp(2.4f * alphaZ - 1.4f * alphaZ * alphaZ, 0, 1);

		float z = CEngine::lerp(startPos.z, apexZ, alphaZ);
		setPosition(CEngine::toPlanar3D(CEngine::to2D(getPosition())) + Vector3f(0, 0, z));

		if(alphaZ == 1) {
			hasPassedApex = true;
			apexClock.restart();
		}
	} else {
		speed = postApexBaseSpeed + sqrt(apexClock.getTimeMs()/1000.f) * postApexSpeedIncrease;

		float distStartTarget = CEngine::getDistance(CEngine::to2D(startPos), CEngine::to2D(targetPos)) - apexDistance;
		float distCurrentTarget = CEngine::getDistance(CEngine::to2D(pos), CEngine::to2D(targetPos));

		float alphaZ = CEngine::invertAlpha(CEngine::clampAlpha(distCurrentTarget/distStartTarget));

		alphaZ = CEngine::clamp(+0.322222f * alphaZ +0.747222f * alphaZ * alphaZ +0.902778f * alphaZ * alphaZ * alphaZ -0.972222f *alphaZ*alphaZ*alphaZ*alphaZ, 0, 1);

		float z = CEngine::lerp(apexZ, targetPos.z, alphaZ);
		setPosition(CEngine::toPlanar3D(CEngine::to2D(getPosition())) + Vector3f(0, 0, z));
	}
}

void CProjectileAlphaBlast::checkTargetLost(float frameTime) {
	if(target == nullptr) {
		return;
	}

	if(!CSceneManager::doesObjectExist(target)) {
		target = nullptr;
		return;
	}

	Vector2f currentTargetPosition = CEngine::to2D(target->getPosition());
	float distance = CEngine::getDistance(lastTargetPosition, currentTargetPosition);
	
	if(distance >= targetLostMaxDistance * frameTime) {
		targetPos = CEngine::toPlanar3D(lastTargetPosition);
		targetPos.z = target->getPosition().z;
		
		dir = CEngine::normalize(CEngine::to2D(targetPos - pos));
		target = nullptr;
	}

	lastTargetPosition = currentTargetPosition;

}
