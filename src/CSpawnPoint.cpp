#include <iostream>
#include "CSpawnPoint.hpp"
#include "CSceneManager.hpp"
#include "CUtilityDefines.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"

CSpawnPoint::CSpawnPoint(Vector3f pos, vector<CEnemy*> spawnList, CSpawningManager* manager) :
		CObject(pos),
		boundingBox(pos, Vector3f(0.2f, 0.2f, 0.8f)),
		graphicBox(pos, Vector3f(0.2f, 0.2f, 0.8f), colorBaseNormal, colorAccentNormal),
		spawnList(spawnList),
		manager(manager) {
	
	Vector2f dim = Vector2f(boundingBox.getDimension().x, boundingBox.getDimension().y);
	
	blockedArea = CRectangleArea(CEngine::to2D(pos) - dim, dim * 2.f);
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->blockArea(blockedArea);
	
	boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);

	riseOffsetX = CRandom::getInRange(-3,3);
	riseOffsetY = CRandom::getInRange(-3,3);
	
	for(CEnemy* enemy : spawnList) {
		enemy->setActive(false);
		enemy->getGraphicContainer()->setVisible(false);
	}
}

CCollisionContainer* CSpawnPoint::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer* CSpawnPoint::getGraphicContainer() {
	return &graphicBox;
}

void CSpawnPoint::tick(float frameTime) {
	if(spawnList.empty() && spawningEnemies.empty() && !isDespawning) {
		deletePoint();
	}

	if(!spawnList.empty() && clockSpawnCall.getElapsedTime().asMilliseconds() >= manager->getSpawnInterval()) {
		clockSpawnCall.restart();

		unsigned int random = (unsigned int) CRandom::getInRangeI(0, (int) (spawnList.size() - 1));
		spawnEnemy(spawnList.at(random));
		spawnList.erase(spawnList.cbegin() + random);
	}
	
	if(isRising) {
		float alpha = clockRise.getElapsedTime().asMilliseconds()/riseDuration;

		if(alpha >= 1) {
			alpha = 1;
			isRising = false;
		}
			
		graphicBox.setPosOffset(Vector3f(CEngine::lerp(riseOffsetX, 0, alpha), CEngine::lerp(riseOffsetY, 0, alpha), CEngine::lerp(40, 0, alpha)));
		graphicBox.setScaleOffset(CEngine::lerp(0.4f, 1, alpha));
	}
	
	// Spawn enemies
	if(!isDespawning && !isRising) {
		for(auto it = spawningEnemies.cbegin(); it != spawningEnemies.cend(); /*no increment*/) {
			SpawningEnemy spawningEnemy = *it;
			CGraphicContainer* graphicContainer = spawningEnemy.enemy->getGraphicContainer();

			float timeAlpha = spawningEnemy.spawningClock.getElapsedTime().asMilliseconds()/spawningTime;

			// Stretch time to archive slow ending
			if(timeAlpha < 0.8f) {
				timeAlpha *= 0.6f;
			} else if(timeAlpha < 0.95f) {
				timeAlpha *= 0.8f;
			}
			
			if(timeAlpha < 1) {
				graphicContainer->setPosition(CEngine::lerp(spawningEnemy.startPoint, spawningEnemy.endPoint, timeAlpha));
				graphicContainer->setScaleOffset(CEngine::lerp(0.4f, 1, timeAlpha));

				// Only fade color if not invisible
				if(spawningEnemy.colorNormalBase.a > 5) {
					graphicContainer->setColorBase(CEngine::lerp(Color(120, 0, 255, 255), spawningEnemy.colorNormalBase, timeAlpha));
					graphicContainer->setColorAccent(CEngine::lerp(Color(120, 0, 255, 255), spawningEnemy.colorNormalAccent, timeAlpha));
				}

				++it;
			} else {
				graphicContainer->setPosition(spawningEnemy.endPoint);
				graphicContainer->setScaleOffset(1);
				graphicContainer->setColorBase(spawningEnemy.colorNormalBase);
				graphicContainer->setColorAccent(spawningEnemy.colorNormalAccent);
				spawningEnemy.enemy->setActive(true);

				it = spawningEnemies.erase(it); // No ++it here
			}		
		}
	}

	// Glow
	glowingTimeOffset += frameTime;
	float sinus = sin(glowingTimeOffset * glowingFrequency);
	float offsetR = 0.6f * glowingAmplitude * sinus;
	float offsetG = 0.2f * glowingAmplitude * sinus;
	float offsetB = 1.f  * glowingAmplitude * sinus;
	float scaleOffset = glowingScaleAmplitude * sinus;

	Color colorBase = colorBaseNormal;
	colorBase.r = (Uint8) CEngine::clamp(colorBase.r + offsetR, 0, 255);
	colorBase.g = (Uint8) CEngine::clamp(colorBase.g + offsetG, 0, 255);
	colorBase.b = (Uint8) CEngine::clamp(colorBase.b + offsetB, 0, 255);
	graphicBox.setColorBase(colorBase);

	Color colorAccent = colorAccentNormal;
	colorAccent.r = (Uint8) CEngine::clamp(colorAccent.r + offsetR, 0, 255);
	colorAccent.g = (Uint8) CEngine::clamp(colorAccent.g + offsetG, 0, 255);
	colorAccent.b = (Uint8) CEngine::clamp(colorAccent.b + offsetB, 0, 255);
	graphicBox.setColorAccent(colorAccent);

	graphicBox.setScaleOffset(1 + scaleOffset);
	
	if(isDespawning) {
		if(clockDespawning.getElapsedTime().asMilliseconds() >= despawnTime) {
			CSceneManager::safeRemoveObject(this);
			return;
		}

		float despawnAlpha = clockDespawning.getElapsedTime().asMilliseconds()/despawnTime;
		despawnAlpha = CEngine::clampAlpha(despawnAlpha);

		Color currentColorBase = graphicBox.getColorBase();
		currentColorBase.a = (Uint8) CEngine::lerp(255, 0, despawnAlpha);
		graphicBox.setColorBase(currentColorBase);

		Color currentColorAccent = graphicBox.getColorAccent();
		currentColorAccent.a = (Uint8) CEngine::lerp(255, 0, despawnAlpha);
		graphicBox.setColorAccent(currentColorAccent);
	}
}

void CSpawnPoint::spawnEnemy(CEnemy* enemy) {
	Vector3f spawnPos = findFreeSpawnPosition(enemy);

	enemy->setPosition(spawnPos);
	Vector3f graphicPosition = getPosition();
	graphicPosition.z = enemy->getPosition().z;
	enemy->getGraphicContainer()->setPosition(graphicPosition);

	if(enemy->getTurningEnemyMovementProfile().isTurningEnemy) {
		Vector2f direction = CEngine::to2D(enemy->getPosition() - getPosition());
		enemy->setRotation(CEngine::getRotation(direction));	
	}
	
	SpawningEnemy spawningEnemy;
	spawningEnemy.enemy = enemy;
	spawningEnemy.startPoint = graphicPosition;
	spawningEnemy.endPoint = enemy->getPosition();
	spawningEnemy.colorNormalBase = enemy->getGraphicContainer()->getColorBase();
	spawningEnemy.colorNormalAccent = enemy->getGraphicContainer()->getColorAccent();

	enemy->getGraphicContainer()->setVisible(true);

	spawningEnemies.push_back(spawningEnemy);

	CSceneManager::safeAddObject(enemy);
}

Vector3f CSpawnPoint::findFreeSpawnPosition(CEnemy* enemy) {
	Vector3f origin = Vector3f(getPosition().x, getPosition().y, enemy->getPosition().z);
	Vector3f dimension = Vector3f(0.5f, 0.5f, 0.1f);
	
	if(dynamic_cast<CBoundingBox*>(enemy->getCollisionContainer()) != nullptr) {
		dimension = static_cast<CBoundingBox*>(enemy->getCollisionContainer())->getDimension();
		dimension.z = 0.1f;
	}
	
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
	
	float spawnRadiusOffset = 0;
	Vector3f spawnPos = VECTOR3D_INVALID;	
	
	while(spawnPos == VECTOR3D_INVALID) {
		spawnPos = CSceneManager::findSpawnPoint(origin, dimension, collisionChannel, 0, spawnRadiusStart + spawnRadiusOffset, spawnRadiusStop + spawnRadiusOffset, 0.35f);
		spawnRadiusOffset += 1;
	}

	return spawnPos;	
}

CSpawnPoint::~CSpawnPoint() {
	for(SpawningEnemy spawningEnemy : spawningEnemies) {
		CSceneManager::safeRemoveObject(spawningEnemy.enemy);
	}
	
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->freeArea(blockedArea);
}

void CSpawnPoint::deletePoint() {
	isDespawning = true;
	clockDespawning.restart();
}

bool CSpawnPoint::getIsDespawning() {
	return isDespawning;
}


