#include "CNavPath.hpp"
#include "CSceneManager.hpp"
#include "CMapBlock.hpp"
#include "CNavMesh.hpp"
#include "CUtilityDefines.hpp"

CNavPath::~CNavPath() {
	removeDebugVisuals();
}

void CNavPath::prependWaypoint(Vector2f waypoint) {
	waypoints.push_front(waypoint);
}

void CNavPath::appendWaypoint(Vector2f waypoint) {
	waypoints.push_back(waypoint);
}

void CNavPath::smoothFromStart(Vector2f start, float agentHalfWidth) {
	while(waypoints.size() > 1) {
		if(envelopingNavMesh->isLineOfSightFree(start, waypoints.at(1), agentHalfWidth)) {
			waypoints.pop_front();
		} else {
			break;
		}		
	}	
}

void CNavPath::smoothCompletely(Vector2f start, float agentHalfWidth) {
	int currentIndex = -1;
	Vector2f current;
	
	while(currentIndex < (int) (waypoints.size()) - 2) {
		if(currentIndex == -1) {
			current = start;
		} else {
			current = waypoints.at((unsigned long) currentIndex);
		}

		if(envelopingNavMesh->isLineOfSightFree(current, waypoints.at((unsigned long) (currentIndex + 2)), agentHalfWidth)) {
			waypoints.erase(waypoints.begin() + currentIndex + 1);
		} else {
			currentIndex++;
		}
	}
}

bool CNavPath::updateTarget(Vector2f start, Vector2f target, float agentHalfWidth) {
	if(waypoints.size() <= 1) {
		waypoints.clear();
		waypoints.push_back(target);
		
		return envelopingNavMesh->isLineOfSightFree(start, target, agentHalfWidth);
	}

	waypoints.pop_back();
	Vector2f prev = waypoints.back();
	waypoints.push_back(target);
	return envelopingNavMesh->isLineOfSightFree(prev, target, agentHalfWidth);
}

void CNavPath::addDebugVisuals(Vector2f startPoint) {
	if(waypoints.size() > 0) {
		debugVisuals.push_back(createLine(startPoint, waypoints.front()));
	}
	
	if(waypoints.size() > 1) {
		for(unsigned long i = 0; i < waypoints.size() - 1; i++) {
			debugVisuals.push_back(createLine(waypoints.at(i), waypoints.at(i + 1)));
		}
	}
	
	for(CObject* visual : debugVisuals) {
		CSceneManager::safeAddObject(visual);
	}
}

void CNavPath::removeDebugVisuals() {
	for(CObject* visual : debugVisuals) {
		CSceneManager::safeRemoveObject(visual);
	}
	debugVisuals.clear();
}

CObject* CNavPath::createLine(Vector2f p1, Vector2f p2) {
	Vector3f pos(((p1+p2)/2.f).x, ((p1+p2)/2.f).y, 0.6f);	
	Vector3f dim(0.35, CEngine::getDistance(p1, p2)/2.f, 0.03f);
	Vector2f connectVec = p2 - p1;
	float rot = CEngine::getRotation(connectVec);
	
	CMapBlock* line = new CMapBlock(pos, dim, Color(0, 0, 200, 150), false);
	line->setRotation(rot);
	line->getCollisionContainer()->setCollisionChannel(CCollisionChannel());

	return line;
}

float CNavPath::getLength(Vector2f start) const {
	float length = 0;
	Vector2f prev = start;
	
	for(Vector2f curr : waypoints) {
		length += CEngine::getDistance(prev, curr);
		prev = curr;
	}

	return length;
}

Vector2f CNavPath::getFirstWaypoint() const {
	if(waypoints.size() > 0) {
		return waypoints.front();
	}

	return VECTOR2D_INVALID;
}

void CNavPath::removeFirstWaypoint() {
	waypoints.pop_front();
}

