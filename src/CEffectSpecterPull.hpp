#ifndef CYBERPUNK_CEFFECTSPECTERPULL_HPP
#define CYBERPUNK_CEFFECTSPECTERPULL_HPP


#include "CEffect.hpp"
#include "CEnemySpecter.hpp"
#include "CLineEffect.hpp"

class CEffectSpecterPull : public CEffect {
private:
	bool isPulling = false;
	
	float stunDuration;
	float pullDuration;
	float pullStrength;
	
	CLineEffect* lineEffect;

public:
	CEffectSpecterPull(CCharacter* initiator, float stunDuration, float pullDuration, float pullStrength) :
	CEffect(initiator, stunDuration),
	stunDuration(stunDuration),
	pullDuration(pullDuration),
	pullStrength(pullStrength) {
		setId("CEffectSpecterPull");
	}
	
	virtual ~CEffectSpecterPull();

	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);

	virtual void onStart();

	virtual std::vector<const sf::Texture*> getVisuals();
	virtual int getVisualPriority();

};


#endif //CYBERPUNK_CEFFECTSPECTERPULL_HPP
