#include "CAIControllerLich.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CEnemyLich.hpp"


void CAIControllerLich::tick(float frameTime) {
	if(host->getIsDodging()) {
		return;
	}
	
	if(host->getHealth() < previousHealth) {
		if(host->isDodgeReady()) {
			host->dodge();
		}
	}
	previousHealth = host->getHealth();
	
    if(host->isActing) {
        if(host->isCasting()) { //do nothing
	        return;
        }
        host->deployCurrentSpell();
    }

	if(host->hasReachedTarget() || host->isStuck(frameTime)) {
		pickNewWanderTarget();
    }

	host->updatePath();
	host->followPath(frameTime);

    // Cast Spell
    if(host->isAnySpellCastReady()) {
        castAnySpell();
    }
}

void CAIControllerLich::pickNewWanderTarget() {
	host->findPathTo(CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(host->getPosition()))->getRandomPointInside());
}

void CAIControllerLich::castAnySpell() {
	LichSpell spell;
	bool foundSpellToCast = false;
	
	while(!foundSpellToCast) {
		 spell = pickNextSpell();

		/* check if spell is reasonable */
		switch(spell) {
			case LichSpell::FIRE:
				if(isFireSpellApplicable()) {
					foundSpellToCast = true;
				}
				break;
			case LichSpell::DMG_FIELD:
				foundSpellToCast = true;
				break;
		}
	}

    host->beginCasting(spell);
}

LichSpell CAIControllerLich::pickNextSpell() {
	int spellNumber = CRandom::getChoiceFromBackground(host->spellProbabilities);

	if(spellNumber == 0) {
		return LichSpell::FIRE;
	} else {
		return LichSpell::DMG_FIELD;
	}
}

bool CAIControllerLich::isFireSpellApplicable() {
	CPlayer* player = CSceneManager::getPlayers()[0];
	float dist = CEngine::getDistance(host->getPosition(), player->getPosition());
 	Vector3f pos = (host->getPosition() + player->getPosition()) * 0.5f;
	Vector3f dim = Vector3f(0.15f, dist/2.f - 0.2f, 0.25f);
	Vector2f dirToPlayer = CEngine::to2D(player->getPosition() - host->getPosition());
	float rot = CEngine::getRotation(dirToPlayer);

	CBoundingBox box(pos, dim, rot);
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::CHARACTER, CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::PROJECTILE});

	bool isWayFree = true;
	for(CObject* colliding : CSceneManager::getIntersecting(&box, collisionChannel)) {
		if(colliding != player && colliding != host) {
			isWayFree = false;
			break;
		}
	}

	return isWayFree;
}

