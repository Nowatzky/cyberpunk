#include "CTopologicalMap.hpp"
#include "CGraphicContainer.hpp"

void CTopologicalMap::addObject(CObject *object) {
	if(object->getIsStatic()) {
		nodesStatic.push_back(CTopologicalNode(object));
		requestReconstruction();
	} else {
		nodesMovable.push_back(CTopologicalNode(object));
	}
}

void CTopologicalMap::removeObject(CObject* object) {
	if(object->getIsStatic()) {
		requestReconstruction();
		for(vector<CTopologicalNode>::iterator it = nodesStatic.begin(); it != nodesStatic.end(); ++it) {
			if(it->object == object) {
				nodesStatic.erase(it);
				break;
			}
		}
	} else {
		for(vector<CTopologicalNode>::iterator it = nodesMovable.begin(); it != nodesMovable.end(); ++it) {
			if(it->object == object) {
				nodesMovable.erase(it);
				break;
			}
		}
	}
}

void CTopologicalMap::constructTopologicalMap() {
	clearNodeChildrenMovable();
	
    for(vector<CTopologicalNode>::iterator it1 = nodesStatic.begin(); it1 != nodesStatic.end(); ++it1) {
	    for(vector<CTopologicalNode>::iterator it2 = nodesMovable.begin(); it2 != nodesMovable.end(); ++it2) {
	        if(it1->object->getGraphicContainer()->doesOverlap(*it2->object->getGraphicContainer())) {
		        if(it1->object->getGraphicContainer()->isInFrontOf(*it2->object->getGraphicContainer())) {
			        it2->childrenMovable.push_back(&(*it1));
		        } else {
			        it1->childrenMovable.push_back(&(*it2));
		        }
	        }
        }
    }

	for(vector<CTopologicalNode>::iterator it1 = nodesMovable.begin(); it1 != nodesMovable.end(); ++it1) {
		for(vector<CTopologicalNode>::iterator it2 = it1 + 1; it2 != nodesMovable.end(); ++it2) {
			if(it1->object->getGraphicContainer()->doesOverlap(*it2->object->getGraphicContainer())) {
				if(it1->object->getGraphicContainer()->isInFrontOf(*it2->object->getGraphicContainer())) {
					it2->childrenMovable.push_back(&(*it1));
				} else {
					it1->childrenMovable.push_back(&(*it2));
				}
			}
		}
	}
}

void CTopologicalMap::reconstructTopologicalMapForStatics() {
	clearNodeChildrenStatic();
	
	for(vector<CTopologicalNode>::iterator it1 = nodesStatic.begin(); it1 != nodesStatic.end(); ++it1) {
		for(vector<CTopologicalNode>::iterator it2 = it1 + 1; it2 != nodesStatic.end(); ++it2) {
			if(it1->object->getGraphicContainer()->doesOverlap(*it2->object->getGraphicContainer())) {
				if(it1->object->getGraphicContainer()->isInFrontOf(*it2->object->getGraphicContainer())) {
					it2->childrenStatic.push_back(&(*it1));
				} else {
					it1->childrenStatic.push_back(&(*it2));
				}
			}
		}
	}
}

void CTopologicalMap::visitNode(CTopologicalNode* node, deque<CObject*>* list) { 
	if(!node->marked) {
        node->marked = true;
        for(CTopologicalNode* m : node->childrenStatic) {
            visitNode(m, list);
        }
	    for(CTopologicalNode* m : node->childrenMovable) {
		    visitNode(m, list);
	    }

	    list->push_front(node->object);
    }
}

deque<CObject*> CTopologicalMap::getObjectHierarchy() {
	if(needsReconstruction) {
		needsReconstruction = false;
		reconstructTopologicalMapForStatics();
	}
	
    constructTopologicalMap();

	unmarkAllNodes();
	deque<CObject*> list;

    for(CTopologicalNode& n: nodesStatic) {
        visitNode(&n, &list);
    }
	for(CTopologicalNode& n: nodesMovable) {
		visitNode(&n, &list);
	}
	
	return list;
}

void CTopologicalMap::requestReconstruction() {
	needsReconstruction = true;
}

void CTopologicalMap::clearNodeChildrenStatic() {
	for(vector<CTopologicalNode>::iterator it = nodesStatic.begin(); it != nodesStatic.end(); ++it) {
		it->childrenStatic.clear();
	}
	for(vector<CTopologicalNode>::iterator it = nodesMovable.begin(); it != nodesMovable.end(); ++it) {
		it->childrenStatic.clear();
	}
}

void CTopologicalMap::clearNodeChildrenMovable() {
	for(vector<CTopologicalNode>::iterator it = nodesStatic.begin(); it != nodesStatic.end(); ++it) {
		it->childrenMovable.clear();
	}
	for(vector<CTopologicalNode>::iterator it = nodesMovable.begin(); it != nodesMovable.end(); ++it) {
		it->childrenMovable.clear();
	}
}

void CTopologicalMap::unmarkAllNodes() {
	for(vector<CTopologicalNode>::iterator it = nodesStatic.begin(); it != nodesStatic.end(); ++it) {
		it->marked = false;
	}
	for(vector<CTopologicalNode>::iterator it = nodesMovable.begin(); it != nodesMovable.end(); ++it) {
		it->marked = false;
	}
}
