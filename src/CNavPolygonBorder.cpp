#include <iostream>
#include "CNavPolygonBorder.hpp"
#include "CEngine.hpp"

bool CNavPolygonBorder::isOpposingParallelAndIntersecting(CNavPolygonBorder other) const {
	return this->opposes(other) && CEngine::doLineSegmentsIntersect(left, right, other.left, other.right);
}

Vector2f CNavPolygonBorder::getLeft() const {
	return left;
}

Vector2f CNavPolygonBorder::getRight() const {
	return right;
}

bool CNavPolygonBorder::opposes(CNavPolygonBorder other) const {
	return CEngine::areInOppositeDirection(left - right, other.left - other.right);
}

bool CNavPolygonBorder::operator==(const CNavPolygonBorder& other) const {
	return this->left == other.left && this->right == other.right;
}

bool CNavPolygonBorder::operator!=(const CNavPolygonBorder& other) const {
	return !(this->operator==(other));
}
