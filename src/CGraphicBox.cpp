#include <iostream>
#include "CGraphicBox.hpp"

using namespace sf;

/*1
 / \
0   2
 \ /
  3

Rotation is clockwise positive
0 degree is at positive y axis ( / )
*/

CGraphicBox::CGraphicBox(Vector3f pos, Vector3f dim, Color color) :
		CGraphicContainer(pos, color),
        dim(dim) {
	sides = VertexArray(sf::Quads, 12);
	lines = VertexArray(sf::LinesStrip, 11);
	
	update();
}

CGraphicBox::CGraphicBox(Vector3f pos, Vector3f dim, Color colorBase, Color colorAccent) :
		CGraphicContainer(pos, colorBase, colorAccent),
		dim(dim) {
	sides = VertexArray(sf::Quads, 12);
	lines = VertexArray(sf::LinesStrip, 11);

	update();
}

bool CGraphicBox::isInFrontOf(const CGraphicContainer& other) const {
	if(const CGraphicBox* graphicBox = dynamic_cast<const CGraphicBox*>(&other)) {
		return CEngine::isInFrontOf(*this, *graphicBox);
	} else if(const CGraphicArea* graphicArea = dynamic_cast<const CGraphicArea*>(&other)) {
		return CEngine::isInFrontOf(*this, *graphicArea);
	}

	return false;
}

bool CGraphicBox::doesOverlap(const CGraphicContainer& other) const {
	return CEngine::doesOverlap(*this, other);
}

void CGraphicBox::drawOverlap(RenderTarget* target) const {
	VertexArray rect = VertexArray(Quads, 4);
	rect[0] = Vector2f(left, top);
	rect[1] = Vector2f(right, top);
	rect[2] = Vector2f(right, bot);
	rect[3] = Vector2f(left, bot);

	for(int i = 0; i < 4; i++) {
		rect[i].color = colorBase;
	}

	target->draw(rect);
}

void CGraphicBox::drawOutline(RenderTarget* target) {
	if(doesNeedUpdate) {
		update();
	}
	target->draw(lines);
}

void CGraphicBox::move(Vector2f movement) {
	needsUpdate();
	Vector3f planarMovement = CEngine::toPlanar3D(movement);
	pos += planarMovement;
}

void CGraphicBox::move(Vector3f movement) {
	needsUpdate();
	pos += movement;
}

void CGraphicBox::setPosition(Vector3f pos) {
	needsUpdate();
	move(pos - this->pos);
}

void CGraphicBox::draw(RenderTarget* target) {
	if(doesNeedUpdate) {
		update();
	}
	target->draw(sides);
	target->draw(lines);
}

void CGraphicBox::update() {
	doesNeedUpdate = false;
	
	Vector2f top0 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(-dim.x, dim.y, dim.z) * scaleOffset, rot));
	Vector2f top1 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(-dim.x, -dim.y, dim.z) * scaleOffset, rot));
	Vector2f top2 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(dim.x, -dim.y, dim.z) * scaleOffset, rot));
	Vector2f top3 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(dim.x, dim.y, dim.z) * scaleOffset, rot));
	Vector2f bot0 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(-dim.x, dim.y, -dim.z) * scaleOffset, rot));
	Vector2f bot1 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(-dim.x, -dim.y, -dim.z) * scaleOffset, rot));
	Vector2f bot2 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(dim.x, -dim.y, -dim.z) * scaleOffset, rot));
	Vector2f bot3 = CEngine::toIso(pos + posOffset + CEngine::rotateZ(Vector3f(dim.x, dim.y, -dim.z) * scaleOffset, rot));

	if(rot < 45 || rot > 315) {
		sides[0] = top0;
		sides[1] = top3;
		sides[2] = bot3;
		sides[3] = bot0;

		sides[4] = top3;
		sides[5] = top2;
		sides[6] = bot2;
		sides[7] = bot3;

		sides[8] = top0;
		sides[9] = top1;
		sides[10] = top2;
		sides[11] = top3;
	} else if(rot >= 45 && rot < 135) {
		sides[0] = top3;
		sides[1] = top2;
		sides[2] = bot2;
		sides[3] = bot3;

		sides[4] = top2;
		sides[5] = top1;
		sides[6] = bot1;
		sides[7] = bot2;

		sides[8] = top3;
		sides[9] = top0;
		sides[10] = top1;
		sides[11] = top2;
	} else if(rot >= 135 && rot < 225) {
		sides[0] = top2;
		sides[1] = top1;
		sides[2] = bot1;
		sides[3] = bot2;

		sides[4] = top1;
		sides[5] = top0;
		sides[6] = bot0;
		sides[7] = bot1;

		sides[8] = top2;
		sides[9] = top3;
		sides[10] = top0;
		sides[11] = top1;
	} else { /* rot >= 225 && rot < 315 */
		sides[0] = top1;
		sides[1] = top0;
		sides[2] = bot0;
		sides[3] = bot1;

		sides[4] = top0;
		sides[5] = top3;
		sides[6] = bot3;
		sides[7] = bot0;

		sides[8] = top1;
		sides[9] = top2;
		sides[10] = top3;
		sides[11] = top0;
	}

	// Assign linestrip with minimal point usage
	lines[0]  = sides[2];
	lines[1]  = sides[3];
	lines[2]  = sides[0];
	lines[3]  = sides[9];
	lines[4]  = sides[5];
	lines[5]  = sides[6];
	lines[6]  = sides[2];
	lines[7]  = sides[1];
	lines[8]  = sides[0];
	lines[9]  = sides[1];
	lines[10] = sides[5];

	// Assign overlap
	left = lines[1].position.x;
	right = lines[4].position.x;
	top = lines[3].position.y;
	bot = lines[6].position.y;

	// Now keep r in range -45 to 45
	float r = rot + CEngine::light_angle;
	while(r > 45) {
		r -= 90;
	}

	float degLeft = abs(r);  // 0 - 45, 0 = full
	float degRight = abs(r - 90); // 45 - 135, 45 most light, over 90 = dark

	Color colorLeft = CEngine::getColorFromLightFactor(colorBase, CEngine::getLightFactor(degLeft));
	Color colorRight = CEngine::getColorFromLightFactor(colorBase, CEngine::getLightFactor(degRight));
	Color colorOutline = colorAccent;

	sides[0].color = colorLeft;
	sides[1].color = colorLeft;
	sides[2].color = colorLeft;
	sides[3].color = colorLeft;

	sides[4].color = colorRight;
	sides[5].color = colorRight;
	sides[6].color = colorRight;
	sides[7].color = colorRight;

	sides[8].color = colorBase;
	sides[9].color = colorBase;
	sides[10].color = colorBase;
	sides[11].color = colorBase;

	for(int i = 0; i < 11; i++) {
		lines[i].color = colorOutline;
	}
}

void CGraphicBox::setRotation(float rot) {
	needsUpdate();
	this->rot = rot;
}

void CGraphicBox::setDimension(Vector3f dim) {
	needsUpdate();
	this->dim = dim;
}

void CGraphicBox::rotate(float angle) {
	needsUpdate();
	rot = CEngine::rotate(rot, angle);
}

void CGraphicBox::scale(float factor) {
	needsUpdate();
	dim *= fmax(0.01f, factor);
}

Vector3f CGraphicBox::getPosition() const {
	return pos;
}

Vector3f CGraphicBox::getDimension() const {
	return dim;
}

float CGraphicBox::getRotation() const {
	return rot;
}

void CGraphicBox::setColorBase(Color color) {
	needsUpdate();
	this->colorBase = color;
}

void CGraphicBox::setColorAccent(Color color) {
	needsUpdate();
	this->colorAccent = color;
}

void CGraphicBox::setScaleOffset(float offset) {
	needsUpdate();
	CGraphicContainer::setScaleOffset(offset);
}

void CGraphicBox::setPosOffset(Vector3f offset) {
	needsUpdate();
	CGraphicContainer::setPosOffset(offset);
}


