#ifndef CYBERPUNK_CENEMYLURKER_HPP
#define CYBERPUNK_CENEMYLURKER_HPP

#include "CHealthBar.hpp"
#include "CAIController.hpp"
#include "CCharacter.hpp"
#include "CAIControllerGhost.hpp"
#include "CEnemy.hpp"
#include "CColorDefines.hpp"

class CObjectSpectralDust;
class CPlayer;

using namespace sf;

class CEnemyGhost : public CEnemy {
private:
	float movingSpeedDashingModifier = 4.25f;
	float maxTurningSpeedDashingModifier = 0.15f;
	float slowdownFactorAtMaxMisalignmentDashingModifier = 2.5f; // so it multiplies to 1 => no slowdown
	
	float healthDecrease = 5; //per second
	//TODO ? increase health reduction after charge ?

	bool isInvisible;

	bool dashing = false;
	float cooldownDash = 2750;
	CClock clockDashCooldown;
	float dashDuration = 550;
	CClock clockDashDuration;

	float explosionRadius = 1.f;
	float explosionBaseDamage = 14.f;
	float explosionHealthFactorDamage = 0.05f;

	float hoveringTimeOffset = 0;
	float hoveringAmplitude = 0.13f;
	float hoveringFrequency = 4.f;
	
	bool calledToDust = false;
	CObjectSpectralDust* specterDust = nullptr;

public:
	explicit CEnemyGhost(Vector3f pos, float rot = 0, bool invisibleGhost = false) :
		CEnemy(new CAIControllerGhost(this), pos, rot, Vector3f(0.275f, 0.275f, 0.5f),
		       invisibleGhost ? COLOR_ENEMY_GHOST_INVISIBLE_BASE : COLOR_ENEMY_GHOST_NORMAL_BASE,
		       100, 2.75f),
			   isInvisible(invisibleGhost) {

		if(isInvisible) {
			healthBar.setIsVisible(false);
			effectManager.setIsVisible(false);
		}

		turningMovementProfile.isTurningEnemy = true;
		turningMovementProfile.maxTurningSpeed = 420.f;
		turningMovementProfile.turningSpeedFactor = 4.25f;
		turningMovementProfile.slowdownFactorAtMaxMisalignment = 0.4f;
		
		graphicBox.setColorAccent(isInvisible ? COLOR_ENEMY_GHOST_INVISIBLE_ACCENT : COLOR_ENEMY_GHOST_NORMAL_ACCENT);
	}

	virtual void tick(float frameTime);
	virtual void tickAuthority(float frameTime);
	virtual void die();
	
	virtual bool canBePushedByBullet();
	virtual void onStunned();
	
	// Special abilities
	void dash();
	void dashRPC(float gameTime);
	void explode();
	void explodeRPC(std::vector<CObject*> targets, float damage);
	static void explodeFallback(std::vector<CObject*> targets, float damage);
	
	static void hitExplosion(CPlayer* target, float damage);

	// AI Controller interface
	bool isDashReady() const;
	
	bool isAnyPlayerInExplosionRange() const;

	bool isCalledToDust() const;
	CObjectSpectralDust* getDust() const;
	
	// SpecterDust Interface
	void callToDust(CObjectSpectralDust* specterDust);

private:
	void stopDashing();

};

#endif //CYBERPUNK_CENEMYLURKER_HPP
