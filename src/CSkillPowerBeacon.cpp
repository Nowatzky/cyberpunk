#include "CSkillPowerBeacon.hpp"
#include "CInputManager.hpp"
#include "CSceneManager.hpp"
#include "CProjectileHealthBait.hpp"
#include "CFlatCircle.hpp"
#include "CPowerBeacon.hpp"

void CSkillPowerBeacon::onButtonPress() {
	spawnPowerBeacon();
}

void CSkillPowerBeacon::spawnPowerBeacon() {
	Vector2f screenPos = CInputManager::getMousePositionScene();
	Vector3f worldPos = CEngine::to3DOnPlane(screenPos, 1.301f);

	for (std::vector<CPowerBeacon*>::iterator iter = beacons.begin(); iter != beacons.end(); ++iter) {
		if(!CSceneManager::doesObjectExist(*iter)) {
			beacons.erase(iter);
			--iter;
		}
	}

	if(beacons.size() >= maxNumOfBeacons) {
		CSceneManager::safeRemoveObject(beacons[0]);
		beacons.erase(beacons.begin());
	}
	CPowerBeacon *beacon = new CPowerBeacon(worldPos, owner->getCharacterClass()->getBaseColor(), owner->getCharacterClass()->getAccentColor());

	beacons.push_back(beacon);
	for (CPowerBeacon * b : beacons) {
		b->updateCommunicatingBeacons(beacons);
	}

	CSceneManager::safeAddObject(beacon);
	CSceneManager::safeAddObject(new CFlatCircle(worldPos, 0.25, owner->getCharacterClass()->getBaseColor(), owner->getCharacterClass()->getAccentColor(),1000));
}
