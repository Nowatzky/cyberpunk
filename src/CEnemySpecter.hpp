#ifndef CYBERPUNK_CENEMYSPECTER_HPP
#define CYBERPUNK_CENEMYSPECTER_HPP


#include "CEnemy.hpp"
#include "CAIControllerSpecter.hpp"
#include "CSpecterDmgField.hpp"
#include "CSceneManager.hpp"
#include "CSpecterAura.hpp"

class CSpecterDmgField;

class CEnemySpecter : public CEnemy {
private:
	float healthRegeneration = 1.5f;
	
	// Pull
    float cooldownPull = 3750.f;
	CClock clockCooldownPull;
	
	float pullMaxRange = 12;
	float pullProjectileSpeed = 27;
	float pullStunDuration = 250;
	float pullDuration = 350;
	float pullStrength = 6.5f;
	
	CClock clockPullDuration;
	bool isPulling = false;
	bool hasFiredProjectile = false;
	bool hasHitPull = false;
	float pullTelegraphDuration = 330;
	float pullStandingDuration = 650; // Must be long enough to register a hit notification

    // Aura
	float auraRadius = 5;
	float auraMinSlowRadius = 1;
    float auraSlowModifierMin = 0.75f;
	float auraSlowModifierMax = 0.35f;
	float auraDamage = 8;
	float auraHealthRegeneration = 15;
	
	// Devour
	float devourRadius = 3.5f;
	int devourHierarchy = 1;
	
	Color colorNormal = Color(40, 40, 90, 220);
	Color colorPullTelegraph = Color(40, 40, 250, 255);
	
	CSpecterAura* specterAura = nullptr;
	long slowEffectId;

public:
    CEnemySpecter(const Vector3f pos, float rot) :
            CEnemy(new CAIControllerSpecter(this), pos, rot, Vector3f(0.35f, 0.35f, 0.45f), Color(40, 40, 90, 220), 180, 1.2f),
            slowEffectId(CEngine::getUniqueId()) {}	
	
    void tick(float frameTime);
    virtual void die();
    void induceAuraEffect(float frameTime);
	
	void devourMergeTarget(CEnemySpecter* target);
	int getDevourHierarchy();

	bool isPullReady();
	void notifyPullProjectileHit();
	bool getIsPulling() const;
	
private:
	void updateIsPulling();
	void pull(CPlayer* target);
	void startPullTelegraph();
	
	CPlayer* findPullTarget();
	bool isPlayerInSight(CPlayer* target);
	
};


#endif //CYBERPUNK_CENEMYSPECTER_HPP
