#ifndef CYBERPUNK_CNAVPOLYGONEDGE_HPP
#define CYBERPUNK_CNAVPOLYGONEDGE_HPP

#include <SFML/System.hpp>
#include <vector>
#include "CEngine.hpp"

using namespace sf;
using namespace std;

class CNavPolygon;

struct CPolygonGatewayHole {
	Vector2f inward;
	Vector2f outward;
	
	CPolygonGatewayHole(Vector2f center, Vector2f inwardOffset) :
			inward(center + inwardOffset),
            outward(center - inwardOffset) {}
};

class CNavPolygonEdge final {
public:
	CNavPolygon* target;
	Vector2f left;
	Vector2f right;
	vector<CPolygonGatewayHole*> holes;

	CNavPolygonEdge(CNavPolygon* target, Vector2f left, Vector2f right);
	~CNavPolygonEdge();


	static constexpr float minEdgeLength = 0.01f;	
};


#endif //CYBERPUNK_CNAVPOLYGONEDGE_HPP
