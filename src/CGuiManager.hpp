#ifndef CYBERPUNK_CGUIMANAGER_HPP
#define CYBERPUNK_CGUIMANAGER_HPP

#include <vector>
#include <SFML/Graphics/Font.hpp>
#include "CGuiElement.hpp"

using namespace std;
using namespace sf;

class CGuiManager {
private:
	static std::string fontNameDisplay;
	static std::string fontNameDebug;
	
	static vector<CGuiElement*> guiElements;
	
	static Font fontDisplay;
	static Font fontDebug;
	
	static Vector2f guiSize;

	CGuiManager() {}
	
public:
	static void loadFonts();
	static Font& getFontDisplay();
	static Font& getFontDebug();
	
	static void addGuiElement(CGuiElement* guiElement);
	static void removeGuiElement(CGuiElement* guiElement);

	static void tickAll(float frameTime);

	static void drawGui(RenderTarget* renderTarget);
	
	static Vector2f getGuiSize();
	
	static float getRelativeToGuiSizeX(float percentageX);
	static float getRelativeToGuiSizeY(float percentageY);
	static Vector2f getRelativeToGuiSize(float percentage);
	static Vector2f getRelativeToGuiSize(Vector2f percentage);
	
	static void setGuiSize(Vector2f size);
	
	static float getGuiFontSizeFactor();
	
};

#endif //CYBERPUNK_CGUIMANAGER_HPP
