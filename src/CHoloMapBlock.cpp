#include "CHoloMapBlock.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"

CHoloMapBlock::CHoloMapBlock(Vector3f pos, Color colorBase, Color colorAccent) :
		CObject(pos),
		boundingBox(pos, Vector3f(0.5f, 0.5f, 0.5f)),
		graphicBox(pos, Vector3f(0.5f, 0.5f, 0.5f), colorBase, colorAccent),
		colorBaseNormal(colorBase),
		colorAccentNormal(colorAccent) {

	boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);

	Vector2f dim = Vector2f(boundingBox.getDimension().x, boundingBox.getDimension().y);

	blockedArea = CRectangleArea(CEngine::to2D(pos) - dim, dim * 2.f);
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->blockArea(blockedArea);
}

CHoloMapBlock::~CHoloMapBlock() {
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->freeArea(blockedArea);
}

void CHoloMapBlock::tick(float frameTime) {
	if(clockLifetime.getElapsedTime().asMilliseconds() > lifeTime) {
		CSceneManager::safeRemoveObject(this);
		return;
	}
	if(clockLifetime.getElapsedTime().asMilliseconds() < fadeDuration) {
		Color base = getGraphicContainer()->getColorBase();
		base.a = (Uint8) (100 * clockLifetime.getElapsedTime().asMilliseconds() / fadeDuration);
		getGraphicContainer()->setColorBase(base);
		Color accent = getGraphicContainer()->getColorAccent();
		accent.a = (Uint8) (255 * clockLifetime.getElapsedTime().asMilliseconds() / fadeDuration);
		getGraphicContainer()->setColorAccent(accent);

	} else if (clockLifetime.getElapsedTime().asMilliseconds() > lifeTime - fadeDuration) {
		Color base = getGraphicContainer()->getColorBase();
		base.a = (Uint8) (100 * (lifeTime - clockLifetime.getElapsedTime().asMilliseconds()) / fadeDuration);
		getGraphicContainer()->setColorBase(base);
		Color accent = getGraphicContainer()->getColorAccent();
		accent.a = (Uint8) (255 * (lifeTime - clockLifetime.getElapsedTime().asMilliseconds()) / fadeDuration);
		getGraphicContainer()->setColorAccent(accent);
	}
}

CCollisionContainer *CHoloMapBlock::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer *CHoloMapBlock::getGraphicContainer() {
	return &graphicBox;
}
