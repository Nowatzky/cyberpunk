#ifndef CYBERPUNK_CMAPFLOORBLOCK_HPP
#define CYBERPUNK_CMAPFLOORBLOCK_HPP


#include "CMapBlock.hpp"
#include "CMapRoom.hpp"

class CMapFloorBlock : public CMapBlock {
private:
	Color colorAccent;
	VertexArray lines;
	
	int lineCountX;
	int lineCountY;
	
public:
	CMapFloorBlock(const Vector3f pos, const Vector3f dim, const Color colorBase, Color colorAccent);
	
	CMapFloorBlock(CRectangleArea area, float posZ, float dimZ, Color colorBase, Color colorAccent);
	
	void draw(RenderTarget* target);

private:
	void update();

};


#endif //CYBERPUNK_CMAPFLOORBLOCK_HPP
