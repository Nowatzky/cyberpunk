#ifndef CYBERPUNK_CSERIALIZATIONFUNCTIONS_HPP
#define CYBERPUNK_CSERIALIZATIONFUNCTIONS_HPP

#include <SFML/Network/Packet.hpp>
#include <SFML/Window.hpp>
#include "CObjectIdManager.hpp"
#include <vector>

struct CSkillShotGunFireHitResult;
struct CSkillShotGunFirePopupBoxNetwork;

using sf::Packet;
using sf::Vector2f;
using sf::Vector3f;

Packet& operator<<(Packet& packet, Vector2f& vec);
Packet& operator>>(Packet& packet, Vector2f& vec);

Packet& operator<<(Packet& packet, Vector3f& vec);
Packet& operator>>(Packet& packet, Vector3f& vec);

Packet& operator<<(Packet& packet, std::vector<CObjectId>& vector);
Packet& operator>>(Packet& packet, std::vector<CObjectId>& vector);

Packet& operator<<(Packet& packet, std::vector<CSkillShotGunFireHitResult>& vector);
Packet& operator>>(Packet& packet, std::vector<CSkillShotGunFireHitResult>& vector);

Packet& operator<<(Packet& packet, std::vector<CSkillShotGunFirePopupBoxNetwork>& vector);
Packet& operator>>(Packet& packet, std::vector<CSkillShotGunFirePopupBoxNetwork>& vector);


#endif //CYBERPUNK_CSERIALIZATIONFUNCTIONS_HPP
