#ifndef CYBERPUNK_CPOWERBEACON_HPP
#define CYBERPUNK_CPOWERBEACON_HPP

#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"
#include "CRectangleArea.hpp"
#include "CFlatCircle.hpp"

class CPowerBeacon : public CObject {
	CBoundingBox boundingBox;
	CGraphicBox graphicBox;

	Color colorBaseNormal;
	Color colorAccentNormal;
	CRectangleArea blockedArea;
	bool isFalling = true;
	bool isMaster = false;

	CClock clockFall;
	float fallDuration = 800;
	float fallOffsetX;
	float fallOffsetY;

	CClock clockLifetime;
	float lifetime = 25000;

	float radius = 1.5f;
	CClock clockSlowApplication;
	float slowModifier = 0.85f;
	float constantDamage = 5; //per second
	float constantSlowDuration = 100;
	long slowEffectId;
	
	CClock clockSapping;
	float sapDamage = 12.5;
	float sapSlowModifier = 0.45f;
	float sapSlowDuration = 1200;
	float sapCooldown = 1000;

	CClock clockCrossBeaconSapping;
	float crossSapCooldown = 500;
	float crossSapMaxDistance = 18.f;

	CFlatCircle* slowAreaVisuals;
	vector<CPowerBeacon*> communicatingBeacons;

public:
	CPowerBeacon(Vector3f pos, Color colorBase, Color colorAccent);

	~CPowerBeacon();
	virtual void tick(float frameTime);

	// Getter & Setter
	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();

	void updateCommunicatingBeacons(vector<CPowerBeacon*> newList);
	void refreshIsMaster();
	void removeCommunicatingBeacon(CPowerBeacon *beacon);

private:
	bool canBeaconsCrossSap(CPowerBeacon* donor, CPowerBeacon* acceptor);
	bool isLineOfSightBetweenBeaconsFree(CPowerBeacon* donor, CPowerBeacon* acceptor);

};


#endif //CYBERPUNK_CPOWERBEACON_HPP
