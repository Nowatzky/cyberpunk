#ifndef CYBERPUNK_CGUIDRAWING_HPP
#define CYBERPUNK_CGUIDRAWING_HPP


#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include "CGuiElement.hpp"

using namespace sf;

class CGuiDrawing : public CGuiElement {
private:
	VertexArray vertices;
	
	Color color;
	Vector2f prevPoint;	
	bool isFirstPoint = true;
	
	float minThickness = 8;
	float maxThickness = 15;

public:
	CGuiDrawing(Vector2f start, Color color);

	virtual void draw(RenderTarget* renderTarget);
	virtual void tick(float frameTime);
	
	void addPoint(Vector2f currentPoint);
	void setMinMaxThickness(float minThickness, float maxThickness);

};


#endif //CYBERPUNK_CGUIDRAWING_HPP
