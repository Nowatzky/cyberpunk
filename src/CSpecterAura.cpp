#include "CSpecterAura.hpp"

void CSpecterAura::tick(float frameTime) {
}

CCollisionContainer* CSpecterAura::getCollisionContainer() {
	return nullptr;
}

CGraphicContainer* CSpecterAura::getGraphicContainer() {
	return &graphicArea;
}

void CSpecterAura::updatePosition(Vector3f pos) {
	this->pos = pos;
	graphicArea.setPosition(Vector3f(pos.x, pos.y, 0.55f));
}
