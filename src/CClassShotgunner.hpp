#ifndef CYBERPUNK_CCLASSSHOTGUNNER_HPP
#define CYBERPUNK_CCLASSSHOTGUNNER_HPP

#include "CCharacterClass.hpp"
#include "CHealthGem.hpp"

using namespace std;

class CClassShotgunner : public CCharacterClass {
private:
	vector<CHealthGem*> healthGemList;
	Vector3f recentGemSpawningPosition = Vector3f(0,0,0);
	float healthGemBonus = 5.f;

public:

	CClassShotgunner(CPlayer *player);

	virtual CSkill* getSkill(SIGN sign);

	virtual Color getBaseColor();
	virtual Color getAccentColor();

	void spawnGem(Vector3f position);
	vector<CHealthGem*> getHealthGemList();
	void clearHealthGemList();

};


#endif //CYBERPUNK_CCLASSSHOTGUNNER_HPP
