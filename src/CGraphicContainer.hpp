#ifndef CYBERPUNK_CGRAPHICCONTAINER_HPP
#define CYBERPUNK_CGRAPHICCONTAINER_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

using sf::Vector2f;
using sf::Vector3f;

class CGraphicContainer {
protected:
	Vector3f pos;
	Vector3f posOffset = Vector3f(0, 0, 0);
	float rot = 0;
	float scaleOffset = 1;
	sf::Color colorBase;
	sf::Color colorAccent;
	bool visible;
	float left, right, top, bot; // Must be updated by implementing class
	
	bool doesNeedUpdate = true;

public:
	CGraphicContainer(Vector3f pos, sf::Color color) :
			pos(pos),
			colorBase(color),
	        colorAccent(color),	        
			visible(true){}

	CGraphicContainer(Vector3f pos, sf::Color colorBase, sf::Color colorAccent) :
			pos(pos),
			colorBase(colorBase),
			colorAccent(colorAccent),
			visible(true){}

	virtual bool doesOverlap(const CGraphicContainer& other) const = 0;
	virtual bool isInFrontOf(const CGraphicContainer& other) const = 0;

	/* Draw */
	virtual void draw(sf::RenderTarget* target) = 0;
	virtual void drawOutline(sf::RenderTarget* target) = 0;
	virtual void drawOverlap(sf::RenderTarget* target) const = 0;

	/* Accessors */
	virtual void setPosition(Vector3f pos);
	virtual void setPosOffset(Vector3f offset);
	virtual void setRotation(float rot);
	virtual void setScaleOffset(float offset);
	virtual void setColorBase(sf::Color color);
	virtual void setColorAccent(sf::Color color);

	virtual void move(Vector3f movement);

	virtual void move(Vector2f movement);
	virtual void rotate(float angle);
	virtual Vector3f getPosition() const;

	virtual Vector3f getPosOffset() const;
	virtual float getRotation() const;
	virtual float getScaleOffset() const;
	
	sf::Color getColorBase() const;
	sf::Color getColorAccent() const;
	float getDepth() const;

	void setVisible(bool visible);
	bool isVisible() const;

	float getLeft() const;
	float getRight() const;
	float getTop() const;
	float getBot() const;

	void needsUpdate();
};


#endif //CYBERPUNK_CGRAPHICCONTAINER_HPP