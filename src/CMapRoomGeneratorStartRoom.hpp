#ifndef CYBERPUNK_CMAPROOMGENERATORSTARTROOM_HPP
#define CYBERPUNK_CMAPROOMGENERATORSTARTROOM_HPP


#include "CMapRoomGenerator.hpp"

class CMapRoomGeneratorStartRoom final : public CMapRoomGenerator{
private:


public:
	CMapRoomGeneratorStartRoom();

	CMapRoom* generateRoom(CRectangleArea roomArea, Doorway entrance, CDirection exitDirection, int exitSize) override;

private:
	std::vector<Vector2f> getExtendedCorners(CRectangleArea area);

};


#endif //CYBERPUNK_CMAPROOMGENERATORSTARTROOM_HPP
