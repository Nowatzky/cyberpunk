#include "CGhostSummonPoint.hpp"
#include "CSceneManager.hpp"
#include "CEnemyGhost.hpp"

void CGhostSummonPoint::tick(float frame) {
	if(clockSummonDuration.getElapsedTime().asMilliseconds() >= summonDuration) {
		CEnemy *newEnemy = new CEnemyGhost(pos, summonAngle, summonInvisibleGhost);
		newEnemy->metaData = metaDataGhost;
		CSceneManager::safeAddObject(newEnemy);
		CSceneManager::safeRemoveObject(this);
	}
}

CCollisionContainer* CGhostSummonPoint::getCollisionContainer() {
	return nullptr;
}

CGraphicContainer* CGhostSummonPoint::getGraphicContainer() {
	return &graphicBox;
}
