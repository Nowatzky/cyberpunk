#ifndef CYBERPUNK_CEFFECTVISIBLE_HPP
#define CYBERPUNK_CEFFECTVISIBLE_HPP


#include "CEffect.hpp"

class CEffectVisible : public CEffect {
private:
	float maxAlpha;

public:
	CEffectVisible(CCharacter* initiator, float maxAlpha, float lifetime) :
			CEffect(initiator, lifetime),
			maxAlpha(maxAlpha) {}

	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);

};


#endif //CYBERPUNK_CEFFECTVISIBLE_HPP
