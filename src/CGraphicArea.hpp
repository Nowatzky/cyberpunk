#ifndef CYBERPUNK_CGRAPHICAREA_HPP
#define CYBERPUNK_CGRAPHICAREA_HPP


#include "CGraphicContainer.hpp"

class CGraphicArea : public CGraphicContainer {
private:
	float radius;
	unsigned int vertexCount;
	sf::VertexArray triangles;
	sf::VertexArray lines;

public:
	CGraphicArea(Vector3f pos, float radius, unsigned int vertexCount, sf::Color colorBase, sf::Color colorAccent);

	virtual bool doesOverlap(const CGraphicContainer& other) const;
	virtual bool isInFrontOf(const CGraphicContainer& other) const;

	virtual void draw(sf::RenderTarget* target);
	virtual void drawOutline(sf::RenderTarget* target);
	virtual void drawOverlap(sf::RenderTarget *target) const;

	/* Accessors */
	virtual void setPosition(Vector3f pos);
	virtual void setPosOffset(Vector3f offset);
	virtual void setRotation(float rot);
	virtual void setScaleOffset(float offset);
	void setRadius(float radius);

	virtual void setColorBase(sf::Color color);
	virtual void setColorAccent(sf::Color color);

	virtual void move(Vector3f movement);
	virtual void move(Vector2f movement);
	virtual void rotate(float angle);
	virtual void scale(float factor);

	virtual Vector3f getPosition() const;
	virtual Vector3f getDimension() const;
	virtual float getRotation() const;
	virtual float getRadius() const;

private:
	void update();
};


#endif //CYBERPUNK_CGRAPHICAREA_HPP
