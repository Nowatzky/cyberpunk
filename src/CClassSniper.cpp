#include "CClassSniper.hpp"
#include "CColorDefines.hpp"
#include "CSkillDash.hpp"
#include "CSkillForceSweep.hpp"
#include "CSkillDecoy.hpp"
#include "CSkillHealthBait.hpp"
#include "CSkillSnipeShot.hpp"
#include "CSkillPowerBeacon.hpp"
#include "CSkillSniperStance.hpp"


CClassSniper::CClassSniper(CPlayer* player) :
		CCharacterClass(CClassName::SNIPER) {
	
	primarySkill = new CSkillSnipeShot(player);
	movementSkill = new CSkillDash(player);

	activeSkills.push_back(new CSkillDecoy(player));
	activeSkills.push_back(new CSkillHealthBait(player));
	activeSkills.push_back(new CSkillPowerBeacon(player));
	activeSkills.push_back(new CSkillSniperStance(player));

	skillDisplayProfiles = {make_pair(SIGN::VLINE_DOWN, "Decoy"), make_pair(SIGN::WEDGE_DOWN, "Health Bait"), make_pair(SIGN::FLASH, "Power Beacon"), make_pair(SIGN::CIRCLE, "Sniper Vision")};
}

CSkill *CClassSniper::getSkill(SIGN sign) {

	if(sign == SIGN::VLINE_UP || sign == SIGN::VLINE_DOWN) {
		return activeSkills[0];
	}
	else if(sign == SIGN::WEDGE_DOWN) {
		return activeSkills[1];
	}
	else if(sign == SIGN::FLASH) {
		return activeSkills[2];
	}	
	else if(sign == SIGN::CIRCLE) {
		return activeSkills[3];
	}
	else {
		return nullptr;
	}
}

Color CClassSniper::getBaseColor() {
	return COLOR_DARK_GREEN;
}

Color CClassSniper::getAccentColor() {
	return COLOR_YELLOW_GREEN;
}
