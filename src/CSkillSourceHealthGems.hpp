#ifndef CYBERPUNK_CSKILLSOURCEHEALTHGEMS_HPP
#define CYBERPUNK_CSKILLSOURCEHEALTHGEMS_HPP
#include "CPlayer.hpp"
#include "CHealthGem.hpp"


class CSkillSourceHealthGems : public CSkill {
private:

	float duration = 3000;

	vector<CHealthGem*> connectedHealthGems;
	bool isDrainingHealthGems = false;
	Clock clockDraining;

public:
	CSkillSourceHealthGems(CPlayer* owner) :
			CSkill(owner, 2000, 30) {}

protected:
	void onButtonPress();
	void tick(float frameTime);
};


#endif //CYBERPUNK_CSKILLSOURCEHEALTHGEMS_HPP
