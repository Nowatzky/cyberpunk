#ifndef CYBERPUNK_CCHAINFLASHEFFECT_HPP
#define CYBERPUNK_CCHAINFLASHEFFECT_HPP


#include "CObject.hpp"
#include "CGraphicBox.hpp"

class CChainFlashEffect : public CObject {
private:
	CGraphicBox graphicBox;
	Clock clock;
	float lifetime = 180;
	
public:
	CChainFlashEffect(Vector3f p1, Vector3f p2);

	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();
	
	void tick(float frameTime);

};

#endif //CYBERPUNK_CCHAINFLASHEFFECT_HPP
