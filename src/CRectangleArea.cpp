#include "CRectangleArea.hpp"
#include "CRandom.hpp"
#include "CDirection.hpp"
#include "CEngine.hpp"

CRectangleArea::CRectangleArea(Vector2f p1, Vector2f p2, Vector2f p3) {
	float minX = min(min(p1.x, p2.x), p3.x);
	float maxX = max(max(p1.x, p2.x), p3.x);
	float minY = min(min(p1.y, p2.y), p3.y);
	float maxY = max(max(p1.y, p2.y), p3.y);

	offset = Vector2f(minX, minY);
	size = Vector2f(maxX, maxY) - offset;
}

// Is inclusive since equation is negated
bool CRectangleArea::isPointInside(Vector2f point) const {
	return !(point.x < offset.x || point.x > offset.x + size.x ||
			 point.y < offset.y || point.y > offset.y + size.y);
}

bool CRectangleArea::isPointInsideExclusiveBorder(Vector2f point) const {
	return !(point.x <= offset.x || point.x >= offset.x + size.x ||
	         point.y <= offset.y || point.y >= offset.y + size.y);
}

bool CRectangleArea::doesIntersect(const CRectangleArea& otherArea) const {
	return !(offset.x > otherArea.offset.x + otherArea.size.x || 
	         offset.x + size.x < otherArea.offset.x ||
	         offset.y > otherArea.offset.y + otherArea.size.y ||
	         offset.y + size.y < otherArea.offset.y);
}

bool CRectangleArea::doesIntersectExclusiveBorder(const CRectangleArea& otherArea) const {
	return !(offset.x >= otherArea.offset.x + otherArea.size.x ||
	         offset.x + size.x <= otherArea.offset.x ||
	         offset.y >= otherArea.offset.y + otherArea.size.y ||
	         offset.y + size.y <= otherArea.offset.y);
}

bool CRectangleArea::isPointOnBorders(Vector2f point) const {
	vector<Vector2f> corners = getCorners();
	
	for(unsigned i = 0; i < 4; i++) {
		unsigned j = (i < 3) ? (i + 1) : 0;
		// X-Axis
		if(CDirection::fromVector(corners.at(i) - corners.at(j)).isParallel(CDirection::PX)) {
			float minX = min(corners.at(i).x, corners.at(j).x);
			float maxX = max(corners.at(i).x, corners.at(j).x);
			float y = corners.at(i).y;

			if(point.x >= minX && point.x <= maxX && abs(y - point.y) < EPS) {
				return true;
			}
		} else { // Y-Axis
			float minY = min(corners.at(i).y, corners.at(j).y);
			float maxY = max(corners.at(i).y, corners.at(j).y);
			float x = corners.at(i).x;

			if(point.y >= minY && point.y <= maxY && abs(x - point.x) < EPS) {
				return true;
			}
		}		
	}

	return false;
}

Vector2f CRectangleArea::getRandomPointInside() const {
	return Vector2f(CRandom::getInRange(offset.x, offset.x + size.x), CRandom::getInRange(offset.y, offset.y + size.y));
}

vector<Vector2f> CRectangleArea::getCorners() const {
	vector<Vector2f> corners;
	corners.push_back(offset);
	corners.push_back(offset + Vector2f(size.x, 0));
	corners.push_back(offset + size);
	corners.push_back(offset + Vector2f(0, size.y));
	return corners;
}

Vector2f CRectangleArea::getCenter() const {
	return offset + size/2.f;
}

float CRectangleArea::getAreaSize() const {
	return size.x * size.y;
}

