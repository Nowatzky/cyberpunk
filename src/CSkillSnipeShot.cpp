#include "CSkillSnipeShot.hpp"
#include "CSceneManager.hpp"
#include "CLineEffect.hpp"
#include "CInputManager.hpp"
#include "CEffectMovementSlow.hpp"
#include "CProjectileHealthBait.hpp"
#include "CPowerBeacon.hpp"
#include "CDebugHelper.hpp"
#include "CRpcHandler.hpp"

void CSkillSnipeShot::onButtonPress() {
	if(!owner->getIsLocal()) {
		return;
	}

	Vector2f dir = CInputManager::getDirToMouseInScene(owner->getPosition());

	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
	CHitResult hitResult = CSceneManager::raycast(owner->getPosition(), dir, collisionChannel);

	if(hitResult.target != nullptr) {
		CEnemy* enemy = dynamic_cast<CEnemy*>(hitResult.target);
		if(enemy != nullptr) {
			fire(hitResult.hitPos, true, enemy);
		} else {
			fire(hitResult.hitPos, false);
		}
		
		/*CProjectileHealthBait* bait = dynamic_cast<CProjectileHealthBait*>(hitResult.target);
		if(bait != nullptr) {
			if(bait->hasTargetAcquired()) {
				CCharacter* baitTarget = bait->getTarget();
				float dist = CEngine::getDistance(owner->getPosition(), hitResult.hitPos) + CEngine::getDistance(baitTarget->getPosition(), baitTarget->getPosition());
				float damage = max(minDamage, dist * damagePerMeter);
				
				baitTarget->reduceHealth(damage + bait->getBaitExplosionDamage());
				baitTarget->addEffect(new CEffectMovementSlow(owner, slowMovementSpeedModifier, slowDuration));

				CSceneManager::safeAddObject(new CLineEffect(bait->getPosition(), baitTarget->getPosition(), owner->getCharacterClass()->getAccentColor()));
				CSceneManager::safeRemoveObject(bait);
			}
		}*/
	}
}

void CSkillSnipeShot::fire(Vector3f hitPosition, bool doesHitTarget, CEnemy *target) {
	float damage = 0;
	CObjectId targetId;

	if(doesHitTarget) {
		float dist = CEngine::getDistance(owner->getPosition(), hitPosition);
		damage = max(minDamage, dist * damagePerMeter);
		target->reduceHealth(damage);
		CPlayer::checkKillPotential(target, owner);

		target->addEffect(new CEffectMovementSlow(owner, slowMovementSpeedModifier, slowDuration));
		targetId = target->getId();
	}
	CLineEffect* lineEffect = new CLineEffect(owner->getPosition(), hitPosition, owner->getCharacterClass()->getAccentColor());
	lineEffect->setLifetime(60);
	CSceneManager::safeAddObject(lineEffect);


	CRpcHandler::sendPlayerSniperPrimaryFire(owner, hitPosition, doesHitTarget, targetId, damage);
}

void CSkillSnipeShot::fireRPC(Vector3f hitPosition, bool doesHitTarget, CEnemy *target, float dmg) {
	if(doesHitTarget) {
		if(target != nullptr) {
			target->reduceHealth(dmg);
			target->addEffect(new CEffectMovementSlow(owner, slowMovementSpeedModifier, slowDuration));
		}
	}
	CLineEffect* lineEffect = new CLineEffect(owner->getPosition(), hitPosition, owner->getCharacterClass()->getAccentColor());
	lineEffect->setLifetime(60);
	CSceneManager::safeAddObject(lineEffect);
}
