#include "CPowerBeacon.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CEffectMovementSlow.hpp"
#include "CEffectStun.hpp"
#include "CLineEffect.hpp"

struct ComparePowerBeacon {
	bool operator()(const CPowerBeacon *lhs, const CPowerBeacon *rhs) const {
		if(lhs->getPosition().x != rhs->getPosition().x) {
			return lhs->getPosition().x < rhs->getPosition().x;
		} else {
			return lhs->getPosition().y < rhs->getPosition().y;
		}
	}
};

CPowerBeacon::CPowerBeacon(Vector3f pos, Color colorBase, Color colorAccent) :
		CObject(pos),
		boundingBox(pos, Vector3f(0.2f, 0.2f, 0.8f)),
		graphicBox(pos, Vector3f(0.2f, 0.2f, 0.8f), colorBase, colorAccent),
		colorBaseNormal(colorBase),
		colorAccentNormal(colorAccent),
		slowEffectId(CEngine::getUniqueId()) {

	boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
	Vector2f dim = Vector2f(boundingBox.getDimension().x, boundingBox.getDimension().y);

	blockedArea = CRectangleArea(CEngine::to2D(pos) - dim, dim * 2.f);
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->blockArea(blockedArea);

	fallOffsetX = CRandom::getInRange(-3,3);
	fallOffsetY = CRandom::getInRange(-3,3);
}

CPowerBeacon::~CPowerBeacon() {
	CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(pos))->freeArea(blockedArea);
	CSceneManager::safeRemoveObject(slowAreaVisuals);
}

void CPowerBeacon::tick(float frameTime) {
	if(isFalling) {
		float alpha = clockFall.getElapsedTime().asMilliseconds() / fallDuration;

		if(alpha >= 1) {
			alpha = 1;
			isFalling = false;
			Color circleAccentColor = colorBaseNormal;
			Color circleBaseColor = colorAccentNormal;
			circleBaseColor.a = 10;

			slowAreaVisuals = new CFlatCircle(pos, radius, circleBaseColor, circleAccentColor, lifetime);
			CSceneManager::safeAddObject(slowAreaVisuals);
		}

		graphicBox.setPosOffset(Vector3f(CEngine::lerp(fallOffsetX, 0, alpha), CEngine::lerp(fallOffsetY, 0, alpha), CEngine::lerp(40, 0, alpha)));
		graphicBox.setScaleOffset(CEngine::lerp(0.4f, 1, alpha));
	}

	if(!isFalling) {
		//Reapplying slow effect after 100 ms for 100 ms and deal damage-- refactor
		if (clockSlowApplication.hasPassed(constantSlowDuration)) {
			clockSlowApplication.restart();
			std::vector<CEnemy*> enemyList;
						
			for(CEnemy* enemy : CSceneManager::getEnemies()) {
				if(CEngine::getDistance(getPosition(), enemy->getPosition()) <= radius) {
					enemyList.push_back(enemy);
					
					CEffectMovementSlow* slowEffect = dynamic_cast<CEffectMovementSlow*>(enemy->getEffectManager()->getEffectWithId(slowEffectId));

					if(slowEffect != nullptr) {
						slowEffect->restartLifetime();
						slowEffect->setMovementSpeedModifier(slowModifier);
					} else {
						slowEffect = new CEffectMovementSlow(nullptr, slowModifier, constantSlowDuration);
						slowEffect->setId(slowEffectId);
						enemy->addEffect(slowEffect);
					}
					
					enemy->reduceHealth(constantDamage * frameTime);
				}
			}

			if(!enemyList.empty()) {
				if(clockSapping.hasPassed(sapCooldown)) {
					clockSapping.restart();
					
					CEnemy* sapTarget = enemyList.at(static_cast<unsigned long>(CRandom::getInRangeI(0, (int) (enemyList.size() - 1))));

					sapTarget->addEffect(new CEffectMovementSlow(nullptr, sapSlowModifier, sapSlowDuration));
					sapTarget->reduceHealth(sapDamage);
					
					CLineEffect* lineEffect = new CLineEffect(getPosition(), sapTarget->getPosition(), colorAccentNormal);
					lineEffect->setLifetime(70);
					CSceneManager::safeAddObject(lineEffect); 
				}
			}
		}

		//manage cross tower sap
		if(isMaster) {
			if(clockCrossBeaconSapping.hasPassed(crossSapCooldown)) {
				for(int i = 0; i < communicatingBeacons.size(); ++i) {
					CPowerBeacon* donor = communicatingBeacons[i];

					for(int j = i; j < communicatingBeacons.size(); ++j) {
						CPowerBeacon* acceptor = communicatingBeacons[j];

						if(canBeaconsCrossSap(donor, acceptor)) {
							CCollisionChannel collisionChannel({CCollisionChannel::Channel::CHARACTER});
							Vector2f dir = CEngine::to2D(CEngine::normalize(acceptor->getPosition() - donor->getPosition()));
							CHitResult hitResult = CSceneManager::raycast(donor->getPosition(), dir, collisionChannel);
							
							float maxDist = CEngine::getDistance(donor->getPosition(), acceptor->getPosition());

							if(hitResult.target != nullptr && hitResult.hitDistance <= maxDist) {
								CEnemy* enemy = dynamic_cast<CEnemy*>(hitResult.target);
								if(enemy != nullptr) {
									enemy->reduceHealth(sapDamage);
									enemy->addEffect(new CEffectMovementSlow(nullptr, sapSlowModifier, sapSlowDuration));
								}

								clockCrossBeaconSapping.restart();
								CSceneManager::safeAddObject(new CLineEffect(donor->getPosition(), acceptor->getPosition(),
								                                             getGraphicContainer()->getColorAccent()));
							}
						}
					}
				}
			}
		}
	}

	if(clockLifetime.hasPassed(lifetime)) {
		for(CPowerBeacon * b : communicatingBeacons) {
			if(b != this) {
				b->removeCommunicatingBeacon(this);
			}
		}
		CSceneManager::safeRemoveObject(this);
	}
}

CCollisionContainer* CPowerBeacon::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer* CPowerBeacon::getGraphicContainer() {
	return &graphicBox;
}

void CPowerBeacon::refreshIsMaster() {
	std::sort(communicatingBeacons.begin(), communicatingBeacons.end(), ComparePowerBeacon());
	if(!communicatingBeacons.empty()) {
		isMaster = (communicatingBeacons[0] == this);
	}
}

void CPowerBeacon::updateCommunicatingBeacons(vector<CPowerBeacon *> newList) {
	communicatingBeacons = newList;
	refreshIsMaster();
}

void CPowerBeacon::removeCommunicatingBeacon(CPowerBeacon *beacon) {
	for(std::vector<CPowerBeacon*>::iterator iter = communicatingBeacons.begin(); iter != communicatingBeacons.end(); ++iter) {
		if(*iter == beacon) {
			communicatingBeacons.erase(iter);
			--iter;
		}
	}
	refreshIsMaster();
}

bool CPowerBeacon::canBeaconsCrossSap(CPowerBeacon* donor, CPowerBeacon* acceptor) {
	bool isInRange = CEngine::getDistance(donor->getPosition(), acceptor->getPosition()) <= crossSapMaxDistance;

	return isInRange && isLineOfSightBetweenBeaconsFree(donor, acceptor);
}

bool CPowerBeacon::isLineOfSightBetweenBeaconsFree(CPowerBeacon* donor, CPowerBeacon* acceptor) {
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC});
	Vector2f dir = CEngine::to2D(CEngine::normalize(acceptor->getPosition() - donor->getPosition()));
	
	float maxBeaconWidth = dynamic_cast<CBoundingBox*>(donor->getCollisionContainer())->getDimension().x * 1.45f;
	
	CHitResult hitResult = CSceneManager::raycast(donor->getPosition() + CEngine::toPlanar3D(dir * maxBeaconWidth), dir, collisionChannel);

	return hitResult.target == acceptor; 
}



