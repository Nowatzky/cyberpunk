#include "CProjectileSpecterBullet.hpp"
#include "CObjectProjectileHitPopUpBox.hpp"
#include "CColorDefines.hpp"

#include "CProjectileSweepingWall.hpp"
#include "CSceneManager.hpp"

void CProjectileSpecterBullet::onHitPlayer(CPlayer* target, float frameTime) {
    target->reduceHealth(dmg);
	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(getPosition(), Vector3f(0.15f, 0.15f, 0.15f), COLOR_BLACK_BLUE_ALPHA, 100.f));
    CSceneManager::safeRemoveObject(this);
}

void CProjectileSpecterBullet::onHitWall(float frameTime) {
    CSceneManager::safeRemoveObject(this);
}

void CProjectileSpecterBullet::onHitProjectile(CProjectile* projectile, float frameTime) {
    CProjectileSweepingWall* projectileSweepingWall = dynamic_cast<CProjectileSweepingWall*>(projectile);
    
    if(projectileSweepingWall != nullptr) {
	    CSceneManager::safeRemoveObject(this);
    }
}

void CProjectileSpecterBullet::tick(float frameTime) {
    setSpeed(startSpeed + clockLifetime.getElapsedTime().asSeconds() * acceleration);

    /* adjust towards player */
	if(isActive) {
		// ToDo make truly frameRate independent
		CPlayer* player = CSceneManager::getPlayers().at(0);
		Vector2f dir = getDirection();
		Vector2f dirToPlayer = CEngine::to2D(player->getPosition() - getPosition());
		float angleDiff = CEngine::getAngleDifference(CEngine::getRotation(dir), CEngine::getRotation(dirToPlayer));
		float angleToTurn = 0;

		if(abs(angleDiff) > 90) {
			isActive = false;
		} else {
			angleToTurn = angleDiff < 0 ? -turningSpeed : turningSpeed;
		}

		dir = CEngine::rotateZ(dir, angleToTurn * frameTime);
		setDirection(dir);
		graphicBox.setRotation(CEngine::getRotation(dir) + 45);
	}
	
    CProjectile::tick(frameTime);
}
