#include <iostream>
#include "CNavPolygon.hpp"
#include "CEngine.hpp"
#include "CMapBlock.hpp"

void CNavPolygon::connectTo(CNavPolygon* target, bool connectViceVersa) {
	if(this == target) {
		return;
	}
	
	vector<CNavPolygonBorder> ownBorders = getBorders();
	vector<CNavPolygonBorder> targetBorders = target->getBorders();

	Vector2f left;
	Vector2f right;
	bool foundGateway = false;
	
	for(CNavPolygonBorder ownBorder : ownBorders) {
		for(CNavPolygonBorder targetBorder : targetBorders) {
			if(ownBorder.isOpposingParallelAndIntersecting(targetBorder)) {
				Vector2f dirToRight = ownBorder.getRight() - ownBorder.getLeft();
				
				// Find rightmost left point
				if(CEngine::isFurtherInDirection(ownBorder.getLeft(), targetBorder.getRight(), dirToRight)) {
					left = ownBorder.getLeft();
				} else {
					left = targetBorder.getRight();
				}
				
				// Find leftmost right point
				if(CEngine::isFurtherInDirection(ownBorder.getRight(), targetBorder.getLeft(), dirToRight)) {
					right = targetBorder.getLeft();
				} else {
					right = ownBorder.getRight();
				}

				if(left == right) {
					continue;
				}
				
				foundGateway = true;				
				goto foundGateway; 
			}
		}
	}
	foundGateway:

	if(foundGateway && CEngine::getDistance(left, right) >= CNavPolygonEdge::minEdgeLength) {
		adjacent.push_back(new CNavPolygonEdge(target, left, right));

		if(connectViceVersa) {
			target->adjacent.push_back(new CNavPolygonEdge(this, right, left));
		}
	}
}

void CNavPolygon::disconnectFrom(CNavPolygon* target) {
	for(CNavPolygonEdge* edge : adjacent) {
		if(edge->target == target) {
			adjacent.erase(remove(adjacent.begin(), adjacent.end(), edge), adjacent.end());

			if(!debugVisuals.empty()) {
				removeDebugVisuals();
				addDebugVisuals();
			}

			delete edge;
			break;
		}
	}
}

vector<CNavPolygonEdge*> CNavPolygon::getAdjacent() const {
	return adjacent;
}

CNavPolygon::~CNavPolygon() {
	for(CNavPolygonEdge* edge : adjacent) {
		edge->target->disconnectFrom(this);
		delete edge;
	}
}
