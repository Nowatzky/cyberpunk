#ifndef CYBERPUNK_CMAP_HPP
#define CYBERPUNK_CMAP_HPP

#include "CObject.hpp"
#include "CSceneManager.hpp"
#include "CMapBlock.hpp"
#include "CDirection.hpp"
#include "CMapRoom.hpp"
#include "CMapRoomGenerator.hpp"
#include "CMapCorridor.hpp"
#include "CNavPath.hpp"

class CMapRoomGeneratorDigOut;

class CMap {
private:
	vector<CMapRoom*> rooms;
	CMapRoomGenerator* mapRoomGenerator;
	
	mutable CMapRoom* lastRequestedRoom;
	
	// TODO outsource
	int minRoomSize = 28;
	int maxRoomSize = 52;
	int rollsRoomSize = 2;
	
	int minCorridorLength = 3;
	int maxCorridorLength = 13;
	int rollsCorridorLength = 3;
	
	int minDoorwaySize = 1;
	int maxDoorwaySize = 5;
	int rollsDoorwaySize = 4;

public:
    // Constructor
    CMap() {};

	void initMap();

	const vector<CMapBlock*>* getWallsForRoom(Vector2f pos);
	const vector<CMapBlock*>* getWallsForRoom(Vector3f pos);

	//vector<Vector2f> getFreeFieldsForRoom(Vector2f objectPosition);
	//vector<Vector2f> getFreeFieldsForRoom(Vector3f objectPosition);
	
	CNavMesh* getNavMeshForPosition(Vector2f position) const;
	
	CNavPath* findPath(Vector2f start, Vector2f target, float agentHalfWidth);
	
	void addDebugVisuals();
	void removeDebugVisuals();

	CMapRoom* getRoomAt(Vector2f pos) const;
	CMapRoom* getRoomAt(Vector3f pos) const;
	CMapRoom* getRoom(int roomNumber) const;
	
	std::vector<Vector2f> getStartingPositions(int playerCount) const;
	
private:
	
	bool generateNextRoom();
	vector<CDirection> getPossibleExitDirections(CRectangleArea roomArea, CDirection entranceDirection);
	bool isAreaFree(CRectangleArea roomArea);
	
	// Helper
	Vector2f getRandomRoomSize();
	int getRandomCorridorLength(int minimumCorridorLength);
	int getRandomDoorwaySize();
	
};

#endif //CYBERPUNK_CMAP_HPP