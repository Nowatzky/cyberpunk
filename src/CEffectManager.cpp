#include <queue>
#include <iostream>
#include "CEffectManager.hpp"
#include "CColorDefines.hpp"
#include "CEngine.hpp"
#include "CRenderManager.hpp"
#include "CEffectStun.hpp"
#include "CEffectForcePush.hpp"

void CEffectManager::draw(sf::RenderTarget* renderTarget) {
	if(effects.empty() || !isVisible) {
		return;
	}
	
	TexturesPriorityQueue textures;
	int icons = 0;
	
	for(CEffect* effect : effects) {
		std::vector<const sf::Texture*> visuals = effect->getVisuals();
		icons += visuals.size();
		
		textures.push(std::make_pair(effect->getVisualPriority(), visuals));
	}

	if(icons == 0) {
		return;
	}
	
	float width = icons * effectIconSize + (icons - 1) * effectIconSpacing;
	Vector2f currentPos = screenPos - Vector2f(width/2.f, 0);
	Sprite sprite;
	
	while(!textures.empty()) {
		vector<const sf::Texture*> textureLists = textures.top().second;
		textures.pop();
		
		for(const sf::Texture* texture : textureLists) {
			sprite.setTexture(*texture);
			sprite.setScale(effectIconSize/texture->getSize().x, effectIconSize/texture->getSize().y);
			sprite.setOrigin(effectIconSize/2.f, effectIconSize/2.f);
			sprite.setPosition(currentPos);
			sprite.setColor(COLOR_PINK);
			
			renderTarget->draw(sprite);
			
			currentPos.x += (effectIconSize + effectIconSpacing); 
		}
	}	
	
}

void CEffectManager::tickEffects(float frameTime) {
	resetEffectAggregate();
	
	vector<CEffect*>::iterator iter = effects.begin();
	
	while(iter != effects.end()) {
		if(!(*iter)->tick(frameTime, effectAggregate)) {
			// Calls onEnd internally
			delete *iter;
			iter = effects.erase(iter);
		} else {
			iter++;
		}
	}
}

void CEffectManager::addEffect(CEffect* effect) {
	effect->setOwner(owner);

	if(dynamic_cast<CEffectStun*>(effect) != nullptr) {
		owner->onStunned();
		owner->createTranslationSnapshotMessage();
	}

	if(dynamic_cast<CEffectForcePush*>(effect) != nullptr) {
		owner->createTranslationSnapshotMessage();
	}
	
	for(CEffect* existingEffect : effects) {
		if(existingEffect->canMergeEffect(effect)) {
			// Calls onStart internally
			delete effect;
			return;
		}
	}
	
	effects.push_back(effect);
	effect->onStart();
}

CEffect* CEffectManager::getEffectWithId(std::string id) {
	for(CEffect* existingEffect : effects) {
		if(existingEffect->getId() == id) {
			return existingEffect;
		}
	}

	return nullptr;
}

CEffect* CEffectManager::getEffectWithId(long id) {
	return getEffectWithId(std::to_string(id));
}

void CEffectManager::removeEffectWithId(std::string id) {
	vector<CEffect*>::iterator iter = effects.begin();

	while(iter != effects.end()) {
		if((*iter)->getId() == id) {
			// Calls onEnd internally if destructor was overwritten
			delete *iter;
			iter = effects.erase(iter);
		} else {
			iter++;
		}
	}
}

void CEffectManager::removeEffectWithId(long id) {
	return removeEffectWithId(std::to_string(id));
}

void CEffectManager::resetEffectAggregate() {
	effectAggregate = CEffectAggregate();
}

float CEffectManager::getMovementSpeedModifier() const {
	return effectAggregate.movementSpeedModifier;
}

bool CEffectManager::hasForcePush() const {
	return effectAggregate.hasForcePush;
}

Vector2f CEffectManager::getForcePush() const {
	return effectAggregate.forcePush;
}

void CEffectManager::setPosition(Vector3f pos) {
	screenPos = CRenderManager::convertSceneToGui(CEngine::toIso(pos) - Vector2f(0, yOffset));
}

CEffectManager::~CEffectManager() {
	vector<CEffect*>::iterator iter = effects.begin();

	while(iter != effects.end()) {
		delete *iter;
		iter++;
	}
}

bool CEffectManager::hasMark(CEffectMark::EffectMarkType markType) const {
	return effectAggregate.marks.find(markType) != effectAggregate.marks.end() &&
           effectAggregate.marks.at(markType) > 0;
}

int CEffectManager::getMarkCount(CEffectMark::EffectMarkType markType) const {
	if(hasMark(markType)) {
		return effectAggregate.marks.at(markType);
	} else {
		return 0;
	}
}

void CEffectManager::removeEffectMark(CEffectMark::EffectMarkType markType, int markCount) {
	std::vector<CEffect*>::iterator iter = effects.begin();
	
	while(iter != effects.end()) {
		CEffectMark* effectMark = dynamic_cast<CEffectMark*>(*iter);
		if(effectMark != nullptr) {
			effectMark->removeEffectMark(markType, markCount);
			break;
		}
		
		iter++;
	}

	if(effectAggregate.marks.find(markType) != effectAggregate.marks.end()) {
		effectAggregate.marks.at(markType) = max(effectAggregate.marks.at(markType) - markCount, 0);
	}	
}

bool CEffectManager::hasStun() const {
	return effectAggregate.hasStun;
}

void CEffectManager::ownerDeath() {
	if(!triggeredOwnerDeathThisFrame) {
		triggeredOwnerDeathThisFrame = true;
		for(CEffect* existingEffect : effects) {
			existingEffect->onOwnerDeath();
		}
	}
}

void CEffectManager::tick(float frameTime) {
	triggeredOwnerDeathThisFrame = false;
}

bool CEffectManager::hasMinAlpha() const {
	return effectAggregate.hasMinAlpha;
}

Uint8 CEffectManager::getMinAlpha() const {
	return static_cast<Uint8>(effectAggregate.minAlpha);
}

