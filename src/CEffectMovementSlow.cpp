#include "CEffectMovementSlow.hpp"
#include "CEffectManager.hpp"

bool CEffectMovementSlow::tick(float frameTime, CEffectAggregate& effectAggregate) {
	if(hasExpiredLifetime()) {
		return false;
	}

	effectAggregate.movementSpeedModifier *= movementSpeedModifier;

	return true;
}

void CEffectMovementSlow::setMovementSpeedModifier(float modifier) {
	movementSpeedModifier = modifier;
}
