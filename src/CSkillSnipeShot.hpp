#ifndef CYBERPUNK_CSKILLSNIPESHOT_HPP
#define CYBERPUNK_CSKILLSNIPESHOT_HPP

#include "CSkill.hpp"
#include "CEnemy.hpp"

class CSkillSnipeShot : public CSkill {
private:
	float minDamage = 35;
	float damagePerMeter = 6.f;
	
	float slowDuration = 700;
	float slowMovementSpeedModifier = 0.45f;

public:
	CSkillSnipeShot(CPlayer* owner) :
			CSkill(owner, 1200, 0) {}

	void fire(Vector3f hitPosition, bool doesHitTarget, CEnemy *target = nullptr);
	void fireRPC(Vector3f hitPosition, bool doesHitTarget, CEnemy *target, float dmg);

protected:
	void onButtonPress();
};

#endif //CYBERPUNK_CSKILLSNIPESHOT_HPP
