#include "CTempSpawningManager.hpp"
#include "CEnemyGhost.hpp"
#include "CEnemyGhostSummoner.hpp"
#include "CEnemySpecter.hpp"
#include "CEnemyLich.hpp"
#include "CNetworkManager.hpp"
#include "CNavMesh.hpp"
#include "CMessageCreateSpawnPoint.hpp"
#include "CMessageEnemySpawn.hpp"
#include "CUtilityDefines.hpp"


CTempSpawningManager::CTempSpawningManager(CDifficulty difficulty, int playerCount) :
		difficulty(difficulty),
		playerCount(playerCount) {
	
	// Starting room doesnt spawn enemies
	isCurrentRoomCompleted = true;
}

void CTempSpawningManager::tick(float frameTime) {
	if(isCurrentRoomCompleted) {
		return;
	}

	if(activeSpawnPoints.empty() && remainingSpawnPoints.empty() && CSceneManager::getEnemies().empty()) {
		isCurrentRoomCompleted = true;
		return;
	}
	
	// Spawn spawn point if possible
	if(spawnPointSpawnClock.hasPassed(spawnPointSpawnInterval)) {
		spawnPointSpawnClock.restart();
		if(activeSpawnPoints.size() < maxActiveSpawnPoints && !remainingSpawnPoints.empty()) {
			CSpawnPointInfo spawnPointInfo = remainingSpawnPoints.back();
			remainingSpawnPoints.pop_back();

			spawnPointInfo.spawnPoint = spawnSpawnPoint();
			spawnPointInfo.spawnClock.restart();

			activeSpawnPoints.push_back(spawnPointInfo);
		}
	}
	
	// Spawn enemies or delete empty spawn points
	for(auto iter = activeSpawnPoints.begin(); iter != activeSpawnPoints.end(); iter++) {
		if(iter->enemies.empty()) {
			iter->spawnPoint->deletePoint();
			
			activeSpawnPoints.erase(iter);
			iter--;
			continue;
		}
		
		if(iter->spawnClock.hasPassed(spawnInterval)) {
			iter->spawnClock.restart();
			
			CEnemyTypeHologram enemyType = iter->enemies.back();
			iter->enemies.pop_back();
			
			spawnEnemy(enemyType, CEngine::to2D(iter->spawnPoint->getPosition()));
		}
	}	
}

void CTempSpawningManager::enterRoom(int roomNumber, CNavMesh* navMesh) {
	std::cout << "Entering room " << roomNumber << endl;
	
	isCurrentRoomCompleted = false;
	currentNavMesh = navMesh;
	activeSpawnPoints.clear();
	
	CRoomSpawnInfo roomSpawnInfo = getRoomSpawnInfo(roomNumber);
	maxActiveSpawnPoints = roomSpawnInfo.maxActiveSpawnPoints;
	
	CWaveType waveType = getRandomWaveType(roomSpawnInfo.budget);
	
	remainingSpawnPoints = generateSpawnPoints(roomSpawnInfo, waveType);
}

std::vector<CSpawnPointInfo> CTempSpawningManager::generateSpawnPoints(CRoomSpawnInfo roomSpawnInfo, CWaveType waveType) {
	// Generate list of enemies
	std::vector<CEnemyTypeHologram> allEnemies;
	int attempts = 0;
	int maxAttempts = 20;
	int remainingBudget = roomSpawnInfo.budget;

	while(attempts < maxAttempts) {
		CEnemyTypeHologram enemyType = static_cast<CEnemyTypeHologram>(CRandom::getChoiceFromBackground(waveType.distribution));
		int cost = getEnemyCost(enemyType);
		attempts++;
		
		if(cost <= remainingBudget) {
			remainingBudget -= cost;
			allEnemies.push_back(enemyType);
			attempts = 0;			
		}
	}
	std::shuffle(std::begin(allEnemies), std::end(allEnemies), std::default_random_engine{});
	std::cout << "Deploying " << allEnemies.size() << " enemies with " << roomSpawnInfo.totalSpawnPointCount << " spawn points" << endl;
	
	// Generate spawn points
	std::vector<CSpawnPointInfo> spawnPoints;
	for(int i = 0; i < roomSpawnInfo.totalSpawnPointCount; i++) {
		spawnPoints.emplace_back();
	}
	
	// Distribute enemies to spawn points
	int currentSpawnPoint = 0;
	while(!allEnemies.empty()) {
		spawnPoints[currentSpawnPoint].enemies.push_back(allEnemies.back());
		allEnemies.pop_back();
		
		currentSpawnPoint = (currentSpawnPoint + 1) % roomSpawnInfo.totalSpawnPointCount;
	}

	return spawnPoints;
}

CRoomSpawnInfo CTempSpawningManager::getRoomSpawnInfo(int roomNumber) {
	// First room has Number 1 but should have basic value
	float roomScaling = roomNumber - 1;
	float playerScaling = static_cast<float>(pow(playerCount, playerScalingExponent));
	
	CRoomSpawnInfo spawnInfo;
	
	switch (difficulty) {
		case CDifficulty::DEBUG:
			spawnInfo.budget = 45;
			spawnInfo.maxActiveSpawnPoints = 1;
			spawnInfo.totalSpawnPointCount = 1;
			break;
		case CDifficulty::EASY:
			spawnInfo.budget = (int) (floor(40 + 8 * roomScaling) * playerScaling);
			spawnInfo.maxActiveSpawnPoints = 1;
			spawnInfo.totalSpawnPointCount = (int)floor(1 + 0.2f * roomScaling);
			break;
		case CDifficulty::MEDIUM:
			spawnInfo.budget = (int) (floor(50 + 11 * roomScaling) * playerScaling);
			spawnInfo.maxActiveSpawnPoints = 2;
			spawnInfo.totalSpawnPointCount = (int)floor(2 + 0.2f * roomScaling);
			break;
		case CDifficulty::HARD:
			spawnInfo.budget = (int) (floor(60 + 15 * roomScaling) * playerScaling);
			spawnInfo.maxActiveSpawnPoints = 3;
			spawnInfo.totalSpawnPointCount = (int)floor(3 + 0.2f * roomScaling);
			break;
	}
	return spawnInfo;
}

CWaveType CTempSpawningManager::getRandomWaveType(int budget) {
	CWaveType waveType;
	waveType.minBudget = 99999;

	std::vector<float> waveProbability = {
			0.2f, // 0 Default
			0.1f, // 1 Only invisible ghosts
			0.2f, // 2 Mainly spawners
			0.2f, // 3 Mixed madness
			0.15f, // 4 Enter the lich
			0.15f, // 5 Some specters			
	};

	while(budget < waveType.minBudget) {
		//int typeNumber = CRandom::getChoiceFromBackground(waveProbability);
		int typeNumber = 0;
		switch(typeNumber) {
			case 0: {
				waveType.name = "Default";
				//waveType.distribution = {0.47f, 0.4f, 0.1f, 0.03f, 0.f};
				waveType.distribution = {0.7f, 0.3f, 0.f, 0.0f, 0.f};
				waveType.minBudget = 0;
				break;
			}
			case 1: {
				waveType.name = "Only invisible ghosts";
				waveType.distribution = {0.f, 1.f, 0.f, 0.f, 0.f};
				waveType.minBudget = 0;
				break;
			}
			case 2: {
				waveType.name = "Mainly spawners";
				waveType.distribution = {0.3f, 0.1f, 0.6f, 0.f, 0.f};
				waveType.minBudget = 40;
				break;
			}
			case 3: {
				waveType.name = "Mixed madness";
				waveType.distribution = {0.35f, 0.25f, 0.15f, 0.15f, 0.1f};
				waveType.minBudget = 80;
				break;
			}
			case 4: {
				waveType.name = "Enter the lich";
				waveType.distribution = {0.25f, 0.25f, 0.15f, 0.f, 0.35f};
				waveType.minBudget = 100;
				break;
			}
			case 5: {
				waveType.name = "Some specters";
				waveType.distribution = {0.5f, 0.25f, 0.f, 0.25f, 0.f};
				waveType.minBudget = 100;
				break;
			}
			default:
				waveType.name = "Error";
				waveType.minBudget = 99999;
				break;
		}
	}

	std::cout << "Spawning wave type " << waveType.name << " with budget " << budget <<  endl;	
	return waveType;
}

int CTempSpawningManager::getEnemyCost(CEnemyTypeHologram enemyType) {
	switch(enemyType) {
		case CEnemyTypeHologram::GHOST:
			return 4;
		case CEnemyTypeHologram::GHOST_INVISIBLE:
			return 7;
		case CEnemyTypeHologram::GHOSTSUMMONER:
			return 22;
		case CEnemyTypeHologram::SPECTER:
			return 25;
		case CEnemyTypeHologram::LICH:
			return 35;
	}

	return 9999;
}

void CTempSpawningManager::spawnEnemy(CEnemyTypeHologram enemyType, Vector2f spawnPointPos) {
	Vector2f pos = findFreeSpawnPos(spawnPointPos);
	Vector2f dir = pos - spawnPointPos;
	float rot = CEngine::getRotation(dir);
	
	CEnemy* newEnemy;
	sf::Vector3f spawnPos = sf::Vector3f(pos.x, pos.y, 0);

	switch(enemyType) {
		case CEnemyTypeHologram::GHOST:
			spawnPos.z = 1.0f;
			newEnemy = new CEnemyGhost(spawnPos, rot, false);
			break;
		case CEnemyTypeHologram::GHOST_INVISIBLE:
			spawnPos.z = 1.0f;
			newEnemy = new CEnemyGhost(spawnPos, rot, true);
			break;
		case CEnemyTypeHologram::GHOSTSUMMONER:
			spawnPos.z = 1.2f;
			newEnemy = new CEnemyGhostSummoner(spawnPos, rot);
			//dynamic_cast<CEnemyGhostSummoner*>(newEnemy)->metaDataGhost = {enemyCostList[(int) GHOST], enemyPointList[(int) GHOST], enemyIntensityList[(int) GHOST]};
			//dynamic_cast<CEnemyGhostSummoner*>(newEnemy)->metaDataInvisibleGhost = {enemyCostList[(int) GHOST_INV], enemyPointList[(int) GHOST_INV], enemyIntensityList[(int) GHOST_INV]};			
			break;
		case CEnemyTypeHologram::SPECTER:
			spawnPos.z = 1.3f;
			newEnemy = new CEnemySpecter(spawnPos, rot);
			break;
		case CEnemyTypeHologram::LICH:
			spawnPos.z = 1.2f;
			newEnemy = new CEnemyLich(spawnPos, rot);
			break;
	}
	//std::vector<float> metaData = {enemyCostList[(int) enemy_instance], enemyPointList[(int) enemy_instance], enemyIntensityList[(int) enemy_instance]};
	//newEnemy->metaData = metaData;
	
	CNetworkManager::addReplicatingObject(newEnemy);
	CSceneManager::safeAddObject(newEnemy);

	auto message = static_cast<CMessageEnemySpawn*>(CNetworkManager::createMessage(CMessageType::ENEMY_SPAWN));
	message->enemyType = enemyType;
	message->objectId = newEnemy->getId();
	message->pos = newEnemy->getPosition();
	message->rot = newEnemy->getRotation();
	CNetworkManager::queueForBroadcast(message);
}

CTempSpawnPoint* CTempSpawningManager::spawnSpawnPoint() {
	Vector2f pos = currentNavMesh->getRandomPointInside();
	CTempSpawnPoint* spawnPoint = new CTempSpawnPoint(pos);
	CSceneManager::safeAddObject(spawnPoint);

	auto message = static_cast<CMessageCreateSpawnPoint*>(CNetworkManager::createMessage(CMessageType::CREATE_SPAWN_POINT));
	message->objectId = spawnPoint->getId();
	message->pos = pos;
	CNetworkManager::queueForBroadcast(message);

	return spawnPoint;
}

Vector2f CTempSpawningManager::findFreeSpawnPos(Vector2f spawnPoint) {
	Vector3f origin = Vector3f(spawnPoint.x, spawnPoint.y, 1.2f);
	Vector3f dimension = Vector3f(0.5f, 0.5f, 0.1f);

	float spawnRadiusStart = 1.25f;
	float spawnRadiusStop = 3.25f;

	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});

	float spawnRadiusOffset = 0;
	Vector3f spawnPos = VECTOR3D_INVALID;

	while(spawnPos == VECTOR3D_INVALID) {
		spawnPos = CSceneManager::findSpawnPoint(origin, dimension, collisionChannel, 0, spawnRadiusStart + spawnRadiusOffset, spawnRadiusStop + spawnRadiusOffset, 0.35f);
		spawnRadiusOffset += 1;
	}

	return Vector2f(spawnPos.x, spawnPos.y);
}

bool CTempSpawningManager::getIsCurrentRoomCompleted() {
	return isCurrentRoomCompleted;
}
