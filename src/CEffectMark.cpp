#include <queue>
#include "CEffectMark.hpp"
#include "CSceneManager.hpp"

bool CEffectMark::tick(float frameTime, CEffectAggregate& effectAggregate) {
	std::map<EffectMarkType, CEffectMarkEntry>::iterator iter = effectMarkEntries.begin();

	while(iter != effectMarkEntries.end()) {
		if((*iter).second.hasExpiredLifetime() || (*iter).second.markCount <= 0) {
			iter = effectMarkEntries.erase(iter);
		} else {
			effectAggregate.marks.insert(std::make_pair((*iter).first, (*iter).second.markCount));
			
			iter++;
		}
	}
	
	if(effectMarkEntries.empty()) {
		return false;
	}

	return true;
}

bool CEffectMark::canMergeEffect(CEffect* otherEffect) {
	CEffectMark* otherEffectMark = dynamic_cast<CEffectMark*>(otherEffect);

	if(otherEffectMark != nullptr) {
		for(pair<EffectMarkType, CEffectMarkEntry> effectMarkEntry : otherEffectMark->effectMarkEntries) {
			addEffectMarkEntry(effectMarkEntry.second);			
		}

		return true;
	}

	return false;
}

void CEffectMark::addEffectMarkEntry(CEffectMark::CEffectMarkEntry newEntry) {
	if(effectMarkEntries.find(newEntry.markType) == effectMarkEntries.end()) {
		effectMarkEntries.insert(make_pair(newEntry.markType, newEntry));
	} else {
		CEffectMark::CEffectMarkEntry& existingEntry = effectMarkEntries.at(newEntry.markType);
		
		// Lifetime
		float minElapsedLifetime = min(existingEntry.lifetimeClock.getTimeMs(), newEntry.lifetimeClock.getTimeMs());
		existingEntry.lifetimeClock.restart(minElapsedLifetime);
		
		// Initiator
		for(unordered_set<CCharacter*>::iterator iter = newEntry.initiators.begin(); iter != newEntry.initiators.end(); iter++) {
			existingEntry.initiators.insert(*iter);
		}
		
		// Mark Count
		existingEntry.markCount = min(existingEntry.markCount + newEntry.markCount, maxMarkCount);		
	}
}

void CEffectMark::onStart() {
}

void CEffectMark::onEnd() {
}

CEffectMark::CEffectMark(CCharacter* initiator, CEffectMark::EffectMarkType markType, int markCount) :
	CEffect(initiator) {
	
	CEffectMarkEntry initialEntry(initiator, markType, markCount);
	addEffectMarkEntry(initialEntry);
}

bool CEffectMark::CEffectMarkEntry::hasExpiredLifetime() const {
	return lifetime > 0 && lifetimeClock.getTimeMs() > lifetime;
}

std::string CEffectMark::getMarkTypeName(CEffectMark::EffectMarkType markType) {
	if(markType == EffectMarkType::Lightning) {
		return "EffectMarkLightning";
	}
	
	return "mark type not found";
}

vector<const sf::Texture*> CEffectMark::getVisuals() {
	vector<const sf::Texture*> textures;
	
	std::priority_queue<CEffectMarkEntry, vector<CEffectMarkEntry>, CEffectMarkEntry::CompareEffectMarkEntries> sortedEntries;
	map<EffectMarkType, CEffectMarkEntry>::iterator iter = effectMarkEntries.begin();

	while(iter != effectMarkEntries.end()) {
		sortedEntries.push((*iter).second);
		iter++;
	}

	while(!sortedEntries.empty()) {
		CEffectMarkEntry entry = sortedEntries.top();
		sortedEntries.pop();
		
		for(int i = 0; i < entry.markCount; i++) {
			textures.push_back(entry.texture);
		}
	}

	return textures;
}

int CEffectMark::getVisualPriority() {
	return 10;
}

void CEffectMark::removeEffectMark(CEffectMark::EffectMarkType markType, int markCount) {
	map<CEffectMark::EffectMarkType, CEffectMarkEntry>::iterator entry = effectMarkEntries.find(markType);

	if(entry != effectMarkEntries.end()) {
		(*entry).second.markCount -= markCount;

		if((*entry).second.markCount <= 0) {
			effectMarkEntries.erase(markType);
		}
	}
}
