#include <iostream>
#include "CMapCorridor.hpp"
#include "CMapFloorBlock.hpp"
#include "CNavMesh.hpp"

CMapCorridor::CMapCorridor(Doorway entrance, Doorway exit, int level) {
	this->entrance = entrance;
	this->exit = exit;

	float colorFactor = CEngine::clamp(1.f - (level * 0.05f), 0.3f, 1);

	colorWallBase = CEngine::scaleColor(colorWallBase, colorFactor);
	colorFloorBase = CEngine::scaleColor(colorFloorBase, colorFactor);
	
	if(entrance.direction == exit.direction) {
		generateStraightCorridor();
	} else {
		cout << "Error: Corridor not straight!" << endl;
	}
	
	generateNavMeshFromFloorBlocks();
}

int CMapCorridor::getLength() const {
	return length;
}

void CMapCorridor::generateStraightCorridor() {
	CDirection direction = entrance.direction.getOpposite();

	length = (int) (abs(entrance.left.x - exit.left.x) + abs(entrance.left.y - exit.left.y));
	
	// wallBlocks
	Vector2f step = direction.toVector();
	Vector2f leftStart = entrance.left + entrance.direction.getLeft().toVector() + step;
	Vector2f leftEnd = entrance.left + entrance.direction.getLeft().toVector() + step * (float) (length-1);
	
	Vector2f rightStart = entrance.right + entrance.direction.getRight().toVector() + step;
	Vector2f rightEnd = entrance.right + entrance.direction.getRight().toVector() + step * (float) (length-1);
	
	wallBlocks.push_back(new CMapBlock(leftStart, leftEnd, 1, colorWallBase, colorWallAccent, true));
	wallBlocks.push_back(new CMapBlock(rightStart, rightEnd, 1, colorWallBase, colorWallAccent, true));
	
	// FloorBlocks	
	generateRoomAreaFromWallBlocks();
	
	Vector3f pos = Vector3f(roomArea.offset.x + roomArea.size.x/2.f - 0.5f, roomArea.offset.y + roomArea.size.y/2.f - 0.5f, 0);
	Vector3f dim = Vector3f(roomArea.size.x/2.f, roomArea.size.y/2.f, 0.5f);

	if(direction == CDirection::PX || direction == CDirection::NX) {
		dim.y -= 1;
	} else {
		dim.x -= 1;
	}
			
	floorBlocks.push_back(new CMapFloorBlock(pos, dim, colorFloorBase, colorFloorAccent));
}
