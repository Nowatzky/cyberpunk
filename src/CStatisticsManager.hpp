#ifndef CYBERPUNK_CSTATISTICSMANAGER_HPP
#define CYBERPUNK_CSTATISTICSMANAGER_HPP


class CStatisticsManager {
private:
	static int score;

	CStatisticsManager() {}

public:
	static void scorePoints(int points);

	static int getScore();
};


#endif //CYBERPUNK_CSTATISTICSMANAGER_HPP
