#include "CEffectStun.hpp"

bool CEffectStun::tick(float frameTime, CEffectAggregate& effectAggregate) {
	if(hasExpiredLifetime()) {
		return false;
	}

	effectAggregate.hasStun = true;

	return true;
}

std::vector<const sf::Texture*> CEffectStun::getVisuals() {
	vector<const sf::Texture*> textures;
	
	textures.push_back(&CTextureManager::getTexture("EffectStunned"));
	

	return textures;
}

int CEffectStun::getVisualPriority() {
	return 5;
}

