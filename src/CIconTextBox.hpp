#ifndef CYBERPUNK_CICONTEXTBOX_HPP
#define CYBERPUNK_CICONTEXTBOX_HPP

#include <string>
#include "CGuiElement.hpp"

using sf::Vector2f;
using sf::Color;

class CIcon;
class CGuiTextElement;

/*
 * An icon inbred with a text box
 */

class CIconTextBox {
private:
	Vector2f screenPos;
	std::string iconSource;
	std::string text;
	Color color;
	float iconSize;
	float textSize;
	float textYOffsetInRelationToIconSize = 0.25f; //pixel


	CGuiTextElement* textElement;
	CIcon* icon;	

public:

	CIconTextBox(Vector2f screenPos, std::string iconSource, std::string text, Color color, float iconSize, float textSize);
	~CIconTextBox();

	void setIsVisibile(bool visible);
	void setColor(Color color);
	void changeIcon(std::string iconSource);
	void updateText(std::string text);
	void setComponentLifetime(float lifetime);

};


#endif //CYBERPUNK_CICONTEXTBOX_HPP
