#include "CGuiPopupText.hpp"
#include "CGuiManager.hpp"
#include "CEngine.hpp"

CGuiPopupText::CGuiPopupText(std::string text, unsigned int size, sf::Color color, sf::Vector2f pos, float lifetime, bool fadeOut) :
		text(text, CGuiManager::getFontDisplay(), size),
        pos(pos),
        lifetime(lifetime),
        fadeOut(fadeOut) {
	
	this->text.setOrigin(Vector2f(this->text.getLocalBounds().width, this->text.getLocalBounds().height) * 0.5f);
	this->text.setPosition(pos);
	this->text.setColor(color);
	this->text.setStyle(sf::Text::Style::Bold);
}

void CGuiPopupText::draw(sf::RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(text);
	}
}

void CGuiPopupText::tick(float frameTime) {
	if(lifetimeClock.getTimeMs() >= lifetime) {
		CGuiManager::removeGuiElement(this);
		return;
	}
	
	if(fadeOut && lifetimeClock.getTimeMs() >= fadeAfter * lifetime) {
		float alpha = CEngine::invertAlpha(CEngine::clampAlpha((lifetimeClock.getTimeMs() - lifetime * fadeAfter) / (lifetime - lifetime * fadeAfter)));
		
		sf::Color color = text.getColor();
		color.a = (Uint8) CEngine::lerp(0, 255, alpha);
		text.setColor(color);
	}
}

void CGuiPopupText::setColor(sf::Color color) {
	this->text.setColor(color);
}
