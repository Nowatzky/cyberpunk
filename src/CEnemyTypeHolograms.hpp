#ifndef CYBERPUNK_CENEMYTYPEHOLOGRAMS_HPP
#define CYBERPUNK_CENEMYTYPEHOLOGRAMS_HPP

#include <vector>
#include "CEnemyType.hpp"

using namespace std;


class CEnemyTypeHolograms : CEnemyType {
private:
	void setUpEnemyLists();

public:
	enum EnemyInstance {
		GHOST = CEnemyType::ENEMY_1,
		GHOST_INV = CEnemyType::ENEMY_2,
		GHOSTSUMMONER = CEnemyType::ENEMY_3,
		SPECTER = CEnemyType::ENEMY_4,
		LICH = CEnemyType::ENEMY_5,
		RANK_FIRST = GHOST, //always adjust!!
		RANK_LAST = LICH
	};


	CEnemyTypeHolograms();
	
	virtual vector<float> getEnemySpawnProbabilities(int spawnAssemblyType);
	virtual float getEnemyMinCosts(vector<float> enemyTypeDistribution);
	virtual CEnemy* getEnemyObject(ENEMY_INSTANCE enemy_instance);

};


#endif //CYBERPUNK_CENEMYTYPEHOLOGRAMS_HPP
