#ifndef CYBERPUNK_CLINEEFFECT_HPP
#define CYBERPUNK_CLINEEFFECT_HPP


#include "CGraphicBox.hpp"
#include "CObject.hpp"

class CLineEffect : public CObject {
private:
	CGraphicBox graphicBox;
	Clock clock;
	float lifetime = 100;

	CObject *o1 = nullptr;
	CObject *o2 = nullptr;
	float width;


public:
	CLineEffect(Vector3f p1, Vector3f p2, Color lineColor, float width = 0.05f);
	CLineEffect(CObject *o1, CObject *o2, Color lineColor, float width = 0.05f);

	CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();
	void setLifetime(float lifetime);

	void tick(float frameTime);

private:
	void updateDimensions(Vector3f p1, Vector3f p2);
};


#endif //CYBERPUNK_CLINEEFFECT_HPP
