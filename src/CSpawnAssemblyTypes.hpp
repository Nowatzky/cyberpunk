#ifndef CYBERPUNK_CSPAWNASSEMBLYTYPES_HPP
#define CYBERPUNK_CSPAWNASSEMBLYTYPES_HPP

#include <vector>

using namespace std;

/*
 * Make the fuck sure that everthing adds up to one
 *
 * If you add a new Enemy Type to any class of enemy, adjust all probability vectors
 * Just don't add new enemies
 *
 */

static vector<float> GET_SPAWN_PROBABILITIES_SPECTER(int type) {
    //probabilities according to foe type enum order: LICH, SPECTER, GHOSTSUMMONER, GHOST_INV, GHOST
    switch (type) {
        case 1:
            return {0,      0.03f,    0.10f,    0.4f,    0.47f};   //default
        case 2:
            return {0,      0,      0,      1,      0};     //only invis
        case 3:
            return {0,      0,      0.6,    0.1,    0.3};   //mainly spawners
        case 4:
            return {0,      0.15,   0,      0.4,   0.45};   //some specters
        case 5:
            return {0.2,   0,      0.2,    0.3,   0.3};  //enter lich
        case 6:
            return {1, 0,0,0,0};

        default:
            return {1, 0, 0, 0, 0};
    }
}


#endif //CYBERPUNK_CSPAWNASSEMBLYTYPES_HPP
