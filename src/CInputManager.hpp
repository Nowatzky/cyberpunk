#ifndef CYBERPUNK_CINPUTMANAGER_HPP
#define CYBERPUNK_CINPUTMANAGER_HPP

#include <SFML/Window.hpp>

#include "CPlayer.hpp"
#include "CPatternRecognitionMngr.hpp"
#include "CIconList.hpp"
#include "CGuiDrawing.hpp"

using namespace sf;

class CInputManager {
    // Private constructor
private:
    CInputManager() {}

public:
    static void setLocalPlayer(CPlayer* localPlayer);
	static void setRenderWindow(RenderWindow* renderWindow);
    static void setPatternRecognitionMngr(CPatternRecognitionMngr *reconMngr);

    static void handleKeyboardInput(float frameTime);
    static void handleMouseInput(float frameTime);

	static Vector2f getMousePositionScene();
	static Vector2f getMousePositionGui();
	static Vector2f getDirToMouseInScene(Vector3f reference);
	
	static Vector2f getNormalizedPlayerMovementDir();

	static vector<CSkill*> playerSignSkillQueue;
	
	// Debug
	static Keyboard::Key keyDebugNavigation;
	static Keyboard::Key keyDebugExecutePrimaryAction;
	static Keyboard::Key keyDebugExecuteSecondaryAction;
	static bool isDebugKeyToggledOn(Keyboard::Key key); 
	
private:
    // Keys
    static Keyboard::Key keyPlayerMoveUp;
    static Keyboard::Key keyPlayerMoveDown;
    static Keyboard::Key keyPlayerMoveLeft;
    static Keyboard::Key keyPlayerMoveRight;

	static Mouse::Button keyPlayerMousePrimary;
	static Mouse::Button keyPlayerMouseSecondary;

	static Keyboard::Key keyPlayerSignCast;
	static Keyboard::Key keyPlayerReload;
	static Keyboard::Key keyPlayerSkill1;
	static Keyboard::Key keyPlayerSwitchClasses;

	static bool isKeySignCastPressed;
	static bool keyMouseSecondaryPressedLastFrame;

    static CPlayer* player;
	static CPatternRecognitionMngr* patternRecognitionMngr;
	static RenderWindow* window;
	static std::vector<Vector2f> drawingPoints;
	static CGuiDrawing* guiDrawing;
	
	//Debug
	static bool isDebugNavigationKeyPressed;
	static bool isDebugExecutePrimaryActionKeyPressed;
	static bool isDebugExecuteSecondaryActionKeyPressed;

};


#endif //CYBERPUNK_CINPUTMANAGER_HPP