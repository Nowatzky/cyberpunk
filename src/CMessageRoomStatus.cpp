#include "CMessageRoomStatus.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CGameStateManager.hpp"

void CMessageRoomStatus::serializeInto(sf::Packet& packet) {
	packet << roomNumber
	       << roomCompleted;
}

void CMessageRoomStatus::deserializeFrom(sf::Packet& packet) {
	packet >> roomNumber
	       >> roomCompleted;
}

void CMessageRoomStatus::process() {
	if(roomCompleted) {
		CSceneManager::getMap()->getRoom(roomNumber)->openExit();
		
		CGameStateManager::reviveParty();
		CGameStateManager::setCurrentRoom(roomNumber + 1);		
	} else {
		CSceneManager::getMap()->getRoom(roomNumber)->permanentlySealEntrance();
	}
}
