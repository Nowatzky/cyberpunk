#ifndef CYBERPUNK_CGHOSTSUMMONPOINT_HPP
#define CYBERPUNK_CGHOSTSUMMONPOINT_HPP

#include "CObject.hpp"
#include "CGraphicBox.hpp"

class CGhostSummonPoint : public CObject {
private:
	Clock clockSummonDuration;
	float summonDuration;
	float summonAngle;
	bool summonInvisibleGhost;
	CGraphicBox graphicBox;

public:

	CGhostSummonPoint(Vector3f pos, float summonDuration, float summonAngle, bool summonInvisibleGhost = false) :
			CObject(pos, 0),
	        summonDuration(summonDuration),
	        summonAngle(summonAngle),
	        summonInvisibleGhost(summonInvisibleGhost),
	        graphicBox(Vector3f(pos.x, pos.y, 0.55f), Vector3f(0.4f, 0.4f, 0.05f),
	                   summonInvisibleGhost ? Color(0, 100, 200, 90) : Color(0, 150, 100, 90)) {}
	
	void tick(float frameTime);

	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();
	
	vector<float> metaDataGhost;
};


#endif //CYBERPUNK_CGHOSTSUMMONPOINT_HPP
