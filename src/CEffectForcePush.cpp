#include "CEffectForcePush.hpp"

bool CEffectForcePush::tick(float frameTime, CEffectAggregate& effectAggregate) {
	if(hasExpiredLifetime()) {
		return false;
	}

	float currentPushStrength = CEngine::invertAlpha(CEngine::clampAlpha(lifetimeClock.getTimeMs()/lifetime)) * pushStrength;
	
	effectAggregate.hasForcePush = true;
	effectAggregate.forcePush += direction * currentPushStrength;

	return true;
}
