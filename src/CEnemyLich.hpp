#ifndef CYBERPUNK_CENEMYLICH_HPP
#define CYBERPUNK_CENEMYLICH_HPP

#include "CEnemy.hpp"
#include "CAIControllerLich.hpp"
#include "CColorDefines.hpp"

enum class LichSpell {FIRE, DMG_FIELD};

class CEnemyLich : public CEnemy {
private:
    float cooldownCastAnySpell = 4000.f;
    float healthRegeneration = 1.f;
    float baseSpeed = 3;
    float speedIncreaseOnMissingHealth = 3; // times percentage of missing health
	
    CClock clockCastAnySpellCooldown;
    CClock clockCastDuration;

    LichSpell currentSpell = LichSpell::FIRE;
    std::vector<float> spellCastDuration = {500, 500};

    // Fire
    float fireSpellDamage = 20.f;

    // Damaging field
    float dmgFieldTriggerDuration = 1050;
    float dmgFieldDimension = 2.1f;
    float playerMovementOffset = 2.f;

    // Spectral dust
    float spectralDustLifetime = 5000.f;
	
	// Dodge
	CClock clockDodgeCooldown;	
	float dodgeCooldown = 3500;
	float dodgeDistance = 3.0f;
	bool isDodging = false;
	CClock clockDodging;
	float dodgeDuration = 100;

	Color colorBase = COLOR_BLACK_BLUE_ALPHA;
	Color colorAccent = COLOR_BLACK_BLUE_ALPHA;
	
public:
    CEnemyLich(const Vector3f& pos, float rot) :
            CEnemy(new CAIControllerLich(this), pos, rot, Vector3f(0.15f, 0.15f, 0.7f), COLOR_BLACK_BLUE_ALPHA, 120, 3.f) {
        turningMovementProfile.isTurningEnemy = false;
	    clockDodgeCooldown.restart(4000);
    }
    
    void tick(float frameTime);

    vector<float> spellProbabilities = {0.7f, 0.3f}; //should add up to 

    bool isAnySpellCastReady();
    void deployFireSpell();
    void spawnDmgField();
    void beginCasting(LichSpell spell);
    bool isCasting();
    bool isActing; //enemy's casting or deploying spell
    void deployCurrentSpell();
	
	bool isDodgeReady();
	bool getIsDodging() const;
	void dodge();
	
    virtual void die();


private:
    void spawnSpectralDust();
	void executeDodge();
	
	Vector3f findDodgePosition();
};
#endif //CYBERPUNK_CENEMYLICH_HPP
