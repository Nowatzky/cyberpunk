#ifndef CYBERPUNK_CPROJECTILESPECTERBULLET_HPP
#define CYBERPUNK_CPROJECTILESPECTERBULLET_HPP


#include "CProjectile.hpp"
#include "CColorDefines.hpp"

class CProjectileSpecterBullet : public CProjectile {
private:
    float dmg;
    float startSpeed = 4.0f;
    float acceleration = 5.f; //per second
    float turningSpeed = 90; //per whatever

	bool isActive = true;
	
public:
    CProjectileSpecterBullet(CCharacter* initiator, const Vector3f &pos, const Vector2f &dir, float dmg) :
            CProjectile(initiator, pos, Vector3f(0.1,0.1,0.1), COLOR_BLACK_BLUE_ALPHA, dir),
            dmg(dmg) {
	    setSpeed(startSpeed);}

    virtual void tick(float frameTime);
	virtual void onHitWall(float frameTime);
	virtual void onHitPlayer(CPlayer* target, float frameTime);
	virtual void onHitProjectile(CProjectile* projectile, float frameTime);
};


#endif //CYBERPUNK_CPROJECTILESPECTERBULLET_HPP
