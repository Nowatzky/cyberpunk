#ifndef CYBERPUNK_CPROJECTILELASERBULLET_HPP
#define CYBERPUNK_CPROJECTILELASERBULLET_HPP


#include "CProjectile.hpp"
#include "CColorDefines.hpp"

class CProjectileLaserBullet : public CProjectile {
private:
	static constexpr float damage = 10;

public:
    CProjectileLaserBullet(CCharacter* initiator, const Vector3f& pos, const Vector2f& dir) :
        CProjectile(initiator, pos, Vector3f(0.045f, 0.45f , 0.045f), COLOR_PINK, dir, 50.f) {}

    void onHitEnemy(CEnemy* target, float frameTime);
    void onHitEnemyRPC(CEnemy* target, bool addMark);
    static void onHitEnemyFallback(CEnemy* target, bool addMark, Vector2f dir);
	void onHitWall(float frameTime);
	
};


#endif //CYBERPUNK_CPROJECTILELASERBULLET_HPP
