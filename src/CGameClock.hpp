#ifndef CYBERPUNK_CGAMECLOCK_HPP
#define CYBERPUNK_CGAMECLOCK_HPP


#include "CClock.hpp"

class CGameClock {
private:
	static CClock gameClock;
	
	static float measuredOffset;
	
	static float offsetToLocalClock;
	
	static int ignorePackets;

	CGameClock() {}

public:
	static void init();
	static float getGameTime();
	
	static void tick(float frameTime);
	static void adjustTowards(float targetTime);
	
};


#endif //CYBERPUNK_CGAMECLOCK_HPP
