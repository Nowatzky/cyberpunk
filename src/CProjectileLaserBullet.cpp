#include "CProjectileLaserBullet.hpp"
#include "CSceneManager.hpp"
#include "CObjectProjectileHitPopUpBox.hpp"
#include "CEffectForcePush.hpp"
#include "CEffectVisible.hpp"
#include "CRpcHandler.hpp"
#include "CGui.hpp"

void CProjectileLaserBullet::onHitEnemy(CEnemy* target, float frameTime) {
	if(!isLocal) {
		return;
	}
	
    target->reduceHealth(damage);
	CPlayer::checkKillPotential(target, initiator);

	Vector2f pushVector = CEngine::normalize(getDirection());
	
	if(target->canBePushedByBullet()) {
		target->addEffect(new CEffectForcePush(nullptr, 10.f, pushVector, 30));
	}

	int markCount = target->getEffectManager()->getMarkCount(CEffectMark::EffectMarkType::Lightning);
	bool addMark = CRandom::occurAtChance(1.f/(markCount+1)); 
	if(addMark) {
		target->addEffect(new CEffectMark(nullptr, CEffectMark::EffectMarkType::Lightning, 1));
	}

	target->addEffect(new CEffectVisible(nullptr, 140, 500));

	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(getPosition(), Vector3f(0.15f, 0.15f, 0.15f), COLOR_PINK_ALPHA - Color(0, 0, 0, 30), 35));
    CSceneManager::safeRemoveObject(this);
	
	CRpcHandler::sendProjectileLaserBulletOnHitEnemy(this, target->getId(), addMark, dir);
}

void CProjectileLaserBullet::onHitEnemyRPC(CEnemy* target, bool addMark) {
	target->reduceHealth(damage);

	Vector2f pushVector = CEngine::normalize(getDirection());

	if(target->canBePushedByBullet()) {
		target->addEffect(new CEffectForcePush(nullptr, 10.f, pushVector, 30));
	}

	if(addMark) {
		target->addEffect(new CEffectMark(nullptr, CEffectMark::EffectMarkType::Lightning, 1));
	}

	target->addEffect(new CEffectVisible(nullptr, 140, 500));

	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(getPosition(), Vector3f(0.15f, 0.15f, 0.15f), COLOR_PINK_ALPHA - Color(0, 0, 0, 30), 35));
	CSceneManager::safeRemoveObject(this);
}

void CProjectileLaserBullet::onHitEnemyFallback(CEnemy* target, bool addMark, Vector2f dir) {
	target->reduceHealth(damage);

	if(target->canBePushedByBullet()) {
		target->addEffect(new CEffectForcePush(nullptr, 10.f, dir, 30));
	}

	if(addMark) {
		target->addEffect(new CEffectMark(nullptr, CEffectMark::EffectMarkType::Lightning, 1));
	}

	target->addEffect(new CEffectVisible(nullptr, 140, 500));
}

void CProjectileLaserBullet::onHitWall(float frameTime) {
	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(getPosition(), Vector3f(0.15f, 0.15f, 0.15f), COLOR_PINK_ALPHA - Color(0, 0, 0, 30), 35));
	CSceneManager::safeRemoveObject(this);
}
