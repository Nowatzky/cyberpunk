#ifndef CYBERPUNK_CMAPROOM_HPP
#define CYBERPUNK_CMAPROOM_HPP

#include "CDirection.hpp"
#include "CMapBlock.hpp"
#include "CRectangleArea.hpp"

class CNavMesh;

struct Doorway {
	Vector2f left;
	Vector2f right;
	CDirection direction;
};

class CMapRoom {
protected:
	CRectangleArea roomArea;
	Doorway entrance;
	Doorway exit;

	vector<CMapBlock*> wallBlocks;
	vector<CMapBlock*> floorBlocks;
	
	vector<CMapBlock*> doorwayBlocks;
	
	CNavMesh* navMesh{};	

public:
	CMapRoom(Doorway entrance, Doorway exit, vector<CMapBlock*> wallBlocks, vector<CMapBlock*> floorBlocks);

protected:
	CMapRoom() = default;
	
	void generateRoomAreaFromWallBlocks();
	void generateNavMeshFromFloorBlocks();
	
public:

	void registerToSceneManager();
	void removeFromSceneManager();
	
	void sealOfExit();
	void openExit();
	void permanentlySealEntrance();
	
	// Getter/Setter
	CRectangleArea getRoomArea() const;
	Doorway getEntrance() const;
	Doorway getExit() const;
	CNavMesh* getNavMesh() const;
	const vector<CMapBlock*>* getWallBlocks() const;
	int getMinimumExitCorridorLength() const;

private:
	void sealOfDoorway(Doorway doorway, bool isSealTemporary = true);	
	
	bool isEntrancePermanentlySealed = false;
	
	
};


#endif //CYBERPUNK_CMAPROOM_HPP
