#ifndef CYBERPUNK_CTOPOLOGICALNODE_HPP
#define CYBERPUNK_CTOPOLOGICALNODE_HPP

#include <vector>

#include "CObject.hpp"

using namespace std;

class CTopologicalNode {
public:
    CObject* object;
    vector<CTopologicalNode*> childrenMovable;
    vector<CTopologicalNode*> childrenStatic;
    bool marked = false;

    CTopologicalNode(CObject* object)
            : object(object) {}
};

#endif //CYBERPUNK_CTOPOLOGICALNODE_HPP