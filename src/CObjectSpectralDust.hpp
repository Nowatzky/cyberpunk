#ifndef CYBERPUNK_COBJECTSPECTERDUST_HPP
#define CYBERPUNK_COBJECTSPECTERDUST_HPP


#include "CObject.hpp"
#include "CBoundingBox.hpp"
#include "CGraphicBox.hpp"
#include "CColorDefines.hpp"

class CObjectSpectralDust : public CObject {
    CBoundingBox boundingBox;
    CGraphicBox graphicBox;
    float lifetime;
    Clock survivalClock;
	
	int callInterval = 0;
	float consumedHealth = 0;
	int minHealth = 150;

public:
    CObjectSpectralDust(Vector3f pos) :
    CObject(pos, 0),
    boundingBox(pos, Vector3f(0.5f,0.5f,0.2f)),
    graphicBox(pos, Vector3f(0.5f,0.5f,0.2f), COLOR_BLACK_BLUE_ALPHA) {
        boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);
    }

    virtual void tick(float frameTime);
    void setLifetime(float lifetime);
    CCollisionContainer* getCollisionContainer();
    CGraphicContainer* getGraphicContainer();

private:
	void callGhosts();
};


#endif //CYBERPUNK_COBJECTSPECTERDUST_HPP
