#ifndef CYBERPUNK_CEFFECTCHAINFLASH_HPP
#define CYBERPUNK_CEFFECTCHAINFLASH_HPP


#include "CEffect.hpp"
#include "CObjectIdManager.hpp"
#include "CObject.hpp"

class CEnemy;

class CEffectChainFlash : public CEffect {
private:
	float maxDistance = 9;
	std::vector<float> damage = {22.5f, 32.5, 40};
	std::vector<float> stunDuration = {750, 1200, 1750};
	
	bool wasAlreadyTriggered = false;
	
	float delay = 150;
	
public:
	CEffectChainFlash(CCharacter* initiator) :
			CEffect(initiator) {
		setId("CEffectChainFlash");
	}

	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);
	
	virtual void onStart();

	void triggerFlash();
	void triggerFlashRPC(std::vector<CObject*> targets);
	static void triggerFlashFallback(std::vector<CObject*> targets);

private:
	CCharacter* findNearestTarget() const;
	
	void hit(CCharacter* enemy);	
	static void hitFallback(CCharacter* enemy);	

};


#endif //CYBERPUNK_CEFFECTCHAINFLASH_HPP
