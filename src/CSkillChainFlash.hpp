#ifndef CYBERPUNK_CSKILLCHAINFLASH_HPP
#define CYBERPUNK_CSKILLCHAINFLASH_HPP

#include "CCharacter.hpp"
#include "CSkill.hpp"

class CEnemy;

class CSkillChainFlash : public CSkill {
private:
	float maxDistance = 10;

public:
    CSkillChainFlash(CPlayer* owner) :
    CSkill(owner, 250, 35) {}
    
    void fire(std::vector<CCharacter*> targets);
    void fireRPC(std::vector<CObjectId> targetIds);
    static void fireFallback(std::vector<CObjectId> targetIds);

protected:
    void onButtonPress();

private:
	CCharacter* findNearestTarget() const;
	CCharacter* findNearestTarget(CCharacter* prevTarget) const;
	
	static void hit(CCharacter* initiator, CCharacter* target);
};


#endif //CYBERPUNK_CSKILLCHAINFLASH_HPP
