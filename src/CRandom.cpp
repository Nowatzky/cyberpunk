#include "CRandom.hpp"
#include "CEngine.hpp"

mt19937 CRandom::rng;
uniform_real_distribution<float> CRandom::distFloat;
uniform_int_distribution<int> CRandom::distInt;

void CRandom::initialize() {
	initialize(static_cast<unsigned long>(std::time(nullptr)));
}

void CRandom::initialize(unsigned long seed) {
	rng.seed(seed);
	distFloat = uniform_real_distribution<float>(0, 1);
	distInt = uniform_int_distribution<int>(0, 1);
}

sf::Uint32 CRandom::getRandomSeed() {
	return static_cast<Uint32>(getInRangeI(0, std::numeric_limits<int>::max() - 1));
}

float CRandom::getInRange(float min, float max) {
	return distFloat(rng) * (max - min) + min;
}

float CRandom::getInRange(float min, float max, int rolls) {
	if(rolls < 2) {
		return getInRange(min, max);
	}
	
	float sum = 0;
	for(int i = 0; i < rolls; i++) {
		sum += distFloat(rng);
	}
	
	return sum/((float) rolls) * (max - min) + min;
}

int CRandom::getInRangeI(int min, int max) {
	distInt = uniform_int_distribution<int>(min, max);
	return distInt(rng);
}

int CRandom::getInRangeI(int min, int max, int rolls) {
	return (int) round(getInRange(min, max, rolls));
}

bool CRandom::occurAtChance(float chance) {
	return distFloat(rng) <= chance;
}

bool CRandom::occurAtChancePerSecond(float chance, float frameTime) {
	return occurAtChance(chance * frameTime);
}

float CRandom::randomSign() {
	return occurAtChance(0.5f) ? -1.f : 1.f;
}

int CRandom::getChoiceFromBackground(vector<float> backgroundProbabilities) {
	float chance = getInRange(0.f, 1.f);
	for(unsigned int i = 0; i < backgroundProbabilities.size(); ++i) {
		chance -= backgroundProbabilities[i];
		if(chance <= 0) {
			return i;
		}
	}

	return -1; //deprecated
}

Vector2f CRandom::getRandomPosInCircle(Vector2f center, float radius) {
	float r = getInRange(0.f, radius);
	float angle = getInRange(0.f, 360.f);
	return center + CEngine::getUnitInDirection(angle) * r;
}

Vector2f CRandom::getRandomPosInSquare(Vector2f center, float side) {
	side *= 0.5f;
	float dx = getInRange(-side, side);
	float dy = getInRange(-side, side);
	return center + Vector2f(dx, dy);
}


