#ifndef CYBERPUNK_CSKILLDECOY_HPP
#define CYBERPUNK_CSKILLDECOY_HPP


#include "CPlayer.hpp"
#include "CPlayerDummy.hpp"

class CSkillDecoy : public CSkill {
private:
	float permanentEnergyConsumption = 40 + 20; //per second (+ standard regeneration)
	CPlayerDummy* dummy;
	bool hasCastDummy = false;

public:
	CSkillDecoy(CPlayer* owner) :
	CSkill(owner, 500, 40) {}

protected:
	void onButtonPress();
	void tick(float frameTime);
	void endDecoy();
};


#endif //CYBERPUNK_CSKILLDECOY_HPP
