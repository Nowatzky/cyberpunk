#ifndef CYBERPUNK_CSKILLPRIMARYLASERFIRE_HPP
#define CYBERPUNK_CSKILLPRIMARYLASERFIRE_HPP

#include <vector>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System.hpp>
#include "CSkill.hpp"
#include "CClock.hpp"
#include "CProjectile.hpp"

class CSkillPrimaryLaserFire : public CSkill {
private:
    const int maxBullets = 8;
	const float fireCooldown = 600;
	const float reloadCooldown = 1500;
	int remainingBullets = maxBullets;
	CClock fireCooldownClock;
	CClock reloadCooldownClock;

	const float doubleShotDelay = 110;
	CClock doubleShootDelayClock;
	bool waitingForDoubleShoot = false;
	Vector2f lastShootDirection;
	CObjectId projectileIdForDoubleShot;

	float doubleShootOffset = 0.225f;
	float doubleShootAngleOffsetInside = 1.5f;

	const float popupSize = 0.16f;
	const float popupDuration = 30;
	const Color popupColor = Color(255, 255, 255, 170);
	
public:
    CSkillPrimaryLaserFire(CPlayer* owner) :
        CSkill(owner, 0, 0) {}

	virtual void tick(float frameTime);
    
	virtual void quickDraw();
	virtual void quickDrawRPC();
    virtual void reload();
	virtual void reloadRPC(float gameTime);

	void fire(Vector2f dir);
	void fireRPC(float gameTime, Vector3f originalPos, Vector2f dir, std::vector<CObjectId> projectileIds);

private:
	CProjectile* spawnProjectile(Vector2f dir, bool isFirstShot);
	
	void stopReloading(bool wasQuickdraw);

};


#endif //CYBERPUNK_CSKILLPRIMARYLASERFIRE_HPP
