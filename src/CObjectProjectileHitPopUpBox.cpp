#include "CObjectProjectileHitPopUpBox.hpp"
#include "CSceneManager.hpp"

void CObjectProjectileHitPopUpBox::tick(float frameTime) {
	if(clock.getElapsedTime().asMilliseconds() >= lifetime) {
		CSceneManager::safeRemoveObject(this);
	}
}

CCollisionContainer* CObjectProjectileHitPopUpBox::getCollisionContainer() {
	return nullptr;
}

CGraphicContainer* CObjectProjectileHitPopUpBox::getGraphicContainer() {
	return &graphicBox;
}
