#include "CSkillDecoy.hpp"
#include "CSceneManager.hpp"


//TODO complete rework required if multiplayer is going to be implemented
void CSkillDecoy::onButtonPress() {
	if(!hasCastDummy) {
		hasCastDummy = true;
		
		Color color = owner->getCharacterClass()->getBaseColor();
		color.a = 25;
		owner->getGraphicContainer()->setColorBase(color);
		owner->restoreEnergy(25); //Restore the required energy which was consumed (to start at 0)

		//Now to the fun part
		dummy = new CPlayerDummy(owner->getPosition(), Vector3f(0.35f, 0.35f, 0.6f), owner->getCharacterClass()->getBaseColor());
		CSceneManager::safeAddObject(dummy);
	}
}

void CSkillDecoy::tick(float frameTime) {
	if(hasCastDummy) {
		if(!owner->consumeEnergy(permanentEnergyConsumption * frameTime)) {
			endDecoy();
		}
	}
}

void CSkillDecoy::endDecoy() {
	owner->getGraphicContainer()->setColorBase(owner->getCharacterClass()->getBaseColor());
	CSceneManager::safeRemoveObject(dummy);
	hasCastDummy = false;
}
