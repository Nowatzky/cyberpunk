#include <iostream>
#include "CPlayerDisplayBar.hpp"
#include "CEngine.hpp"
#include "CGuiManager.hpp"
#include "CRenderManager.hpp"

CPlayerDisplayBar::CPlayerDisplayBar(float maxValue, Color color, int yOffset) :
		maxValue(maxValue),
		currentValue(maxValue),
		yOffset(yOffset) {
	
	textBar = new Text("2", CGuiManager::getFontDebug());
	textBar->setStyle(Text::Bold);
	textBar->setCharacterSize((unsigned int) (32 * CGuiManager::getGuiFontSizeFactor()));
	textBar->setColor(color);
	barDashCount = 33;

	tick(0);

	textBar->setOrigin(textBar->getLocalBounds().width / 2.f, textBar->getLocalBounds().height / 2.f);

	CGuiManager::addGuiElement(this);
}

void CPlayerDisplayBar::draw(RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(*textBar);
	}
}

void CPlayerDisplayBar::tick(float frameTime) {
	String textBarString = "[";
	float currentDisplayValue = currentValue * pow(currentValue/maxValue, skew);
	
	for(int i = 0; i < barDashCount; i++) {
		textBarString += maxValue / (float)(barDashCount) * i < currentDisplayValue ? "|" : "-";
	}
	textBarString += "]";
	
	textBar->setString(textBarString);

	View* view = CRenderManager::getViewGui();
	setCenterPosition(view->getCenter() + Vector2f(0, view->getSize().y / 2.f - yOffset));
}

void CPlayerDisplayBar::setCurrentValue(float value) {
	currentValue = CEngine::clamp(value, 0, maxValue);
}

void CPlayerDisplayBar::setCenterPosition(Vector2f position) {
	textBar->setPosition(position);
}

CPlayerDisplayBar::~CPlayerDisplayBar() {
	CGuiManager::removeGuiElement(this);
	delete textBar;
}

void CPlayerDisplayBar::setColor(Color color) {
	textBar->setColor(color);
}

void CPlayerDisplayBar::setSkew(float skew) {
	this->skew = skew;
}
	
