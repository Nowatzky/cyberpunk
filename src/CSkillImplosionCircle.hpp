#ifndef CYBERPUNK_CSKILLIMPLOSIONCIRCLE_HPP
#define CYBERPUNK_CSKILLIMPLOSIONCIRCLE_HPP

#include "CPlayer.hpp"
#include "CFlatCircle.hpp"

class CSkillImplosionCircle : public CSkill {
private:
	float radius = 7.5f;
	float implosionDuration = 300;
	float implosionDelay = 300;
	float implosionStrength = 25.f;
	float implosionAccelerator = 20.f;
	float closestDistance = 0.25f;
	bool isImploding = false;
	bool isDelayed = true;

	Vector3f origin;
	CClock implosionClock;
	vector<CEnemy *> affectedEnemies;
	CFlatCircle *visualCircle;

public:
	CSkillImplosionCircle(CPlayer* owner) :
			CSkill(owner, 500, 30) {}

protected:
	void onButtonPress();
	void tick(float frameTime);
};


#endif //CYBERPUNK_CSKILLIMPLOSIONCIRCLE_HPP
