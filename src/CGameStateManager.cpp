#include "CGameStateManager.hpp"
#include "CNetworkManager.hpp"
#include "CGuiPopupText.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CMessageRoomStatus.hpp"
#include "CRenderManager.hpp"

bool CGameStateManager::isAuthoritative;
std::vector<CPlayer*> CGameStateManager::actualPlayers;
CPlayer* CGameStateManager::localPlayer;
CTempSpawningManager* CGameStateManager::spawningManager;
int CGameStateManager::currentRoom;
bool CGameStateManager::arePlayersTravelingToCurrentRoom;
bool CGameStateManager::wasGameOverTriggered;
bool CGameStateManager::waitingForRoomEnterDelay;
CClock CGameStateManager::roomEnterDelayClock;

void CGameStateManager::init(CPlayer* localPlayer_, CTempSpawningManager* spawningManager_) {
	isAuthoritative = !CNetworkManager::isActive() || CNetworkManager::getRole() == CNetworkRole::HOST;
	localPlayer = localPlayer_;
	spawningManager = spawningManager_;
	actualPlayers.push_back(localPlayer);
	
	currentRoom = 1;
	arePlayersTravelingToCurrentRoom = true;	
	wasGameOverTriggered = false;
	waitingForRoomEnterDelay = false;
}

void CGameStateManager::addRemotePlayer(CPlayer* remotePlayer) {
	actualPlayers.push_back(remotePlayer);
}

void CGameStateManager::tick() {
	updateBlurEffect();	
	
	if(!isAuthoritative) {
		return;
	}

	if(arePlayersTravelingToCurrentRoom) {
		if(waitingForRoomEnterDelay) {
			if(areAllPlayersInCurrentRoom()) {
				if(roomEnterDelayClock.hasPassed(1500)) {
					CSceneManager::getMap()->getRoom(currentRoom)->permanentlySealEntrance();
					arePlayersTravelingToCurrentRoom = false;
					spawningManager->enterRoom(currentRoom, CSceneManager::getMap()->getRoom(currentRoom)->getNavMesh());

					broadcastRoomStatus(false);					
				}
			} else {
				roomEnterDelayClock.restart();
			}
		} else {
			if(areAllPlayersInCurrentRoom()) {
				waitingForRoomEnterDelay = true;
				roomEnterDelayClock.restart();
			}
		}		
	} else { // Players fighting
		if(spawningManager->getIsCurrentRoomCompleted()) {
			reviveParty();
			CSceneManager::getMap()->getRoom(currentRoom)->openExit();

			broadcastRoomStatus(true);

			arePlayersTravelingToCurrentRoom = true;
			currentRoom++;
		}
	}
}

int CGameStateManager::getSurvivedWaves() {
	return max(currentRoom - 1, 0);
}

Vector3f CGameStateManager::getViewCenter() {
	if(localPlayer->getIsAlive()) {
		return localPlayer->getPosition();
	} 
	
	for(CPlayer* player : actualPlayers) {
		if(player->getIsAlive()) {
			return player->getPosition();
		}
	}

	return localPlayer->getPosition();
}

bool CGameStateManager::isLocalPlayerAlive() {
	return localPlayer->getIsAlive();
}

bool CGameStateManager::isGameOver() {
	if(wasGameOverTriggered) {
		return true;
	}
	
	for(CPlayer* player : actualPlayers) {
		if(player->getIsAlive()) {
			return false;
		}
	}

	triggerGameOver();
	return true;
}

void CGameStateManager::triggerGameOver() {
	if(wasGameOverTriggered) {
		return;
	}
	wasGameOverTriggered = true;
	
	// TODO send status -> vermutlich nicht ntig da jeder selbst bestimmt wann alle tot
	
	std::string message = "GAME   OVER";
	CGuiManager::addGuiElement(new CGuiPopupText(message, (unsigned int) (140 * CGuiManager::getGuiFontSizeFactor()), Color::Red,
	                                             CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.25f)), 99999, false));

	message = "YOU   SURVIVED  " + std::to_string(CGameStateManager::getSurvivedWaves()) + "  WAVES";
	CGuiManager::addGuiElement(new CGuiPopupText(message, (unsigned int) (80 * CGuiManager::getGuiFontSizeFactor()), Color::Red,
	                                             CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.65f)), 99999, false));

}

bool CGameStateManager::areAllPlayersInCurrentRoom() {
	CNavMesh* currentNavMesh = CSceneManager::getMap()->getRoom(currentRoom)->getNavMesh();
	
	for(CPlayer* player : actualPlayers) {
		if(!currentNavMesh->isInside(player->getPosition())) {
			return false;
		}
	}

	return true;
}

void CGameStateManager::reviveParty() {
	for(CPlayer* player : actualPlayers) {
		if(!player->getIsAlive()) {
			cout << "revived player" << endl;
			player->revive();
		}

		player->setHealth(100);
		player->restoreEnergy(100);

		if(player->getIsLocal()) {
			player->reloadPrimarySkill();
		}
	}
}

void CGameStateManager::broadcastRoomStatus(bool roomCompleted) {
	auto message = static_cast<CMessageRoomStatus*>(CNetworkManager::createMessage(CMessageType::ROOM_STATUS));
	message->roomNumber = currentRoom;
	message->roomCompleted = roomCompleted;
	CNetworkManager::queueForBroadcast(message);
}

void CGameStateManager::updateBlurEffect() {
	if(isGameOver()) {
		CRenderManager::setBlurStrength(0.008f);
	} else {
		if(localPlayer->getIsAlive()) {
			CRenderManager::setBlurStrength(0.0005f + localPlayer->damagedRecently * 0.002f);
		} else {
			float spectatingPlayerBlur = 0.f;
			for(CPlayer* player : actualPlayers) {
				if(player->getIsAlive()) {
					spectatingPlayerBlur = player->damagedRecently * 0.0025f;
					break;
				}
			}

			CRenderManager::setBlurStrength(0.0005f + 0.004f + spectatingPlayerBlur);
		}
	}
}

void CGameStateManager::setCurrentRoom(int currentRoom_) {
	currentRoom = currentRoom_;
}
