//
// Created by chaperone on 16.07.17.
//

#ifndef CYBERPUNK_CPLAYERDUMMY_HPP
#define CYBERPUNK_CPLAYERDUMMY_HPP


#include "CPlayer.hpp"

class CPlayerDummy : public CPlayer {
public:
	CPlayerDummy(const Vector3f &pos, const Vector3f &dim, const Color &color) :
			CPlayer(pos, dim, CClassName::SNIPER) {};

	/* Overwritten from CPlayer */
	void tick(float frameTime) {};
	void reduceHealth(float damage) {};
};


#endif //CYBERPUNK_CPLAYERDUMMY_HPP
