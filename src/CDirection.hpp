#ifndef CYBERPUNK_CDIRECTION_HPP
#define CYBERPUNK_CDIRECTION_HPP

#include <vector>
#include <SFML/System/Vector2.hpp>

using namespace sf;
using namespace std;

class CDirection {
private:
	enum class Direction{PX, NX, PY, NY};
	
	Direction dir;
public:
	static const Direction PX = Direction::PX;
	static const Direction NX = Direction::NX;
	static const Direction PY = Direction::PY;
	static const Direction NY = Direction::NY;
	
	static vector<CDirection> allDirections() {return { PX, PY, NX, NY}; };

	CDirection() {}
	CDirection(const Direction direction) : dir(direction) {};

	bool operator==(const CDirection& other) const;
	bool operator!=(const CDirection& other) const;
	void operator=(const CDirection& other);
	
	CDirection getLeft() const;
	CDirection getRight() const;
	CDirection getOpposite() const;
	
	bool isParallel(const CDirection& other) const;
	bool isOrthogonal(const CDirection& other) const;
	
	Vector2f toVector();
	static CDirection fromVector(Vector2f vector);
};


#endif //CYBERPUNK_CDIRECTION_HPP
