#ifndef CYBERPUNK_CMAPROOMDIGOUT_HPP
#define CYBERPUNK_CMAPROOMDIGOUT_HPP

#include <SFML/System.hpp>
#include <vector>
#include "CMap.hpp"
#include "CColorDefines.hpp"
#include "CDirection.hpp"
#include "CMapRoom.hpp"
#include "CMapRoomGenerator.hpp"

using namespace sf;
using namespace std;

class CMapRoomGeneratorDigOut final : public CMapRoomGenerator{
private:
	struct Wall {
		Vector2f left;
		Vector2f right;
		CDirection direction;
	};

	struct Block {
		Vector2f pos;
		bool isSolid = true;
		bool isInWall = false;
		bool isDoorway = false;
		bool isPartOfDoorway = false;
		
		bool hasMapBlock = false;
	};
	
	CRectangleArea roomArea;
	Doorway entrance;
	Doorway exit;
	vector<vector<Block>> blocks;
	vector<Wall> walls;
	
	vector<CMapBlock*> mapWallBlocks;
	vector<CMapBlock*> mapFloorBlocks;
		
	// Configure
	int minDigDepth = 4;
	int maxDigDepth = 10;
	int minDigWallLength = 4;
	
	float digOutFactor = 0.32f;
	
public:
	CMapRoomGeneratorDigOut(int level) {
		float colorFactor = CEngine::clamp(1.f - (level * 0.05f), 0.3f, 1);
		
		colorWallBase = CEngine::scaleColor(colorWallBase, colorFactor);
		colorFloorBase = CEngine::scaleColor(colorFloorBase, colorFactor);
	}
	
	~CMapRoomGeneratorDigOut() {}
	
	virtual CMapRoom* generateRoom(CRectangleArea roomArea, Doorway entrance, CDirection exitDirection, int exitSize);

private:
	void init();
	void generateMapBlocks();
	
	void generateExit(CDirection exitDirection, int exitSize);
	bool isDoorwayPossible(Doorway doorway);
	
	void digBlock(Vector2f pos);
	void fillBlock(Vector2f pos);
	Block getBlock(Vector2f pos);
	
	void updateWalls();
	
	void generateObstacles();
	void generateObstacle();
	
	vector<Vector2f> getDoorwayBlocks(Doorway doorway);
	vector<Vector2f> getWallBlocks(Wall wall);
	vector<Vector2f> getNeighbours(Vector2f pos);
	vector<Vector2f> getNeighboursExtended(Vector2f pos);
	
	void digWall(Wall wall, int depth);
	
	void clearAllIsInWall();
	
	bool isCornerWall(Vector2f pos);
	bool isWallRequired(Vector2f pos);
	bool isSlimWall(Vector2f pos);
	bool isGangway(Vector2f pos);
	
	int getDiggedFields();
	Vector2f getLastWallInDirection(Vector2f currentWall, Vector2f currentFree, Vector2f step);
	
	bool canWallBeDigged(Wall wall);
	int getWallLength(Wall wall);
	int getMaxDigDepth(Wall wall);
	
	void printWalls();
	void printWallsAscii();
			
	bool isPosValid(Vector2f pos);
	
	Doorway getEntranceWithOffset();
	Doorway getExitWithOffset();
	
	vector<CMapBlock*> reduceFloorBlocks(vector<CMapBlock*> vector);
};


#endif //CYBERPUNK_CMAPROOMDIGOUT_HPP
