#ifndef CYBERPUNK_CDEBUGHELPER_HPP
#define CYBERPUNK_CDEBUGHELPER_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Text.hpp>
#include "CDebugText.hpp"
#include "CClock.hpp"
#include <deque>

using namespace sf;

class CDebugHelper {
private:
	static CDebugText* debugConsole;
	static std::deque<std::string> debugConsoleMessages;
	static unsigned int maxMessageCount;
	
	static CClock messageTimeoutClock;
	static float messageTimeout;

public:
	static void initDebugHelper();
	
	static void updateDebugContent();
	
	static void print(std::string message);

private:
	CDebugHelper() = default;
};


#endif //CYBERPUNK_CDEBUGHELPER_HPP
