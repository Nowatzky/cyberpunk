#ifndef CYBERPUNK_CMAPROOMGENERATOR_HPP
#define CYBERPUNK_CMAPROOMGENERATOR_HPP


#include <SFML/Graphics.hpp>
#include "CColorDefines.hpp"
#include "CMapRoom.hpp"

using namespace sf;

class CMapRoomGenerator {
protected:
	Color colorWallBase = COLOR_NRW_DARK_WALL_BASE;
	Color colorWallAccent = COLOR_NRW_DARK_WALL_ACCENT;
	Color colorFloorBase = COLOR_NRW_DARK_FLOOR_BASE;
	Color colorFloorAccent = COLOR_NRW_DARK_FLOOR_ACCENT;

	CMapRoomGenerator() = default;

public:
	virtual ~CMapRoomGenerator() = default;

	virtual CMapRoom* generateRoom(CRectangleArea roomArea, Doorway entrance, CDirection exitDirection, int exitSize) = 0;
	
	void setColorWallBase(Color color);
	void setColorWallAccent(Color color);
	void setColorFloorBase(Color color);
	void setColorFloorAccent(Color color);
};


#endif //CYBERPUNK_CMAPROOMGENERATOR_HPP
