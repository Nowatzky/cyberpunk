#include "CObject.hpp"
#include "CEngine.hpp"
#include "CSceneManager.hpp"
#include "CGameClock.hpp"
#include "CNetworkManager.hpp"
#include "CMessageObjTranslationUpdate.hpp"
#include "CUtilityDefines.hpp"

void CObject::tick(float frameTime) {
	if(!isLocal && isMovementReplicated) {
		applyReplicatedMovement();
	}
}

CObjectId CObject::getId() const {
	return id;
}

void CObject::setPosition(Vector3f pos) {
    this->pos = pos;
	getCollisionContainer()->setPosition(pos);
	getGraphicContainer()->setPosition(pos);
}

void CObject::setRotation(float rot) {
	this->rot = rot;
	getCollisionContainer()->setRotation(rot);
	getGraphicContainer()->setRotation(rot);
}

bool CObject::move(Vector3f movement) {
	pos += movement;
	getCollisionContainer()->move(movement);
	getGraphicContainer()->move(movement);
	return true;
}

bool CObject::move(Vector2f movement) {
	pos += CEngine::toPlanar3D(movement);
	getCollisionContainer()->move(movement);
	getGraphicContainer()->move(movement);
	return true;
}

void CObject::rotate(float angle) {
	rot = CEngine::rotate(rot, angle);
	getCollisionContainer()->rotate(angle);
	getGraphicContainer()->rotate(angle);
}

Vector3f CObject::getPosition() const {
	return pos;
}

float CObject::getRotation() const {
	return rot;
}

void CObject::draw(RenderTarget* target) {
	// ToDo: Rework using screen rectangle intersection
	if(getGraphicContainer()->isVisible()) {
		getGraphicContainer()->draw(target);
	}
}

bool CObject::getIsStatic() const {
	return isStatic;
}

void CObject::updateIsStatic(bool isStatic) {
	this->isStatic = isStatic;
	CSceneManager::requestReconstructionForTopologicalMap();
}

void CObject::updateTranslationSnapshotBuffer(CTranslationSnapshot translationSnapshot) {
	if(isLocal) {
		return;
	}
	translationSnapshotBuffer.insert(translationSnapshot);
}

void CObject::setNotLocal(CObjectId id) {
	isLocal = false;
	this->id = id;
}

bool CObject::getIsLocal() const {
	return isLocal;
}

void CObject::setIsMovementReplicated(bool isMovementReplicated) {
	this->isMovementReplicated = isMovementReplicated;
}

bool CObject::getIsMovementReplicated() const {
	return isMovementReplicated;
}

void CObject::applyReplicatedMovement() {
	if(!isMovementReplicated) {
		return;
	}
	
	//float delay = CNetworkManager::getNetworkTickFrequency() * CNetworkManager::getMovementInterpolationSafetyPackets() + translationSnapshotBuffer.getPing();
	float delay = CNetworkManager::getNetworkTickFrequency() * CNetworkManager::getMovementInterpolationSafetyPackets();
	float gameTime = CGameClock::getGameTime() - delay;
	CTranslationSnapshot snapshot = translationSnapshotBuffer.interpolateForTime(gameTime);

	if(snapshot.gameTime != 0) {
		setPosition(snapshot.pos);
		setRotation(snapshot.rot);
	}
}

void CObject::setId(CObjectId id) {
	this->id = id;
}

void CObject::createTranslationSnapshotMessage() const {
	if(CNetworkManager::isActive()) {
		createTranslationSnapshotMessage(CGameClock::getGameTime());
	}
}

void CObject::createTranslationSnapshotMessage(float gameTime) const {
	if(CNetworkManager::isActive() && isLocal) {
		auto* message = (CMessageObjTranslationUpdate*) CNetworkManager::createMessage(CMessageType::OBJ_TRANSLATION_UPDATE);
		message->objectId = getId();
		message->translationSnapshot.gameTime = gameTime;
		message->translationSnapshot.pos = getPosition();
		message->translationSnapshot.rot = getRotation();
		CNetworkManager::queueForBroadcast(message);
	}
}
