#include "CMapBlock.hpp"
#include "CSceneManager.hpp"

CCollisionContainer* CMapBlock::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer* CMapBlock::getGraphicContainer() {
	return &graphicBox;
}

void CMapBlock::tick(float frameTime) {
}

Vector3f CMapBlock::getDim() const {
	return dim;
}

CMapBlock::CMapBlock(Vector2f start, Vector2f end, float z, Color colorBase, Color colorAccent, bool isStatic) :
		CObject(Vector3f((start.x + end.x)/2.f, (start.y + end.y)/2.f, z), isStatic),
        boundingBox(Vector3f((start.x + end.x)/2.f, (start.y + end.y)/2.f, z), 
                    Vector3f(abs(start.x - end.x)/2.f + 0.5f, abs(start.y - end.y)/2.f + 0.5f, 0.5f)),
        graphicBox(Vector3f((start.x + end.x)/2.f, (start.y + end.y)/2.f, z),
                   Vector3f(abs(start.x - end.x)/2.f + 0.5f, abs(start.y - end.y)/2.f + 0.5f, 0.5f),
                   colorBase, colorAccent),
        dim(Vector3f(abs(start.x - end.x)/2.f + 0.5f, abs(start.y - end.y)/2.f + 0.5f, 0.5f)) {

	boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::STATIC);

	if(dim.x > dim.y) {
		wallDirection = CDirection::PX;
	} else {
		wallDirection = CDirection::PY;
	}
	
	lineCount = ((int)( (wallDirection == CDirection::PX ? dim.x : dim.y) * 2.f) - 1);
	lines = VertexArray(sf::Lines, (size_t) (lineCount) * 4);

	for(unsigned int i = 0; i < (unsigned int) (lineCount) * 4; i++) {
		lines[i].color = colorAccent;
	}

	update();
}

void CMapBlock::draw(RenderTarget* target) {
	CObject::draw(target);

	if(lineCount > 0) {
		target->draw(lines);
	}
}

void CMapBlock::update() {
	if(lineCount <= 0) {
		return;
	}
	
	unsigned int lineIndex = 0;

	if(wallDirection == CDirection::PX) {
		float x = pos.x - graphicBox.getDimension().x + 1;
		float front = pos.y + graphicBox.getDimension().y;
		float back = pos.y - graphicBox.getDimension().y;
		float top = pos.z + graphicBox.getDimension().z;
		float bot = pos.z - graphicBox.getDimension().z;

		for(int step = 0; step < lineCount; step++) {
			lines[lineIndex + 0].position = CEngine::toIso(Vector3f(x, back, top));
			lines[lineIndex + 1].position = CEngine::toIso(Vector3f(x, front, top));
			
			lines[lineIndex + 2].position = lines[lineIndex + 1].position;
			lines[lineIndex + 3].position = CEngine::toIso(Vector3f(x, front, bot));

			x++;
			lineIndex += 4;
		}		
	} else {
		float y = pos.y - graphicBox.getDimension().y + 1;
		float front = pos.x + graphicBox.getDimension().x;
		float back = pos.x - graphicBox.getDimension().x;
		float top = pos.z + graphicBox.getDimension().z;
		float bot = pos.z - graphicBox.getDimension().z;

		for(int step = 0; step < lineCount; step++) {
			lines[lineIndex + 0].position = CEngine::toIso(Vector3f(back, y, top));
			lines[lineIndex + 1].position = CEngine::toIso(Vector3f(front, y, top));

			lines[lineIndex + 2].position = lines[lineIndex + 1].position;
			lines[lineIndex + 3].position = CEngine::toIso(Vector3f(front, y, bot));

			y++;
			lineIndex += 4;
		}
	}	
}
