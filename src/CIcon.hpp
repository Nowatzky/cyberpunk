#ifndef CYBERPUNK_CICON_HPP
#define CYBERPUNK_CICON_HPP

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include "CGuiElement.hpp"
#include "CPatternRecognitionMngr.hpp"
#include "CClock.hpp"

using namespace sf;

class CIcon : public CGuiElement {
private:
	bool isLifetimeLimited = false;
	bool isFading = false;
	float lifetime;
	CClock clockLifetime;

	Sprite sprite;

	Vector2f position;
	Vector2f size;

	const float fadeAfter = 0.9f;
	bool keepAfterFadeout = false;

public:
	virtual void draw(RenderTarget* renderTarget);
	virtual void tick(float frameTime);

	CIcon(std::string source, Vector2f position, Vector2f size);

	void setImage(std::string source);
	void setColor(Color color);
	void setPosition(Vector2f pos);
	void setSize(Vector2f size);
	void setLifetime(float lifetime);
	void setKeepAfterFadeout(bool keepAfterFadeout);

	static std::string getIconPath(SIGN sign);
};


#endif //CYBERPUNK_CICON_HPP
