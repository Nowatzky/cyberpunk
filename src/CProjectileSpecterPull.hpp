#ifndef CYBERPUNK_CPROJECTILESPECTERPULL_HPP
#define CYBERPUNK_CPROJECTILESPECTERPULL_HPP

#include "CProjectile.hpp"
#include "CLineEffect.hpp"
#include "CSceneManager.hpp"
#include "CEnemySpecter.hpp"

class CProjectileSpecterPull : public CProjectile {
private:
	CEnemySpecter* owner;
	CLineEffect* lineEffect;
	float maxRange;

	float stunDuration;
	float pullDuration;
	float pullStrength;	
	
public:
	CProjectileSpecterPull(CCharacter* initiator, const Vector3f pos, const Vector2f dir, CEnemySpecter* owner, float projectileSpeed, float maxRange, float stunDuration, float pullDuration, float pullStrength);
	
	virtual ~CProjectileSpecterPull();

	virtual void tick(float frameTime);
	virtual void onHitPlayer(CPlayer* player, float frameTime);
	virtual void onHitProjectile(CProjectile* projectile, float frameTime);
	virtual void onHitWall(float frameTime);

};


#endif //CYBERPUNK_CPROJECTILESPECTERPULL_HPP
