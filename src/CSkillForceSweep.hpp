#ifndef CYBERPUNK_CSKILLFORCESWEEP_HPP
#define CYBERPUNK_CSKILLFORCESWEEP_HPP

#include "CSkill.hpp"
#include "CCharacter.hpp"

class CSkillForceSweep : public CSkill {
private:


public:
	CSkillForceSweep(CPlayer* owner) :
			CSkill(owner, 250, 65) {}

	virtual void fire(Vector2f dir);
	virtual void fireRPC(float gameTime, Vector3f originalPos, Vector2f dir);
	static void fireFallback(Vector3f pos, Vector2f dir);
	
	static void createWall(CCharacter *initiator, Vector3f pos, Vector2f dir);

protected:
	void onButtonPress();
};

#endif //CYBERPUNK_CSKILLFORCESWEEP_HPP