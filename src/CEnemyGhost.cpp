#include <iostream>
#include "CEnemyGhost.hpp"
#include "CSceneManager.hpp"
#include "CStatisticsManager.hpp"
#include "CGameClock.hpp"
#include "CDebugHelper.hpp"
#include "CUtilityDefines.hpp"
#include "CRpcHandler.hpp"
#include "CObjectSpectralDust.hpp"

void CEnemyGhost::tickAuthority(float frameTime) {
	if(!isActive) {
		return;
	}
	
	CEnemy::tickAuthority(frameTime);
}

void CEnemyGhost::tick(float frameTime) {
	if(!isActive) {
		return;
	}
	
	// Check if dust is valid
	if(calledToDust) {
		if(!CSceneManager::doesObjectExist(specterDust)) {
			calledToDust = false;
		}
	}
	
	CEnemy::tick(frameTime);

	if(!isInvisible) {
		reduceHealth(healthDecrease * frameTime);
	}

	// Control in non authority mode because everybody can broadcast if he was hit
	if(isAnyPlayerInExplosionRange()) {
		explode();
		return;
	}

	// TODO Maybe to tickAuthority
	if(dashing && effectManager.hasForcePush() && CEngine::getLength(effectManager.getForcePush()) >= 12.f) {
		stopDashing();
	}

	if(dashing && clockDashDuration.getElapsedTime().asMilliseconds() >= dashDuration) {
		stopDashing();
	}

	// Cosmetic stuff
	if(isInvisible && !dashing) {
		if(effectManager.hasMinAlpha()) {
			Color colorBase = COLOR_ENEMY_GHOST_INVISIBLE_BASE;
			Color colorAccent = COLOR_ENEMY_GHOST_INVISIBLE_ACCENT;

			colorBase.a = (Uint8) max(effectManager.getMinAlpha(), colorBase.a);
			colorAccent.a = (Uint8) max(effectManager.getMinAlpha(), colorAccent.a);

			graphicBox.setColorBase(colorBase);
			graphicBox.setColorAccent(colorAccent);
		} else {
			graphicBox.setColorBase(COLOR_ENEMY_GHOST_INVISIBLE_BASE);
			graphicBox.setColorAccent(COLOR_ENEMY_GHOST_INVISIBLE_ACCENT);
		}
	}

	// Hovering
	hoveringTimeOffset += frameTime;
	Vector3f offset = getGraphicContainer()->getPosOffset();
	offset.z = hoveringAmplitude * sin(hoveringTimeOffset * hoveringFrequency);
	getGraphicContainer()->setPosOffset(offset);
}

bool CEnemyGhost::isDashReady() const {
	return clockDashCooldown.getElapsedTime().asMilliseconds() >= cooldownDash;
}

void CEnemyGhost::dash() {
	if(!dashing && clockDashCooldown.getElapsedTime().asMilliseconds() >= cooldownDash) {
		clockDashCooldown.restart();
		clockDashDuration.restart();
		dashing = true;

		movementSpeed *= movingSpeedDashingModifier;
		turningMovementProfile.maxTurningSpeed *= maxTurningSpeedDashingModifier;
		turningMovementProfile.slowdownFactorAtMaxMisalignment *= slowdownFactorAtMaxMisalignmentDashingModifier;

		if(isInvisible) {
			getGraphicContainer()->setColorBase(COLOR_ENEMY_GHOST_INVISIBLE_DASHING_BASE);
			getGraphicContainer()->setColorAccent(COLOR_ENEMY_GHOST_INVISIBLE_DASHING_ACCENT);
			healthBar.setIsVisible(true);
			effectManager.setIsVisible(true);
			isInvisible = false;
		} else {
			getGraphicContainer()->setColorBase(COLOR_ENEMY_GHOST_NORMAL_DASHING_BASE);
			getGraphicContainer()->setColorAccent(COLOR_ENEMY_GHOST_NORMAL_DASHING_ACCENT);
		}
	}
	
	CRpcHandler::sendEnemyGhostDash(this);
}

void CEnemyGhost::dashRPC(float gameTime) {
	clockDashDuration.restartAtGameTime(gameTime);
	dashing = true;

	if(isInvisible) {
		getGraphicContainer()->setColorBase(COLOR_ENEMY_GHOST_INVISIBLE_DASHING_BASE);
		getGraphicContainer()->setColorAccent(COLOR_ENEMY_GHOST_INVISIBLE_DASHING_ACCENT);
		healthBar.setIsVisible(true);
		effectManager.setIsVisible(true);
		isInvisible = false;
	} else {
		getGraphicContainer()->setColorBase(COLOR_ENEMY_GHOST_NORMAL_DASHING_BASE);
		getGraphicContainer()->setColorAccent(COLOR_ENEMY_GHOST_NORMAL_DASHING_ACCENT);
	}
}

void CEnemyGhost::explode() {
	std::vector<CObjectId> targets;
	float damage = explosionBaseDamage + health * explosionHealthFactorDamage;
	
	for(CPlayer* player : CSceneManager::getTargetablePlayers()) {
		if(CEngine::getDistance(player->getPosition(), getPosition()) <= explosionRadius) {
			hitExplosion(player, damage);			
			targets.push_back(player->getId());
		}
	}

	CSceneManager::safeRemoveObject(this);
	
	CRpcHandler::sendEnemyGhostExplode(this, targets, damage); 
}

void CEnemyGhost::explodeRPC(std::vector<CObject*> targets, float damage) {
	// Hit remote targets
	for(CObject* target : targets) {
		CPlayer* targetPlayer = dynamic_cast<CPlayer*>(target);
		hitExplosion(targetPlayer, damage);
	}
	
	// Check if local player is target
	// TODO

	CSceneManager::safeRemoveObject(this);
}

void CEnemyGhost::explodeFallback(std::vector<CObject*> targets, float damage) {
	for(CObject* target : targets) {
		CPlayer* targetPlayer = dynamic_cast<CPlayer*>(target);
		hitExplosion(targetPlayer, damage);
	}
}

bool CEnemyGhost::isAnyPlayerInExplosionRange() const {
	for(CPlayer* player : CSceneManager::getPlayers()) {
		if(CEngine::getDistance(player->getPosition(), getPosition()) <= explosionRadius) {
			return true;
		}
	}
	return false;
}

void CEnemyGhost::die() {
	CEnemy::die();

	CStatisticsManager::scorePoints(5);
}

void CEnemyGhost::callToDust(CObjectSpectralDust* specterDust) {
	calledToDust = true;
	this->specterDust = specterDust;
}

bool CEnemyGhost::isCalledToDust() const {
	return calledToDust;
}

CObjectSpectralDust* CEnemyGhost::getDust() const {
	return specterDust;
}

bool CEnemyGhost::canBePushedByBullet() {
	return !dashing;
}

void CEnemyGhost::onStunned() {
	stopDashing();
}

void CEnemyGhost::stopDashing() {
	if(dashing) {
		dashing = false;
		movementSpeed /= movingSpeedDashingModifier;
		turningMovementProfile.maxTurningSpeed /= maxTurningSpeedDashingModifier;
		turningMovementProfile.slowdownFactorAtMaxMisalignment /= slowdownFactorAtMaxMisalignmentDashingModifier;

		isInvisible = false;
		getGraphicContainer()->setColorBase(COLOR_ENEMY_GHOST_NORMAL_BASE);
		getGraphicContainer()->setColorAccent(COLOR_ENEMY_GHOST_NORMAL_ACCENT);	
	}
}

void CEnemyGhost::hitExplosion(CPlayer* target, float damage) {
	target->reduceHealth(damage);
}


