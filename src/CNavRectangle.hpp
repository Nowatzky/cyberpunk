#ifndef CYBERPUNK_CNAVRECTANGLE_HPP
#define CYBERPUNK_CNAVRECTANGLE_HPP

#include "CNavPolygon.hpp"
#include "CMapRoom.hpp"

class CNavRectangle : public CNavPolygon {
private:
	Vector2f negPoint;
	Vector2f posPoint;


public:
	CNavRectangle(Vector2f negPoint, Vector2f posPoint) :
			CNavPolygon(),
			negPoint(negPoint),
	        posPoint(posPoint) {}
	
	CNavRectangle(CRectangleArea roomArea) :
			CNavPolygon(),
	        negPoint(roomArea.offset),
	        posPoint(roomArea.offset + roomArea.size) {}
	
	virtual ~CNavRectangle();
	
	virtual bool isInside(Vector2f pos) const;
	virtual bool isInside(Vector3f pos) const;

	virtual bool isInsideExclusiveBorder(Vector2f pos) const;
	virtual bool isInsideExclusiveBorder(Vector3f pos) const;
	
	virtual Vector2f getRandomPointInside() const;

	virtual vector<CNavPolygonBorder> getBorders() const;

	virtual CNavPolygonExtend getExtend() const;
	virtual float getAreaSize() const;
	virtual Vector2f getCenter() const;

	virtual bool intersectsRectangleArea(CRectangleArea rectangleArea) const;
	virtual bool intersectsRectangleAreaExclusiveBorders(CRectangleArea rectangleArea) const;

	virtual vector<CNavPolygon*> splitUp(CRectangleArea rectangleArea);
	
	virtual void addDebugVisuals();
	virtual void removeDebugVisuals();

private:
	CObject* createLine(Vector2f p1, Vector2f p2);

	vector<pair<Vector2f, Vector2f>> getCuts(const CRectangleArea& rectangleArea) const;
	vector<CRectangleArea> splitIntoRectangles(const CRectangleArea& rectangleArea) const;
};


#endif //CYBERPUNK_CNAVRECTANGLE_HPP
