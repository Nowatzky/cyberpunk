#include <iostream>
#include "CHealthBar.hpp"
#include "CEngine.hpp"
#include "CGuiManager.hpp"
#include "CRenderManager.hpp"

CHealthBar::CHealthBar(float maxHealth) :
	maxHealth(maxHealth) {

	currentHealth = maxHealth;
	previewHealth = currentHealth;
	
	height += (maxHealth-100) / 30.f;
	yOffset += (maxHealth-100) / 30.f;
	width += (maxHealth-100) / 20.f;

	vertexArray = VertexArray(Quads, 12);
	for(int i = 0; i < 4; i++) {
		vertexArray[i].color = backgroundColor;
		vertexArray[i + 4].color = previewColor;
		vertexArray[i + 8].color = color;
	}
	
	vertexArraySeparators = VertexArray(Quads, static_cast<size_t>(getHealthSeparatorCount() * 4));
	for(int i = 0; i < getHealthSeparatorCount() * 4; i++) {
		vertexArraySeparators[i].color = backgroundColor;
	}

	CGuiManager::addGuiElement(this);
}

void CHealthBar::draw(RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(vertexArray);
		renderTarget->draw(vertexArraySeparators);
	}
}

void CHealthBar::setCurrentHealth(float currentHealth) {
	this->currentHealth = CEngine::clamp(currentHealth, 0, maxHealth);
}

void CHealthBar::setPosition(Vector3f position) {
	screenPos = CRenderManager::convertSceneToGui(CEngine::toIso(position) - Vector2f(0, yOffset));
}

void CHealthBar::update(float frameTime) {
	previewHealth += ((currentHealth - previewHealth) * frameTime * (previewUpdateSpeedPercentage * maxHealth));

	float healthWidth = CEngine::clamp((currentHealth/maxHealth) * width, 0, maxHealth);
	float previewHealthWidth = CEngine::clamp((previewHealth/maxHealth) * width, 0, maxHealth);
	
	float xLeft = screenPos.x - width/2;
	float xRight = screenPos.x + width/2;
	
	float yTop = screenPos.y - height/2;
	float yBot = screenPos.y + height/2;

	vertexArray[0].position = Vector2f(xLeft, yTop);
	vertexArray[3].position = Vector2f(xLeft, yBot);

	vertexArray[4].position = vertexArray[0].position;
	vertexArray[7].position = vertexArray[3].position;
	vertexArray[8].position = vertexArray[0].position;
	vertexArray[11].position = vertexArray[3].position;

	vertexArray[1].position = Vector2f(xRight, yTop);
	vertexArray[2].position = Vector2f(xRight, yBot);

	vertexArray[5].position = Vector2f(xLeft + previewHealthWidth, yTop);
	vertexArray[6].position = Vector2f(xLeft + previewHealthWidth, yBot);

	vertexArray[9].position = Vector2f(xLeft + healthWidth, yTop);
	vertexArray[10].position = Vector2f(xLeft + healthWidth, yBot);
	
	for(int i = 0; i < getHealthSeparatorCount(); i++) {
		float posX = xLeft + (((i+1) * separatorHealthSize)/maxHealth) * width;
		vertexArraySeparators[i*4    ].position = Vector2f(posX, yTop);
		vertexArraySeparators[i*4 + 1].position = Vector2f(posX, yBot);
		vertexArraySeparators[i*4 + 2].position = Vector2f(posX + separatorPixelSize, yBot);
		vertexArraySeparators[i*4 + 3].position = Vector2f(posX + separatorPixelSize, yTop);
	}
}

void CHealthBar::tick(float frameTime) {

}

void CHealthBar::setYOffset(float yOffset) {
	this->yOffset = yOffset;
}

void CHealthBar::setMaxHealth(float maxHealth) {
	this->maxHealth = maxHealth;

	vertexArraySeparators.clear();
	vertexArraySeparators = VertexArray(Lines, static_cast<size_t>(getHealthSeparatorCount() * 4));
	for(int i = 0; i < getHealthSeparatorCount() * 4; i++) {
		vertexArraySeparators[i].color = backgroundColor;
	}
}

int CHealthBar::getHealthSeparatorCount() {
	return static_cast<int>(maxHealth / separatorHealthSize - 1);
}
