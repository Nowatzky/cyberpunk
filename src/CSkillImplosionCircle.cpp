#include "CSkillImplosionCircle.hpp"
#include "CSceneManager.hpp"
#include "CEffectStun.hpp"
#include "CEffectForcePush.hpp"
#include "CFlatCircle.hpp"
#include "CLineEffect.hpp"


void CSkillImplosionCircle::onButtonPress() {
	implosionClock.restart();
	isImploding = true;
	isDelayed = true;
	origin = owner->getPosition();
	Color implosionBaseColor =  owner->getCharacterClass()->getBaseColor();
	implosionBaseColor.a = 50;
	visualCircle = new CFlatCircle(origin, radius, implosionBaseColor, owner->getCharacterClass()->getAccentColor(), implosionDelay + implosionDuration);
	CSceneManager::safeAddObject(visualCircle);
	CSceneManager::safeAddObject(new CFlatCircle(origin, 0.25f, owner->getCharacterClass()->getAccentColor(), owner->getCharacterClass()->getAccentColor(), implosionDelay + implosionDuration));
}

void CSkillImplosionCircle::tick(float frameTime) {
	if(isImploding) {
		if(implosionClock.getElapsedTime().asMilliseconds() < implosionDuration + implosionDelay) {
			if(implosionClock.getElapsedTime().asMilliseconds() < implosionDelay) {
				return;
			}
			if (isDelayed) {
				isDelayed = false;

				Color implosionBaseColor =  owner->getCharacterClass()->getBaseColor();
				implosionBaseColor.a = 100;
				visualCircle->getGraphicContainer()->setColorBase(implosionBaseColor);
				for(CEnemy *e: CSceneManager::getEnemies()) {
					if (CEngine::getDistance(origin, e->getPosition()) < radius) {
						affectedEnemies.push_back(e);

						Vector3f dir = CEngine::normalize(origin - e->getPosition());
						e->addEffect(new CEffectForcePush(nullptr, implosionStrength, CEngine::to2D(dir), implosionDuration));
					}
				}
			}

			/*float accelerationFactor = (implosionClock.getElapsedTime().asMilliseconds() - implosionDelay) / implosionDuration;
			accelerationFactor = 2* min(accelerationFactor, 1 - accelerationFactor);
			for (CEnemy *e : affectedEnemies) {
				if(CSceneManager::doesObjectExist(e)) {
					if(CEngine::getDistance(e->getPosition(), origin) > closestDistance) {
						Vector3f dir = CEngine::normalize(origin - e->getPosition());
						e->addEffect(new CEffectForcePush(nullptr, accelerationFactor * implosionAccelerator + implosionStrength,CEngine::to2D(dir), frameTime));
					}
				}
			}*/
		} else {
			isImploding = false;
			for(CEnemy *e : affectedEnemies) {
				if (CSceneManager::doesObjectExist(e))
					e->addEffect(new CEffectStun(nullptr, 500.f)); //this ** ?
			}
			affectedEnemies.clear();
		}
	}
}