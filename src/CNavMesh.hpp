#ifndef CYBERPUNK_CNAVMESH_HPP
#define CYBERPUNK_CNAVMESH_HPP

#include <vector>
#include <SFML/System.hpp>
#include <cfloat>
#include "CNavPolygon.hpp"
#include "CMapRoom.hpp"
#include "CNavPath.hpp"
#include "CNavMeshPathfindingInformation.hpp"

using namespace sf;
using namespace std;


class CNavMesh final {
private:
	vector<CNavPolygon*> polygons;
	CRectangleArea roomAreaCovered;
	
	vector<float> probabilityTable;
	
	bool hasDebugVisuals = false;
	
	void updateRoomAreaCovered();
	void updateProbabilityTable();

public:
	CNavMesh(vector<CRectangleArea> roomAreas);
	
	~CNavMesh();
	
	bool isInside(Vector2f pos) const;
	bool isInside(Vector3f pos) const;
	
	Vector2f getRandomPointInside() const;
	
	CNavPolygon* getPolygonAt(Vector2f pos) const;
	CNavPolygon* getPolygonAt(Vector3f pos) const;
	
	CNavPath* findPath(Vector2f start, Vector2f target, float agentHalfWidth) const;
	
	void blockArea(CRectangleArea rectangleArea);
	void freeArea(CRectangleArea rectangleArea);
	void addPolygon(CRectangleArea rectangleArea);
	
	bool isLineOfSightFree(Vector2f start, Vector2f end, float agentHalfWidth) const;
	
	void addDebugVisuals();
	void removeDebugVisuals();

private:
	bool isLineInsideMesh(Vector2f start, Vector2f end) const;
};


#endif //CYBERPUNK_CNAVMESH_HPP
