import matplotlib.pyplot as plt

x = [0]
y = [0]
y2 = [0]
i = 1
with open("./statistics/intensityDistribution.tsv") as infile:
    for line in infile:
        array = line.split('\t')
        x.append(x[i-1] + float(array[0]))
        y.append(float(array[1]))
        y2.append(float(array[2])/1000)
        i+=1

x = [s/60 for s in x]
plt.plot(x, y)
plt.plot(x, y2)
plt.plot([0,x[-1]], [100, 100], '-k')
plt.ylabel('Intensity')
plt.xlabel('Time (min)')
plt.show()